﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer;
using DataLayer.Models;

namespace BusinessLayer
{
    public class CenaBusiness
    {
        CenaData cd;
        public CenaBusiness()
        {
            cd = new CenaData();
        }
        
        public DataTable GetAllCenePremaIDLokacije(int idLokacije)
        {
            DataTable dt = new DataTable();
            List<Cena> listaCena = cd.GetAllCenePremaIDLokacije(idLokacije);

            dt.Columns.Add("cenaID");
            dt.Columns.Add("Iznos cene");
            dt.Columns.Add("lokacijaID");
            dt.Columns.Add("nadzornikID");
            dt.Columns.Add("Nadzornik postavio cenu");
            NadzornikBusiness nb = new NadzornikBusiness();
            

            foreach (var v in listaCena)
            {
                Nadzornik nad = nb.GetNadzornikaNaOsnovuIDa(v.NadzornikID);
                dt.Rows.Add(v.CenaID, v.CenaIznos, v.LokacijaID, v.NadzornikID, nad.Korisnicko);
            }

            return dt;
        }

        public int InsertCene(Cena c)
        {
            return cd.InsertCene(c);
        }

    }
}
