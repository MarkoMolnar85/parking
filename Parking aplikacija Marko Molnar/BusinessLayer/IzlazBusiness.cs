﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer;
using DataLayer.Models;

namespace BusinessLayer
{
    public class IzlazBusiness
    {
        IzlaziData id;
        public IzlazBusiness()
        {
            id = new IzlaziData();
        }

        public DataTable GetAllIzlaziNaOsnovuIdLokacijeDT(int idLokacije)
        {
            DataTable dt = new DataTable();
            List<IzlazSaOstalim> lista = id.GetAllIzlaziNaOsnovuIdLokacije(idLokacije);

            dt.Columns.Add("Tablica");
            dt.Columns.Add("Vreme");
            dt.Columns.Add("Tip karte");
            dt.Columns.Add("Korisničko ime operatera");
            dt.Columns.Add("Smena");
            dt.Columns.Add("Lokacija");

            foreach (var v in lista)
            {
                dt.Rows.Add(v.Tablica, v.VremeP, v.TipKarte, v.OperaterKorisnicko, v.SmenaNaziv, v.LokacijaNaziv);
            }
            return dt;
        }

        public DataTable PretragaIzlazaPoSvimParametrimaDT(IzlazSaOstalim isoParametar, DateTime OD, DateTime DO, int idLokacije)
        {
            DataTable dt = new DataTable();
            List<IzlazSaOstalim> lista = id.PretragaIzlazaPoSvimParametrima(isoParametar, OD, DO, idLokacije);

            dt.Columns.Add("Tablica");
            dt.Columns.Add("Vreme");
            dt.Columns.Add("Tip karte");
            dt.Columns.Add("Korisničko ime operatera");
            dt.Columns.Add("Smena");
            dt.Columns.Add("Lokacija");

            foreach (var v in lista)
            {
                dt.Rows.Add(v.Tablica, v.VremeP, v.TipKarte, v.OperaterKorisnicko, v.SmenaNaziv, v.LokacijaNaziv);
            }
            return dt;
        }
        public Izlaz GetIzlazPremaIDeu(int idIzlaza)
        {
            return id.GetIzlazPremaIDeu(idIzlaza);
        }
    }
}
