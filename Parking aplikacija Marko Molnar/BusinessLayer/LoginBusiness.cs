﻿using DataLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class LoginBusiness
    {
        LogInData logInData;
        public LoginBusiness()
        {
            logInData = new LogInData();
        }
        public string ProveraLogiNa(LoginPodaci loginPodaci)
        {
            return logInData.ProveraLogiNa(loginPodaci);
        }
        
    }
}
