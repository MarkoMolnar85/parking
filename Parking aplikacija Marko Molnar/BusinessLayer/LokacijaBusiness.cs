﻿using DataLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class LokacijaBusiness
    {
        LokacijaData ld = new LokacijaData();
        public LokacijaBusiness()
        {
            ld = new LokacijaData();
        }
        public Lokacija GetLokacijaPremaImenuMasine(string imeMasine)
        {
            Lokacija lok = new Lokacija();
            List<Lokacija> lstLokacija = new List<Lokacija>();
            lstLokacija = ld.GetLokacije();
            var lokacija = from l in lstLokacija where l.ImeMasine == imeMasine select l;
            foreach (Lokacija Loka in lokacija)
            {
                lok = Loka;
                break;
            }
            return lok;
            //List<Lokacija> lstLokacija = ld.GetLokacije();
            //return lstLokacija[0];

        }
        public int IdLokacijePremaImenuMasine(string imeMasine)
        {
            return ld.IdLokacijePremaImenuMasine(imeMasine);
        }

        public int BrojSlobodnihMestaNaParkinguNaOsnovuIdLokacije(int idLokacije)
        {
            UlazBusiness ub = new UlazBusiness();
            int brojAutaNaParkingu = ub.GetBrojAutaNaParkingu(idLokacije);

            ld = new LokacijaData();
            return ld.BrojSlobodnihMestaNaParkinguNaOsnovuIdLokacijeBrojaAutaNaLokaciji(idLokacije, brojAutaNaParkingu);
        }

        public List<Lokacija> GetLokacije()
        {
            return ld.GetLokacije();
        }

        public bool PostojiLokacijaSaImenomMasine(string imeMasine)
        {
            return ld.PostojiLokacijaSaImenomMasine(imeMasine);
        }

        public bool PostojiLokacijaSaImenomLokacije(string imeLokacije)
        {
            return ld.PostojiLokacijaSaImenomLokacije(imeLokacije);
        }

        public List<string> GetAllNaziviLokacijaZaKomboBox()
        {
            return ld.GetAllNaziviLokacijaZaKomboBox();
        }

        public DataTable GetLokacijeDT()
        {
            DataTable dt = new DataTable();
            List<Lokacija> lista = ld.GetLokacije();
            dt.Columns.Add("LokacijaID");
            dt.Columns.Add("Naziv lokacije");
            dt.Columns.Add("Broj mesta");
            dt.Columns.Add("Broj slobodnih mesta");
            dt.Columns.Add("Ime kompljutera");

            foreach (var v in lista)
            {
                dt.Rows.Add(v.ID, v.Naziv, v.BrojMesta, v.BrojSlobodnihMesta, v.ImeMasine);
            }

            return dt;
        }
        public int InserLokacijeSaCenom(Lokacija l, int cenaSata, int idNadzornika)
        {
            return ld.InserLokacijeSaCenom(l, cenaSata, idNadzornika);
        }

        public int GetZadnjiIdentLokacije()
        {
            return ld.GetZadnjiIdentLokacije();
        }
        public bool PostojiLiLokacijaSaImenomMasineIliNazivomLokacijeProveraZaUpdate(Lokacija lok)
        {
            return ld.PostojiLiLokacijaSaImenomMasineIliNazivomLokacijeProveraZaUpdate(lok);
        }
        public int UpdateLokacije(Lokacija l)
        {
            return ld.UpdateLokacije(l);
        }

        public Lokacija GetLokacijaPremaIDu(int id)
        {
            return ld.GetLokacijaPremaIDu(id);
        }

        public int DaLiJeMoguceBrisanjeLokacijeKolikoImaDece(int idLokacije)
        {
            return ld.DaLiJeMoguceBrisanjeLokacijeKolikoImaDece(idLokacije);
        }
        public int ObrisiSveCeneZaLokaciju(int idLokacije)
        {
            return ld.ObrisiSveCeneZaLokaciju(idLokacije);
        }

        public int DeleteLokaciju(int idLokacije)
        {
            return ld.DeleteLokaciju(idLokacije);
        }
    }
}
