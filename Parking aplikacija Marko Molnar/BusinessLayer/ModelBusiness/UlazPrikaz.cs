﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.ModelBusiness
{
    public class UlazPrikaz
    {
        public int ulazID { get; set; }
        public string Tablica { get; set; }
        public DateTime Vreme { get; set; }
        public string NaParkingu { get; set; }
        public string TipKarte { get; set; }
        public string OperatorID { get; set; }
        public string SmenaID { get; set; }
        public string LokacijaID { get; set; }
    }
}
