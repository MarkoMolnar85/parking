﻿using DataLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class NadzornikBusiness
    {
        NadzornikData nd;
        public NadzornikBusiness()
        {
            nd = new NadzornikData();
        }
        public Nadzornik GetNadzornikNaOsnovuKorisnickogImena(string korisnickoIme)
        {
            return nd.GetNadzornikNaOsnovuKorisnickogImena(korisnickoIme);
        }
        public DataTable GetSviNadzornici()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("NadzornikID");
            dt.Columns.Add("Ime");
            dt.Columns.Add("Korisnicko");
            dt.Columns.Add("Ulica");
            dt.Columns.Add("Mesto");
            dt.Columns.Add("Telefon");
            dt.Columns.Add("Sifra");
            dt.Columns.Add("Privilegija");

            NadzornikData nd = new NadzornikData();
            List<Nadzornik> listaNadzornika = nd.GetSviNadzornici();

            foreach (var ln in listaNadzornika)
            {
                dt.Rows.Add(ln.NadzornikID, ln.Ime, ln.Korisnicko, ln.Ulica, ln.Mesto, ln.Telefon, ln.Sifra, ln.Privilegija);
            }
            return dt;
        }
        public DataTable PretragaNadzornika(Nadzornik nadzornik)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("NadzornikID");
            dt.Columns.Add("Ime");
            dt.Columns.Add("Korisnicko");
            dt.Columns.Add("Ulica");
            dt.Columns.Add("Mesto");
            dt.Columns.Add("Telefon");
            dt.Columns.Add("Sifra");
            dt.Columns.Add("Privilegija");

            NadzornikData nd = new NadzornikData();
            List<Nadzornik> listaNadzornika = nd.PretragaNadzornika(nadzornik);

            foreach (var ln in listaNadzornika)
            {
                dt.Rows.Add(ln.NadzornikID, ln.Ime, ln.Korisnicko, ln.Ulica, ln.Mesto, ln.Telefon, ln.Sifra, ln.Privilegija);
            }
            return dt;

        }

        public Nadzornik GetNadzornikaNaOsnovuIDa(int idNadzornika)
        {
            return nd.GetNadzornikaNaOsnovuIDa(idNadzornika);
        }

        public int UpdateNadzornika(Nadzornik nadzornik)
        {
            return nd.UpdateNadzornika(nadzornik);
        }

        public int PostojiOvakvoKorisnickoImeNaOsnovuImenaIIDa(string korisnickoIme, int idKorisnika)
        {
            return nd.PostojiOvakvoKorisnickoImeNaOsnovuImenaIIDa(korisnickoIme, idKorisnika);
        }

        public int InsertNadzornika(Nadzornik nadzornik)
        {
            return nd.InsertNadzornika(nadzornik);
        }

        public int GetZadnjiInsertovaniIdNadzornika()
        {
            return nd.GetZadnjiInsertovaniIdNadzornika();
        }

        public bool ProveriDaLINadzornikImaDecuOsimLogaOdnosnoDaLiJEBrisanjeMoguce(int id)
        {
            return nd.ProveriDaLINadzornikImaDecuOsimLogaOdnosnoDaLiJEBrisanjeMoguce(id);
        }

        public int DeleteNadzornika(int idNadzornika)
        {
            return nd.DeleteNadzornika(idNadzornika);
        }
        public List<string> GetNadzorniciIzKomboBoxa()
        {
            return nd.GetNadzorniciIzKomboBoxa();
        }
    }
}
