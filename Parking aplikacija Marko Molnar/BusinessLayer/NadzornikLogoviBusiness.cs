﻿using DataLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class NadzornikLogoviBusiness
    {
        NadzornikLogoviData nld;
        public NadzornikLogoviBusiness()
        {
            nld = new NadzornikLogoviData();
        }
        public int InsertLogaNadzornika(NadzornikLog nadzornikLog)
        {
            return nld.InsertLogaNadzornika(nadzornikLog);
        }
        public int UpdateNadzornikLogIzmenaVremenaIzlaza(NadzornikLog nadzornikLog)
        {
            return nld.UpdateNadzornikLogIzmenaVremenaIzlaza(nadzornikLog);
        }
        public List<NadzornikLogSaImenom> GetAllNadzornikLogoviSaImenom()
        {
            return nld.GetAllNadzornikLogoviSaImenom();
        }
        public List<NadzornikLogSaImenom> PretragaNaOsnovuParametraKorisnickoIme(string korisnickoIme)
        {
            return nld.PretragaNaOsnovuParametraKorisnickoIme(korisnickoIme);
        }
    }
}
