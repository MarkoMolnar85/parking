﻿using DataLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class OperaterBusiness
    {
        OperaterData operaterData;
        public OperaterBusiness()
        {
            operaterData = new OperaterData();
        }
        public Operater GetOperaterPoIDu(int idOperatera)
        {
            return operaterData.GetOperaterPoIDu(idOperatera);
        }

        public Operater GetOperaterPoKorisnickomImenu(string korisnickoIme)
        {
            return operaterData.GetOperaterPoKorisnickomImenu(korisnickoIme);
        }
        public DataTable GetSviOperateriSaNadzornikom()
        {
            List<OperaterSaNadzornikom> listaOperateraSaNadzornikom = operaterData.GetSviOperateriSaNadzornikom();
            DataTable dt = new DataTable();

            dt.Columns.Add("OperaterID");
            dt.Columns.Add("Korisnicko");
            dt.Columns.Add("Ime");
            dt.Columns.Add("Sifra");
            dt.Columns.Add("Adresa");
            dt.Columns.Add("Mesto");
            dt.Columns.Add("Telefon");
            dt.Columns.Add("NadzornikID");
            dt.Columns.Add("NadzornikKorisnicko");

            foreach (var l in listaOperateraSaNadzornikom)
            {
                dt.Rows.Add(l.OperaterID, l.Korisnicko, l.Ime, l.Sifra, l.Adresa, l.Mesto, l.Telefon, l.NadzornikID, l.KorisnickoNadzornik);
            }
            return dt;
        }

        public DataTable GetPretrazeneOperatereNaOnsovuSvihParametaraIzKlase(OperaterSaNadzornikom oParametar)
        {
            List<OperaterSaNadzornikom> listaOperateraSaNadzornikom = operaterData.GetPretrazeneOperatereNaOnsovuSvihParametaraIzKlase(oParametar);
            DataTable dt = new DataTable();

            dt.Columns.Add("OperaterID");
            dt.Columns.Add("Korisnicko");
            dt.Columns.Add("Ime");
            dt.Columns.Add("Sifra");
            dt.Columns.Add("Adresa");
            dt.Columns.Add("Mesto");
            dt.Columns.Add("Telefon");
            dt.Columns.Add("NadzornikID");
            dt.Columns.Add("NadzornikKorisnicko");

            foreach (var l in listaOperateraSaNadzornikom)
            {
                dt.Rows.Add(l.OperaterID, l.Korisnicko, l.Ime, l.Sifra, l.Adresa, l.Mesto, l.Telefon, l.NadzornikID, l.KorisnickoNadzornik);
            }
            return dt;
        }

        public int InsertOperatera(OperaterSaNadzornikom osn)
        {
            return operaterData.InsertOperatera(osn);
        }

        public int PostojiOvakvoKorisnickoImeNaOsnovuImenaIIDa(string korisnickoIme, int idKorisnika)
        {
            return operaterData.PostojiOvakvoKorisnickoImeNaOsnovuImenaIIDa(korisnickoIme, idKorisnika);
        }

        public int UpdateOperatera(Operater o)
        {
            return operaterData.UpdateOperatera(o);
        }
        public int DeleteOperateraNaOsnovuIDa(int id)
        {
            return operaterData.DeleteOperateraNaOsnovuIDa(id);
        }

        public bool DaLijeMoguceBrisanjeOperateraPostojeLiNjegovaDecaTrueAkoJeste(int idOperatera)
        {
            return operaterData.DaLijeMoguceBrisanjeOperateraPostojeLiNjegovaDecaTrueAkoJeste(idOperatera);
        }
        public int BrisanjeLogovaOperatera(int idOperatera)
        {
            return operaterData.BrisanjeLogovaOperatera(idOperatera);
        }
        public List<string> GetAllKorisnickaImenaZaKomboBox()
        {
            return operaterData.GetAllKorisnickaImenaZaKomboBox();
        }
    }
}
