﻿using DataLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class OperaterLogInBusiness
    {
        OperaterLoginData operaterLogInData;
        public OperaterLogInBusiness()
        {
            operaterLogInData = new OperaterLoginData();
        }

        public int InsertOperaterLog(OperaterLogin operaterLogin)
        {
            return operaterLogInData.InsertOperaterLog(operaterLogin);
        }

        public int UpdateOperaterLogVremeZaLogOut(int idOperatera)
        {
            return operaterLogInData.UpdateOperaterLogVremeZaLogOut(idOperatera);
        }

        public int BrojLogovaOperatera(int idOperatera)
        {
            return operaterLogInData.BrojLogovaOperatera(idOperatera);
        }
        public List<OperaterLogSaImenom> GetAllOperaterLogoviSaImenom()
        {
            return operaterLogInData.GetAllOperaterLogoviSaImenom();
        }
        public List<OperaterLogSaImenom> PretragaOperaterLogova(string korisnickoIme)
        {
            return operaterLogInData.PretragaOperaterLogova(korisnickoIme);
        }
    }
}
