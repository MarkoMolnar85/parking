﻿using DataLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace BusinessLayer
{
    public class PretplatneKarteBusiness
    {
        PretplatneKarteData pkd;
        public PretplatneKarteBusiness()
        {
            pkd = new PretplatneKarteData();
        }
        public bool DaLiJePretplatnaKartaVazeca(string registracija)
        {
            bool vazecaJe = false;

            List<PretplatnaKarta> svePretplatneKarte = pkd.GetAllPretplatneKarte();
            var listaPretplatnihKarataSaPredatomTablicom = from karta in svePretplatneKarte where karta.Registracija == registracija select karta;
            foreach (var v in listaPretplatnihKarataSaPredatomTablicom)
            {
                if (v.OD <= DateTime.Now && v.DO >= DateTime.Now)
                {
                    vazecaJe = true;
                }
            }
            return vazecaJe;
        }

        public DataTable GetAllPretplatneKarteSaNadzornikom()
        {
            DataTable dt = new DataTable();
            List<PretplatnaKartaSaNadzornikom>listaPretplatni = pkd.GetAllPretplatneKarteSaNadzornikom();

            dt.Columns.Add("PretplatneKarteID", typeof(int));
            dt.Columns.Add("Registracija", typeof(string));
            dt.Columns.Add("VaziOD", typeof(DateTime));
            dt.Columns.Add("VaziDO", typeof(DateTime));
            dt.Columns.Add("NadzornikID", typeof(int));
            dt.Columns.Add("Cena", typeof(double));
            dt.Columns.Add("DatumIzdavanja", typeof(DateTime));
            dt.Columns.Add("KorisnickoImeNadzornika", typeof(string));

            foreach (var l in listaPretplatni)
            {
                dt.Rows.Add(l.ID, l.Registracija, l.OD, l.DO, l.NadzornikId, l.Cena, l.DatumIzdavanja, l.NadzornikKorisnicko);
            }
            return dt;
        }

        public bool DaLiJeDatumNoveKarteVeciOdStarihZaIstiRegistarskiBroj(PretplatnaKarta pk)
        {
            return pkd.DaLiJeDatumNoveKarteVeciOdStarihZaIstiRegistarskiBroj(pk);
        }
        public int InsertPretplatneKarte(PretplatnaKarta pretplatnaKarta)
        {
            return pkd.InsertPretplatneKarte(pretplatnaKarta);
        }
        public int DeletePretplatneKarte(int idPretplatneKarte)
        {
            return pkd.DeletePretplatneKarte(idPretplatneKarte);
        }
        public int UpdatePretplatneKarte(PretplatnaKarta pretplatnaKarta)
        {
            return pkd.UpdatePretplatneKarte(pretplatnaKarta);
        }
        public PretplatnaKarta GetPretplatnaKartaNaOsnovuIDa(int id)
        {
            List<PretplatnaKarta> listaPretplatnih = pkd.GetAllPretplatneKarte();
            var novaLista = listaPretplatnih.Where(idIzPretplatne => idIzPretplatne.ID == id);
            PretplatnaKarta pk = new PretplatnaKarta();
            foreach (var v in novaLista)
            {
                pk = v;
            }
            return pk;
        }
        public int ZadnjInsertovaniID()
        {
            return pkd.ZadnjInsertovaniID();
        }

        public DataTable GetPretplatneKarteSaNadzornikomKaoRezultatPretragePosSvimParametrimaDT(string registracija, DateTime datumOd, DateTime datumDo, double manjiBroj, double veciBroj)
        //public DataTable GetPretplatneKarteSaNadzornikomKaoRezultatPretragePosSvimParametrimaDT(object o)
        {
            DataTable dt = new DataTable();
            List<PretplatnaKartaSaNadzornikom> listaPretplatni = pkd.GetPretplatneKarteSaNadzornikomKaoRezultatPretragePosSvimParametrima(registracija, datumOd, datumDo, manjiBroj, veciBroj);
            //List<PretplatnaKartaSaNadzornikom> listaPretplatni = pkd.GetPretplatneKarteSaNadzornikomKaoRezultatPretragePosSvimParametrima(o);

            dt.Columns.Add("PretplatneKarteID", typeof(int));
            dt.Columns.Add("Registracija", typeof(string));
            dt.Columns.Add("VaziOD", typeof(DateTime));
            dt.Columns.Add("VaziDO", typeof(DateTime));
            dt.Columns.Add("NadzornikID", typeof(int));
            dt.Columns.Add("Cena", typeof(double));
            dt.Columns.Add("DatumIzdavanja", typeof(DateTime));
            dt.Columns.Add("KorisnickoImeNadzornika", typeof(string));

            foreach (var l in listaPretplatni)
            {
                dt.Rows.Add(l.ID, l.Registracija, l.OD, l.DO, l.NadzornikId, l.Cena, l.DatumIzdavanja, l.NadzornikKorisnicko);
            }
            return dt;
        }
    }
}
