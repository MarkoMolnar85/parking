﻿using DataLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer.Models;

namespace BusinessLayer
{
    public class RacunBusiness
    {
        RacunData rd;
        public RacunBusiness()
        {
            rd = new RacunData();
        }
        public int InsertRacunaIzlazaIzmenaUlaza(int idLokacije, string tablica, int idOperatera)
        {
            return rd.InsertRacunaIzlazaIzmenaUlaza(idLokacije, tablica, idOperatera);
        }
        public DataTable GetSviRacuniNaOsnovuLokacije(int idLokacije)
        {
            DataTable dt = new DataTable();
            List<Racun> listaRacuna = rd.GetSviRacuniNaOsnovuLokacije(idLokacije);

            dt.Columns.Add("Broj računa");
            dt.Columns.Add("Tablica");
            dt.Columns.Add("IzlazID");
            dt.Columns.Add("UlazID");
            dt.Columns.Add("Vreme na parkingu");
            dt.Columns.Add("Ukupna cena");
            dt.Columns.Add("CenaID");
            dt.Columns.Add("Sati na parkingu");

            foreach (var v in listaRacuna)
            {
                dt.Rows.Add(v.RacunID, v.Tablica, v.IzlazID, v.UlazID, v.VremeNaParkingu, v.UkupnaCena, v.CenaID, v.SatiNaParkinguPlaceniNeplaceni);
            }
            return dt;
        }

        public DataTable PretragaRacunaPoSvimParametrima(Racun rm, int satiOD, int satiDo, double cenaOD, double cenaDO, int IDlokacija)
        {
            DataTable dt = new DataTable();
            List<Racun> listaRacuna = rd.PretragaRacunaPoSvimParametrima(rm, satiOD, satiDo, cenaOD, cenaDO, IDlokacija);

            dt.Columns.Add("Broj računa");
            dt.Columns.Add("Tablica");
            dt.Columns.Add("IzlazID");
            dt.Columns.Add("UlazID");
            dt.Columns.Add("Vreme na parkingu");
            dt.Columns.Add("Ukupna cena");
            dt.Columns.Add("CenaID");
            dt.Columns.Add("Sati na parkingu");

            foreach (var v in listaRacuna)
            {
                dt.Rows.Add(v.RacunID, v.Tablica, v.IzlazID, v.UlazID, v.VremeNaParkingu, v.UkupnaCena, v.CenaID, v.SatiNaParkinguPlaceniNeplaceni);
            }
            return dt;
        }

        public List<Pazar> GetAllPazariPremaIdeuLokacije(int idLokacije)
        {
            return rd.GetAllPazariPremaIdeuLokacije(idLokacije);
        }
        public List<Pazar> PretragaPazaraPoParametrima(Pazar paz, DateTime OD, DateTime DO, double zaradaOD, double zaradaDO, int idLokacije)
        {
            return rd.PretragaPazaraPoParametrima(paz, OD, DO, zaradaOD, zaradaDO, idLokacije);
        }
    }
}
