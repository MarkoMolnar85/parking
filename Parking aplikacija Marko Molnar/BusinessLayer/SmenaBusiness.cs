﻿using DataLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class SmenaBusiness
    {
        SmenaData sd;
        public SmenaBusiness()
        {
            sd = new SmenaData();
        }
        public int IdSmeneNaOsnovuNaziva(string nazivSmene)
        {
            return sd.IdSmeneNaOsnovuNaziva(nazivSmene);
        }
        public List<Smena> GetAllSmene()
        {
            return sd.GetAllSmene();
        }
        public Smena GetSmenaNaOsnovuTrenutnogVremena()
        {
            return sd.GetSmenaNaOsnovuTrenutnogVremena();
        }
        public List<string> GetAllNaziviSmenaZaKomboBox()
        {
            return sd.GetAllNaziviSmenaZaKomboBox();
        }
        public List<Smena> GetAktuelneSmene()
        {
            return sd.GetAktuelneSmene();
        }
        public int InsertovanjeSmeneUpdateovanjeStarih(List<Smena> listaSmena)
        {
            return sd.InsertovanjeSmeneUpdateovanjeStarih(listaSmena);
        }

    }
}
