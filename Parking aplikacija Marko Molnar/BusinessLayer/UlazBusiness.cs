﻿using BusinessLayer.ModelBusiness;
using DataLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class UlazBusiness
    {
        UlaziData ud;
        public UlazBusiness()
        {
            ud = new UlaziData();
        }

        public List<Ulaz> GetAllUlaziLista()
        {
            return ud.GetAllUlazi();
        }

        public DataTable GetAllUlaziDT()
        {
            DataTable dt = new DataTable();

            dt.Columns.Add("ulazID");
            dt.Columns.Add("Tablica");
            dt.Columns.Add("Vreme");
            dt.Columns.Add("NaParkingu");
            dt.Columns.Add("TipKarte");
            dt.Columns.Add("OperaterKorisnicko");

            List<Ulaz> lstUlaza = ud.GetAllUlazi();

            foreach (var ul in lstUlaza)
            {
                UlazPrikaz up = new UlazPrikaz();
                up.ulazID = ul.ulazID;
                up.Tablica = ul.Tablica;
                up.Vreme = ul.Vreme;
                if (ul.NaParkingu == 0)
                {
                    up.NaParkingu = "Nije prisutan";
                }
                else
                {
                    up.NaParkingu = "Prisutan";
                }
                up.TipKarte = ul.TipKarte;
                OperaterBusiness operaterBusiness = new OperaterBusiness();

                //up.operator je string odnosno ime operatera
                up.OperatorID = operaterBusiness.GetOperaterPoIDu(ul.OperatorID).Korisnicko;

                dt.Rows.Add(up.ulazID, up.Tablica, up.Vreme, up.NaParkingu, up.TipKarte, up.OperatorID);
            }
            return dt;
        }

        //ova funckija izvlaci podatke za grid na ulazu
        public DataTable GetUlaziPremaIDuLokacijeDT(int idLokacije)
        {
            DataTable dt = new DataTable();

            dt.Columns.Add("ulazID");
            dt.Columns.Add("Tablica");
            dt.Columns.Add("Vreme");
            dt.Columns.Add("NaParkingu");
            dt.Columns.Add("TipKarte");
            dt.Columns.Add("OperaterKorisnicko");

            List<Ulaz> lstUlaza = ud.GetUlaziPremaIDuLokacije(idLokacije);

            foreach (var ul in lstUlaza)
            {
                UlazPrikaz up = new UlazPrikaz();
                up.ulazID = ul.ulazID;
                up.Tablica = ul.Tablica;
                up.Vreme = ul.Vreme;
                if (ul.NaParkingu == 0)
                {
                    up.NaParkingu = "Nije prisutan";
                }
                else
                {
                    up.NaParkingu = "Prisutan";
                }
                up.TipKarte = ul.TipKarte;
                OperaterBusiness operaterBusiness = new OperaterBusiness();

                //up.operator je string odnosno ime operatera
                up.OperatorID = operaterBusiness.GetOperaterPoIDu(ul.OperatorID).Korisnicko;

                dt.Rows.Add(up.ulazID, up.Tablica, up.Vreme, up.NaParkingu, up.TipKarte, up.OperatorID);
            }
            return dt;
        }

        public int InsertUlaza(Ulaz u)
        {
            return ud.InsertUlaza(u);
        }

        public int GetZadnjiInsertovaniID()
        {
            return ud.GetZadnjiInsertovaniID();
        }

        public int GetBrojAutaNaParkingu(int idLokacije)
        {
            return ud.GetBrojAutaNaParkingu(idLokacije);
        }
        public bool DaLiPostojiAutoSaOvomRegistracijomNaOvomParkinguNaOsnovuIdLokacijeITablice(string tablica)
        {
            int broj = 0;
            broj = ud.GetBrojAutomobilaSaOdredjenomRegistracijomPrisutnihNaParkignuNaOsnovuRegistracije(tablica);
            if (broj > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
            
        }

        public DataTable GetAllAutiKojiSeNalazeNaParkinguPremaIdLokacijeVracaDT(int lokacijaID)
        {
            List<Ulaz> listaUlazi = ud.GetAllAutiKojiSeNalazeNaParkinguPremaIdLokacije(lokacijaID);
            DataTable dt = new DataTable();
            dt.Columns.Add("Ulaz ID", typeof(int));
            dt.Columns.Add("Tablica", typeof(string));
            dt.Columns.Add("Vreme ulaza", typeof(DateTime));
            //dt.Columns.Add("Na parkingu", typeof(string));
            dt.Columns.Add("Tip karte", typeof(string));
            //dt.Columns.Add("Korisničko ime operatera", typeof(string));
            dt.Columns.Add("Korisničko ime operatera", typeof(string));

            foreach (var u in listaUlazi)
            {
                OperaterBusiness ob = new OperaterBusiness();
                Operater operater = ob.GetOperaterPoIDu(u.OperatorID);
                dt.Rows.Add(u.ulazID, u.Tablica, u.Vreme, u.TipKarte, operater.Korisnicko);
            }
            return dt;
        }
        public DataTable GetAutiKojiSuNaParkinguPremaIdLokacijiATablicaPocinjeStringom(int lokacijaID, string tablica)
        {
            List<Ulaz> listaUlazi = ud.GetAllAutiKojiSeNalazeNaParkinguPremaIdLokacije(lokacijaID);
            var listaSkracena = from ulaz in listaUlazi where ulaz.Tablica.StartsWith(tablica) select ulaz;

            DataTable dt = new DataTable();
            dt.Columns.Add("Ulaz ID", typeof(int));
            dt.Columns.Add("Tablica", typeof(string));
            dt.Columns.Add("Vreme ulaza", typeof(DateTime));
            //dt.Columns.Add("Na parkingu", typeof(string));
            dt.Columns.Add("Tip karte", typeof(string));
            dt.Columns.Add("Korisničko ime operatera", typeof(string));

            foreach (var u in listaSkracena)
            {
                OperaterBusiness ob = new OperaterBusiness();
                Operater operater = ob.GetOperaterPoIDu(u.OperatorID);
                dt.Rows.Add(u.ulazID, u.Tablica, u.Vreme, u.TipKarte, operater.Korisnicko);
            }
            return dt;
        }

        public bool DaLiJeAutomobilSaPredatomTablicomIIdeomLokacijeNaParkingu(int idLokacije, string tablica)
        {
            bool jesteAutomobilNaParkingu = false;
            List<Ulaz> listaUlaza = ud.GetAllAutiKojiSeNalazeNaParkinguPremaIdLokacije(idLokacije);
            var autiNaParkinguSaOvomTablicom = from auto in listaUlaza where auto.Tablica == tablica select auto;
            //ako program udje u petlju to znaci da postoji jedan clan liste i rezultat ce biti true. Na var listi ne radi count pa sam morao ovako
            foreach (var v in autiNaParkinguSaOvomTablicom)
            {
                jesteAutomobilNaParkingu = true;
            }
            return jesteAutomobilNaParkingu;
        }

        public DataTable GetAllAutSaDodacimaiNaOsnovuLokacije(int lokacijaID)
        {
            DataTable dt = new DataTable();
            List<UlazSaOstalim> lista = ud.GetAllAutSaDodacimaiNaOsnovuLokacije(lokacijaID);

            //dt.Columns.Add("Ulaz ID", typeof(int));
            dt.Columns.Add("Tablica", typeof(string));
            dt.Columns.Add("Vreme ulaza", typeof(DateTime));
            dt.Columns.Add("Na parkingu", typeof(string));
            dt.Columns.Add("Tip karte", typeof(string));
            dt.Columns.Add("Operater korsničko ime", typeof(string));
            dt.Columns.Add("Naziv smene", typeof(string));
            dt.Columns.Add("Naziv lokacije", typeof(string));

            foreach (var v in lista)
            {
                string naParkinguStr = "";
                if (v.NaParkingu == 0)
                {
                    naParkinguStr = "Nije prisutan";
                }
                else
                {
                    naParkinguStr = "Prisutan";
                }
                dt.Rows.Add(v.Tablica, v.Vreme, naParkinguStr, v.TipKarte, v.OperaterKorisnicko, v.SmenaNaziv, v.LokacijaNaziv);
            }
            return dt;
        }

        public DataTable PretragaUlazaNaOsnovuSvihParametaraUlaziIOstaloDT(UlazSaOstalim uso, DateTime datumOD, DateTime datumDO, int lokacijaID)
        {
            DataTable dt = new DataTable();
            List<UlazSaOstalim> lista = ud.PretragaUlazaNaOsnovuSvihParametaraUlaziIOstalo(uso, datumOD, datumDO, lokacijaID);

            //dt.Columns.Add("Ulaz ID", typeof(int));
            dt.Columns.Add("Tablica", typeof(string));
            dt.Columns.Add("Vreme ulaza", typeof(DateTime));
            dt.Columns.Add("Na parkingu", typeof(string));
            dt.Columns.Add("Tip karte", typeof(string));
            dt.Columns.Add("Operater korsničko ime", typeof(string));
            dt.Columns.Add("Naziv smene", typeof(string));
            dt.Columns.Add("Naziv lokacije", typeof(string));

            foreach (var v in lista)
            {
                string naParkinguStr = "";
                if (v.NaParkingu == 0)
                {
                    naParkinguStr = "Nije prisutan";
                }
                else
                {
                    naParkinguStr = "Prisutan";
                }
                dt.Rows.Add(v.Tablica, v.Vreme, naParkinguStr, v.TipKarte, v.OperaterKorisnicko, v.SmenaNaziv, v.LokacijaNaziv);
            }
            return dt;
        }
        public Ulaz GetUlazPremaIDu(int idUlaza)
        {
            return ud.GetUlazPremaIDu(idUlaza);
        }
    }
}
