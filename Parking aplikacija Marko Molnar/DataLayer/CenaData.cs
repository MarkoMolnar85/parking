﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using DataLayer.Models;
using System.Data.SqlClient;

namespace DataLayer
{
    public class CenaData
    {
        string konStr = "";
        public CenaData()
        {
            konStr = ConfigurationManager.ConnectionStrings["Konekcija"].ConnectionString;
        }
        public Cena GetCenaNaOsnovuIdaCene(int cenaID)
        {
            Cena c = null;
            string upit = "select * from cene where cenaID = " + cenaID + "";
            using (SqlConnection con = new SqlConnection(konStr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(upit, con);
                cmd.CommandText = upit;
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    c = new Cena();
                    c.CenaID = Convert.ToInt32(reader["CenaID"]);
                    c.CenaIznos = Convert.ToDouble(reader["CenaIznos"]);
                    c.LokacijaID = Convert.ToInt32(reader["LokacijaID"]);
                    c.NadzornikID = Convert.ToInt32(reader["NadzornikID"]);
                }
            }
            return c;
        }
        public int GetZadnjaCenaID(int lokacijaID)
        {
            string upit = "select max(cenaID) from cene where lokacijaID = " + lokacijaID + "";
            using (SqlConnection con = new SqlConnection(konStr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(upit, con);
                cmd.CommandText = upit;
                return (int)cmd.ExecuteScalar();
            }
        }

        public List<Cena> GetAllCenePremaIDLokacije(int idLokacije)
        {
            List<Cena> listaCena = new List<Cena>();
            string upit = "select * from cene where lokacijaID = " + idLokacije + "";
            using (SqlConnection con = new SqlConnection(konStr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(upit, con);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Cena c = new Cena();
                    c.CenaID = Convert.ToInt32(reader["cenaID"]);
                    c.CenaIznos = Convert.ToInt32(reader["cenaIznos"]);
                    c.LokacijaID = Convert.ToInt32(reader["lokacijaID"]);
                    c.NadzornikID = Convert.ToInt32(reader["nadzornikID"]);
                    listaCena.Add(c);
                }
            }
            return listaCena;
        }

        public int InsertCene(Cena c)
        {
            string upit = "insert into cene values (@cenaIznos, @lokacijaID, @nadzornikID)";
            using (SqlConnection con = new SqlConnection(konStr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(upit, con);
                cmd.Parameters.AddWithValue("@cenaIznos", c.CenaIznos);
                cmd.Parameters.AddWithValue("@lokacijaID", c.LokacijaID);
                cmd.Parameters.AddWithValue("@nadzornikID", c.NadzornikID);
                return cmd.ExecuteNonQuery();
            }
        }
    }
}
