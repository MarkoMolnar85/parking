﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer.Models;
using System.Data.SqlClient;

namespace DataLayer
{
    public class IzlaziData
    {
        string konStr = "";
        public IzlaziData()
        {
            konStr = ConfigurationManager.ConnectionStrings["Konekcija"].ConnectionString;
        }
        public List<IzlazSaOstalim> GetAllIzlaziNaOsnovuIdLokacije(int idLokacije)
        {
            List<IzlazSaOstalim> lst = new List<IzlazSaOstalim>();
            string upit = "select i.Tablica, i.Vreme, i.TipKarte, o.Korisnicko, s.NazivSmene, l.NazivLokacije " +
                "from Izlazi i join Operateri o on i.OperaterID = o.OperaterID join Smene s on s.SmenaID = i.SmenaID " +
                "join Lokacije l on l.LokacijaID = i.LokacijaID where i.LokacijaID = " + idLokacije + "";
            using (SqlConnection con = new SqlConnection(konStr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(upit, con);
                SqlDataReader reader = cmd.ExecuteReader();
                while(reader.Read())
                {
                    IzlazSaOstalim iso = new IzlazSaOstalim();
                    iso.Tablica = reader["Tablica"].ToString();
                    iso.VremeP = DateTime.Parse(reader["Vreme"].ToString());
                    iso.TipKarte = reader["TipKarte"].ToString();
                    iso.OperaterKorisnicko = reader["Korisnicko"].ToString();
                    iso.SmenaNaziv = reader["NazivSmene"].ToString();
                    iso.LokacijaNaziv = reader["NazivLokacije"].ToString();
                    lst.Add(iso);
                }
            }
            return lst;
        }

        public List<IzlazSaOstalim> PretragaIzlazaPoSvimParametrima(IzlazSaOstalim isoParametar, DateTime OD, DateTime DO, int idLokacije)
        {
            List<IzlazSaOstalim> lst = new List<IzlazSaOstalim>();
            string upit = "select i.Tablica, i.Vreme, i.TipKarte, o.Korisnicko, s.NazivSmene, l.NazivLokacije " +
                "from Izlazi i join Operateri o on i.OperaterID = o.OperaterID join Smene s on s.SmenaID = i.SmenaID " +
                "join Lokacije l on l.LokacijaID = i.LokacijaID where i.LokacijaID = @LokacijaID " +
                "and Tablica like @Tablica +'%' and Vreme between @datumOD and @datumDO " +
                "and TipKarte like @TipKarte +'%' and Korisnicko like @Korisnicko +'%' and " +
                "NazivSmene like @NazivSmene +'%' ";
            using (SqlConnection con = new SqlConnection(konStr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(upit, con);
                cmd.Parameters.AddWithValue("@LokacijaID", idLokacije);
                cmd.Parameters.AddWithValue("@Tablica", isoParametar.Tablica);
                cmd.Parameters.AddWithValue("@datumDO", DO);
                cmd.Parameters.AddWithValue("@datumOD", OD);
                cmd.Parameters.AddWithValue("@TipKarte", isoParametar.TipKarte);
                cmd.Parameters.AddWithValue("@Korisnicko", isoParametar.OperaterKorisnicko);
                cmd.Parameters.AddWithValue("@NazivSmene", isoParametar.SmenaNaziv);

                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    IzlazSaOstalim iso = new IzlazSaOstalim();
                    iso.Tablica = reader["Tablica"].ToString();
                    iso.VremeP = DateTime.Parse(reader["Vreme"].ToString());
                    iso.TipKarte = reader["TipKarte"].ToString();
                    iso.OperaterKorisnicko = reader["Korisnicko"].ToString();
                    iso.SmenaNaziv = reader["NazivSmene"].ToString();
                    iso.LokacijaNaziv = reader["NazivLokacije"].ToString();
                    lst.Add(iso);
                }
            }
            return lst;
        }

        public Izlaz GetIzlazPremaIDeu(int idIzlaza)
        {
            Izlaz izlaz = new Izlaz();
            string upit = "select * from izlazi where izlazID = " + idIzlaza + "";
            using (SqlConnection con = new SqlConnection(konStr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(upit, con);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    izlaz = new Izlaz();
                    izlaz.Tablica = reader["Tablica"].ToString();
                    izlaz.VremeP = DateTime.Parse(reader["Vreme"].ToString());
                    izlaz.TipKarte = reader["TipKarte"].ToString();
                    izlaz.OperaterID = int.Parse(reader["OperaterID"].ToString());
                    izlaz.SmenaID = int.Parse(reader["SmenaID"].ToString());
                    izlaz.LokacijaID = int.Parse(reader["LokacijaID"].ToString());
                }
            }
            return izlaz;
        }
    }
}
