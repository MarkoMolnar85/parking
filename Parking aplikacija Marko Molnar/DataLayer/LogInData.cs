﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;
using DataLayer.Models;

namespace DataLayer
{
    public class LogInData
    {
        string konStr;
        public LogInData()
        {
            konStr = ConfigurationManager.ConnectionStrings["Konekcija"].ConnectionString;
        }
        public string ProveraLogiNa(LoginPodaci loginPodaci)
        {
            string operatorNadzornikNista = "nista";
            if (ProveraDaLIPostojiOPerater(loginPodaci) > 0)
            {
                operatorNadzornikNista = "operator";
            }
            if (ProveriDaLiPostojiNadzornik(loginPodaci) > 0)
            {
                operatorNadzornikNista = "nadzornik";
            }
            return operatorNadzornikNista;
        }
        private int ProveraDaLIPostojiOPerater(LoginPodaci loginPodaci)
        {
            string upit = "select count(*) from Operateri where korisnicko = '" + loginPodaci.KorisnickoIme + "' AND sifra = '" + loginPodaci.LozinkaZaLogin + "'  ";
            using (SqlConnection con = new SqlConnection(konStr))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = con;
                    cmd.CommandText = upit;
                    int brojPodataka = Convert.ToInt32(cmd.ExecuteScalar());
                    return brojPodataka;
                }
            }
        }
        private int ProveriDaLiPostojiNadzornik(LoginPodaci loginPodaci)
        {
            string upit = "select count(*) from Nadzornici where korisnicko = '" + loginPodaci.KorisnickoIme + "' AND sifra = '" + loginPodaci.LozinkaZaLogin + "'  ";
            using (SqlConnection con = new SqlConnection(konStr))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = con;
                    cmd.CommandText = upit;
                    int brojPodataka = Convert.ToInt32(cmd.ExecuteScalar());
                    return brojPodataka;
                }
            }
        }


    }
}
