﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class LokacijaData
    {
        string conStr = "";
        public LokacijaData()
        {
            conStr = ConfigurationManager.ConnectionStrings["Konekcija"].ConnectionString;
        }
        public List<Lokacija> GetLokacije()
        {
            using (SqlConnection con = new SqlConnection(conStr))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = con;
                    cmd.CommandText = "select * from Lokacije";
                    List<Lokacija> lstLokacije = new List<Lokacija>();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        Lokacija l = new Lokacija();
                        l.ID = Convert.ToInt32(reader["LokacijaID"].ToString());
                        l.BrojMesta = Convert.ToInt32(reader["BrojMesta"].ToString());
                        l.BrojSlobodnihMesta = Convert.ToInt32(reader["BrojSlobodnihMesta"].ToString());
                        l.Naziv = reader["NazivLokacije"].ToString();
                        l.ImeMasine = reader["ImeMasine"].ToString();
                        lstLokacije.Add(l);
                    }
                    return lstLokacije;
                }
            }
        }

        public int IdLokacijePremaImenuMasine(string imeMasine)
        {
            int idLokacije = 0;

            using (SqlConnection con = new SqlConnection(conStr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandText = "select lokacijaID from lokacije where imeMasine = N'" + imeMasine + "'";
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    idLokacije = Convert.ToInt32(reader["lokacijaID"]);
                }
            }
            return idLokacije;
        }
        public int BrojSlobodnihMestaNaParkinguNaOsnovuIdLokacijeBrojaAutaNaLokaciji(int idLokacije, int brojAutaNaLokaciji)
        {
            int brojSlobodnihMestaNaParkingu = 0;
            int ukupanBrojMestaNaParkingu = 0;

            string upit = "select BrojMesta from lokacije where lokacijaID = " + idLokacije + "";
            using (SqlConnection con = new SqlConnection(conStr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandText = upit;
                ukupanBrojMestaNaParkingu = Convert.ToInt32(cmd.ExecuteScalar());
                brojSlobodnihMestaNaParkingu = ukupanBrojMestaNaParkingu - brojAutaNaLokaciji;
            }

            int brojUpdate = UpdateBrojSLobodnihMestaNaParkingu(idLokacije, brojSlobodnihMestaNaParkingu);
            if (brojUpdate == 0)
            {
                throw new Exception("Greška pri promeni broja praznih mesta na parkingu");
            }

            return brojSlobodnihMestaNaParkingu;
        }
        public int UpdateBrojSLobodnihMestaNaParkingu(int idLokacije, int brojSlobodnihMestaNaParkingu)
        {
            string upit = "update lokacije set BrojSlobodnihMesta = " + brojSlobodnihMestaNaParkingu + " where lokacijaID  =" + idLokacije + "";
            using (SqlConnection con = new SqlConnection(conStr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(upit, con);
                return cmd.ExecuteNonQuery();
            }
        }

        public bool PostojiLokacijaSaImenomMasine(string imeMasine)
        {
            bool postojiLokacija = false;

            using (SqlConnection con = new SqlConnection(conStr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandText = "select count(*) from lokacije where imeMasine = N'" + imeMasine + "'";
                int brojLokacija = int.Parse(cmd.ExecuteScalar().ToString());
                if (brojLokacija > 0)
                {
                    postojiLokacija = true;
                }
                else
                {
                    postojiLokacija = false;
                }
            }
            return postojiLokacija;
        }

        public bool PostojiLokacijaSaImenomLokacije(string imeLokacije)
        {
            bool postojiLokacija = false;

            using (SqlConnection con = new SqlConnection(conStr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandText = "select count(*) from lokacije where nazivLokacije = N'" + imeLokacije + "'";
                int brojLokacija = int.Parse(cmd.ExecuteScalar().ToString());
                if (brojLokacija > 0)
                {
                    postojiLokacija = true;
                }
                else
                {
                    postojiLokacija = false;
                }
            }
            return postojiLokacija;
        }

        public List<string> GetAllNaziviLokacijaZaKomboBox()
        {
            List<string> lista = new List<string>();
            string upit = "select nazivLokacije from Lokacije";
            using (SqlConnection con = new SqlConnection(conStr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandText = upit;
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    lista.Add(reader["nazivLokacije"].ToString());
                }
                return lista;
            }
        }

        public int InserLokacijeSaCenom(Lokacija l, int cenaSata, int idNadzornika)
        {
            string upit = "begin try begin transaction insert into lokacije " +
                "values(@nazivLokacije, @brojMesta, @brojSlobodnihMesta, @imeMasine) " +
                "insert into Cene values(@cenaIznos, (select IDENT_CURRENT('lokacije')), @idNadzornika) " +
                "commit end try" +
                " begin catch rollback end catch ";
            using (SqlConnection con = new SqlConnection(conStr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandText = upit;
                cmd.Parameters.AddWithValue("@nazivLokacije", l.Naziv);
                cmd.Parameters.AddWithValue("@brojMesta", l.BrojMesta);
                cmd.Parameters.AddWithValue("@brojSlobodnihMesta", l.BrojMesta);
                cmd.Parameters.AddWithValue("@imeMasine", l.ImeMasine);
                cmd.Parameters.AddWithValue("@cenaIznos", cenaSata);
                cmd.Parameters.AddWithValue("@idNadzornika", idNadzornika);
                return cmd.ExecuteNonQuery();
            }
        }
        public int GetZadnjiIdentLokacije()
        {
            string upit = "select IDENT_CURRENT('lokacije')";
            using (SqlConnection con = new SqlConnection(conStr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(upit, con);
                return int.Parse(cmd.ExecuteScalar().ToString());
            }
        }

        public bool PostojiLiLokacijaSaImenomMasineIliNazivomLokacijeProveraZaUpdate(Lokacija lok)
        {
            bool postoji = false;

            using (SqlConnection con = new SqlConnection(conStr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                //broji ge nije predati id
                cmd.CommandText = "select count(*) from lokacije where (nazivLokacije = N'" + lok.Naziv + "' " +
                    " or imeMasine = N'" + lok.ImeMasine + "') and lokacijaID != " + lok.ID + " ";
                int brojLokacija = int.Parse(cmd.ExecuteScalar().ToString());
                if (brojLokacija > 0)
                {
                    postoji = true;
                }
                else
                {
                    postoji = false;
                }
            }
            return postoji;
        }

        public int UpdateLokacije(Lokacija l)
        {
            string upit = "update Lokacije set NazivLokacije = @NazivLokacije, " +
                "BrojMesta = @BrojMesta, ImeMasine = @ImeMasine where LokacijaID = @LokacijaID";
            using (SqlConnection con = new SqlConnection(conStr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(upit, con);
                cmd.Parameters.AddWithValue("@NazivLokacije", l.Naziv);
                cmd.Parameters.AddWithValue("@BrojMesta", l.BrojMesta);
                cmd.Parameters.AddWithValue("@ImeMasine", l.ImeMasine);
                cmd.Parameters.AddWithValue("@LokacijaID", l.ID);
                return cmd.ExecuteNonQuery();
            }
        }

        public Lokacija GetLokacijaPremaIDu(int id)
        {
            Lokacija lok = new Lokacija();
            string upit = "select * from lokacije where lokacijaID = " + id + "";
            using (SqlConnection con = new SqlConnection(conStr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(upit, con);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    lok.Naziv = reader["nazivLokacije"].ToString();
                    lok.ImeMasine = reader["imeMasine"].ToString();
                    lok.BrojMesta = int.Parse(reader["brojMesta"].ToString());
                    lok.ID = int.Parse(reader["LokacijaID"].ToString());
                    lok.BrojSlobodnihMesta = int.Parse(reader["BrojSlobodnihMesta"].ToString());
                }
                return lok;
            }
        }

        public int DaLiJeMoguceBrisanjeLokacijeKolikoImaDece(int idLokacije)
        {
            string upit = "select (select count(*) from izlazi where Izlazi.LokacijaID = " + idLokacije + ") + " +
                "(select count(*) from Ulazi where Ulazi.LokacijaID = " + idLokacije + ") as brojUlazaIzlaza";
            using (SqlConnection con = new SqlConnection(conStr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(upit, con);
                return int.Parse(cmd.ExecuteScalar().ToString());
            }
        }
        public int ObrisiSveCeneZaLokaciju(int idLokacije)
        {
            string upit = "delete from cene where lokacijaID = " + idLokacije + "";
            using (SqlConnection con = new SqlConnection(conStr))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(upit, con))
                {
                    return cmd.ExecuteNonQuery();
                }

            }
        }
        public int DeleteLokaciju(int idLokacije)
        {
            string upit = "delete from lokacije where lokacijaID = " + idLokacije + "";
            using (SqlConnection con = new SqlConnection(conStr))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(upit, con))
                {
                    return cmd.ExecuteNonQuery();
                }

            }
        }

    }
}
