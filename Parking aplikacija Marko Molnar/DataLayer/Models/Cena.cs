﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Models
{
    public class Cena
    {
        public int CenaID { get; set; }
        public double CenaIznos { get; set; }
        public int LokacijaID { get; set; }
        public int NadzornikID { get; set; }
    }
}
