﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Models
{
    public class Izlaz
    {
        public int IzlazID { get; set; }
        public string Tablica { get; set; }
        public DateTime VremeP { get; set; }
        public string TipKarte { get; set; }
        public int OperaterID { get; set; }
        public int SmenaID { get; set; }
        public int LokacijaID { get; set; }
    }
}
