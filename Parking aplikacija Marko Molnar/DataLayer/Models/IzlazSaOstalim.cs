﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Models
{
    public class IzlazSaOstalim : Izlaz
    {
        public string OperaterKorisnicko { get; set; }
        public string SmenaNaziv { get; set; }
        public string LokacijaNaziv { get; set; }
    }
}
