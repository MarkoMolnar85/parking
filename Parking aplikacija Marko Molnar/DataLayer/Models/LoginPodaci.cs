﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Models
{
    public class LoginPodaci
    {
        public string KorisnickoIme { get; set; }
        public string LozinkaZaLogin { get; set; }
        public string ImeMasine { get; set; }
    }
}
