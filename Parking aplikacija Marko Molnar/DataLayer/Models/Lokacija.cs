﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Models
{
    public class Lokacija
    {
        public int ID { get; set; }
        public string Naziv { get; set; }
        public int BrojMesta { get; set; }
        public int BrojSlobodnihMesta { get; set; }
        public string ImeMasine { get; set; }
    }
}
