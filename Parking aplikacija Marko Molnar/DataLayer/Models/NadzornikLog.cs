﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Models
{
    public class NadzornikLog
    {
        public int NadzornikLogID { get; set; }
        public DateTime VremeLoga { get; set; }
        public DateTime VremePosle { get; set; }
        public int NadzornikID { get; set; }
    }
}
