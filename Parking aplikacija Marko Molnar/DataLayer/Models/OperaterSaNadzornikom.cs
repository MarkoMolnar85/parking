﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Models
{
    public class OperaterSaNadzornikom
    {
        public int OperaterID { get; set; }
        public string Korisnicko { get; set; }
        public string Ime { get; set; }
        public string Sifra { get; set; }
        public string Adresa { get; set; }
        public string Mesto { get; set; }
        public string Telefon { get; set; }
        public int NadzornikID { get; set; }
        public string KorisnickoNadzornik { get; set; }
    }
}
