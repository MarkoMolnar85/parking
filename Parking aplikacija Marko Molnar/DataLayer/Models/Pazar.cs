﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Models
{
    public class Pazar
    {
        public string Korisnicko { get; set; }
        public DateTime Datum { get; set; }
        public string NazivSmene { get; set; }
        public double Suma { get; set; }
        public int Sati { get; set; }
        public double ProsecnoPlacanje { get; set; }
        public double ProsecnoSati { get; set; }
        public int MaksimalnoSati { get; set; }
        public int MinimalnoSati { get; set; }
    }
}
