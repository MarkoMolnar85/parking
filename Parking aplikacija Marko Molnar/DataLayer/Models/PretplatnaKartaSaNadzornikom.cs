﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Models
{
    public class PretplatnaKartaSaNadzornikom
    {
        public int ID { get; set; }
        public string Registracija { get; set; }
        public DateTime OD { get; set; }
        public DateTime DO { get; set; }
        public int NadzornikId { get; set; }
        public double Cena { get; set; }
        public DateTime DatumIzdavanja { get; set; }
        public string NadzornikKorisnicko { get; set; }
    }
}
