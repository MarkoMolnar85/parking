﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Models
{
    public class Racun
    {
        public int RacunID { get; set; }
        public string Tablica { get; set; }
        public int IzlazID { get; set; }
        public int UlazID { get; set; }
        //vreme na parkingu je broj sati, a to je integer
        public int VremeNaParkingu { get; set; }
        public float UkupnaCena{ get; set; }
        public int CenaID { get; set; } 
        public int SatiNaParkinguPlaceniNeplaceni { get; set; }
    }
}
