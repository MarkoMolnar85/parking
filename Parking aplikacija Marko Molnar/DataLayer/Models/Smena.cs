﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Models
{
    public class Smena
    {
        public int ID { get; set; }
        public string NazivSmene { get; set; }
        public TimeSpan OD { get; set; }
        public TimeSpan DO { get; set; }
        public int TrenutnoVazeca { get; set; }
    }
}
