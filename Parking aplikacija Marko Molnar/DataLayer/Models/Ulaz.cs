﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Models
{
    public class Ulaz
    {
        public int ulazID { get; set; }
        public string Tablica { get; set; }
        public DateTime Vreme { get; set; }
        public int NaParkingu { get; set; }
        public string TipKarte { get; set; }
        public int OperatorID { get; set; }
        public int SmenaID { get; set; }
        public int LokacijaID { get; set; }
    }
}
