﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class NadzornikData
    {
        string konStr = "";
        public NadzornikData()
        {
            konStr = ConfigurationManager.ConnectionStrings["Konekcija"].ConnectionString;
        }
        public Nadzornik GetNadzornikNaOsnovuKorisnickogImena(string korisnickoIme)
        {
            Nadzornik nadzornik = null;
            string upit = "select * from Nadzornici where Korisnicko = @Korisnicko ";
            //string upit = "select * from nadzornici where korisnicko = '" + korisnickoIme + " '";
            using (SqlConnection con = new SqlConnection(konStr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(upit, con);
                cmd.Parameters.AddWithValue("@Korisnicko", korisnickoIme);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    nadzornik = new Nadzornik();
                    nadzornik.NadzornikID = Convert.ToInt32(reader["NadzornikID"]);
                    nadzornik.Ime = reader["Ime"].ToString();
                    nadzornik.Korisnicko = reader["Korisnicko"].ToString();
                    nadzornik.Ulica = reader["Ulica"].ToString();
                    nadzornik.Mesto = reader["Mesto"].ToString();
                    nadzornik.Telefon = reader["Telefon"].ToString();
                    nadzornik.Sifra = reader["Sifra"].ToString();
                    nadzornik.Privilegija = Convert.ToInt32(reader["Privilegija"]);
                }
            }
            return nadzornik;
        }

        public List<Nadzornik> GetSviNadzornici()
        {
            List<Nadzornik> listaNadzornika = new List<Nadzornik>();
            string upit = "select * from nadzornici";
            using (SqlConnection con = new SqlConnection(konStr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(upit, con);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Nadzornik nadzornik = new Nadzornik();
                    nadzornik.NadzornikID = Convert.ToInt32(reader["NadzornikID"]);
                    nadzornik.Ime = reader["Ime"].ToString();
                    nadzornik.Korisnicko = reader["Korisnicko"].ToString();
                    nadzornik.Ulica = reader["Ulica"].ToString();
                    nadzornik.Mesto = reader["Mesto"].ToString();
                    nadzornik.Telefon = reader["Telefon"].ToString();
                    nadzornik.Sifra = reader["Sifra"].ToString();
                    nadzornik.Privilegija = Convert.ToInt32(reader["Privilegija"]);
                    listaNadzornika.Add(nadzornik);
                }
            }
            return listaNadzornika;
        }

        public List<Nadzornik> PretragaNadzornika(Nadzornik nadzornik)
        {
            List<Nadzornik> listaNadzornika = new List<Nadzornik>();
            //string upit = "select * from nadzornici where ime like '" + nadzornik.Ime + "%' ";
            string upit = "select * from nadzornici where ime like @ime + '%' and Korisnicko like @Korisnicko + '%' " +
                "and Ulica like @Ulica + '%' and Mesto like @Mesto + '%' and Telefon like @Telefon + '%' ";


            using (SqlConnection con = new SqlConnection(konStr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(upit, con);
                cmd.Parameters.AddWithValue("@ime", nadzornik.Ime);
                cmd.Parameters.AddWithValue("@Korisnicko", nadzornik.Korisnicko);
                cmd.Parameters.AddWithValue("@Ulica", nadzornik.Ulica);
                cmd.Parameters.AddWithValue("@Mesto", nadzornik.Mesto);
                cmd.Parameters.AddWithValue("@Telefon", nadzornik.Telefon);

                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Nadzornik nadzornikObj = new Nadzornik();
                    nadzornikObj.NadzornikID = Convert.ToInt32(reader["NadzornikID"]);
                    nadzornikObj.Ime = reader["Ime"].ToString();
                    nadzornikObj.Korisnicko = reader["Korisnicko"].ToString();
                    nadzornikObj.Ulica = reader["Ulica"].ToString();
                    nadzornikObj.Mesto = reader["Mesto"].ToString();
                    nadzornikObj.Telefon = reader["Telefon"].ToString();
                    nadzornikObj.Sifra = reader["Sifra"].ToString();
                    nadzornikObj.Privilegija = Convert.ToInt32(reader["Privilegija"]);
                    listaNadzornika.Add(nadzornikObj);
                }
            }
            return listaNadzornika;
        }

        public Nadzornik GetNadzornikaNaOsnovuIDa(int idNadzornika)
        {
            Nadzornik nadzornik = null;
            string upit = "select * from nadzornici where NadzornikID = " + idNadzornika + "";
            using (SqlConnection con = new SqlConnection(konStr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(upit, con);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    nadzornik = new Nadzornik();
                    nadzornik.NadzornikID = Convert.ToInt32(reader["NadzornikID"]);
                    nadzornik.Ime = reader["Ime"].ToString();
                    nadzornik.Korisnicko = reader["Korisnicko"].ToString();
                    nadzornik.Ulica = reader["Ulica"].ToString();
                    nadzornik.Mesto = reader["Mesto"].ToString();
                    nadzornik.Telefon = reader["Telefon"].ToString();
                    nadzornik.Sifra = reader["Sifra"].ToString();
                    nadzornik.Privilegija = Convert.ToInt32(reader["Privilegija"]);
                }
            }
            return nadzornik;
        }

        public int UpdateNadzornika(Nadzornik nadzornik)
        {
            int uspeh = 0;
            string upit = "update nadzornici set Ime = @Ime, Korisnicko = @Korisnicko, Ulica = @Ulica, Mesto = @Mesto, " +
                "Telefon = @Telefon, Sifra = @Sifra, Privilegija = @Privilegija where NadzornikID = @NadzornikID";
            using (SqlConnection con = new SqlConnection(konStr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(upit, con);
                cmd.Parameters.AddWithValue("@Ime", nadzornik.Ime);
                cmd.Parameters.AddWithValue("@Korisnicko", nadzornik.Korisnicko.ToUpper());
                cmd.Parameters.AddWithValue("@Ulica", nadzornik.Ulica);
                cmd.Parameters.AddWithValue("@Mesto", nadzornik.Mesto);
                cmd.Parameters.AddWithValue("@Telefon", nadzornik.Telefon);
                cmd.Parameters.AddWithValue("@Sifra", nadzornik.Sifra.ToUpper());
                cmd.Parameters.AddWithValue("@Privilegija", nadzornik.Privilegija);
                cmd.Parameters.AddWithValue("@NadzornikID", nadzornik.NadzornikID);
                uspeh = cmd.ExecuteNonQuery();
            }
            return uspeh;
        }

        public int PostojiOvakvoKorisnickoImeNaOsnovuImenaIIDa(string korisnickoIme, int idKorisnika)
        {
            int brojImena = 0;
            string upitNadzornici = "select count(*) from nadzornici where nadzornikID != " + idKorisnika + " and korisnicko = N'" + korisnickoIme + "'";
            string upitoperateri = "select count(*) from operateri where korisnicko N'" + korisnickoIme + "'";
            using (SqlConnection con = new SqlConnection(konStr))
            {
                con.Open();
                SqlTransaction transakcija = con.BeginTransaction();
                SqlCommand cmd = con.CreateCommand();
                cmd.Transaction = transakcija;
                
                try
                {
                    brojImena = 0;
                    cmd.CommandText = upitNadzornici;
                    brojImena += int.Parse(cmd.ExecuteScalar().ToString());
                    cmd.CommandText = upitoperateri;
                    brojImena += int.Parse(cmd.ExecuteScalar().ToString());
                    transakcija.Commit();
                }
                catch (Exception)
                {
                    transakcija.Rollback();
                }
            }
            return brojImena;
        }

        public int InsertNadzornika(Nadzornik nadzornik)
        {
            int uspeh = 0;
            string upit = "insert into nadzornici (ime, korisnicko, ulica, mesto, telefon, sifra, privilegija) " +
                " values(@ime, @korisnicko, @ulica, @mesto, @telefon, @sifra, @privilegija)";
            using (SqlConnection con = new SqlConnection(konStr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(upit, con);
                cmd.Parameters.AddWithValue("@Ime", nadzornik.Ime);
                cmd.Parameters.AddWithValue("@Korisnicko", nadzornik.Korisnicko.ToUpper());
                cmd.Parameters.AddWithValue("@Ulica", nadzornik.Ulica);
                cmd.Parameters.AddWithValue("@Mesto", nadzornik.Mesto);
                cmd.Parameters.AddWithValue("@Telefon", nadzornik.Telefon);
                cmd.Parameters.AddWithValue("@Sifra", nadzornik.Sifra.ToUpper());
                cmd.Parameters.AddWithValue("@Privilegija", nadzornik.Privilegija);
                uspeh = cmd.ExecuteNonQuery();
            }
            return uspeh;
        }

        public int GetZadnjiInsertovaniIdNadzornika()
        {
            string upit = "select ident_current ('nadzornici')";
            using (SqlConnection con = new SqlConnection(konStr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(upit, con);
                return int.Parse(cmd.ExecuteScalar().ToString());
            }
        }

        public bool ProveriDaLINadzornikImaDecuOsimLogaOdnosnoDaLiJEBrisanjeMoguce(int id)
        {
            int brojDeceNadzornika = 0;

            string upit1 = "select count(*) from operateri where nadzornikID = " + id + "";
            string upit2= "select count(*) from PretplatneKarte where nadzornikID = " + id + "";
            string upit3 = "select count(*) from Cene where nadzornikID = " + id + "";

            using (SqlConnection con = new SqlConnection(konStr))
            {
                con.Open();
                SqlTransaction transakcija = con.BeginTransaction();
                SqlCommand cmd = con.CreateCommand();
                cmd.Transaction = transakcija;

                try
                {
                    brojDeceNadzornika = 0;
                    cmd.CommandText = upit1;
                    brojDeceNadzornika += int.Parse(cmd.ExecuteScalar().ToString());
                    cmd.CommandText = upit2;
                    brojDeceNadzornika += int.Parse(cmd.ExecuteScalar().ToString());
                    cmd.CommandText = upit3;
                    brojDeceNadzornika += int.Parse(cmd.ExecuteScalar().ToString());
                }
                catch (Exception)
                {
                    transakcija.Rollback();
                }
            }
            if (brojDeceNadzornika > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public int DeleteLogoveNadzornikaNaOsnovuIDaNadzornika(int idNadzornika)
        {
            string upit = "delete from NadzornikLogovi where NadzornikLogId = " + idNadzornika + "";
            using (SqlConnection con = new SqlConnection(konStr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(upit, con);
                return cmd.ExecuteNonQuery();
            }
        }
        public int DeleteNadzornika(int idNadzornika)
        {
            int uspehBrisanja = 0;
            uspehBrisanja = DeleteLogoveNadzornikaNaOsnovuIDaNadzornika(idNadzornika);

            using (SqlConnection con = new SqlConnection(konStr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandText = "select count(*) from NadzornikLogovi where NadzornikLogId = " + idNadzornika + "";
                while (int.Parse(cmd.ExecuteScalar().ToString()) > 0)
                {
                    DeleteLogoveNadzornikaNaOsnovuIDaNadzornika(idNadzornika);
                }
                int brojSlogovaZaOperatera = int.Parse(cmd.ExecuteScalar().ToString());

                if (brojSlogovaZaOperatera == 0)
                {
                    cmd.CommandText = "delete from nadzornici where nadzornikID = " + idNadzornika + "";
                    return cmd.ExecuteNonQuery();
                }
                else
                {
                    return 0;
                }
            }
        }
        public List<string> GetNadzorniciIzKomboBoxa()
        {
            List<string> lista = new List<string>();
            string upit = "select korisnicko from nadzornici";
            using (SqlConnection con = new SqlConnection(konStr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(upit, con);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    lista.Add(reader["korisnicko"].ToString());
                }
            }
            return lista;
        }
    }
}
