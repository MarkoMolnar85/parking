﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class NadzornikLogoviData
    {
        string konStr = "";
        public NadzornikLogoviData()
        {
            konStr = ConfigurationManager.ConnectionStrings["Konekcija"].ConnectionString;
        }
        public int InsertLogaNadzornika(NadzornikLog nadzornikLog)
        {
            string upit = "insert into NadzornikLogovi (vremeLoga, nadzornikID) values(@vremeLoga, @nadzornikID)";
            using (SqlConnection con = new SqlConnection(konStr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(upit, con);
                cmd.Parameters.AddWithValue("@vremeLoga", nadzornikLog.VremeLoga);
                cmd.Parameters.AddWithValue("@nadzornikID", nadzornikLog.NadzornikID);
                return cmd.ExecuteNonQuery();
            }
        }
        public int UpdateNadzornikLogIzmenaVremenaIzlaza(NadzornikLog nadzornikLog)
        {
            string upit = "update NadzornikLogovi set vremePosle = @vremePosle where nadzornikID = @nadzornikID and vremePosle is null ";
            //string upit = "update NadzornikLogovi set vremePosle = '" + DateTime.Now + "' where nadzornikID = " + nadzornikLog.NadzornikID + " and vremePosle is null";
            SqlConnection con = new SqlConnection(konStr);
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(upit, con);
                cmd.Parameters.AddWithValue("@vremePosle", DateTime.Now);
                cmd.Parameters.AddWithValue("@nadzornikID", nadzornikLog.NadzornikID);
                return cmd.ExecuteNonQuery();
            }
        }

        public List<NadzornikLogSaImenom> GetAllNadzornikLogoviSaImenom()
        {
            List<NadzornikLogSaImenom> listaNadzornikLogova = new List<NadzornikLogSaImenom>();
            string upit = "select NadzornikLogovi.*, Nadzornici.Korisnicko from NadzornikLogovi " +
                " join Nadzornici on NadzornikLogovi.NadzornikID = Nadzornici.NadzornikID and nadzornikLogovi.vremePosle is not null";
            using (SqlConnection con = new SqlConnection(konStr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(upit, con);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    NadzornikLogSaImenom nli = new NadzornikLogSaImenom();
                    nli.NadzornikLogID = Convert.ToInt32(reader["nadzornikLogID"]);
                    nli.VremeLoga = DateTime.Parse(reader["VremeLoga"].ToString());
                    nli.VremePosle = DateTime.Parse(reader["VremePosle"].ToString());
                    nli.NadzornikKorisnicko = reader["Korisnicko"].ToString();
                    listaNadzornikLogova.Add(nli);
                }
            }
            return listaNadzornikLogova;
        }
        public List<NadzornikLogSaImenom> PretragaNaOsnovuParametraKorisnickoIme(string korisnickoIme)
        {
            List<NadzornikLogSaImenom> listaNadzornikLogova = new List<NadzornikLogSaImenom>();
            string upit = "select NadzornikLogovi.*, Nadzornici.Korisnicko from NadzornikLogovi " +
                " join Nadzornici on NadzornikLogovi.NadzornikID = Nadzornici.NadzornikID " +
                " where Nadzornici.korisnicko like @korisnicko +'%' and NadzornikLogovi.VremePosle is not null";
            using (SqlConnection con = new SqlConnection(konStr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(upit, con);
                cmd.Parameters.AddWithValue("@korisnicko", korisnickoIme);

                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    NadzornikLogSaImenom nli = new NadzornikLogSaImenom();
                    nli.NadzornikLogID = Convert.ToInt32(reader["nadzornikLogID"]);
                    nli.VremeLoga = DateTime.Parse(reader["VremeLoga"].ToString());
                    nli.VremePosle = DateTime.Parse(reader["VremePosle"].ToString());
                    nli.NadzornikKorisnicko = reader["Korisnicko"].ToString();
                    listaNadzornikLogova.Add(nli);
                }
            }
            return listaNadzornikLogova;
        }
    }
}
