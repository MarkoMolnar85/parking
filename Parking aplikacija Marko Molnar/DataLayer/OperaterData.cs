﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DataLayer
{
    public class OperaterData
    {
        string konekcioniString = "";
        public OperaterData()
        {
            konekcioniString = ConfigurationManager.ConnectionStrings["Konekcija"].ConnectionString;
        }

        public Operater GetOperaterPoIDu(int idOperatera)
        {
            string upit = "select * from operateri where operaterID = " + idOperatera + "";
            using (SqlConnection con = new SqlConnection(konekcioniString))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = upit;
                cmd.Connection = con;
                SqlDataReader reader = cmd.ExecuteReader();
                Operater operater = new Operater();
                while (reader.Read())
                {
                    operater.OperaterID = Convert.ToInt32(reader["OperaterID"]);
                    operater.Korisnicko = reader["Korisnicko"].ToString();
                    operater.Ime = reader["ime"].ToString();
                    operater.Sifra = reader["sifra"].ToString();
                    operater.Adresa = reader["adresa"].ToString();
                    operater.Mesto = reader["mesto"].ToString();
                    operater.Telefon = reader["telefon"].ToString();
                    operater.NadzornikID = Convert.ToInt32(reader["nadzornikID"]);
                }
                return operater;
            }
        }

        public Operater GetOperaterPoKorisnickomImenu(string korisnickoIme)
        {
            string upit = "select * from operateri where korisnicko = '" + korisnickoIme + "'  ";
            using (SqlConnection con = new SqlConnection(konekcioniString))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = upit;
                cmd.Connection = con;
                SqlDataReader reader = cmd.ExecuteReader();
                Operater operater = new Operater();
                while (reader.Read())
                {
                    operater.OperaterID = Convert.ToInt32(reader["OperaterID"]);
                    operater.Korisnicko = reader["Korisnicko"].ToString();
                    operater.Ime = reader["ime"].ToString();
                    operater.Sifra = reader["sifra"].ToString();
                    operater.Adresa = reader["adresa"].ToString();
                    operater.Mesto = reader["mesto"].ToString();
                    operater.Telefon = reader["telefon"].ToString();
                    operater.NadzornikID = Convert.ToInt32(reader["nadzornikID"]);
                }
                return operater;
            }
        }

        public List<OperaterSaNadzornikom> GetSviOperateriSaNadzornikom()
        {
            List<OperaterSaNadzornikom> listaOperateraSaNadzornicima = new List<OperaterSaNadzornikom>();

            string upit = "select Operateri.*, Nadzornici.Korisnicko as korsnickoNadzornik from Nadzornici join Operateri on Nadzornici.NadzornikID = Operateri.NadzornikID";
            using (SqlConnection con = new SqlConnection(konekcioniString))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(upit, con);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    OperaterSaNadzornikom o = new OperaterSaNadzornikom();
                    o.OperaterID = int.Parse(reader["OperaterID"].ToString());
                    o.Korisnicko = reader["Korisnicko"].ToString();
                    o.Ime = reader["Ime"].ToString();
                    o.Sifra = reader["Sifra"].ToString();
                    o.Adresa = reader["Adresa"].ToString();
                    o.Mesto = reader["Mesto"].ToString();
                    o.Telefon = reader["Telefon"].ToString();
                    o.NadzornikID = int.Parse(reader["NadzornikID"].ToString());
                    o.KorisnickoNadzornik = reader["korsnickoNadzornik"].ToString();
                    listaOperateraSaNadzornicima.Add(o);
                }
            }
            return listaOperateraSaNadzornicima;
        }

        public List<OperaterSaNadzornikom> GetPretrazeneOperatereNaOnsovuSvihParametaraIzKlase(OperaterSaNadzornikom oParametar)
        {
            List<OperaterSaNadzornikom> listaOperateraSaNadzornikom = new List<OperaterSaNadzornikom>();
            string upit = "select o.*, n.Korisnicko as korisnickoNadzornika from operateri o join nadzornici n on o.nadzornikID = n.nadzornikID " +
                " where o.korisnicko like @korisnicko +'%' and o.ime like @ime +'%' and o.sifra like @sifra +'%' and o.adresa like @adresa +'%' " +
                " and o.mesto like @mesto +'%' and o.telefon like @telefon +'%' and n.korisnicko like @korisnickoNadzornika +'%' ";

            using (SqlConnection con = new SqlConnection(konekcioniString))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(upit, con);

                cmd.Parameters.AddWithValue("@korisnicko", oParametar.Korisnicko);
                cmd.Parameters.AddWithValue("@ime", oParametar.Ime);
                cmd.Parameters.AddWithValue("@sifra", oParametar.Sifra);
                cmd.Parameters.AddWithValue("@adresa", oParametar.Adresa);
                cmd.Parameters.AddWithValue("@mesto", oParametar.Mesto);
                cmd.Parameters.AddWithValue("@telefon", oParametar.Telefon);
                cmd.Parameters.AddWithValue("@korisnickoNadzornika", oParametar.KorisnickoNadzornik);

                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    OperaterSaNadzornikom o = new OperaterSaNadzornikom();
                    o.OperaterID = int.Parse(reader["OperaterID"].ToString());
                    o.Korisnicko = reader["Korisnicko"].ToString();
                    o.Ime = reader["Ime"].ToString();
                    o.Sifra = reader["Sifra"].ToString();
                    o.Adresa = reader["Adresa"].ToString();
                    o.Mesto = reader["Mesto"].ToString();
                    o.Telefon = reader["Telefon"].ToString();
                    o.NadzornikID = int.Parse(reader["NadzornikID"].ToString());
                    o.KorisnickoNadzornik = reader["korisnickoNadzornika"].ToString();
                    listaOperateraSaNadzornikom.Add(o);
                }
            }

            return listaOperateraSaNadzornikom;
        }

        public int InsertOperatera(OperaterSaNadzornikom osn)
        {
            string upit = "insert into operateri (korisnicko, ime, sifra, adresa, mesto, telefon, nadzornikID) " +
                " values (@korisnicko, @ime, @sifra, @adresa, @mesto, @telefon, @nadzornikID)";
            using (SqlConnection con = new SqlConnection(konekcioniString))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(upit, con);
                cmd.Parameters.AddWithValue("@korisnicko", osn.Korisnicko);
                cmd.Parameters.AddWithValue("@ime", osn.Ime);
                cmd.Parameters.AddWithValue("@sifra", osn.Sifra);
                cmd.Parameters.AddWithValue("@adresa", osn.Adresa);
                cmd.Parameters.AddWithValue("@mesto", osn.Mesto);
                cmd.Parameters.AddWithValue("@telefon", osn.Telefon);
                cmd.Parameters.AddWithValue("@nadzornikID", osn.NadzornikID);
                return cmd.ExecuteNonQuery();
            }
        }

        public int PostojiOvakvoKorisnickoImeNaOsnovuImenaIIDa(string korisnickoIme, int idKorisnika)
        {
            int brojImena = 0;
            string upitNadzornici = "select count(*) from nadzornici where korisnicko = N'" + korisnickoIme + "'";
            string upitoperateri = "select count(*) from operateri where operaterID != " + idKorisnika + " and korisnicko = N'" + korisnickoIme + "'";
            using (SqlConnection con = new SqlConnection(konekcioniString))
            {
                con.Open();
                SqlTransaction transakcija = con.BeginTransaction();
                SqlCommand cmd = con.CreateCommand();
                cmd.Transaction = transakcija;

                try
                {
                    brojImena = 0;
                    cmd.CommandText = upitNadzornici;
                    brojImena += int.Parse(cmd.ExecuteScalar().ToString());
                    cmd.CommandText = upitoperateri;
                    brojImena += int.Parse(cmd.ExecuteScalar().ToString());
                    transakcija.Commit();
                }
                catch (Exception)
                {
                    transakcija.Rollback();
                }
            }
            return brojImena;
        }
        public int UpdateOperatera(Operater o)
        {
            string upit = "Update operateri set korisnicko = @korisnicko, ime = @ime, " +
                " sifra = @sifra, adresa = @adresa, mesto = @mesto, telefon = @telefon where operaterID = @operaterID";

            using (SqlConnection con = new SqlConnection(konekcioniString))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(upit, con);
                cmd.Parameters.AddWithValue("@korisnicko", o.Korisnicko);
                cmd.Parameters.AddWithValue("@ime", o.Ime);
                cmd.Parameters.AddWithValue("@sifra", o.Sifra);
                cmd.Parameters.AddWithValue("@adresa", o.Adresa);
                cmd.Parameters.AddWithValue("@mesto", o.Mesto);
                cmd.Parameters.AddWithValue("@telefon", o.Telefon);
                cmd.Parameters.AddWithValue("@operaterID", o.OperaterID);

                return cmd.ExecuteNonQuery();
            }
        }

        public int DeleteOperateraNaOsnovuIDa(int id)
        {
            string upit = "delete operateri where operaterID = " + id + "";
            using (SqlConnection con = new SqlConnection(konekcioniString))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(upit, con);
                return cmd.ExecuteNonQuery();
            }
        }

        public bool DaLijeMoguceBrisanjeOperateraPostojeLiNjegovaDecaTrueAkoJeste(int idOperatera)
        {
            bool moguceBrisanje = false;
            int brojDece = 0;
            string upit = "select count(*) from ulazi where operaterID = " + idOperatera + "";
            string upit2 = "select count(*) from izlazi where operaterID = " + idOperatera + "";
            using (SqlConnection con = new SqlConnection(konekcioniString))
            {
                con.Open();
                SqlTransaction transakcija = con.BeginTransaction();
                SqlCommand cmd = con.CreateCommand();
                cmd.Transaction = transakcija;
                try
                {
                    brojDece = 0;
                    cmd.CommandText = upit;
                    brojDece += int.Parse(cmd.ExecuteScalar().ToString());
                    cmd.CommandText = upit2;
                    brojDece += int.Parse(cmd.ExecuteScalar().ToString());
                    transakcija.Commit();

                    if (brojDece > 0)
                    {
                        moguceBrisanje =  false;
                    }
                    else
                    {
                        moguceBrisanje = true;
                    }
                }
                catch(Exception)
                {
                    transakcija.Rollback();
                }
            }
            return moguceBrisanje;
        }

        public int BrisanjeLogovaOperatera(int idOperatera)
        {
            string upit = "delete from OperaterLogovi where operaterID = " + idOperatera + "";
            using (SqlConnection con = new SqlConnection(konekcioniString))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(upit, con);
                return cmd.ExecuteNonQuery();
            }
        }
        public List<string> GetAllKorisnickaImenaZaKomboBox()
        {
            List<string> lista = new List<string>();
            string upit = "select Korisnicko from Operateri";
            using (SqlConnection con = new SqlConnection(konekcioniString))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandText = upit;
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    lista.Add(reader["Korisnicko"].ToString());
                }
                return lista;
            }
        }
    }
}
