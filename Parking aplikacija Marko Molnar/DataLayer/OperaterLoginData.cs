﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class OperaterLoginData
    {
        string konStr = "";
        public OperaterLoginData()
        {
            konStr = ConfigurationManager.ConnectionStrings["Konekcija"].ConnectionString;
        }
        public int InsertOperaterLog(OperaterLogin operaterLogin)
        {
            string upit = "insert into operaterLogovi (VremeLoga, OperaterID) values(@VremeLoga, @OperaterID)";
            using (SqlConnection con = new SqlConnection(konStr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(upit, con);
                cmd.Parameters.AddWithValue("@VremeLoga", operaterLogin.VremeLoga);
                cmd.Parameters.AddWithValue("@OperaterID", operaterLogin.OperaterID);
                return cmd.ExecuteNonQuery();
            }
        }
        public int UpdateOperaterLogVremeZaLogOut(int idOperatera)
        {
            string upit = "update operaterLogovi set VremePosle = @VremePosle where OperaterID = @OperaterID and vremePosle is null";
            using (SqlConnection con = new SqlConnection(konStr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(upit, con);
                cmd.Parameters.AddWithValue("@VremePosle", DateTime.Now);
                cmd.Parameters.AddWithValue("@OperaterID", idOperatera);
                return cmd.ExecuteNonQuery();
            }
        }

        public int BrojLogovaOperatera(int idOperatera)
        {
            string upit = "Select count(*) from OperaterLogovi where operaterID = " + idOperatera + "";
            using (SqlConnection con = new SqlConnection(konStr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(upit, con);
                return int.Parse(cmd.ExecuteScalar().ToString());
            }
        }

        public List<OperaterLogSaImenom> GetAllOperaterLogoviSaImenom()
        {
            List<OperaterLogSaImenom> listaOperaterLogova = new List<OperaterLogSaImenom>();
            string upit = "select OperaterLogovi.*, Operateri.Korisnicko from OperaterLogovi join" +
                " Operateri on OperaterLogovi.OperaterID = Operateri.OperaterID and OperaterLogovi.VremePosle is not null";
            using (SqlConnection con = new SqlConnection(konStr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(upit, con);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    OperaterLogSaImenom olsi = new OperaterLogSaImenom();
                    olsi.LogOperateraID = Convert.ToInt32(reader["logOperateraID"]);
                    olsi.VremeLoga = DateTime.Parse(reader["VremeLoga"].ToString());
                    olsi.VremePosle = DateTime.Parse(reader["VremePosle"].ToString());
                    olsi.OperaterKorisnickoIme = reader["Korisnicko"].ToString();
                    listaOperaterLogova.Add(olsi);
                }
            }
            return listaOperaterLogova;
        }

        public List<OperaterLogSaImenom> PretragaOperaterLogova(string korisnickoIme)
        {
            List<OperaterLogSaImenom> listaOperaterLogova = new List<OperaterLogSaImenom>();
            string upit = "select OperaterLogovi.*, Operateri.Korisnicko from OperaterLogovi join" +
                " Operateri on OperaterLogovi.OperaterID = Operateri.OperaterID " +
                " where Operateri.Korisnicko like @Korisnicko +'%' and OperaterLogovi.VremePosle is not null";
            using (SqlConnection con = new SqlConnection(konStr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(upit, con);
                cmd.Parameters.AddWithValue("@Korisnicko", korisnickoIme);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    OperaterLogSaImenom olsi = new OperaterLogSaImenom();
                    olsi.LogOperateraID = Convert.ToInt32(reader["logOperateraID"]);
                    olsi.VremeLoga = DateTime.Parse(reader["VremeLoga"].ToString());
                    olsi.VremePosle = DateTime.Parse(reader["VremePosle"].ToString());
                    olsi.OperaterKorisnickoIme = reader["Korisnicko"].ToString();
                    listaOperaterLogova.Add(olsi);
                }
            }
            return listaOperaterLogova;
        }
    }
}
