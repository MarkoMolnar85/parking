﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataLayer
{
    public class PretplatneKarteData
    {
        string konStr = "";
        public PretplatneKarteData()
        {
            konStr = ConfigurationManager.ConnectionStrings["Konekcija"].ConnectionString;
        }
        public List<PretplatnaKarta> GetAllPretplatneKarte()
        {
            List<PretplatnaKarta> lstPretplatnekarte = new List<PretplatnaKarta>();
            string upit = "select * from PretplatneKarte";
            using (SqlConnection con = new SqlConnection(konStr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandText = upit;
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    PretplatnaKarta pk = new PretplatnaKarta();
                    pk.ID = Convert.ToInt32(reader["PretplatneKarteID"]);
                    pk.Registracija = reader["Registracija"].ToString();
                    pk.OD = DateTime.Parse(reader["vaziOd"].ToString());
                    pk.DO = DateTime.Parse(reader["vaziDo"].ToString());
                    pk.NadzornikId = Convert.ToInt32(reader["NadzornikID"]);
                    pk.Cena = (float)Convert.ToDouble(reader["Cena"]);
                    pk.DatumIzdavanja = DateTime.Parse(reader["DatumIzdavanja"].ToString());
                    lstPretplatnekarte.Add(pk);
                }
                return lstPretplatnekarte;
            }
        }
        public bool DaLiPretplatnaKartaVaziZaOvajRegistarskiBroj(string registarskiBroj)
        {
            bool vazi = false;
            string upit = "select count(*) from pretplatneKarte where vaziOd <= '" + DateTime.Now + "' and vaziDo >= '" + DateTime.Now + "' and registracija = N'" + registarskiBroj + "'";
            using (SqlConnection con = new SqlConnection(konStr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(upit, con);
                //if (cmd.ExecuteNonQuery() > 0)
                if ((int)cmd.ExecuteScalar() > 0)
                {
                    vazi = true;
                }
            }
            return vazi;
        }

        public PretplatnaKarta GetPretplatnaKartaNaOsnovuRegistracijeiVremena(string registracija, DateTime vreme)
        {
            PretplatnaKarta pk = null;
            string upit = "select * from pretplatnekarte where registracija = N'" + registracija + "' and vaziOD <= '" + vreme + "' and vaziDO >= '" + vreme + "' ";
            using (SqlConnection con = new SqlConnection(konStr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(upit, con);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    pk = new PretplatnaKarta();
                    pk.ID = Convert.ToInt32(reader["PretplatneKarteID"]);
                    pk.Registracija = reader["Registracija"].ToString();
                    pk.OD = DateTime.Parse(reader["vaziOd"].ToString());
                    pk.DO = DateTime.Parse(reader["vaziDo"].ToString());
                    pk.NadzornikId = Convert.ToInt32(reader["NadzornikID"]);
                    pk.Cena = (float)Convert.ToDouble(reader["cena"]);
                    pk.DatumIzdavanja = DateTime.Parse(reader["DatumIzdavanja"].ToString());
                }
            }
            return pk;
        }

        public List<PretplatnaKartaSaNadzornikom> GetAllPretplatneKarteSaNadzornikom()
        {
            List<PretplatnaKartaSaNadzornikom> lstPretplatnekarte = new List<PretplatnaKartaSaNadzornikom>();
            string upit = "select p.*, n.korisnicko from PretplatneKarte p join Nadzornici n on p.nadzornikID = n.nadzornikID";
            using (SqlConnection con = new SqlConnection(konStr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandText = upit;
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    PretplatnaKartaSaNadzornikom pk = new PretplatnaKartaSaNadzornikom();
                    pk.ID = Convert.ToInt32(reader["PretplatneKarteID"].ToString());
                    pk.Registracija = reader["Registracija"].ToString();
                    pk.OD = DateTime.Parse(reader["vaziOd"].ToString());
                    pk.DO = DateTime.Parse(reader["vaziDo"].ToString());
                    pk.NadzornikId = Convert.ToInt32(reader["NadzornikID"].ToString());
                    pk.Cena = (float)Convert.ToDouble(reader["Cena"]);
                    pk.DatumIzdavanja = DateTime.Parse(reader["DatumIzdavanja"].ToString());
                    pk.NadzornikKorisnicko = reader["korisnicko"].ToString();
                    lstPretplatnekarte.Add(pk);
                }
                return lstPretplatnekarte;
            }
        }

        public bool DaLiJeDatumNoveKarteVeciOdStarihZaIstiRegistarskiBroj(PretplatnaKarta pk)
        {
            string upit = "select count(*) from pretplatneKarte where (vaziOD >= '" + pk.OD + "' or vaziDO >= '" + pk.OD + "') and registracija = '" + pk.Registracija + "' and pretplatneKarteID != " + pk.ID + "";
            bool jesteVeci = false;
            using (SqlConnection con = new SqlConnection(konStr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(upit, con);
                int brojRezultata = int.Parse(cmd.ExecuteScalar().ToString());

                if (brojRezultata > 0)
                {
                    jesteVeci = false;
                }
                else
                {
                    jesteVeci = true;
                }
            }
            return jesteVeci;
        }

        public int InsertPretplatneKarte(PretplatnaKarta pretplatnaKarta)
        {
            string upit = "insert into pretplatneKarte (registracija, vaziOD, vaziDO, nadzornikID, cena, datumIzdavanja) " +
                " values(@registracija, @vaziOD, @vaziDO, @nadzornikID, @cena, @datumIzdavanja)";
            int uspeh = 0;
            using (SqlConnection con = new SqlConnection(konStr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(upit, con);
                cmd.Parameters.AddWithValue("@registracija", pretplatnaKarta.Registracija);
                cmd.Parameters.AddWithValue("@vaziOD", pretplatnaKarta.OD);
                cmd.Parameters.AddWithValue("@VaziDO", pretplatnaKarta.DO);
                cmd.Parameters.AddWithValue("@nadzornikID", pretplatnaKarta.NadzornikId);
                cmd.Parameters.AddWithValue("@cena", pretplatnaKarta.Cena);
                cmd.Parameters.AddWithValue("@datumIzdavanja", pretplatnaKarta.DatumIzdavanja);
                uspeh = cmd.ExecuteNonQuery();
            }
            return uspeh;
        }

        public int DeletePretplatneKarte(int idPretplatneKarte)
        {
            string upit = "delete from pretplatneKarte where pretplatneKarteID = " + idPretplatneKarte + "";
            using (SqlConnection con = new SqlConnection(konStr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(upit, con);
                return cmd.ExecuteNonQuery();
            }
        }

        public int UpdatePretplatneKarte(PretplatnaKarta pretplatnaKarta)
        {

            string upit = "update pretplatneKarte set registracija = @registracija, vaziOD = @vaziOD, " +
                "vaziDO = @vaziDO, nadzornikID = @nadzornikID, cena = @cena, datumIzdavanja = @datumIzdavanja " +
                " where pretplatneKarteID = @pretplatneKarteID";
            int uspeh = 0;
            using (SqlConnection con = new SqlConnection(konStr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(upit, con);
                cmd.Parameters.AddWithValue("@registracija", pretplatnaKarta.Registracija);
                cmd.Parameters.AddWithValue("@vaziOD", pretplatnaKarta.OD);
                cmd.Parameters.AddWithValue("@VaziDO", pretplatnaKarta.DO);
                cmd.Parameters.AddWithValue("@nadzornikID", pretplatnaKarta.NadzornikId);
                cmd.Parameters.AddWithValue("@cena", pretplatnaKarta.Cena);
                cmd.Parameters.AddWithValue("@datumIzdavanja", pretplatnaKarta.DatumIzdavanja);
                cmd.Parameters.AddWithValue("@pretplatneKarteID", pretplatnaKarta.ID);
                uspeh = cmd.ExecuteNonQuery();
            }
            return uspeh;
        }
        public int ZadnjInsertovaniID()
        {
            string upit = "select ident_current('pretplatneKarte')";
            using (SqlConnection con = new SqlConnection(konStr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(upit, con);
                return int.Parse(cmd.ExecuteScalar().ToString());
            }
        }

        public List<PretplatnaKartaSaNadzornikom> GetPretplatneKarteSaNadzornikomKaoRezultatPretragePosSvimParametrima(string registracija, DateTime datumOd, DateTime datumDo, double manjiBroj, double veciBroj)
        //public List<PretplatnaKartaSaNadzornikom> GetPretplatneKarteSaNadzornikomKaoRezultatPretragePosSvimParametrima(object o)
        {
            //dynamic dynamicObjekat = o;
            List < PretplatnaKartaSaNadzornikom > lstPretplatnekarte = new List<PretplatnaKartaSaNadzornikom>();
            string upit = "select p.pretplatneKarteID, p.registracija, p.vaziOD, p.vaziDO, p.nadzornikID, p.cena, p.datumIzdavanja," +
                " n.korisnicko from PretplatneKarte p join Nadzornici n on p.nadzornikID = n.nadzornikID " +
                " where p.registracija like @registracija +'%' and @datumPocetka >= p.vaziOD and " +
                " @datumZavrsetka >= p.vaziDO and cena >= @donjaCena and cena <= @gornjaCena ";
            using (SqlConnection con = new SqlConnection(konStr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandText = upit;

                cmd.Parameters.AddWithValue("@registracija", registracija);
                cmd.Parameters.AddWithValue("@datumPocetka", datumOd);
                cmd.Parameters.AddWithValue("@datumZavrsetka", datumDo);
                cmd.Parameters.AddWithValue("@donjaCena", manjiBroj);
                cmd.Parameters.AddWithValue("@gornjaCena", veciBroj);

                //cmd.Parameters.AddWithValue("@registracija", dynamicObjekat.RegistracijaVozila);
                //cmd.Parameters.AddWithValue("@datumPocetka", dynamicObjekat.datumOD);
                //cmd.Parameters.AddWithValue("@datumZavrsetka", dynamicObjekat.datumDO);
                //cmd.Parameters.AddWithValue("@donjaCena", dynamicObjekat.ManjiBroj);
                //cmd.Parameters.AddWithValue("@gornjaCena", dynamicObjekat.VeciBroj);

                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    PretplatnaKartaSaNadzornikom pk = new PretplatnaKartaSaNadzornikom();
                    pk.ID = int.Parse(reader["PretplatneKarteID"].ToString());
                    pk.Registracija = reader["Registracija"].ToString();
                    pk.OD = DateTime.Parse(reader["vaziOd"].ToString());
                    pk.DO = DateTime.Parse(reader["vaziDo"].ToString());
                    pk.NadzornikId = Convert.ToInt32(reader["NadzornikID"].ToString());
                    pk.Cena = (float)Convert.ToDouble(reader["Cena"]);
                    pk.DatumIzdavanja = DateTime.Parse(reader["DatumIzdavanja"].ToString());
                    pk.NadzornikKorisnicko = reader["korisnicko"].ToString();
                    lstPretplatnekarte.Add(pk);
                }
                return lstPretplatnekarte;
            }
        }

    }
}
