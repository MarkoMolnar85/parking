﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//using BusinessLayer;

namespace DataLayer
{
    public class RacunData
    {
        string konStr = "";
        public RacunData()
        {
            konStr = ConfigurationManager.ConnectionStrings["Konekcija"].ConnectionString;
        }

        public int InsertRacunaIzlazaIzmenaUlaza(int idLokacije, string tablica, int idOperatera)
        {
            int uspehInsertIzlaza = 0;
            int uspehUpdateUlaza = 0;
            int uspehInsertRacuna = 0;
            int uspehUpdateRacuna = 0;

            //sadasnje vreme
            DateTime vremeSad = DateTime.Now;
            //vazi li pretplatna karta na izlazu
            PretplatneKarteData pkd = new PretplatneKarteData();
            bool pretplatnaKartaNaIzlazu = pkd.DaLiPretplatnaKartaVaziZaOvajRegistarskiBroj(tablica);
            string PretplatnaObicna = "Obična";
            if (pretplatnaKartaNaIzlazu)
            {
                PretplatnaObicna = "Pretplatna";
            }
            //smenaid
            SmenaData sd = new SmenaData();
            Smena smena = sd.GetSmenaNaOsnovuTrenutnogVremena();
            int smenaID = smena.ID;
            //podaci o ulazu, id bitan za racun, a vazno i za azuriranje ulaza. Dobijam i da li je karta obicna ili nije
            UlaziData ud = new UlaziData();
            Ulaz ulaz = ud.GetUlazGdeJeNaParkinguNaOsnoviIdLokacijeiTablica(idLokacije, tablica);

            using (SqlConnection con = new SqlConnection(konStr))
            {
                con.Open();
                SqlTransaction transakcija = con.BeginTransaction();
                SqlCommand cmd = con.CreateCommand();
                cmd.Transaction = transakcija;
                try
                {
                    string upitInsertIzlaza = "insert into izlazi (tablica, vreme, tipKarte, operaterID, smenaID, lokacijaID) " +
                        "values (N'" + tablica + "', '" + vremeSad + "', N'" + PretplatnaObicna + "', " + idOperatera + ", " + smenaID + ", " + idLokacije + ")";
                    cmd.CommandText = upitInsertIzlaza;
                    uspehInsertIzlaza = cmd.ExecuteNonQuery();

                    string upitZadnjiIDInsertaIzlaza = "select IDENT_CURRENT ('izlazi')";
                    cmd.CommandText = upitZadnjiIDInsertaIzlaza;
                    int ZadnjiIDInsertovanogIzlaza = int.Parse(cmd.ExecuteScalar().ToString());

                    string upitAzuriranjeUlaza = "update ulazi set naParkingu = 0 where ulazID = " + ulaz.ulazID + "";
                    cmd.CommandText = upitAzuriranjeUlaza;
                    uspehUpdateUlaza = cmd.ExecuteNonQuery();

                    string upitInsertRacuna = "insert into racuni (tablica, izlazID, ulazID, cenaID) " +
                        "values(N'" + tablica + "', " + ZadnjiIDInsertovanogIzlaza + ", " + ulaz.ulazID + ", (select max(cenaid) from cene where lokacijaID = " + idLokacije + "))";
                    cmd.CommandText = upitInsertRacuna;
                    uspehInsertRacuna = cmd.ExecuteNonQuery();

                    string upitZadnjiIDInsertaRacuna = "select IDENT_CURRENT ('racuni')";
                    cmd.CommandText = upitZadnjiIDInsertaRacuna;
                    int zadnjiIdInsertaRacuna = int.Parse(cmd.ExecuteScalar().ToString());

                    //izracunati vreme na parkingu
                    DateTime vremeNaUlazu = ulaz.Vreme;
                    DateTime vremeNaIzlazu = vremeSad;

                    //deklarisem sate koji ce biti izracunati u zavisnosti od tipa karte dole ispod
                    int sati = 0;

                    //izvlacim cenu
                    CenaData cd = new CenaData();
                    Cena cena = cd.GetCenaNaOsnovuIdaCene(cd.GetZadnjaCenaID(idLokacije));

                    //odredjivanje cene na racunu ispod

                    //provera da li je na ulazu bila pretplatna ili obicna
                    bool pretplatnaKartaNaUlazu = false;
                    if (ulaz.TipKarte == "Pretplatna")
                    {
                        pretplatnaKartaNaUlazu = true;
                    }
                    else
                    {
                        pretplatnaKartaNaUlazu = false;
                    }
                    //racunane cene za razne mogucnosti
                    int satiPlaceniNeplaceni = (int)(vremeNaIzlazu - vremeNaUlazu).TotalHours + 1;
                    if (pretplatnaKartaNaIzlazu == true && pretplatnaKartaNaUlazu == true)
                    {
                        sati = 0;
                    }
                    else if (pretplatnaKartaNaIzlazu == false && pretplatnaKartaNaUlazu == false)
                    {
                        sati = (int)(vremeNaIzlazu - vremeNaUlazu).TotalHours;
                        sati += 1;
                    }
                    else if (pretplatnaKartaNaIzlazu == true && pretplatnaKartaNaUlazu == false)
                    {
                        PretplatnaKarta pretplatnaKartaKojaVaziNaIzlazu = pkd.GetPretplatnaKartaNaOsnovuRegistracijeiVremena(tablica, vremeNaIzlazu);
                        sati = (int)(pretplatnaKartaKojaVaziNaIzlazu.OD - vremeNaUlazu).TotalHours;
                        sati += 1;
                    }
                    else if (pretplatnaKartaNaIzlazu == false && pretplatnaKartaNaUlazu == true)
                    {
                        PretplatnaKarta pretplatnaKartaKojavaziNaUlazu = pkd.GetPretplatnaKartaNaOsnovuRegistracijeiVremena(tablica, vremeNaUlazu);
                        sati = (int)(vremeNaIzlazu - pretplatnaKartaKojavaziNaUlazu.DO).TotalHours;
                        sati += 1;
                    }

                    double ukupnaCena = sati * cena.CenaIznos;

                    string upitUpdateracuna = "update racuni set vremeNaParkingu = " + sati + ", ukupnaCena = " + ukupnaCena + ", satiNaParkinguPlaceniNeplaceni = " + satiPlaceniNeplaceni + " where racunID = " + zadnjiIdInsertaRacuna + " ";
                    cmd.CommandText = upitUpdateracuna;
                    uspehUpdateRacuna = cmd.ExecuteNonQuery();

                    transakcija.Commit();
                }
                catch (Exception)
                {
                    try
                    {
                        transakcija.Rollback();
                    }
                    catch (Exception)
                    {
                        throw new Exception("Upis nije uspeo");
                    }
                }

            }
            if (uspehInsertIzlaza > 0 && uspehInsertRacuna > 0 && uspehUpdateRacuna > 0 && uspehUpdateUlaza > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public List<Racun> GetSviRacuniNaOsnovuLokacije(int idLokacije)
        {
            List<Racun> listaRacuna = new List<Racun>();
            string upit = "select racuni.* from racuni join Ulazi on racuni.UlazID = Ulazi.UlazID and Ulazi.LokacijaID = " + idLokacije + "";
            using (SqlConnection con = new SqlConnection(konStr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(upit, con);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Racun r = new Racun();
                    r.RacunID = Convert.ToInt32(reader["RacunID"]);
                    r.Tablica = reader["Tablica"].ToString();
                    r.IzlazID = Convert.ToInt32(reader["IzlazID"]);
                    r.UlazID = Convert.ToInt32(reader["UlazID"]);
                    r.VremeNaParkingu = Convert.ToInt32(reader["VremeNaParkingu"]);
                    r.UkupnaCena = (float)Convert.ToDouble(reader["UkupnaCena"]);
                    r.CenaID = Convert.ToInt32(reader["CenaID"]);
                    r.SatiNaParkinguPlaceniNeplaceni = Convert.ToInt32(reader["SatiNaParkinguPlaceniNeplaceni"]);
                    listaRacuna.Add(r);
                }
            }
            return listaRacuna;
        }

        public List<Racun> PretragaRacunaPoSvimParametrima(Racun rm, int satiOD, int satiDo, double cenaOD, double cenaDO, int IDlokacija)
        {
            string brojRacuna = rm.RacunID.ToString();
            string brojTablice = rm.Tablica;
            List<Racun> listaRacuna = new List<Racun>();
            string upit = "select racuni.RacunID, racuni.tablica as TablicaRacun, racuni.IzlazID, racuni.UlazID, racuni.VremeNaParkingu, racuni.UkupnaCena, racuni.CenaID, racuni.SatiNaParkinguPlaceniNeplaceni " +
                "from racuni join Ulazi on racuni.UlazID = Ulazi.UlazID and Ulazi.LokacijaID = " + IDlokacija + " " +
                " and cast(racunID as varchar(25)) like @brojRacuna +'%' and racuni.tablica like @Tablica +'%' " +
                " and racuni.SatiNaParkinguPlaceniNeplaceni between @satiOD and @satiDO and racuni.UkupnaCena between @cenaOD and @cenaDO";
            using (SqlConnection con = new SqlConnection(konStr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(upit, con);
                cmd.Parameters.AddWithValue("@brojRacuna", brojRacuna);
                cmd.Parameters.AddWithValue("@Tablica", brojTablice);
                cmd.Parameters.AddWithValue("@satiOD", satiOD);
                cmd.Parameters.AddWithValue("@satiDO", satiDo);
                cmd.Parameters.AddWithValue("@cenaOD", cenaOD);
                cmd.Parameters.AddWithValue("@cenaDO", cenaDO);

                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Racun r = new Racun();
                    r.RacunID = Convert.ToInt32(reader["RacunID"]);
                    r.Tablica = reader["TablicaRacun"].ToString();
                    r.IzlazID = Convert.ToInt32(reader["IzlazID"]);
                    r.UlazID = Convert.ToInt32(reader["UlazID"]);
                    r.VremeNaParkingu = Convert.ToInt32(reader["VremeNaParkingu"]);
                    r.UkupnaCena = (float)Convert.ToDouble(reader["UkupnaCena"]);
                    r.CenaID = Convert.ToInt32(reader["CenaID"]);
                    r.SatiNaParkinguPlaceniNeplaceni = Convert.ToInt32(reader["SatiNaParkinguPlaceniNeplaceni"]);
                    listaRacuna.Add(r);
                }
            }
            return listaRacuna;
        }

        public List<Pazar> GetAllPazariPremaIdeuLokacije(int idLokacije)
        {
            List<Pazar> listaPazara = new List<Pazar>();
            string upit = "select Operateri.Korisnicko, convert(varchar, Izlazi.Vreme, 105) datum, " +
                "Smene.NazivSmene, sum(Racuni.UkupnaCena) as Suma, sum(Racuni.SatiNaParkinguPlaceniNeplaceni) sati," +
                " AVG(Racuni.UkupnaCena) as 'Prosecno placanje', AVG(Racuni.SatiNaParkinguPlaceniNeplaceni) as 'Prosecni sati', " +
                "MAX(Racuni.SatiNaParkinguPlaceniNeplaceni) as 'Maksimalni sati', min(Racuni.SatiNaParkinguPlaceniNeplaceni) as 'Minimalni sati'" +
                " from Racuni join Izlazi on Racuni.IzlazID = Izlazi.IzlazID join Smene on" +
                " Smene.SmenaID = Izlazi.SmenaID join Operateri on Operateri.OperaterID = Izlazi.OperaterID " +
                "where Izlazi.LokacijaID = " + idLokacije + " " +
                "group by convert(varchar, Izlazi.Vreme, 105), Smene.NazivSmene, Operateri.Korisnicko ";
            using (SqlConnection con = new SqlConnection(konStr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(upit, con);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Pazar p = new Pazar();
                    p.Korisnicko = reader["Korisnicko"].ToString();
                    p.Datum = DateTime.Parse(reader["datum"].ToString());
                    p.NazivSmene = reader["NazivSmene"].ToString();
                    p.Suma = double.Parse(reader["Suma"].ToString());
                    p.Sati = int.Parse(reader["Sati"].ToString());
                    p.ProsecnoPlacanje = double.Parse(reader["Prosecno placanje"].ToString());
                    p.ProsecnoSati = double.Parse(reader["Prosecni sati"].ToString());
                    p.MaksimalnoSati = int.Parse(reader["Maksimalni sati"].ToString());
                    p.MinimalnoSati = int.Parse(reader["Minimalni sati"].ToString());
                    listaPazara.Add(p);
                }
            }
            return listaPazara;
        }

        public List<Pazar> PretragaPazaraPoParametrima(Pazar paz, DateTime OD, DateTime DO, double zaradaOD, double zaradaDO, int idLokacije)
        {
            List<Pazar> listaPazara = new List<Pazar>();
            string upit = "select Operateri.Korisnicko, convert(date, Izlazi.Vreme, 105) datum, " +
                "Smene.NazivSmene, sum(Racuni.UkupnaCena) as Suma, sum(Racuni.SatiNaParkinguPlaceniNeplaceni) sati," +
                " AVG(Racuni.UkupnaCena) as 'Prosecno placanje', AVG(Racuni.SatiNaParkinguPlaceniNeplaceni) as 'Prosecni sati', " +
                "MAX(Racuni.SatiNaParkinguPlaceniNeplaceni) as 'Maksimalni sati', min(Racuni.SatiNaParkinguPlaceniNeplaceni) as 'Minimalni sati'" +
                " from Racuni join Izlazi on Racuni.IzlazID = Izlazi.IzlazID join Smene on" +
                " Smene.SmenaID = Izlazi.SmenaID join Operateri on Operateri.OperaterID = Izlazi.OperaterID " +
                "where Izlazi.LokacijaID = " + idLokacije + " " +
                "group by convert(date, Izlazi.Vreme, 105), Smene.NazivSmene, Operateri.Korisnicko " +
                "having Operateri.Korisnicko like @Korisnicko +'%' and Smene.NazivSmene like @NazivSmene +'%' " +
                "and convert(date, Izlazi.Vreme, 105) between @datumOD and @datumDO " +
                "and sum(Racuni.UkupnaCena) between @sumaOD and @sumaDO ";
            using (SqlConnection con = new SqlConnection(konStr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(upit, con);
                cmd.Parameters.AddWithValue("@Korisnicko", paz.Korisnicko);
                cmd.Parameters.AddWithValue("@NazivSmene", paz.NazivSmene);
                cmd.Parameters.AddWithValue("@datumOD", OD);
                cmd.Parameters.AddWithValue("@datumDO", DO);
                cmd.Parameters.AddWithValue("@sumaOD", zaradaOD);
                cmd.Parameters.AddWithValue("@sumaDO", zaradaDO);

                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Pazar p = new Pazar();
                    p.Korisnicko = reader["Korisnicko"].ToString();
                    p.Datum = DateTime.Parse(reader["datum"].ToString());
                    p.NazivSmene = reader["NazivSmene"].ToString();
                    p.Suma = double.Parse(reader["Suma"].ToString());
                    p.Sati = int.Parse(reader["Sati"].ToString());
                    p.ProsecnoPlacanje = double.Parse(reader["Prosecno placanje"].ToString());
                    p.ProsecnoSati = double.Parse(reader["Prosecni sati"].ToString());
                    p.MaksimalnoSati = int.Parse(reader["Maksimalni sati"].ToString());
                    p.MinimalnoSati = int.Parse(reader["Minimalni sati"].ToString());
                    listaPazara.Add(p);
                }
            }
            return listaPazara;
        }

    }
}
