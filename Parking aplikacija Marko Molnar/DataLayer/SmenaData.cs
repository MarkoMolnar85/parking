﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class SmenaData
    {
        string konStr;
        public SmenaData()
        {
            konStr = ConfigurationManager.ConnectionStrings["Konekcija"].ConnectionString;
        }
        public int IdSmeneNaOsnovuNaziva(string nazivSmene)
        {
            int id = 0;
            using (SqlConnection con = new SqlConnection(konStr))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = con;
                    cmd.CommandText = "select smenaId from smene where nazivSmene = N'" + nazivSmene + "' ";
                    id = Convert.ToInt32(cmd.ExecuteScalar());
                    return id;
                }
            }
        }

        public List<Smena> GetAllSmene()
        {
            List<Smena> lstSmene = new List<Smena>();

            string upit = "select * from smene";
            using (SqlConnection con = new SqlConnection(konStr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(upit, con);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Smena smena = new Smena();
                    smena.ID = Convert.ToInt32(reader["SmenaID"]);
                    smena.NazivSmene = reader["NazivSmene"].ToString();
                    smena.OD = TimeSpan.Parse(reader["OD"].ToString());
                    smena.DO = TimeSpan.Parse(reader["DO"].ToString());
                    lstSmene.Add(smena);
                }
            }
            return lstSmene;
        }

        public Smena GetSmenaNaOsnovuTrenutnogVremena()
        {
            Smena smena = null;
            TimeSpan vremeSad = TimeSpan.Parse(DateTime.Now.ToString("HH:mm:ss"));
            //vremeSad = new TimeSpan(03, 00, 00);
            string upit = "select * from smene where OD <= '" + vremeSad + "' and DO >= '" + vremeSad + "' and TrenutnoVazeca = 1 ";
            using (SqlConnection con = new SqlConnection(konStr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(upit, con);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    smena = new Smena();
                    smena.ID = Convert.ToInt32(reader["SmenaID"]);
                    smena.NazivSmene = reader["NazivSmene"].ToString();
                    smena.OD = TimeSpan.Parse(reader["OD"].ToString());
                    smena.DO = TimeSpan.Parse(reader["DO"].ToString());
                }
                if (smena == null)
                {
                    //string upit2 = "select * from smene where od in (select max(od) from smene) and do in (select min(do) from smene) and TrenutnoVazeca = 1";
                    string upit2 = "select * from smene where od in (select max(od) from smene where TrenutnoVazeca = 1) and do in (select min(do) from smene where TrenutnoVazeca = 1) and TrenutnoVazeca = 1";
                    using (SqlConnection con2 = new SqlConnection(konStr))
                    {
                        con2.Open();
                        SqlCommand cmd2 = new SqlCommand(upit2, con2);
                        SqlDataReader reader2 = cmd2.ExecuteReader();
                        while (reader2.Read())
                        {
                            smena = new Smena();
                            smena.ID = Convert.ToInt32(reader2["SmenaID"]);
                            smena.NazivSmene = reader2["NazivSmene"].ToString();
                            smena.OD = TimeSpan.Parse(reader2["OD"].ToString());
                            smena.DO = TimeSpan.Parse(reader2["DO"].ToString());
                        }
                    }
                }
            }
            return smena;
        }

        public List<string> GetAllNaziviSmenaZaKomboBox()
        {
            List<string> lista = new List<string>();
            string upit = "select NazivSmene from smene where trenutnoVazeca = 1";
            using (SqlConnection con = new SqlConnection(konStr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandText = upit;
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    lista.Add(reader["NazivSmene"].ToString());
                }
                return lista;
            }
        }

        public List<Smena> GetAktuelneSmene()
        {
            List<Smena> listaSmena = new List<Smena>();
            string upit = "select * from smene where trenutnoVazeca = 1";
            using (SqlConnection con = new SqlConnection(konStr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(upit, con);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Smena s = new Smena();
                    s.ID = Convert.ToInt32(reader["smenaId"]);
                    s.NazivSmene = reader["nazivSmene"].ToString();
                    s.OD= TimeSpan.Parse(reader["od"].ToString());
                    s.DO = TimeSpan.Parse(reader["do"].ToString());
                    s.TrenutnoVazeca = Convert.ToInt32(reader["TrenutnoVazeca"]);
                    listaSmena.Add(s);
                }
            }
            return listaSmena;
        }

        public int InsertovanjeSmeneUpdateovanjeStarih(List<Smena>listaSmena)
        {
            string upit = "begin try " +
                "begin transaction " +
                "update smene set trenutnoVazeca = 0 " +
                "insert into smene(nazivSmene, OD, DO, trenutnoVazeca) values(@nazivSmene1, @OD1, @DO1, @trenutnoVazeca1) " +
                "insert into smene(nazivSmene, OD, DO, trenutnoVazeca) values(@nazivSmene2, @OD2, @DO2, @trenutnoVazeca2) " +
                "insert into smene(nazivSmene, OD, DO, trenutnoVazeca) values(@nazivSmene3, @OD3, @DO3, @trenutnoVazeca3) " +
                "commit " +
                "end try " +
                "begin catch rollback end catch";
            using (SqlConnection con = new SqlConnection(konStr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(upit, con);
                cmd.Parameters.AddWithValue("@nazivSmene1", listaSmena[0].NazivSmene);
                cmd.Parameters.AddWithValue("@OD1", listaSmena[0].OD);
                cmd.Parameters.AddWithValue("@DO1", listaSmena[0].DO);
                cmd.Parameters.AddWithValue("@trenutnoVazeca1", listaSmena[0].TrenutnoVazeca);

                cmd.Parameters.AddWithValue("@nazivSmene2", listaSmena[1].NazivSmene);
                cmd.Parameters.AddWithValue("@OD2", listaSmena[1].OD);
                cmd.Parameters.AddWithValue("@DO2", listaSmena[1].DO);
                cmd.Parameters.AddWithValue("@trenutnoVazeca2", listaSmena[1].TrenutnoVazeca);

                cmd.Parameters.AddWithValue("@nazivSmene3", listaSmena[2].NazivSmene);
                cmd.Parameters.AddWithValue("@OD3", listaSmena[2].OD);
                cmd.Parameters.AddWithValue("@DO3", listaSmena[2].DO);
                cmd.Parameters.AddWithValue("@trenutnoVazeca3", listaSmena[2].TrenutnoVazeca);

                return cmd.ExecuteNonQuery();
            }
        }

    }
}
