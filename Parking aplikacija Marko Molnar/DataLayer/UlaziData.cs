﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DataLayer
{
    public class UlaziData
    {
        string konekcioniString = "";
        public UlaziData()
        {
            konekcioniString = ConfigurationManager.ConnectionStrings["Konekcija"].ConnectionString;
        }

        public List<Ulaz> GetAllUlazi()
        {
            List<Ulaz> lstUlaz = new List<Ulaz>();
            string upit = "select * from ulazi";
            using (SqlConnection con = new SqlConnection(konekcioniString))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = con;
                    cmd.CommandText = upit;
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        Ulaz u = new Ulaz();
                        u.ulazID = Convert.ToInt32(reader["UlazID"]);
                        u.Tablica = reader["Tablica"].ToString();
                        u.Vreme = DateTime.Parse(reader["Vreme"].ToString());
                        u.NaParkingu = Convert.ToInt32(reader["NaParkingu"]);
                        u.TipKarte = reader["TipKarte"].ToString();
                        u.OperatorID = Convert.ToInt32(reader["OperaterID"]);
                        u.SmenaID = Convert.ToInt32(reader["SmenaID"]);
                        u.LokacijaID = Convert.ToInt32(reader["LokacijaID"]);

                        lstUlaz.Add(u);
                    }
                    return lstUlaz;
                }
            }
        }

        public List<Ulaz> GetUlaziPremaIDuLokacije(int idLokacije)
        {
            List<Ulaz> lstUlaz = new List<Ulaz>();
            string upit = "select * from ulazi where lokacijaID = " + idLokacije + " order by ulazID desc";
            using (SqlConnection con = new SqlConnection(konekcioniString))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = con;
                    cmd.CommandText = upit;
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        Ulaz u = new Ulaz();
                        u.ulazID = Convert.ToInt32(reader["UlazID"]);
                        u.Tablica = reader["Tablica"].ToString();
                        u.Vreme = DateTime.Parse(reader["Vreme"].ToString());
                        u.NaParkingu = Convert.ToInt32(reader["NaParkingu"]);
                        u.TipKarte = reader["TipKarte"].ToString();
                        u.OperatorID = Convert.ToInt32(reader["OperaterID"]);
                        u.SmenaID = Convert.ToInt32(reader["SmenaID"]);
                        u.LokacijaID = Convert.ToInt32(reader["LokacijaID"]);

                        lstUlaz.Add(u);
                    }
                    return lstUlaz;
                }
            }
        }

        public int InsertUlaza(Ulaz u)
        {
            //string upit = "INSERT INTO ulazi (Tablica, Vreme, NaParkingu, TipKarte, OperaterID, SmenaID, LokacijaID) " +
            //    " VALUES (N'" + u.Tablica + "', '" + u.Vreme + "', " + u.NaParkingu + ", N'" + u.TipKarte + "', " + u.OperatorID + ", " + u.SmenaID + ", " + u.LokacijaID + ")";
            string upit = "INSERT INTO ulazi (Tablica, Vreme, NaParkingu, TipKarte, OperaterID, SmenaID, LokacijaID) " +
                " VALUES (@Tablica, @Vreme, @NaParkingu, @TipKarte, @OperaterID, @SmenaID, @LokacijaID)";
            using (SqlConnection con = new SqlConnection(konekcioniString))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = con;
                    cmd.CommandText = upit;

                    cmd.Parameters.AddWithValue("@Tablica", u.Tablica);
                    cmd.Parameters.AddWithValue("@Vreme", u.Vreme);
                    cmd.Parameters.AddWithValue("@NaParkingu", u.NaParkingu);
                    cmd.Parameters.AddWithValue("@TipKarte", u.TipKarte);
                    cmd.Parameters.AddWithValue("@OperaterID", u.OperatorID);
                    cmd.Parameters.AddWithValue("@SmenaID", u.SmenaID);
                    cmd.Parameters.AddWithValue("@LokacijaID", u.LokacijaID);

                    return cmd.ExecuteNonQuery();
                }
            }
        }
        public int GetZadnjiInsertovaniID()
        {
            using (SqlConnection con = new SqlConnection(konekcioniString))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = con;
                    cmd.CommandText = "SELECT IDENT_CURRENT('ulazi')";
                    return int.Parse(cmd.ExecuteScalar().ToString());
                }
            }
        }

        public int GetBrojAutaNaParkingu(int idLokacije)
        {
            int brojAutaNaparkingu = 0;
            string upit = "select count (*) from ulazi where NaParkingu = 1 and lokacijaID = " + idLokacije + "";
            using (SqlConnection con = new SqlConnection(konekcioniString))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = con;
                    cmd.CommandText = upit;
                    brojAutaNaparkingu = Convert.ToInt32(cmd.ExecuteScalar());
                }
                return brojAutaNaparkingu;
            }
        }

        //ova funckija trazi registraciju na bilo kom parkingu, jer kad proveravam da li je auto prisutan na parkingu on ne moze biti napolju bez obzira na kom je parkingu 
        public int GetBrojAutomobilaSaOdredjenomRegistracijomPrisutnihNaParkignuNaOsnovuRegistracije(string tablice)
        {
            int brojAutomobila = 0;

            string upit = "select count(*) from ulazi where tablica = N'" + tablice + "'";
            using (SqlConnection con = new SqlConnection(konekcioniString))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(upit, con);
                brojAutomobila = Convert.ToInt32(cmd.ExecuteScalar());
            }
            return brojAutomobila;
        }

        public List<Ulaz> GetAllAutiKojiSeNalazeNaParkinguPremaIdLokacije(int lokacijaID)
        {
            List<Ulaz> listaUlaza = new List<Ulaz>();
            string upit = "select * from Ulazi where lokacijaID = " + lokacijaID + " and NaParkingu = 1 order by ulazID desc";
            using (SqlConnection con = new SqlConnection(konekcioniString))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(upit, con);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Ulaz ulaz = new Ulaz();
                    ulaz.ulazID = Convert.ToInt32(reader["UlazID"]);
                    ulaz.Tablica = reader["Tablica"].ToString();
                    ulaz.Vreme = DateTime.Parse(reader["Vreme"].ToString());
                    ulaz.NaParkingu = Convert.ToInt32(reader["NaParkingu"]);
                    ulaz.TipKarte = reader["TipKarte"].ToString();
                    ulaz.OperatorID = Convert.ToInt32(reader["OperaterID"]);
                    ulaz.SmenaID = Convert.ToInt32(reader["SmenaID"]);
                    ulaz.LokacijaID = Convert.ToInt32(reader["LokacijaID"]);
                    listaUlaza.Add(ulaz);
                }
                return listaUlaza;
            }
        }
        public Ulaz GetUlazGdeJeNaParkinguNaOsnoviIdLokacijeiTablica(int idLokacije, string tablice)
        {
            Ulaz u = null;
            string upit = "select * from ulazi where lokacijaID = " + idLokacije + " and tablica = N'" + tablice + "' and NaParkingu = 1";
            using (SqlConnection con = new SqlConnection(konekcioniString))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(upit, con);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    u = new Ulaz();
                    u.ulazID = Convert.ToInt32(reader["ulazID"]);
                    u.Tablica = reader["Tablica"].ToString();
                    u.Vreme = DateTime.Parse(reader["Vreme"].ToString());
                    u.NaParkingu = Convert.ToInt32(reader["NaParkingu"]);
                    u.TipKarte = reader["TipKarte"].ToString();
                    u.OperatorID = Convert.ToInt32(reader["OperaterID"]);
                    u.SmenaID = Convert.ToInt32(reader["SmenaID"]);
                    u.LokacijaID = Convert.ToInt32(reader["LokacijaID"]);
                }
            }
            return u;
        }

        public List<UlazSaOstalim> GetAllAutSaDodacimaiNaOsnovuLokacije(int lokacijaID)
        {
            List<UlazSaOstalim> listaUlaza = new List<UlazSaOstalim>();
            string upit = "select u.*, o.Korisnicko, s.NazivSmene, l.NazivLokacije from Operateri o join Ulazi u " +
                "on o.OperaterID = u.OperaterID join Smene s on s.SmenaID = u.SmenaID join Lokacije l " +
                "on l.LokacijaID = u.LokacijaID where u.lokacijaID = " + lokacijaID + "";
            using (SqlConnection con = new SqlConnection(konekcioniString))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(upit, con);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    UlazSaOstalim ulaz = new UlazSaOstalim();
                    ulaz.ulazID = Convert.ToInt32(reader["UlazID"]);
                    ulaz.Tablica = reader["Tablica"].ToString();
                    ulaz.Vreme = DateTime.Parse(reader["Vreme"].ToString());
                    ulaz.NaParkingu = Convert.ToInt32(reader["NaParkingu"]);
                    ulaz.TipKarte = reader["TipKarte"].ToString();
                    ulaz.OperatorID = Convert.ToInt32(reader["OperaterID"]);
                    ulaz.SmenaID = Convert.ToInt32(reader["SmenaID"]);
                    ulaz.LokacijaID = Convert.ToInt32(reader["LokacijaID"]);
                    ulaz.OperaterKorisnicko = reader["Korisnicko"].ToString();
                    ulaz.SmenaNaziv = reader["NazivSmene"].ToString();
                    ulaz.LokacijaNaziv = reader["NazivLokacije"].ToString();

                    listaUlaza.Add(ulaz);
                }
                return listaUlaza;
            }
        }

        public List<UlazSaOstalim> PretragaUlazaNaOsnovuSvihParametaraUlaziIOstalo(UlazSaOstalim uso, DateTime datumOD, DateTime datumDO, int lokacijaID)
        {
            List<UlazSaOstalim> listaUlaza = new List<UlazSaOstalim>();
            string upit = "select u.*, o.Korisnicko, s.NazivSmene, l.NazivLokacije from Operateri o join Ulazi u " +
                "on o.OperaterID = u.OperaterID join Smene s on s.SmenaID = u.SmenaID join Lokacije l " +
                "on l.LokacijaID = u.LokacijaID " +
                "where u.Tablica like @Tablica + '%' and u.Vreme >= @datumOD and u.Vreme <= @datumDO and " +
                "u.NaParkingu = @NaParkingu and u.TipKarte like @TipKarte + '%' and o.Korisnicko like @Korisnicko +'%' and " +
                "s.NazivSmene like @NazivSmene + '%' " +
                " and u.lokacijaID = @lokacijaID";
            using (SqlConnection con = new SqlConnection(konekcioniString))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(upit, con);
                cmd.Parameters.AddWithValue("@Tablica", uso.Tablica);
                cmd.Parameters.AddWithValue("@datumOD", datumOD);
                cmd.Parameters.AddWithValue("@datumDO", datumDO);
                cmd.Parameters.AddWithValue("@NaParkingu", uso.NaParkingu);
                cmd.Parameters.AddWithValue("@TipKarte", uso.TipKarte);
                cmd.Parameters.AddWithValue("@Korisnicko", uso.OperaterKorisnicko);
                cmd.Parameters.AddWithValue("@NazivSmene", uso.SmenaNaziv);
                //cmd.Parameters.AddWithValue("@NazivLokacije", uso.LokacijaNaziv);
                cmd.Parameters.AddWithValue("@lokacijaID", lokacijaID);


                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    UlazSaOstalim ulaz = new UlazSaOstalim();
                    ulaz.ulazID = Convert.ToInt32(reader["UlazID"]);
                    ulaz.Tablica = reader["Tablica"].ToString();
                    ulaz.Vreme = DateTime.Parse(reader["Vreme"].ToString());
                    ulaz.NaParkingu = Convert.ToInt32(reader["NaParkingu"]);
                    ulaz.TipKarte = reader["TipKarte"].ToString();
                    ulaz.OperatorID = Convert.ToInt32(reader["OperaterID"]);
                    ulaz.SmenaID = Convert.ToInt32(reader["SmenaID"]);
                    ulaz.LokacijaID = Convert.ToInt32(reader["LokacijaID"]);
                    ulaz.OperaterKorisnicko = reader["Korisnicko"].ToString();
                    ulaz.SmenaNaziv = reader["NazivSmene"].ToString();
                    ulaz.LokacijaNaziv = reader["NazivLokacije"].ToString();

                    listaUlaza.Add(ulaz);
                }
                return listaUlaza;
            }
        }
        public Ulaz GetUlazPremaIDu(int idUlaza)
        {
            Ulaz ulaz = null;
            string upit = "select * from ulazi where ulazID = " + idUlaza + "";
            using (SqlConnection con = new SqlConnection(konekcioniString))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(upit, con);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ulaz = new Ulaz();
                    ulaz.ulazID = Convert.ToInt32(reader["UlazID"]);
                    ulaz.Tablica = reader["Tablica"].ToString();
                    ulaz.Vreme = DateTime.Parse(reader["Vreme"].ToString());
                    ulaz.NaParkingu = Convert.ToInt32(reader["NaParkingu"]);
                    ulaz.TipKarte = reader["TipKarte"].ToString();
                    ulaz.OperatorID = Convert.ToInt32(reader["OperaterID"]);
                    ulaz.SmenaID = Convert.ToInt32(reader["SmenaID"]);
                    ulaz.LokacijaID = Convert.ToInt32(reader["LokacijaID"]);
                }
                return ulaz;
            }
        }


    }
}
