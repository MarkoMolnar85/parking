﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Parking_aplikacija_Marko_Molnar
{
    static class Program
    {
        public static string korisnickoIme;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new FormaGlavna());
            Application.Run(new frmLogIn());
        }
    }
}
