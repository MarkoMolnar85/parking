﻿using BusinessLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class PartialFrmInsertNadzornika : Form
    {
        public PartialFrmInsertNadzornika()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
        }

        private void PartialFrmInsertNadzornika_Load(object sender, EventArgs e)
        {
            cmbPrivilegija.Text = "Nivo 1";
        }

        private void btnDodaj_Click(object sender, EventArgs e)
        {
            DodajNadzornika();
        }

        private void DodajNadzornika()
        {
            if (PopunjenaSvaPolja() == false)
            {
                MessageBox.Show("Morate popuniti sva polja");
                return;
            }


            Nadzornik n = new Nadzornik();
            n.Ime = txtIme.Text;
            n.Korisnicko = txtKorisnicko.Text.ToUpper();
            n.Ulica = txtUlica.Text;
            n.Mesto = txtMesto.Text;
            n.Telefon = txtTelefon.Text;
            n.Sifra = txtSifra.Text.ToUpper();

            if (cmbPrivilegija.Text == "Nivo 1")
            {
                n.Privilegija = 1;
            }
            else if (cmbPrivilegija.Text == "Nivo 2")
            {
                n.Privilegija = 2;
            }
            else
            {
                n.Privilegija = 3;
            }

            NadzornikBusiness nb = new NadzornikBusiness();

            //upit pretrazuje broj imena prema predatom krisnickom imenu i izuzima predati id. Posto ni jedan slog nema id nula nista nece biti izuzeto
            int brojKorisnickihImena = nb.PostojiOvakvoKorisnickoImeNaOsnovuImenaIIDa(n.Korisnicko, 0);
            if (brojKorisnickihImena > 0)
            {
                MessageBox.Show("Postoji korisnik sa ovim korisničkim imenom");
            }
            else
            {
                int uspeh = nb.InsertNadzornika(n);

                if (uspeh <= 0)
                {
                    MessageBox.Show("Dodavanje nadzornika nije uspelo");
                }
                else
                {
                    frmNadzorniciInsertPrikazAzuriranjeBrisanje.uspehInsertaNadzornikaUPartialu = 1;

                    frmNadzorniciInsertPrikazAzuriranjeBrisanje.DodatiNadzornikUPartialu.NadzornikID = nb.GetZadnjiInsertovaniIdNadzornika();
                    frmNadzorniciInsertPrikazAzuriranjeBrisanje.DodatiNadzornikUPartialu.Korisnicko = n.Korisnicko;
                    frmNadzorniciInsertPrikazAzuriranjeBrisanje.DodatiNadzornikUPartialu.Ime = n.Ime;
                    frmNadzorniciInsertPrikazAzuriranjeBrisanje.DodatiNadzornikUPartialu.Ulica = n.Ulica;
                    frmNadzorniciInsertPrikazAzuriranjeBrisanje.DodatiNadzornikUPartialu.Mesto = n.Mesto;
                    frmNadzorniciInsertPrikazAzuriranjeBrisanje.DodatiNadzornikUPartialu.Telefon = n.Telefon;
                    frmNadzorniciInsertPrikazAzuriranjeBrisanje.DodatiNadzornikUPartialu.Sifra = n.Sifra;
                    frmNadzorniciInsertPrikazAzuriranjeBrisanje.DodatiNadzornikUPartialu.Privilegija = n.Privilegija;

                    this.Close();
                }
            }
        }

        private bool PopunjenaSvaPolja()
        {
            if (txtIme.Text == "" || string.IsNullOrEmpty(txtIme.Text) || txtKorisnicko.Text == "" || string.IsNullOrEmpty(txtKorisnicko.Text)
                || txtMesto.Text == "" || string.IsNullOrEmpty(txtMesto.Text) || txtSifra.Text == "" || string.IsNullOrEmpty(txtSifra.Text)
                || txtTelefon.Text == "" || string.IsNullOrEmpty(txtTelefon.Text) || txtUlica.Text == "" || string.IsNullOrEmpty(txtUlica.Text))
            {
                return false;
            }
            else
            {
                return true;
            }
        }


    }
}
