﻿using BusinessLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class PartialFrmUpdateOperatera : Form
    {
        int idOperatera = 0;
        public PartialFrmUpdateOperatera(int idOperatera)
        {
            InitializeComponent();
            this.idOperatera = idOperatera;
        }

        private void btnIzmeni_Click(object sender, EventArgs e)
        {
            UpdateOperatera();
        }
        private void UpdateOperatera()
        {
            if (PopunjenaSvaPolja() == true)
            {
                Operater o = new Operater();
                o.Korisnicko = txtKorisnicko.Text.ToUpper();
                o.Ime = txtIme.Text;
                o.Sifra = txtSifra.Text.ToUpper();
                o.Adresa = txtAdresa.Text;
                o.Mesto = txtMesto.Text;
                o.Telefon = txtTelefon.Text;
                o.OperaterID = idOperatera;

                OperaterBusiness ob = new OperaterBusiness();
                int brojIstihKorisnickihImena = ob.PostojiOvakvoKorisnickoImeNaOsnovuImenaIIDa(o.Korisnicko, idOperatera);
                if (brojIstihKorisnickihImena <= 0)
                {
                    int uspehUpdate = ob.UpdateOperatera(o);
                    if (uspehUpdate > 0)
                    {
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Izmena nije uspela");
                    }
                }
                else
                {
                    MessageBox.Show("Postoji ovakvo korisničko ime");
                }
            }
            else
            {
                MessageBox.Show("Sva polja moraju biti popunjena");
            }
        }
        private bool PopunjenaSvaPolja()
        {
            if (txtKorisnicko.Text == "" || string.IsNullOrEmpty(txtKorisnicko.Text)
                || txtIme.Text == "" || string.IsNullOrEmpty(txtIme.Text)
                || txtSifra.Text == "" || string.IsNullOrEmpty(txtSifra.Text)
                || txtAdresa.Text == "" || string.IsNullOrEmpty(txtAdresa.Text)
                || txtMesto.Text == "" || string.IsNullOrEmpty(txtMesto.Text)
                || txtTelefon.Text == "" || string.IsNullOrEmpty(txtTelefon.Text))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private void PartialFrmUpdateOperatera_Load(object sender, EventArgs e)
        {
            PopuniPoljaNaOsnovuIDa();
        }

        private void PopuniPoljaNaOsnovuIDa()
        {
            OperaterBusiness ob = new OperaterBusiness();
            Operater o = ob.GetOperaterPoIDu(idOperatera);
            txtKorisnicko.Text = o.Korisnicko;
            txtIme.Text = o.Ime;
            txtAdresa.Text = o.Adresa;
            txtMesto.Text = o.Mesto;
            txtTelefon.Text = o.Telefon;
            txtSifra.Text = o.Sifra;
        }
    }
}
