﻿using BusinessLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class PartialInsertOperatera : Form
    {
        public PartialInsertOperatera()
        {
            InitializeComponent();
        }

        private void PartialInsertOperatera_Load(object sender, EventArgs e)
        {

        }

        private bool PopunjenaSvaPolja()
        {
            if (txtKorisnicko.Text == "" || string.IsNullOrEmpty(txtKorisnicko.Text)
                || txtIme.Text == "" || string.IsNullOrEmpty(txtIme.Text)
                || txtSifra.Text == "" || string.IsNullOrEmpty(txtSifra.Text)
                || txtAdresa.Text == "" || string.IsNullOrEmpty(txtAdresa.Text)
                || txtMesto.Text == "" || string.IsNullOrEmpty(txtMesto.Text)
                || txtTelefon.Text == "" || string.IsNullOrEmpty(txtTelefon.Text))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private void btnUnos_Click(object sender, EventArgs e)
        {
            InsertOperatera();
        }

        private void InsertOperatera()
        {
            if (PopunjenaSvaPolja() == true)
            {
                OperaterSaNadzornikom o = new OperaterSaNadzornikom();
                o.Korisnicko = txtKorisnicko.Text.ToUpper();
                o.Ime = txtIme.Text;
                o.Sifra = txtSifra.Text.ToUpper();
                o.Adresa = txtAdresa.Text;
                o.Mesto = txtMesto.Text;
                o.Telefon = txtTelefon.Text;


                //da bi otvarao samo ovu formu podesavam na silu promenjivu ime korisnika
                //Program.korisnickoIme = "MOLNAR"; //ovu liniju koda izbrisati
                o.KorisnickoNadzornik = Program.korisnickoIme;

                OperaterBusiness ob = new OperaterBusiness();

                int brojIstihKorisnickihImena = ob.PostojiOvakvoKorisnickoImeNaOsnovuImenaIIDa(o.Korisnicko, 0);
                if (brojIstihKorisnickihImena <= 0)
                {
                    NadzornikBusiness nb = new NadzornikBusiness();
                    Nadzornik nad = nb.GetNadzornikNaOsnovuKorisnickogImena(o.KorisnickoNadzornik);
                    o.NadzornikID = nad.NadzornikID;

                    int uspeh = ob.InsertOperatera(o);
                    if (uspeh > 0)
                    {
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Upis nije uspeo");
                    }
                }
                else
                {
                    MessageBox.Show("Postoji ovakvo korisničko ime");
                }
            }
            else
            {
                MessageBox.Show("Moraju biti popunjena sva polja");
            }
        }
    }
}
