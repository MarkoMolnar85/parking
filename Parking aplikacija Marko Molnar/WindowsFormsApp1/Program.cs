﻿using Parking_aplikacija_Marko_Molnar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace WindowsFormsApp1
{
    static class Program
    {
        public static string korisnickoIme;
        public static string imeMasine;
        public static int idLokacijeIzKomboBoxaKodFormeZaNadzornika;
        
        //public static int smenaRadnika = 0;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new frmLogIn());
                //Application.Run(new frmAktivnostiNaStanici());
                //Application.Run(new frmIzaberiSmenu());
                //Application.Run(new frmAktivnostiNaStanici());
                //Application.Run(new frmZaNadzornika());
                //Application.Run(new frmNadzorniciInsertPrikazAzuriranjeBrisanje());
                //Application.Run(new partialFrmIzmenaNadzornika(1004));
                //Application.Run(new frmOperater());
                //Application.Run(new frmPretplatneKarteInsertUpdatePretragacs());
                //Application.Run(new frmUlazi());
                //Application.Run(new frmIzlazi());
                //Application.Run(new frmRacuni());
                //Application.Run(new frmPazari());
                //Application.Run(new frmLogovanja());
                //Application.Run(new frmLokacijeCene());
                //Application.Run(new frmSmene());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

    }
}
