﻿namespace WindowsFormsApp1
{
    partial class frmAktivnostiNaStanici
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControlUlazIlzaz = new System.Windows.Forms.TabControl();
            this.tabPageUlaz = new System.Windows.Forms.TabPage();
            this.tableLayoutPanelUlaz = new System.Windows.Forms.TableLayoutPanel();
            this.panelUnosPodatakaUlaz = new System.Windows.Forms.Panel();
            this.groupBoxUlaz = new System.Windows.Forms.GroupBox();
            this.lblBrojSlobodnihMestaBroj = new System.Windows.Forms.Label();
            this.lblBrojSlobodnihMestaNaslov = new System.Windows.Forms.Label();
            this.btnUnos = new System.Windows.Forms.Button();
            this.txtRegistracijaUlaz = new System.Windows.Forms.TextBox();
            this.panelPrikazUnosPodatakaUlaz = new System.Windows.Forms.Panel();
            this.dataGridViewUlaz = new System.Windows.Forms.DataGridView();
            this.tabPageIzlaz = new System.Windows.Forms.TabPage();
            this.tableLayoutPanelIzlaz = new System.Windows.Forms.TableLayoutPanel();
            this.panelIzlazZaUnos = new System.Windows.Forms.Panel();
            this.groupBoxIzlaz = new System.Windows.Forms.GroupBox();
            this.lblBrojSlobodnihMestaIzlaz = new System.Windows.Forms.Label();
            this.lblBrojSlobodnihMestaNaslovIzlas = new System.Windows.Forms.Label();
            this.btnUnosIzlaza = new System.Windows.Forms.Button();
            this.cmbUnosIzlaza = new System.Windows.Forms.ComboBox();
            this.panelIzlazZaGrid = new System.Windows.Forms.Panel();
            this.dataGridViewIzlaz = new System.Windows.Forms.DataGridView();
            this.tabControlUlazIlzaz.SuspendLayout();
            this.tabPageUlaz.SuspendLayout();
            this.tableLayoutPanelUlaz.SuspendLayout();
            this.panelUnosPodatakaUlaz.SuspendLayout();
            this.groupBoxUlaz.SuspendLayout();
            this.panelPrikazUnosPodatakaUlaz.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewUlaz)).BeginInit();
            this.tabPageIzlaz.SuspendLayout();
            this.tableLayoutPanelIzlaz.SuspendLayout();
            this.panelIzlazZaUnos.SuspendLayout();
            this.groupBoxIzlaz.SuspendLayout();
            this.panelIzlazZaGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewIzlaz)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControlUlazIlzaz
            // 
            this.tabControlUlazIlzaz.Controls.Add(this.tabPageUlaz);
            this.tabControlUlazIlzaz.Controls.Add(this.tabPageIzlaz);
            this.tabControlUlazIlzaz.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlUlazIlzaz.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControlUlazIlzaz.Location = new System.Drawing.Point(0, 0);
            this.tabControlUlazIlzaz.Name = "tabControlUlazIlzaz";
            this.tabControlUlazIlzaz.SelectedIndex = 0;
            this.tabControlUlazIlzaz.Size = new System.Drawing.Size(1350, 730);
            this.tabControlUlazIlzaz.TabIndex = 0;
            this.tabControlUlazIlzaz.SelectedIndexChanged += new System.EventHandler(this.tabControlUlazIlzaz_SelectedIndexChanged);
            // 
            // tabPageUlaz
            // 
            this.tabPageUlaz.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.tabPageUlaz.Controls.Add(this.tableLayoutPanelUlaz);
            this.tabPageUlaz.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPageUlaz.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.tabPageUlaz.Location = new System.Drawing.Point(4, 33);
            this.tabPageUlaz.Name = "tabPageUlaz";
            this.tabPageUlaz.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageUlaz.Size = new System.Drawing.Size(1342, 693);
            this.tabPageUlaz.TabIndex = 0;
            this.tabPageUlaz.Text = "Ulaz";
            // 
            // tableLayoutPanelUlaz
            // 
            this.tableLayoutPanelUlaz.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.tableLayoutPanelUlaz.ColumnCount = 1;
            this.tableLayoutPanelUlaz.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelUlaz.Controls.Add(this.panelUnosPodatakaUlaz, 0, 0);
            this.tableLayoutPanelUlaz.Controls.Add(this.panelPrikazUnosPodatakaUlaz, 0, 1);
            this.tableLayoutPanelUlaz.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelUlaz.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanelUlaz.Name = "tableLayoutPanelUlaz";
            this.tableLayoutPanelUlaz.RowCount = 2;
            this.tableLayoutPanelUlaz.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanelUlaz.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelUlaz.Size = new System.Drawing.Size(1336, 687);
            this.tableLayoutPanelUlaz.TabIndex = 0;
            // 
            // panelUnosPodatakaUlaz
            // 
            this.panelUnosPodatakaUlaz.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panelUnosPodatakaUlaz.Controls.Add(this.groupBoxUlaz);
            this.panelUnosPodatakaUlaz.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelUnosPodatakaUlaz.Location = new System.Drawing.Point(3, 3);
            this.panelUnosPodatakaUlaz.Name = "panelUnosPodatakaUlaz";
            this.panelUnosPodatakaUlaz.Size = new System.Drawing.Size(1330, 144);
            this.panelUnosPodatakaUlaz.TabIndex = 0;
            // 
            // groupBoxUlaz
            // 
            this.groupBoxUlaz.Controls.Add(this.lblBrojSlobodnihMestaBroj);
            this.groupBoxUlaz.Controls.Add(this.lblBrojSlobodnihMestaNaslov);
            this.groupBoxUlaz.Controls.Add(this.btnUnos);
            this.groupBoxUlaz.Controls.Add(this.txtRegistracijaUlaz);
            this.groupBoxUlaz.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxUlaz.Location = new System.Drawing.Point(164, 17);
            this.groupBoxUlaz.Name = "groupBoxUlaz";
            this.groupBoxUlaz.Size = new System.Drawing.Size(867, 108);
            this.groupBoxUlaz.TabIndex = 0;
            this.groupBoxUlaz.TabStop = false;
            this.groupBoxUlaz.Text = "Unos registracije vozila koje dolazi";
            // 
            // lblBrojSlobodnihMestaBroj
            // 
            this.lblBrojSlobodnihMestaBroj.AutoSize = true;
            this.lblBrojSlobodnihMestaBroj.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBrojSlobodnihMestaBroj.Location = new System.Drawing.Point(698, 56);
            this.lblBrojSlobodnihMestaBroj.Name = "lblBrojSlobodnihMestaBroj";
            this.lblBrojSlobodnihMestaBroj.Size = new System.Drawing.Size(20, 24);
            this.lblBrojSlobodnihMestaBroj.TabIndex = 3;
            this.lblBrojSlobodnihMestaBroj.Text = "0";
            // 
            // lblBrojSlobodnihMestaNaslov
            // 
            this.lblBrojSlobodnihMestaNaslov.AutoSize = true;
            this.lblBrojSlobodnihMestaNaslov.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBrojSlobodnihMestaNaslov.Location = new System.Drawing.Point(492, 56);
            this.lblBrojSlobodnihMestaNaslov.Name = "lblBrojSlobodnihMestaNaslov";
            this.lblBrojSlobodnihMestaNaslov.Size = new System.Drawing.Size(186, 24);
            this.lblBrojSlobodnihMestaNaslov.TabIndex = 2;
            this.lblBrojSlobodnihMestaNaslov.Text = "Broj slobodnih mesta";
            // 
            // btnUnos
            // 
            this.btnUnos.Location = new System.Drawing.Point(288, 53);
            this.btnUnos.Name = "btnUnos";
            this.btnUnos.Size = new System.Drawing.Size(102, 32);
            this.btnUnos.TabIndex = 1;
            this.btnUnos.Text = "Unos";
            this.btnUnos.UseVisualStyleBackColor = true;
            this.btnUnos.Click += new System.EventHandler(this.btnUnos_Click);
            // 
            // txtRegistracijaUlaz
            // 
            this.txtRegistracijaUlaz.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRegistracijaUlaz.Location = new System.Drawing.Point(20, 53);
            this.txtRegistracijaUlaz.Name = "txtRegistracijaUlaz";
            this.txtRegistracijaUlaz.Size = new System.Drawing.Size(234, 29);
            this.txtRegistracijaUlaz.TabIndex = 0;
            this.txtRegistracijaUlaz.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtRegistracijaUlaz_KeyDown);
            // 
            // panelPrikazUnosPodatakaUlaz
            // 
            this.panelPrikazUnosPodatakaUlaz.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panelPrikazUnosPodatakaUlaz.Controls.Add(this.dataGridViewUlaz);
            this.panelPrikazUnosPodatakaUlaz.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelPrikazUnosPodatakaUlaz.Location = new System.Drawing.Point(3, 153);
            this.panelPrikazUnosPodatakaUlaz.Name = "panelPrikazUnosPodatakaUlaz";
            this.panelPrikazUnosPodatakaUlaz.Size = new System.Drawing.Size(1330, 531);
            this.panelPrikazUnosPodatakaUlaz.TabIndex = 1;
            // 
            // dataGridViewUlaz
            // 
            this.dataGridViewUlaz.AllowUserToAddRows = false;
            this.dataGridViewUlaz.AllowUserToDeleteRows = false;
            this.dataGridViewUlaz.BackgroundColor = System.Drawing.SystemColors.ActiveCaption;
            this.dataGridViewUlaz.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewUlaz.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewUlaz.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewUlaz.Name = "dataGridViewUlaz";
            this.dataGridViewUlaz.ReadOnly = true;
            this.dataGridViewUlaz.Size = new System.Drawing.Size(1330, 531);
            this.dataGridViewUlaz.TabIndex = 0;
            this.dataGridViewUlaz.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewUlaz_CellClick);
            this.dataGridViewUlaz.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewUlaz_CellContentClick);
            // 
            // tabPageIzlaz
            // 
            this.tabPageIzlaz.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.tabPageIzlaz.Controls.Add(this.tableLayoutPanelIzlaz);
            this.tabPageIzlaz.Location = new System.Drawing.Point(4, 33);
            this.tabPageIzlaz.Name = "tabPageIzlaz";
            this.tabPageIzlaz.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageIzlaz.Size = new System.Drawing.Size(1342, 693);
            this.tabPageIzlaz.TabIndex = 1;
            this.tabPageIzlaz.Text = "Izlaz";
            // 
            // tableLayoutPanelIzlaz
            // 
            this.tableLayoutPanelIzlaz.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.tableLayoutPanelIzlaz.ColumnCount = 1;
            this.tableLayoutPanelIzlaz.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelIzlaz.Controls.Add(this.panelIzlazZaUnos, 0, 0);
            this.tableLayoutPanelIzlaz.Controls.Add(this.panelIzlazZaGrid, 0, 1);
            this.tableLayoutPanelIzlaz.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelIzlaz.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanelIzlaz.Name = "tableLayoutPanelIzlaz";
            this.tableLayoutPanelIzlaz.RowCount = 2;
            this.tableLayoutPanelIzlaz.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanelIzlaz.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelIzlaz.Size = new System.Drawing.Size(1336, 687);
            this.tableLayoutPanelIzlaz.TabIndex = 0;
            // 
            // panelIzlazZaUnos
            // 
            this.panelIzlazZaUnos.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panelIzlazZaUnos.Controls.Add(this.groupBoxIzlaz);
            this.panelIzlazZaUnos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelIzlazZaUnos.Location = new System.Drawing.Point(3, 3);
            this.panelIzlazZaUnos.Name = "panelIzlazZaUnos";
            this.panelIzlazZaUnos.Size = new System.Drawing.Size(1330, 144);
            this.panelIzlazZaUnos.TabIndex = 0;
            // 
            // groupBoxIzlaz
            // 
            this.groupBoxIzlaz.Controls.Add(this.lblBrojSlobodnihMestaIzlaz);
            this.groupBoxIzlaz.Controls.Add(this.lblBrojSlobodnihMestaNaslovIzlas);
            this.groupBoxIzlaz.Controls.Add(this.btnUnosIzlaza);
            this.groupBoxIzlaz.Controls.Add(this.cmbUnosIzlaza);
            this.groupBoxIzlaz.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxIzlaz.Location = new System.Drawing.Point(151, 23);
            this.groupBoxIzlaz.Name = "groupBoxIzlaz";
            this.groupBoxIzlaz.Size = new System.Drawing.Size(854, 100);
            this.groupBoxIzlaz.TabIndex = 0;
            this.groupBoxIzlaz.TabStop = false;
            this.groupBoxIzlaz.Text = "Unos registracije vozila koje odlazi";
            // 
            // lblBrojSlobodnihMestaIzlaz
            // 
            this.lblBrojSlobodnihMestaIzlaz.AutoSize = true;
            this.lblBrojSlobodnihMestaIzlaz.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBrojSlobodnihMestaIzlaz.Location = new System.Drawing.Point(708, 51);
            this.lblBrojSlobodnihMestaIzlaz.Name = "lblBrojSlobodnihMestaIzlaz";
            this.lblBrojSlobodnihMestaIzlaz.Size = new System.Drawing.Size(20, 24);
            this.lblBrojSlobodnihMestaIzlaz.TabIndex = 3;
            this.lblBrojSlobodnihMestaIzlaz.Text = "0";
            // 
            // lblBrojSlobodnihMestaNaslovIzlas
            // 
            this.lblBrojSlobodnihMestaNaslovIzlas.AutoSize = true;
            this.lblBrojSlobodnihMestaNaslovIzlas.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBrojSlobodnihMestaNaslovIzlas.Location = new System.Drawing.Point(498, 51);
            this.lblBrojSlobodnihMestaNaslovIzlas.Name = "lblBrojSlobodnihMestaNaslovIzlas";
            this.lblBrojSlobodnihMestaNaslovIzlas.Size = new System.Drawing.Size(186, 24);
            this.lblBrojSlobodnihMestaNaslovIzlas.TabIndex = 2;
            this.lblBrojSlobodnihMestaNaslovIzlas.Text = "Broj slobodnih mesta";
            // 
            // btnUnosIzlaza
            // 
            this.btnUnosIzlaza.Location = new System.Drawing.Point(284, 43);
            this.btnUnosIzlaza.Name = "btnUnosIzlaza";
            this.btnUnosIzlaza.Size = new System.Drawing.Size(95, 32);
            this.btnUnosIzlaza.TabIndex = 1;
            this.btnUnosIzlaza.Text = "Unos";
            this.btnUnosIzlaza.UseVisualStyleBackColor = true;
            this.btnUnosIzlaza.Click += new System.EventHandler(this.btnUnosIzlaza_Click);
            // 
            // cmbUnosIzlaza
            // 
            this.cmbUnosIzlaza.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbUnosIzlaza.FormattingEnabled = true;
            this.cmbUnosIzlaza.Location = new System.Drawing.Point(21, 43);
            this.cmbUnosIzlaza.Name = "cmbUnosIzlaza";
            this.cmbUnosIzlaza.Size = new System.Drawing.Size(216, 32);
            this.cmbUnosIzlaza.TabIndex = 0;
            this.cmbUnosIzlaza.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbUnosIzlaza_KeyDown);
            this.cmbUnosIzlaza.KeyUp += new System.Windows.Forms.KeyEventHandler(this.cmbUnosIzlaza_KeyUp);
            // 
            // panelIzlazZaGrid
            // 
            this.panelIzlazZaGrid.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panelIzlazZaGrid.Controls.Add(this.dataGridViewIzlaz);
            this.panelIzlazZaGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelIzlazZaGrid.Location = new System.Drawing.Point(3, 153);
            this.panelIzlazZaGrid.Name = "panelIzlazZaGrid";
            this.panelIzlazZaGrid.Size = new System.Drawing.Size(1330, 531);
            this.panelIzlazZaGrid.TabIndex = 1;
            // 
            // dataGridViewIzlaz
            // 
            this.dataGridViewIzlaz.AllowUserToAddRows = false;
            this.dataGridViewIzlaz.AllowUserToDeleteRows = false;
            this.dataGridViewIzlaz.BackgroundColor = System.Drawing.SystemColors.ActiveCaption;
            this.dataGridViewIzlaz.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewIzlaz.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewIzlaz.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewIzlaz.Name = "dataGridViewIzlaz";
            this.dataGridViewIzlaz.ReadOnly = true;
            this.dataGridViewIzlaz.Size = new System.Drawing.Size(1330, 531);
            this.dataGridViewIzlaz.TabIndex = 0;
            this.dataGridViewIzlaz.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewIzlaz_CellClick);
            // 
            // frmAktivnostiNaStanici
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1350, 730);
            this.Controls.Add(this.tabControlUlazIlzaz);
            this.Name = "frmAktivnostiNaStanici";
            this.Text = "Stanica";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmAktivnostiNaStanici_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.SizeChanged += new System.EventHandler(this.frmAktivnostiNaStanici_SizeChanged);
            this.tabControlUlazIlzaz.ResumeLayout(false);
            this.tabPageUlaz.ResumeLayout(false);
            this.tableLayoutPanelUlaz.ResumeLayout(false);
            this.panelUnosPodatakaUlaz.ResumeLayout(false);
            this.groupBoxUlaz.ResumeLayout(false);
            this.groupBoxUlaz.PerformLayout();
            this.panelPrikazUnosPodatakaUlaz.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewUlaz)).EndInit();
            this.tabPageIzlaz.ResumeLayout(false);
            this.tableLayoutPanelIzlaz.ResumeLayout(false);
            this.panelIzlazZaUnos.ResumeLayout(false);
            this.groupBoxIzlaz.ResumeLayout(false);
            this.groupBoxIzlaz.PerformLayout();
            this.panelIzlazZaGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewIzlaz)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControlUlazIlzaz;
        private System.Windows.Forms.TabPage tabPageIzlaz;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelUlaz;
        private System.Windows.Forms.Panel panelUnosPodatakaUlaz;
        private System.Windows.Forms.Panel panelPrikazUnosPodatakaUlaz;
        private System.Windows.Forms.DataGridView dataGridViewUlaz;
        private System.Windows.Forms.GroupBox groupBoxUlaz;
        private System.Windows.Forms.Button btnUnos;
        private System.Windows.Forms.TextBox txtRegistracijaUlaz;
        private System.Windows.Forms.Label lblBrojSlobodnihMestaBroj;
        private System.Windows.Forms.Label lblBrojSlobodnihMestaNaslov;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelIzlaz;
        private System.Windows.Forms.Panel panelIzlazZaUnos;
        private System.Windows.Forms.Panel panelIzlazZaGrid;
        private System.Windows.Forms.GroupBox groupBoxIzlaz;
        private System.Windows.Forms.Label lblBrojSlobodnihMestaIzlaz;
        private System.Windows.Forms.Label lblBrojSlobodnihMestaNaslovIzlas;
        private System.Windows.Forms.Button btnUnosIzlaza;
        private System.Windows.Forms.ComboBox cmbUnosIzlaza;
        private System.Windows.Forms.DataGridView dataGridViewIzlaz;
        private System.Windows.Forms.TabPage tabPageUlaz;
    }
}

