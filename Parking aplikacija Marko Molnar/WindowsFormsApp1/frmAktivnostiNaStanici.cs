﻿using BusinessLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class frmAktivnostiNaStanici : Form
    {
        public frmAktivnostiNaStanici()
        {
            InitializeComponent();
            this.WindowState = FormWindowState.Maximized;
        }

        DataTable dtUlaz = new DataTable();
        DataTable dtIzlaz = new DataTable();

        private void Form1_Load(object sender, EventArgs e)
        {
            //MOLNAR-PC
            //defaultne vrednosti ako ne koristim login formu
            if (Program.imeMasine == "" || Program.imeMasine == null)
            {
                Program.imeMasine = "MOLNAR-PC";
            }
            if (Program.korisnickoIme == "" || Program.korisnickoIme == null)
            {
                Program.korisnickoIme = "Maks";
            }
            //if (Program.smenaRadnika == 0)
            //{
            //    Program.smenaRadnika = 1;
            //}

            dataGridViewUlaz.AllowUserToAddRows = false;
            dataGridViewIzlaz.AllowUserToAddRows = false;

            LoadUlazi();
        }

        private void tabControlUlazIlzaz_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabControlUlazIlzaz.SelectedTab == tabControlUlazIlzaz.TabPages["tabPageUlaz"])
            {
                //MessageBox.Show("ulaz");
            }
            else if (tabControlUlazIlzaz.SelectedTab == tabControlUlazIlzaz.TabPages["tabPageIzlaz"])
            {
                //MessageBox.Show("izlaz");
                LoadIzlaza();
                SakrivanjeKolonaNaIzlazu();
                ResajzovanjeKolonaIzlaz();
            }
        }

        private void btnUnos_Click(object sender, EventArgs e)
        {
            UnosUlaza();
        }

        private void PromenaImenaKolona()
        {
            dataGridViewUlaz.Columns["ulazID"].HeaderText = "Ulaz id";
            dataGridViewUlaz.Columns["Tablica"].HeaderText = "Tablica";
            dataGridViewUlaz.Columns["Vreme"].HeaderText = "Vreme";
            dataGridViewUlaz.Columns["NaParkingu"].HeaderText = "Na parkingu";
            dataGridViewUlaz.Columns["TipKarte"].HeaderText = "Tip karte";
            dataGridViewUlaz.Columns["OperaterKorisnicko"].HeaderText = "Korisničko ime operatera";
        }
        private void SakrivanjeKolona()
        {
            //this.dataGridViewUlaz.Columns[""]
            dataGridViewUlaz.Columns["ulazID"].Visible = false;
        }
        private void SakrivanjeKolonaNaIzlazu()
        {
            //this.dataGridViewUlaz.Columns[""]
            this.dataGridViewIzlaz.Columns["Ulaz ID"].Visible = false;
        }
        private void ResajzovanjeKolona()
        {
            //MessageBox.Show(dataGridViewUlaz.Width.ToString());
            //MessageBox.Show(this.Width.ToString());
            if (!(this.dataGridViewUlaz.DataSource == null))
            {
                int sirinaGrida = dataGridViewUlaz.Width - this.dataGridViewUlaz.RowHeadersWidth;

                this.dataGridViewUlaz.Columns["Tablica"].Width = sirinaGrida / 5;
                this.dataGridViewUlaz.Columns["Vreme"].Width = sirinaGrida / 5;
                this.dataGridViewUlaz.Columns["NaParkingu"].Width = sirinaGrida / 5;
                this.dataGridViewUlaz.Columns["TipKarte"].Width = sirinaGrida / 5;
                this.dataGridViewUlaz.Columns["OperaterKorisnicko"].Width = sirinaGrida / 5;
            }
        }
        private void ResajzovanjeKolonaIzlaz()
        {
            if (!(this.dataGridViewIzlaz.DataSource == null))
            {
                int sirinaGrida = this.dataGridViewIzlaz.Width - this.dataGridViewIzlaz.RowHeadersWidth;
                this.dataGridViewIzlaz.Columns["Tablica"].Width = sirinaGrida / 4;
                this.dataGridViewIzlaz.Columns["Vreme ulaza"].Width = sirinaGrida / 4;
                this.dataGridViewIzlaz.Columns["Tip karte"].Width = sirinaGrida / 4;
                this.dataGridViewIzlaz.Columns["Korisničko ime operatera"].Width = sirinaGrida / 4;
            }
        }
        private void PromenaBojeSemafora()
        {
            if (int.Parse(lblBrojSlobodnihMestaBroj.Text) > 0)
            {
                lblBrojSlobodnihMestaBroj.ForeColor = Color.Green;
                lblBrojSlobodnihMestaNaslov.ForeColor = Color.Green;

                lblBrojSlobodnihMestaNaslovIzlas.ForeColor = Color.Green;
                lblBrojSlobodnihMestaIzlaz.ForeColor = Color.Green;
            }
            else
            {
                lblBrojSlobodnihMestaBroj.ForeColor = Color.Red;
                lblBrojSlobodnihMestaNaslov.ForeColor = Color.Red;

                lblBrojSlobodnihMestaNaslovIzlas.ForeColor = Color.Red;
                lblBrojSlobodnihMestaIzlaz.ForeColor = Color.Red;
            }
        }

        private void LoadUlazi()
        {
            //MessageBox.Show("korisnicko " + Program.korisnickoIme + " masina " + Program.imeMasine + " smena " + Program.smenaRadnika.ToString());
            string imeMasine = Program.imeMasine;

            LokacijaBusiness lb = new LokacijaBusiness();
            Lokacija vracenaLokacija = lb.GetLokacijaPremaImenuMasine(imeMasine);
            //MessageBox.Show("Naziv lokacije " + vracenaLokacija.Naziv + " id lokacije " + vracenaLokacija.ID);
            //MessageBox.Show("slobodna mesta " + lb.BrojSlobodnihMestaNaParkinguNaOsnovuIdLokacije(vracenaLokacija.ID).ToString());

            UlazBusiness ulazBusiness = new UlazBusiness();

            //dataGridViewUlaz.DataSource = ulazBusiness.GetAllUlaziLista();

            int idLokacijeZaPretraguUlaza = lb.IdLokacijePremaImenuMasine(Program.imeMasine);
            dtUlaz = ulazBusiness.GetUlaziPremaIDuLokacijeDT(idLokacijeZaPretraguUlaza);
            dataGridViewUlaz.DataSource = dtUlaz;

            lblBrojSlobodnihMestaBroj.Text = lb.BrojSlobodnihMestaNaParkinguNaOsnovuIdLokacije(vracenaLokacija.ID).ToString();
            PromenaBojeSemafora();

            PromenaImenaKolona();
            SakrivanjeKolona();
            ResajzovanjeKolona();
        }

        private object ProveraIspravnostiUpisaneTablice(string str)
        {
            var objekat = new { textZaProveru = "", uspesno = false };
            //MessageBox.Show("text " + objekat.textZaProveru + " uspesnost " + objekat.uspesno.ToString());
            Regex reg = new Regex("^[a-zA-ZŽĐŠĆČžđšćč]{2}-{1}[0-9]{3}-{1}[a-zA-ZŽĐŠĆČžđšćč]{2}$");
            Regex regBezCrtica = new Regex("^[a-zA-ZŽĐŠĆČžđšćč]{2}[0-9]{3}[a-zA-ZŽĐŠĆČžđšćč]{2}$");
            if (reg.IsMatch(str))
            {
                //objekat.textZaProveru = "da";
                objekat = new { textZaProveru = str, uspesno = true };
            }
            else if (regBezCrtica.IsMatch(str))
            {
                string dodavanjeCrtica = "";
                dodavanjeCrtica = "" + str[0] + str[1] + "-" + str[2] + str[3] + str[4] + "-" + str[5] + str[6];
                objekat = new { textZaProveru = dodavanjeCrtica, uspesno = true };
            }
            else
            {
                objekat = new { textZaProveru = str, uspesno = false };
            }

            return objekat;
        }

        private void txtRegistracijaUlaz_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 13)
            {
                UnosUlaza();
            }
        }

        private void UnosUlaza()
        {
            if (txtRegistracijaUlaz.Text == "" || String.IsNullOrEmpty(txtRegistracijaUlaz.Text) || String.IsNullOrWhiteSpace(txtRegistracijaUlaz.Text))
            {
                MessageBox.Show("Potrebno je uneti registraciju");
            }
            else
            {
                UlazBusiness ub = new UlazBusiness();
                Ulaz ulaz = new Ulaz();

                ulaz.Tablica = txtRegistracijaUlaz.Text.ToUpper();

                dynamic objekatIzProvereIspravnostiTablice = ProveraIspravnostiUpisaneTablice(ulaz.Tablica);
                if (objekatIzProvereIspravnostiTablice.uspesno == false)
                {
                    DialogResult dialog = MessageBox.Show("Uneta registracija ne odgovara formatu registracije republike Srbije, ako je u pitanju strana tablica potvrdi", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dialog == DialogResult.No)
                    {
                        return;
                    }
                    else
                    {
                        ulaz.Tablica = objekatIzProvereIspravnostiTablice.textZaProveru;
                    }
                }
                else
                {
                    ulaz.Tablica = objekatIzProvereIspravnostiTablice.textZaProveru;
                }

                ulaz.Vreme = DateTime.Now;
                ulaz.NaParkingu = 1;

                PretplatneKarteBusiness pkb = new PretplatneKarteBusiness();
                bool daLijePretplatna = pkb.DaLiJePretplatnaKartaVazeca(ulaz.Tablica);

                if (daLijePretplatna == true)
                {
                    ulaz.TipKarte = "Pretplatna";
                }
                else
                {
                    ulaz.TipKarte = "Obična";
                }

                OperaterBusiness ob = new OperaterBusiness();
                Operater operater = ob.GetOperaterPoKorisnickomImenu(Program.korisnickoIme);

                ulaz.OperatorID = operater.OperaterID;

                SmenaBusiness sb = new SmenaBusiness();
                Smena smena = sb.GetSmenaNaOsnovuTrenutnogVremena();
                ulaz.SmenaID = smena.ID;
                //ulaz.SmenaID = Program.smenaRadnika;

                LokacijaBusiness lb = new LokacijaBusiness();
                int idLokacijeZaPretraguUlaza = lb.IdLokacijePremaImenuMasine(Program.imeMasine);
                ulaz.LokacijaID = idLokacijeZaPretraguUlaza;

                //ovde staviti proveru da li je auto vec na parkingu, ako jeste izlazi iz funkcije i sprecava unos
                bool postojiTajAutoNaParkingu = ub.DaLiPostojiAutoSaOvomRegistracijomNaOvomParkinguNaOsnovuIdLokacijeITablice(ulaz.Tablica);
                if (postojiTajAutoNaParkingu == true)
                {
                    MessageBox.Show("Vozilo sa ovim registarskim brojem je na parkingu");
                    return;
                }

                //broj auta na parkingu je broj slobodnih mesta na parkingu
                int brojAutaNaParkingu = lb.BrojSlobodnihMestaNaParkinguNaOsnovuIdLokacije(ulaz.LokacijaID);

                if (brojAutaNaParkingu <= 0)
                {
                    MessageBox.Show("Parking je trenutno pun");
                    return;
                }

                //ovde staviti proveru da li ima mesto na parkingu

                int uspesnost = ub.InsertUlaza(ulaz);

                int zadnjiID = ub.GetZadnjiInsertovaniID();

                if (uspesnost == 0)
                {
                    MessageBox.Show("Podatak nije upisan");
                }
                else if (uspesnost > 0)
                {
                    DataRow noviRedPriInsertu = dtUlaz.NewRow();
                    noviRedPriInsertu["UlazID"] = zadnjiID;
                    noviRedPriInsertu["Tablica"] = ulaz.Tablica;
                    noviRedPriInsertu["Vreme"] = ulaz.Vreme;

                    string prisutan = "";
                    if (ulaz.NaParkingu == 1) { prisutan = "Prisutan"; } else { prisutan = "Nije prisutan"; }
                    noviRedPriInsertu["NaParkingu"] = prisutan;
                    noviRedPriInsertu["TipKarte"] = ulaz.TipKarte;
                    noviRedPriInsertu["OperaterKorisnicko"] = Program.korisnickoIme;

                    dtUlaz.Rows.InsertAt(noviRedPriInsertu, 0);
                }
            }

            LokacijaBusiness lb1 = new LokacijaBusiness();
            int idLokacijeZaPretraguUlaza1 = lb1.IdLokacijePremaImenuMasine(Program.imeMasine);

            lblBrojSlobodnihMestaBroj.Text = lb1.BrojSlobodnihMestaNaParkinguNaOsnovuIdLokacije(idLokacijeZaPretraguUlaza1).ToString();
            PromenaBojeSemafora();
            txtRegistracijaUlaz.Text = "";
        }

        private void frmAktivnostiNaStanici_FormClosing(object sender, FormClosingEventArgs e)
        {
            OperaterBusiness operaterBusiness = new OperaterBusiness();
            Operater operater = operaterBusiness.GetOperaterPoKorisnickomImenu(Program.korisnickoIme);

            OperaterLogInBusiness opLogBusiness = new OperaterLogInBusiness();
            opLogBusiness.UpdateOperaterLogVremeZaLogOut(operater.OperaterID);
        }

        private void LoadIzlaza()
        {
            LokacijaBusiness lb = new LokacijaBusiness();
            Lokacija lokacija = lb.GetLokacijaPremaImenuMasine(Program.imeMasine);
            int lokacijaID = lokacija.ID;

            UlazBusiness ub = new UlazBusiness();
            dataGridViewIzlaz.DataSource = null;
            dtIzlaz = ub.GetAllAutiKojiSeNalazeNaParkinguPremaIdLokacijeVracaDT(lokacijaID);
            dataGridViewIzlaz.DataSource = dtIzlaz;

            PopunjavanjeKomboBoxaNaIzlazuPrilikomLoada(lokacijaID);
            
            lblBrojSlobodnihMestaIzlaz.Text = lb.BrojSlobodnihMestaNaParkinguNaOsnovuIdLokacije(lokacijaID).ToString();
            PromenaBojeSemafora();
            //popuniti broj slobodnih mesta i boju
        }
        private void PopunjavanjeKomboBoxaNaIzlazuPrilikomLoada(int idLokacije)
        {
            cmbUnosIzlaza.Text = "";

            cmbUnosIzlaza.DataSource = dtIzlaz;
            cmbUnosIzlaza.DisplayMember = "Tablica";
            cmbUnosIzlaza.ValueMember = "Ulaz ID";

            cmbUnosIzlaza.Text = "";
        }

        private void cmbUnosIzlaza_KeyUp(object sender, KeyEventArgs e)
        {
            LokacijaBusiness lb = new LokacijaBusiness();
            Lokacija lokacija = lb.GetLokacijaPremaImenuMasine(Program.imeMasine);
            int lokacijaID = lokacija.ID;

            UlazBusiness ub = new UlazBusiness();
            dtIzlaz = ub.GetAutiKojiSuNaParkinguPremaIdLokacijiATablicaPocinjeStringom(lokacijaID, cmbUnosIzlaza.Text.ToUpper());

            string trenutnaVrednost = cmbUnosIzlaza.Text;

            cmbUnosIzlaza.DataSource = dtIzlaz;
            cmbUnosIzlaza.DisplayMember = "Tablica";
            cmbUnosIzlaza.ValueMember = "Ulaz ID";

            this.dataGridViewIzlaz.DataSource = dtIzlaz;

            cmbUnosIzlaza.Text = trenutnaVrednost;
            cmbUnosIzlaza.Focus();
            cmbUnosIzlaza.SelectionStart = cmbUnosIzlaza.Text.Length;

            ResajzovanjeKolonaIzlaz();
        }

        private void btnUnosIzlaza_Click(object sender, EventArgs e)
        {
            UnosIzlaza();
            cmbUnosIzlaza.Text = "";
        }

        private void UnosIzlaza()
        {
            LokacijaBusiness lb = new LokacijaBusiness();
            Lokacija lokacija = lb.GetLokacijaPremaImenuMasine(Program.imeMasine);
            int lokacijaID = lokacija.ID;

            UlazBusiness ub = new UlazBusiness();
            string registracija = cmbUnosIzlaza.Text;

            if (registracija == "" || String.IsNullOrEmpty(registracija))
            {
                MessageBox.Show("Registarski broj mora biti unet");
            }
            else
            {
                bool imaOvaRegistracijaNaParkingu = ub.DaLiJeAutomobilSaPredatomTablicomIIdeomLokacijeNaParkingu(lokacijaID, registracija);
                if (imaOvaRegistracijaNaParkingu)
                {
                    //brisanje reda na gridu u izlazu
                    //menjanje reda u ulazu na nije prisutan
                    lokacija = lb.GetLokacijaPremaImenuMasine(Program.imeMasine);

                    OperaterBusiness ob = new OperaterBusiness();
                    Operater operater = ob.GetOperaterPoKorisnickomImenu(Program.korisnickoIme);

                    RacunBusiness rb = new RacunBusiness();
                    int uspeh = rb.InsertRacunaIzlazaIzmenaUlaza(lokacija.ID, registracija, operater.OperaterID);
                    if (uspeh > 0)
                    {
                        //MessageBox.Show("Uspesan unos");
                        LokacijaBusiness lb1 = new LokacijaBusiness();
                        int idLokacijeZaPretraguUlaza1 = lb1.IdLokacijePremaImenuMasine(Program.imeMasine);
                        lblBrojSlobodnihMestaBroj.Text = lb1.BrojSlobodnihMestaNaParkinguNaOsnovuIdLokacije(idLokacijeZaPretraguUlaza1).ToString();
                        lblBrojSlobodnihMestaIzlaz.Text = lb1.BrojSlobodnihMestaNaParkinguNaOsnovuIdLokacije(idLokacijeZaPretraguUlaza1).ToString();
                        PromenaBojeSemafora();

                        cmbUnosIzlaza.Text = "";

                        ObrisiRedNaIzlazuIzmeniRedNaUlazuPosleUnosaIzlaza(registracija);
                    }
                    else
                    {
                        MessageBox.Show("Neuspesan unos");
                    }
                }
                else
                {
                    MessageBox.Show("Ne postoji vozilo sa ovom registracijom na parkingu");
                }
            }
        }
        private void ObrisiRedNaIzlazuIzmeniRedNaUlazuPosleUnosaIzlaza(string registracija)
        {
            //foreach (DataRow red in dtIzlaz.Rows)
            //{
            //    if (red["Tablica"].ToString() == registracija)
            //    {
            //        red.Delete();
            //    }
            //}
            //dtIzlaz.AcceptChanges();

            for (int i = 0; i < dtIzlaz.Rows.Count; i++)
            {
                DataRow red = dtIzlaz.Rows[i];
                if (red["Tablica"].ToString() == registracija)
                {
                    red.Delete();
                }
            }
            dtIzlaz.AcceptChanges();

            //foreach (DataRow red in dtUlaz.Rows)
            //{
            //    if (red["Tablica"].ToString() == registracija && red["NaParkingu"].ToString() == "Prisutan")
            //    {
            //        red["NaParkingu"] = "Nije prisutan";
            //    }
            //}
            //dtUlaz.AcceptChanges();

            for (int i = 0; i < dtUlaz.Rows.Count; i++)
            {
                DataRow red = dtUlaz.Rows[i];
                if (red["Tablica"].ToString() == registracija && red["NaParkingu"].ToString() == "Prisutan")
                {
                    red["NaParkingu"] = "Nije prisutan";
                }
            }
            dtUlaz.AcceptChanges();

        }

        private void cmbUnosIzlaza_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 13)
            {
                UnosIzlaza();
                cmbUnosIzlaza.Text = "";
            }
        }

        private void dataGridViewUlaz_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void dataGridViewIzlaz_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0)
            {
                return;
            }
            else
            {
                int index = e.RowIndex;
                dataGridViewIzlaz.Rows[index].Selected = true;
            }
        }

        private void dataGridViewUlaz_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0)
            {
                return;
            }
            else
            {
                int index = e.RowIndex;
                dataGridViewUlaz.Rows[index].Selected = true;
            }
        }

        private void frmAktivnostiNaStanici_SizeChanged(object sender, EventArgs e)
        {
            ResajzovanjeKolona();
            ResajzovanjeKolonaIzlaz();
        }
    }
}
