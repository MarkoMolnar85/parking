﻿namespace WindowsFormsApp1
{
    partial class frmIzaberiSmenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.rbPrvaSmena = new System.Windows.Forms.RadioButton();
            this.rbDrugaSmena = new System.Windows.Forms.RadioButton();
            this.btnPotvrdiSmenu = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(59, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(109, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Izaberi smenu";
            // 
            // rbPrvaSmena
            // 
            this.rbPrvaSmena.AutoSize = true;
            this.rbPrvaSmena.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbPrvaSmena.Location = new System.Drawing.Point(62, 79);
            this.rbPrvaSmena.Name = "rbPrvaSmena";
            this.rbPrvaSmena.Size = new System.Drawing.Size(110, 24);
            this.rbPrvaSmena.TabIndex = 1;
            this.rbPrvaSmena.TabStop = true;
            this.rbPrvaSmena.Text = "Prva smena";
            this.rbPrvaSmena.UseVisualStyleBackColor = true;
            this.rbPrvaSmena.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rbPrvaSmena_KeyDown);
            // 
            // rbDrugaSmena
            // 
            this.rbDrugaSmena.AutoSize = true;
            this.rbDrugaSmena.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbDrugaSmena.Location = new System.Drawing.Point(176, 79);
            this.rbDrugaSmena.Name = "rbDrugaSmena";
            this.rbDrugaSmena.Size = new System.Drawing.Size(123, 24);
            this.rbDrugaSmena.TabIndex = 2;
            this.rbDrugaSmena.TabStop = true;
            this.rbDrugaSmena.Text = "Druga smena";
            this.rbDrugaSmena.UseVisualStyleBackColor = true;
            this.rbDrugaSmena.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rbDrugaSmena_KeyDown);
            // 
            // btnPotvrdiSmenu
            // 
            this.btnPotvrdiSmenu.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPotvrdiSmenu.Location = new System.Drawing.Point(176, 126);
            this.btnPotvrdiSmenu.Name = "btnPotvrdiSmenu";
            this.btnPotvrdiSmenu.Size = new System.Drawing.Size(123, 29);
            this.btnPotvrdiSmenu.TabIndex = 3;
            this.btnPotvrdiSmenu.Text = "Potvrdi smenu";
            this.btnPotvrdiSmenu.UseVisualStyleBackColor = true;
            this.btnPotvrdiSmenu.Click += new System.EventHandler(this.btnPotvrdiSmenu_Click);
            // 
            // frmIzaberiSmenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(381, 201);
            this.Controls.Add(this.btnPotvrdiSmenu);
            this.Controls.Add(this.rbDrugaSmena);
            this.Controls.Add(this.rbPrvaSmena);
            this.Controls.Add(this.label1);
            this.Name = "frmIzaberiSmenu";
            this.Text = "frmIzaberiSmenu";
            this.Load += new System.EventHandler(this.frmIzaberiSmenu_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton rbPrvaSmena;
        private System.Windows.Forms.RadioButton rbDrugaSmena;
        private System.Windows.Forms.Button btnPotvrdiSmenu;
    }
}