﻿using BusinessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class frmIzaberiSmenu : Form
    {
        public frmIzaberiSmenu()
        {
            InitializeComponent();
        }

        private void btnPotvrdiSmenu_Click(object sender, EventArgs e)
        {
            PotvrdiSmenu();
        }

        private void frmIzaberiSmenu_Load(object sender, EventArgs e)
        {
            this.CenterToScreen();
        }

        private void rbPrvaSmena_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 13)
            {
                PotvrdiSmenu();
            }
        }

        private void rbDrugaSmena_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 13)
            {
                PotvrdiSmenu();
            }
        }

        private void PotvrdiSmenu()
        {
            //if (rbPrvaSmena.Checked == true)
            //{
            //    DialogResult dr = MessageBox.Show("Izabrao si prvu smenu", "Siguran?", MessageBoxButtons.YesNo);
            //    if (dr == DialogResult.No)
            //    {
            //        return;
            //    }
            //    else if (dr == DialogResult.Yes)
            //    {
            //        SmenaBusiness smenaBusiness = new SmenaBusiness();
            //        //Upisuje se u globalnu staticku promenjivu ID smene
            //        Program.smenaRadnika = smenaBusiness.IdSmeneNaOsnovuNaziva("Prva");
            //        frmAktivnostiNaStanici aktivnostiNaStanici = new frmAktivnostiNaStanici();
            //        aktivnostiNaStanici.ShowDialog();
            //        this.Close();
            //    }
            //}
            //else if (rbDrugaSmena.Checked == true)
            //{
            //    DialogResult dr = MessageBox.Show("Izabrao si drugu smenu", "Siguran?", MessageBoxButtons.YesNo);
            //    if (dr == DialogResult.No)
            //    {
            //        return;
            //    }
            //    else if (dr == DialogResult.Yes)
            //    {
            //        SmenaBusiness smenaBusiness = new SmenaBusiness();
            //        //Upisuje se u globalnu staticku promenjivu ID smene
            //        Program.smenaRadnika = smenaBusiness.IdSmeneNaOsnovuNaziva("Druga");
            //        frmAktivnostiNaStanici aktivnostiNaStanici = new frmAktivnostiNaStanici();
            //        aktivnostiNaStanici.ShowDialog();
            //        this.Close();
            //    }

            //}
        }
        
    }
}
