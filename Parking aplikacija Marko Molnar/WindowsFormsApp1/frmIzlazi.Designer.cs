﻿namespace WindowsFormsApp1
{
    partial class frmIzlazi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tableLayoutPanelGlavni = new System.Windows.Forms.TableLayoutPanel();
            this.panelFilteri = new System.Windows.Forms.Panel();
            this.btnPretraga = new System.Windows.Forms.Button();
            this.cmbSmena = new System.Windows.Forms.ComboBox();
            this.cmbOperater = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.dateTimePickerDO = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerOD = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbTipKarte = new System.Windows.Forms.ComboBox();
            this.txtTablica = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panelZaGrid = new System.Windows.Forms.Panel();
            this.dataGridViewIzlazi = new System.Windows.Forms.DataGridView();
            this.tableLayoutPanelGlavni.SuspendLayout();
            this.panelFilteri.SuspendLayout();
            this.panelZaGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewIzlazi)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanelGlavni
            // 
            this.tableLayoutPanelGlavni.ColumnCount = 1;
            this.tableLayoutPanelGlavni.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelGlavni.Controls.Add(this.panelFilteri, 0, 0);
            this.tableLayoutPanelGlavni.Controls.Add(this.panelZaGrid, 0, 1);
            this.tableLayoutPanelGlavni.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelGlavni.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanelGlavni.Name = "tableLayoutPanelGlavni";
            this.tableLayoutPanelGlavni.RowCount = 2;
            this.tableLayoutPanelGlavni.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanelGlavni.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelGlavni.Size = new System.Drawing.Size(1276, 694);
            this.tableLayoutPanelGlavni.TabIndex = 0;
            // 
            // panelFilteri
            // 
            this.panelFilteri.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panelFilteri.Controls.Add(this.btnPretraga);
            this.panelFilteri.Controls.Add(this.cmbSmena);
            this.panelFilteri.Controls.Add(this.cmbOperater);
            this.panelFilteri.Controls.Add(this.label6);
            this.panelFilteri.Controls.Add(this.label5);
            this.panelFilteri.Controls.Add(this.dateTimePickerDO);
            this.panelFilteri.Controls.Add(this.dateTimePickerOD);
            this.panelFilteri.Controls.Add(this.label3);
            this.panelFilteri.Controls.Add(this.label4);
            this.panelFilteri.Controls.Add(this.cmbTipKarte);
            this.panelFilteri.Controls.Add(this.txtTablica);
            this.panelFilteri.Controls.Add(this.label2);
            this.panelFilteri.Controls.Add(this.label1);
            this.panelFilteri.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelFilteri.Location = new System.Drawing.Point(3, 3);
            this.panelFilteri.Name = "panelFilteri";
            this.panelFilteri.Size = new System.Drawing.Size(1270, 144);
            this.panelFilteri.TabIndex = 0;
            // 
            // btnPretraga
            // 
            this.btnPretraga.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPretraga.Location = new System.Drawing.Point(942, 76);
            this.btnPretraga.Name = "btnPretraga";
            this.btnPretraga.Size = new System.Drawing.Size(121, 32);
            this.btnPretraga.TabIndex = 7;
            this.btnPretraga.Text = "Pretraga";
            this.btnPretraga.UseVisualStyleBackColor = true;
            this.btnPretraga.Click += new System.EventHandler(this.btnPretraga_Click);
            // 
            // cmbSmena
            // 
            this.cmbSmena.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbSmena.FormattingEnabled = true;
            this.cmbSmena.Location = new System.Drawing.Point(754, 81);
            this.cmbSmena.Name = "cmbSmena";
            this.cmbSmena.Size = new System.Drawing.Size(135, 26);
            this.cmbSmena.TabIndex = 6;
            this.cmbSmena.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbSmena_KeyDown);
            // 
            // cmbOperater
            // 
            this.cmbOperater.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbOperater.FormattingEnabled = true;
            this.cmbOperater.Location = new System.Drawing.Point(754, 34);
            this.cmbOperater.Name = "cmbOperater";
            this.cmbOperater.Size = new System.Drawing.Size(135, 26);
            this.cmbOperater.TabIndex = 5;
            this.cmbOperater.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbOperater_KeyDown);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(671, 82);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 18);
            this.label6.TabIndex = 9;
            this.label6.Text = "Smena";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(671, 37);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(66, 18);
            this.label5.TabIndex = 8;
            this.label5.Text = "Operater";
            // 
            // dateTimePickerDO
            // 
            this.dateTimePickerDO.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePickerDO.Location = new System.Drawing.Point(396, 78);
            this.dateTimePickerDO.Name = "dateTimePickerDO";
            this.dateTimePickerDO.Size = new System.Drawing.Size(218, 24);
            this.dateTimePickerDO.TabIndex = 4;
            this.dateTimePickerDO.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dateTimePickerDO_KeyDown);
            // 
            // dateTimePickerOD
            // 
            this.dateTimePickerOD.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePickerOD.Location = new System.Drawing.Point(396, 37);
            this.dateTimePickerOD.Name = "dateTimePickerOD";
            this.dateTimePickerOD.Size = new System.Drawing.Size(218, 24);
            this.dateTimePickerOD.TabIndex = 3;
            this.dateTimePickerOD.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dateTimePickerOD_KeyDown);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(335, 85);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 18);
            this.label3.TabIndex = 5;
            this.label3.Text = "DO";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(335, 37);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 18);
            this.label4.TabIndex = 4;
            this.label4.Text = "OD";
            // 
            // cmbTipKarte
            // 
            this.cmbTipKarte.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbTipKarte.FormattingEnabled = true;
            this.cmbTipKarte.Items.AddRange(new object[] {
            "Obična",
            "Pretplatna"});
            this.cmbTipKarte.Location = new System.Drawing.Point(155, 81);
            this.cmbTipKarte.Name = "cmbTipKarte";
            this.cmbTipKarte.Size = new System.Drawing.Size(135, 26);
            this.cmbTipKarte.TabIndex = 2;
            this.cmbTipKarte.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbTipKarte_KeyDown);
            // 
            // txtTablica
            // 
            this.txtTablica.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTablica.Location = new System.Drawing.Point(155, 36);
            this.txtTablica.Name = "txtTablica";
            this.txtTablica.Size = new System.Drawing.Size(135, 24);
            this.txtTablica.TabIndex = 1;
            this.txtTablica.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtTablica_KeyDown);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(69, 85);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 18);
            this.label2.TabIndex = 1;
            this.label2.Text = "Tip karte";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(69, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tablica";
            // 
            // panelZaGrid
            // 
            this.panelZaGrid.Controls.Add(this.dataGridViewIzlazi);
            this.panelZaGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelZaGrid.Location = new System.Drawing.Point(3, 153);
            this.panelZaGrid.Name = "panelZaGrid";
            this.panelZaGrid.Size = new System.Drawing.Size(1270, 538);
            this.panelZaGrid.TabIndex = 1;
            // 
            // dataGridViewIzlazi
            // 
            this.dataGridViewIzlazi.AllowUserToAddRows = false;
            this.dataGridViewIzlazi.AllowUserToDeleteRows = false;
            this.dataGridViewIzlazi.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewIzlazi.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewIzlazi.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewIzlazi.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewIzlazi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewIzlazi.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewIzlazi.Name = "dataGridViewIzlazi";
            this.dataGridViewIzlazi.ReadOnly = true;
            this.dataGridViewIzlazi.Size = new System.Drawing.Size(1270, 538);
            this.dataGridViewIzlazi.TabIndex = 8;
            this.dataGridViewIzlazi.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewIzlazi_CellClick);
            // 
            // frmIzlazi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1276, 694);
            this.Controls.Add(this.tableLayoutPanelGlavni);
            this.Name = "frmIzlazi";
            this.Text = "Izlazi";
            this.Load += new System.EventHandler(this.frmIzlazi_Load);
            this.SizeChanged += new System.EventHandler(this.frmIzlazi_SizeChanged);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmIzlazi_KeyDown);
            this.tableLayoutPanelGlavni.ResumeLayout(false);
            this.panelFilteri.ResumeLayout(false);
            this.panelFilteri.PerformLayout();
            this.panelZaGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewIzlazi)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelGlavni;
        private System.Windows.Forms.Panel panelFilteri;
        private System.Windows.Forms.Panel panelZaGrid;
        private System.Windows.Forms.DataGridView dataGridViewIzlazi;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbTipKarte;
        private System.Windows.Forms.TextBox txtTablica;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dateTimePickerDO;
        private System.Windows.Forms.DateTimePicker dateTimePickerOD;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnPretraga;
        private System.Windows.Forms.ComboBox cmbSmena;
        private System.Windows.Forms.ComboBox cmbOperater;
    }
}