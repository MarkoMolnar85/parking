﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BusinessLayer;
using DataLayer.Models;

namespace WindowsFormsApp1
{
    public partial class frmIzlazi : Form
    {
        IzlazBusiness ib;
        DataTable dtIzlazi = new DataTable();
        public frmIzlazi()
        {
            InitializeComponent();
            this.WindowState = FormWindowState.Maximized;
            KeyPreview = true;
        }

        private void tableLayoutPanelGlavni_Paint(object sender, PaintEventArgs e)
        {

        }

        private void frmIzlazi_Load(object sender, EventArgs e)
        {
            ib = new IzlazBusiness();
            PodesiDateTimePickere();
            LoadIzlazi();
            ResajzovanjeKolona();
            BogacenjeKomboBoxova();
        }

        private void PodesiDateTimePickere()
        {
            this.dateTimePickerOD.Format = DateTimePickerFormat.Custom;
            this.dateTimePickerOD.CustomFormat = "dd-MM-yyyy";
            //this.dateTimePickerOD.CustomFormat = "dd-MM-yyyy hh:mm:ss";
            this.dateTimePickerDO.Format = DateTimePickerFormat.Custom;
            this.dateTimePickerDO.CustomFormat = "dd-MM-yyyy";

            this.dateTimePickerOD.Value = DateTime.Today.AddDays(-1);
        }
        private void LoadIzlazi()
        {
            //izbrisati blok koda. Koristi se da ne bih morao da idem na login
            if (Program.idLokacijeIzKomboBoxaKodFormeZaNadzornika == 0)
            {
                Program.idLokacijeIzKomboBoxaKodFormeZaNadzornika = 1002;
            }
            dtIzlazi = ib.GetAllIzlaziNaOsnovuIdLokacijeDT(Program.idLokacijeIzKomboBoxaKodFormeZaNadzornika);
            this.dataGridViewIzlazi.DataSource = dtIzlazi;
            
        }
        private void ResajzovanjeKolona()
        {
            if(this.dataGridViewIzlazi.DataSource != null)
            {
                int brojKolona = 6;
                int sirinaKolone = (this.dataGridViewIzlazi.Width - this.dataGridViewIzlazi.RowHeadersWidth) / brojKolona;
                this.dataGridViewIzlazi.Columns["Tablica"].Width = sirinaKolone;
                this.dataGridViewIzlazi.Columns["Vreme"].Width = sirinaKolone;
                this.dataGridViewIzlazi.Columns["Tip karte"].Width = sirinaKolone;
                this.dataGridViewIzlazi.Columns["Korisničko ime operatera"].Width = sirinaKolone;
                this.dataGridViewIzlazi.Columns["Smena"].Width = sirinaKolone;
                this.dataGridViewIzlazi.Columns["Lokacija"].Width = sirinaKolone;
            }
        }

        private void frmIzlazi_SizeChanged(object sender, EventArgs e)
        {
            ResajzovanjeKolona();
        }

        private void dataGridViewIzlazi_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (!(e.RowIndex < 0))
            {
                this.dataGridViewIzlazi.Rows[e.RowIndex].Selected = true;
            }
        }

        private void BogacenjeKomboBoxova()
        {
            OperaterBusiness ob = new OperaterBusiness();
            cmbOperater.Items.Clear();
            foreach (var v in ob.GetAllKorisnickaImenaZaKomboBox())
            {
                cmbOperater.Items.Add(v);
            }

            SmenaBusiness sb = new SmenaBusiness();
            cmbSmena.Items.Clear();
            foreach (var v in sb.GetAllNaziviSmenaZaKomboBox())
            {
                cmbSmena.Items.Add(v);
            }
        }

        private void btnPretraga_Click(object sender, EventArgs e)
        {
            PretragaIzlaza();
        }
        private void PretragaIzlaza()
        {
            IzlazSaOstalim iso = new IzlazSaOstalim();
            iso.Tablica = txtTablica.Text;
            iso.TipKarte = cmbTipKarte.Text;
            iso.OperaterKorisnicko = cmbOperater.Text;
            iso.SmenaNaziv = cmbSmena.Text;
            DateTime datumOD = this.dateTimePickerOD.Value;
            DateTime datumDO = this.dateTimePickerDO.Value;


            dtIzlazi = ib.PretragaIzlazaPoSvimParametrimaDT(iso, datumOD, datumDO, Program.idLokacijeIzKomboBoxaKodFormeZaNadzornika);
            this.dataGridViewIzlazi.DataSource = dtIzlazi;
        }

        private void txtTablica_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 13)
            {
                PretragaIzlaza();
            }
        }

        private void cmbTipKarte_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 13)
            {
                PretragaIzlaza();
            }
        }

        private void dateTimePickerOD_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 13)
            {
                PretragaIzlaza();
            }
        }

        private void dateTimePickerDO_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 13)
            {
                PretragaIzlaza();
            }
        }

        private void cmbOperater_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 13)
            {
                PretragaIzlaza();
            }
        }

        private void cmbSmena_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 13)
            {
                PretragaIzlaza();
            }
        }

        private void ExportGridaExcel(DataGridView dataGridViewPrikaz)
        {
            //nema nevidljivih kolona

            // Creating a Excel object. 
            Microsoft.Office.Interop.Excel._Application excel = new Microsoft.Office.Interop.Excel.Application();
            Microsoft.Office.Interop.Excel._Workbook workbook = excel.Workbooks.Add(Type.Missing);
            Microsoft.Office.Interop.Excel._Worksheet worksheet = workbook.Worksheets.Add();


            string putanja = "";
            try
            {

                worksheet = workbook.ActiveSheet;

                worksheet.Name = "Izlazi";

                int indexKolone = 1;
                int indexReda = 2;

                for (int i = 0; i < dataGridViewPrikaz.Columns.Count; i++)
                {
                    //if (!(i == 0 || i == 7))
                    {
                        worksheet.Cells[1, indexKolone] = dataGridViewPrikaz.Columns[i].HeaderText;
                        indexKolone++;
                    }
                }

                indexKolone = 1;

                for (int i = 0; i < dataGridViewPrikaz.Rows.Count; i++)
                {
                    for (int j = 0; j < dataGridViewPrikaz.Columns.Count; j++)
                    {
                        //if (!(j == 0 || j == 7))
                        {
                            worksheet.Cells[indexReda, indexKolone] = dataGridViewPrikaz.Rows[i].Cells[j].Value.ToString();
                            indexKolone++;
                        }
                    }
                    indexKolone = 1;
                    indexReda++;
                }


                //Getting the location and file name of the excel to save from user. 
                SaveFileDialog saveDialog = new SaveFileDialog();
                saveDialog.Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
                saveDialog.FilterIndex = 2;

                if (saveDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    workbook.SaveAs(saveDialog.FileName);
                    MessageBox.Show("Excel datoteka je uspeno izvezena");
                    putanja = saveDialog.FileName;
                }

            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                workbook.Close(false);
                excel.Quit();
                workbook = null;
                excel = null;
                GC.Collect();
                UbiProces(putanja);
            }
        }

        private void UbiProces(string putanja)
        {
            System.Diagnostics.Process[] process = System.Diagnostics.Process.GetProcessesByName("Excel");
            foreach (System.Diagnostics.Process p in process)
            {
                if (!string.IsNullOrEmpty(p.ProcessName))
                {
                    try
                    {
                        p.Kill();
                    }
                    catch { }
                }
            }
        }

        private void frmIzlazi_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F12)
            {
                ExportGridaExcel(this.dataGridViewIzlazi);
            }
        }
    }
}
