﻿using BusinessLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1;
//using BusinessLayer;

namespace Parking_aplikacija_Marko_Molnar
{
    public partial class frmLogIn : Form
    {
        public frmLogIn()
        {
            InitializeComponent();
            //LogInData f = new LogInData()
            //LoginBusiness d
        }

        private void frmLogIn_Load(object sender, EventArgs e)
        {
            this.CenterToScreen();
        }

        private void btnPrijaviSe_Click(object sender, EventArgs e)
        {
            Prijavljivanje();
        }

        private void Prijavljivanje()
        {
            LoginPodaci loginPodaci = new LoginPodaci();
            //string korisnickoIme = "";
            loginPodaci.KorisnickoIme = "";
            //string lozinka = "";
            loginPodaci.LozinkaZaLogin = "";

            if (txtKorisnickoIme.Text.Trim() == "")
            {
                MessageBox.Show("Ubavezno popuniti korisničko ime");
                return;
            }
            if (txtSifra.Text.Trim() == "")
            {
                MessageBox.Show("Ubavezno uneti lozinku");
                return;
            }
            loginPodaci.KorisnickoIme = txtKorisnickoIme.Text.ToUpper();
            Program.korisnickoIme = txtKorisnickoIme.Text.ToUpper();
            loginPodaci.LozinkaZaLogin = txtSifra.Text.ToUpper();
            Program.imeMasine = Environment.MachineName.ToUpper();

            string operaterNadzornikNista = "nista";
            LoginBusiness loginBusiness = new LoginBusiness();
            //Funfkija proverava da li postoji nadzornik ili operater pa vraca ono sto nadje. Ako ne nadje vraca string nista
            operaterNadzornikNista = loginBusiness.ProveraLogiNa(loginPodaci);
            if (operaterNadzornikNista == "operator")
            {
                //otvara formu za smenu koja otvara za stanicu
                //frmIzaberiSmenu smena = new frmIzaberiSmenu();
                //smena.ShowDialog();

                //kreira se operater business kako bi dobio id od operatera. Zatim unosim u objekat i sadasnje vreme za log.
                //onda kreiram operaterBusiness kako bi insertovao podatak o logovanju

                //ovaj blok koda sprecava da se pokrene forma na stanici bez poklapanja sa imenom racunara
                LokacijaBusiness lb = new LokacijaBusiness();
                bool postojiLokacijaSaImenomMasine = lb.PostojiLokacijaSaImenomMasine(Environment.MachineName);
                if (!postojiLokacijaSaImenomMasine)
                {
                    MessageBox.Show("Nepostoji lokacija sa ovim imenom kompljutera");
                    this.Close();
                }

                OperaterBusiness operaterBusiness = new OperaterBusiness();
                Operater operater = operaterBusiness.GetOperaterPoKorisnickomImenu(loginPodaci.KorisnickoIme);

                OperaterLogin operaterLogin = new OperaterLogin();
                operaterLogin.OperaterID = operater.OperaterID;
                operaterLogin.VremeLoga = DateTime.Now;

                OperaterLogInBusiness opLogBusiness = new OperaterLogInBusiness();
                int uspehInsertaLoga = opLogBusiness.InsertOperaterLog(operaterLogin);
                if (uspehInsertaLoga <= 0)
                {
                    MessageBox.Show("Greška pri upisu logovanja");
                    return;
                }

                frmAktivnostiNaStanici aktivnostNaStanici = new frmAktivnostiNaStanici();
                aktivnostNaStanici.ShowDialog();
                //this.Close();
            }
            else if (operaterNadzornikNista == "nadzornik")
            {
                //kreiram nadzorBusiness kako bi dobio nadzornika na osnovu korisnickog imena odnosno njegov id koji
                //je potreban za insert loga nadzornika
                NadzornikBusiness nadzorBusiness = new NadzornikBusiness();
                Nadzornik nadzornik = nadzorBusiness.GetNadzornikNaOsnovuKorisnickogImena(loginPodaci.KorisnickoIme);

                NadzornikLogoviBusiness nlb = new NadzornikLogoviBusiness();

                NadzornikLog nadzornikLog = new NadzornikLog();
                nadzornikLog.VremeLoga = DateTime.Now;
                nadzornikLog.NadzornikID = nadzornik.NadzornikID;

                int uspesnostInserta = nlb.InsertLogaNadzornika(nadzornikLog);
                if (uspesnostInserta <= 0)
                {
                    MessageBox.Show("Neupešan upis loga");
                    return;
                }

                frmZaNadzornika zaNadzornika = new frmZaNadzornika();
                zaNadzornika.ShowDialog();
                //this.Close();
                //otvara formu za nadzornika
            }
            else
            {
                MessageBox.Show("Nedozvoljen pristup");
            }

            //LoginBusiness
            //LogInData
        }

        private void txtKorisnickoIme_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 13)
            {
                Prijavljivanje();
            }
        }

        private void txtSifra_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 13)
            {
                Prijavljivanje();
            }
        }
    }
}
