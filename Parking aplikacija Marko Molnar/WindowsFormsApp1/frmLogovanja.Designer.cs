﻿namespace WindowsFormsApp1
{
    partial class frmLogovanja
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tableLayoutPanelGlavni = new System.Windows.Forms.TableLayoutPanel();
            this.panelOperateriFilter = new System.Windows.Forms.Panel();
            this.groupBoxOperateri = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbOperater = new System.Windows.Forms.ComboBox();
            this.panelNadzorniciFilter = new System.Windows.Forms.Panel();
            this.groupBoxNadzornici = new System.Windows.Forms.GroupBox();
            this.cmbNadzornik = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panelGridOperateri = new System.Windows.Forms.Panel();
            this.dataGridViewOperateriLogovi = new System.Windows.Forms.DataGridView();
            this.panelGridNadzornici = new System.Windows.Forms.Panel();
            this.dataGridViewNadzorniciLogovi = new System.Windows.Forms.DataGridView();
            this.tableLayoutPanelGlavni.SuspendLayout();
            this.panelOperateriFilter.SuspendLayout();
            this.groupBoxOperateri.SuspendLayout();
            this.panelNadzorniciFilter.SuspendLayout();
            this.groupBoxNadzornici.SuspendLayout();
            this.panelGridOperateri.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewOperateriLogovi)).BeginInit();
            this.panelGridNadzornici.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewNadzorniciLogovi)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanelGlavni
            // 
            this.tableLayoutPanelGlavni.ColumnCount = 2;
            this.tableLayoutPanelGlavni.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelGlavni.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelGlavni.Controls.Add(this.panelOperateriFilter, 0, 0);
            this.tableLayoutPanelGlavni.Controls.Add(this.panelNadzorniciFilter, 1, 0);
            this.tableLayoutPanelGlavni.Controls.Add(this.panelGridOperateri, 0, 1);
            this.tableLayoutPanelGlavni.Controls.Add(this.panelGridNadzornici, 1, 1);
            this.tableLayoutPanelGlavni.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelGlavni.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanelGlavni.Name = "tableLayoutPanelGlavni";
            this.tableLayoutPanelGlavni.RowCount = 2;
            this.tableLayoutPanelGlavni.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanelGlavni.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelGlavni.Size = new System.Drawing.Size(1276, 694);
            this.tableLayoutPanelGlavni.TabIndex = 0;
            // 
            // panelOperateriFilter
            // 
            this.panelOperateriFilter.Controls.Add(this.groupBoxOperateri);
            this.panelOperateriFilter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelOperateriFilter.Location = new System.Drawing.Point(3, 3);
            this.panelOperateriFilter.Name = "panelOperateriFilter";
            this.panelOperateriFilter.Size = new System.Drawing.Size(632, 144);
            this.panelOperateriFilter.TabIndex = 0;
            // 
            // groupBoxOperateri
            // 
            this.groupBoxOperateri.Controls.Add(this.label1);
            this.groupBoxOperateri.Controls.Add(this.cmbOperater);
            this.groupBoxOperateri.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxOperateri.Location = new System.Drawing.Point(9, 3);
            this.groupBoxOperateri.Name = "groupBoxOperateri";
            this.groupBoxOperateri.Size = new System.Drawing.Size(620, 138);
            this.groupBoxOperateri.TabIndex = 0;
            this.groupBoxOperateri.TabStop = false;
            this.groupBoxOperateri.Text = "Operateri";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(98, 57);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(174, 18);
            this.label1.TabIndex = 1;
            this.label1.Text = "Korisničko ime operatera";
            // 
            // cmbOperater
            // 
            this.cmbOperater.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbOperater.FormattingEnabled = true;
            this.cmbOperater.Location = new System.Drawing.Point(303, 54);
            this.cmbOperater.Name = "cmbOperater";
            this.cmbOperater.Size = new System.Drawing.Size(169, 26);
            this.cmbOperater.TabIndex = 0;
            this.cmbOperater.SelectionChangeCommitted += new System.EventHandler(this.cmbOperater_SelectionChangeCommitted);
            this.cmbOperater.KeyUp += new System.Windows.Forms.KeyEventHandler(this.cmbOperater_KeyUp);
            // 
            // panelNadzorniciFilter
            // 
            this.panelNadzorniciFilter.Controls.Add(this.groupBoxNadzornici);
            this.panelNadzorniciFilter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelNadzorniciFilter.Location = new System.Drawing.Point(641, 3);
            this.panelNadzorniciFilter.Name = "panelNadzorniciFilter";
            this.panelNadzorniciFilter.Size = new System.Drawing.Size(632, 144);
            this.panelNadzorniciFilter.TabIndex = 1;
            // 
            // groupBoxNadzornici
            // 
            this.groupBoxNadzornici.Controls.Add(this.cmbNadzornik);
            this.groupBoxNadzornici.Controls.Add(this.label2);
            this.groupBoxNadzornici.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxNadzornici.Location = new System.Drawing.Point(3, 9);
            this.groupBoxNadzornici.Name = "groupBoxNadzornici";
            this.groupBoxNadzornici.Size = new System.Drawing.Size(626, 132);
            this.groupBoxNadzornici.TabIndex = 0;
            this.groupBoxNadzornici.TabStop = false;
            this.groupBoxNadzornici.Text = "Nadzornici";
            // 
            // cmbNadzornik
            // 
            this.cmbNadzornik.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbNadzornik.FormattingEnabled = true;
            this.cmbNadzornik.Location = new System.Drawing.Point(286, 48);
            this.cmbNadzornik.Name = "cmbNadzornik";
            this.cmbNadzornik.Size = new System.Drawing.Size(169, 26);
            this.cmbNadzornik.TabIndex = 1;
            this.cmbNadzornik.SelectionChangeCommitted += new System.EventHandler(this.cmbNadzornik_SelectionChangeCommitted);
            this.cmbNadzornik.KeyUp += new System.Windows.Forms.KeyEventHandler(this.cmbNadzornik_KeyUp);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(75, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(182, 18);
            this.label2.TabIndex = 0;
            this.label2.Text = "korisničko ime nadzornika";
            // 
            // panelGridOperateri
            // 
            this.panelGridOperateri.Controls.Add(this.dataGridViewOperateriLogovi);
            this.panelGridOperateri.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelGridOperateri.Location = new System.Drawing.Point(3, 153);
            this.panelGridOperateri.Name = "panelGridOperateri";
            this.panelGridOperateri.Size = new System.Drawing.Size(632, 538);
            this.panelGridOperateri.TabIndex = 2;
            // 
            // dataGridViewOperateriLogovi
            // 
            this.dataGridViewOperateriLogovi.AllowUserToAddRows = false;
            this.dataGridViewOperateriLogovi.AllowUserToDeleteRows = false;
            this.dataGridViewOperateriLogovi.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewOperateriLogovi.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewOperateriLogovi.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewOperateriLogovi.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewOperateriLogovi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewOperateriLogovi.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewOperateriLogovi.Name = "dataGridViewOperateriLogovi";
            this.dataGridViewOperateriLogovi.ReadOnly = true;
            this.dataGridViewOperateriLogovi.Size = new System.Drawing.Size(632, 538);
            this.dataGridViewOperateriLogovi.TabIndex = 0;
            this.dataGridViewOperateriLogovi.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewOperateriLogovi_CellClick);
            // 
            // panelGridNadzornici
            // 
            this.panelGridNadzornici.Controls.Add(this.dataGridViewNadzorniciLogovi);
            this.panelGridNadzornici.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelGridNadzornici.Location = new System.Drawing.Point(641, 153);
            this.panelGridNadzornici.Name = "panelGridNadzornici";
            this.panelGridNadzornici.Size = new System.Drawing.Size(632, 538);
            this.panelGridNadzornici.TabIndex = 3;
            // 
            // dataGridViewNadzorniciLogovi
            // 
            this.dataGridViewNadzorniciLogovi.AllowUserToAddRows = false;
            this.dataGridViewNadzorniciLogovi.AllowUserToDeleteRows = false;
            this.dataGridViewNadzorniciLogovi.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewNadzorniciLogovi.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewNadzorniciLogovi.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewNadzorniciLogovi.DefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridViewNadzorniciLogovi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewNadzorniciLogovi.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewNadzorniciLogovi.Name = "dataGridViewNadzorniciLogovi";
            this.dataGridViewNadzorniciLogovi.ReadOnly = true;
            this.dataGridViewNadzorniciLogovi.Size = new System.Drawing.Size(632, 538);
            this.dataGridViewNadzorniciLogovi.TabIndex = 0;
            this.dataGridViewNadzorniciLogovi.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewNadzorniciLogovi_CellClick);
            // 
            // frmLogovanja
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1276, 694);
            this.Controls.Add(this.tableLayoutPanelGlavni);
            this.Name = "frmLogovanja";
            this.Text = "Logovanja";
            this.Load += new System.EventHandler(this.frmLogovanja_Load);
            this.SizeChanged += new System.EventHandler(this.frmLogovanja_SizeChanged);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmLogovanja_KeyDown);
            this.tableLayoutPanelGlavni.ResumeLayout(false);
            this.panelOperateriFilter.ResumeLayout(false);
            this.groupBoxOperateri.ResumeLayout(false);
            this.groupBoxOperateri.PerformLayout();
            this.panelNadzorniciFilter.ResumeLayout(false);
            this.groupBoxNadzornici.ResumeLayout(false);
            this.groupBoxNadzornici.PerformLayout();
            this.panelGridOperateri.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewOperateriLogovi)).EndInit();
            this.panelGridNadzornici.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewNadzorniciLogovi)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelGlavni;
        private System.Windows.Forms.Panel panelOperateriFilter;
        private System.Windows.Forms.Panel panelNadzorniciFilter;
        private System.Windows.Forms.Panel panelGridOperateri;
        private System.Windows.Forms.DataGridView dataGridViewOperateriLogovi;
        private System.Windows.Forms.Panel panelGridNadzornici;
        private System.Windows.Forms.DataGridView dataGridViewNadzorniciLogovi;
        private System.Windows.Forms.GroupBox groupBoxOperateri;
        private System.Windows.Forms.GroupBox groupBoxNadzornici;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbOperater;
        private System.Windows.Forms.ComboBox cmbNadzornik;
        private System.Windows.Forms.Label label2;
    }
}