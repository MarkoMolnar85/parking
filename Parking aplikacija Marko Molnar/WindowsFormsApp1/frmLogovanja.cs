﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BusinessLayer;

namespace WindowsFormsApp1
{
    public partial class frmLogovanja : Form
    {
        OperaterLogInBusiness olb = new OperaterLogInBusiness();
        NadzornikLogoviBusiness nlb = new NadzornikLogoviBusiness();
        public frmLogovanja()
        {
            InitializeComponent();
            this.WindowState = FormWindowState.Maximized;
            KeyPreview = true;
        }

        private void frmLogovanja_Load(object sender, EventArgs e)
        {
            LoadOperateriLogovi();
            SakriKoloneOperateriLogovi();
            ResajzovanjeKolona();
            PromenaImenaKolonaOperateri();
            PopuniKomboboxeve();

            LoadNadzornikLogovi();
            SakriKoloneNadzornikLogovi();
            ResajzovanjeKolonaNadzornikLog();
            PromenaImenaKoloneNadzornici();
        }
        private void LoadOperateriLogovi()
        {
            this.dataGridViewOperateriLogovi.DataSource = olb.GetAllOperaterLogoviSaImenom();
        }
        private void LoadNadzornikLogovi()
        {
            this.dataGridViewNadzorniciLogovi.DataSource = nlb.GetAllNadzornikLogoviSaImenom();
        }
        private void SakriKoloneOperateriLogovi()
        {
            this.dataGridViewOperateriLogovi.Columns["logOperateraID"].Visible = false;
            this.dataGridViewOperateriLogovi.Columns["operaterID"].Visible = false;
        }
        private void SakriKoloneNadzornikLogovi()
        {
            this.dataGridViewNadzorniciLogovi.Columns["nadzornikLogID"].Visible = false;
            this.dataGridViewNadzorniciLogovi.Columns["nadzornikID"].Visible = false;
        }
        private void ResajzovanjeKolona()
        {
            if (dataGridViewOperateriLogovi.DataSource != null)
            {
                int brojKolona = 3;
                int sirinaKolone = (this.dataGridViewOperateriLogovi.Width - dataGridViewOperateriLogovi.RowHeadersWidth - 25) / brojKolona;
                this.dataGridViewOperateriLogovi.Columns["operaterKorisnickoIme"].Width = sirinaKolone;
                this.dataGridViewOperateriLogovi.Columns["vremeLoga"].Width = sirinaKolone;
                this.dataGridViewOperateriLogovi.Columns["vremePosle"].Width = sirinaKolone;
            }
        }
        private void ResajzovanjeKolonaNadzornikLog()
        {
            if (dataGridViewNadzorniciLogovi.DataSource != null)
            {
                int brojKolona = 3;
                int sirinaKolone = (this.dataGridViewNadzorniciLogovi.Width - dataGridViewNadzorniciLogovi.RowHeadersWidth - 25) / brojKolona;
                this.dataGridViewNadzorniciLogovi.Columns["nadzornikKorisnicko"].Width = sirinaKolone;
                this.dataGridViewNadzorniciLogovi.Columns["vremeLoga"].Width = sirinaKolone;
                this.dataGridViewNadzorniciLogovi.Columns["vremePosle"].Width = sirinaKolone;
            }
        }

        private void frmLogovanja_SizeChanged(object sender, EventArgs e)
        {
            ResajzovanjeKolona();
            ResajzovanjeKolonaNadzornikLog();
        }

        private void dataGridViewOperateriLogovi_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int index = e.RowIndex;
            if (!(index < 0))
            {
                this.dataGridViewOperateriLogovi.Rows[index].Selected = true;
            }
        }
        private void PromenaImenaKolonaOperateri()
        {
            this.dataGridViewOperateriLogovi.Columns["operaterKorisnickoIme"].HeaderText = "Operater korisničko ime";
            this.dataGridViewOperateriLogovi.Columns["vremeLoga"].HeaderText = "Vreme prijavljivanja";
            this.dataGridViewOperateriLogovi.Columns["vremePosle"].HeaderText = "Vreme odjavljivanja";
        }
        private void PromenaImenaKoloneNadzornici()
        {
            this.dataGridViewNadzorniciLogovi.Columns["nadzornikKorisnicko"].HeaderText = "Nadzornik korisničko ime";
            this.dataGridViewNadzorniciLogovi.Columns["vremeLoga"].HeaderText = "Vreme prijavljivanja";
            this.dataGridViewNadzorniciLogovi.Columns["vremePosle"].HeaderText = "Vreme odjavljivanja";
        }
        private void PopuniKomboboxeve()
        {
            
            OperaterBusiness ob = new OperaterBusiness();
            cmbOperater.DataSource = ob.GetAllKorisnickaImenaZaKomboBox();
            cmbOperater.DisplayMember = "Korisnicko";

            NadzornikBusiness nb = new NadzornikBusiness();
            cmbNadzornik.DataSource = nb.GetNadzorniciIzKomboBoxa();
            cmbNadzornik.DisplayMember = "korisnicko";
        }
        

        private void cmbOperater_SelectionChangeCommitted(object sender, EventArgs e)
        {
            OperaterLogInBusiness ob = new OperaterLogInBusiness();
            this.dataGridViewOperateriLogovi.DataSource = ob.PretragaOperaterLogova(cmbOperater.SelectedValue.ToString());
        }

        private void cmbOperater_KeyUp(object sender, KeyEventArgs e)
        {
            OperaterLogInBusiness ob = new OperaterLogInBusiness();
            this.dataGridViewOperateriLogovi.DataSource = ob.PretragaOperaterLogova(cmbOperater.Text);
        }

        private void dataGridViewNadzorniciLogovi_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int index = e.RowIndex;
            if (!(index < 0))
            {
                this.dataGridViewNadzorniciLogovi.Rows[index].Selected = true;
            }
        }

        private void cmbNadzornik_KeyUp(object sender, KeyEventArgs e)
        {
            this.dataGridViewNadzorniciLogovi.DataSource = nlb.PretragaNaOsnovuParametraKorisnickoIme(cmbNadzornik.Text);
        }

        private void cmbNadzornik_SelectionChangeCommitted(object sender, EventArgs e)
        {
            this.dataGridViewNadzorniciLogovi.DataSource = nlb.PretragaNaOsnovuParametraKorisnickoIme(cmbNadzornik.SelectedValue.ToString());
        }

        private void ExportGridaExcel(DataGridView dataGridViewPrikaz)
        {
            //nevidljive kolone su 2 i 5 odnosno 1 i 4

            // Creating a Excel object. 
            Microsoft.Office.Interop.Excel._Application excel = new Microsoft.Office.Interop.Excel.Application();
            Microsoft.Office.Interop.Excel._Workbook workbook = excel.Workbooks.Add(Type.Missing);
            Microsoft.Office.Interop.Excel._Worksheet worksheet = workbook.Worksheets.Add();


            string putanja = "";
            try
            {

                worksheet = workbook.ActiveSheet;

                worksheet.Name = "Logovanja";

                int indexKolone = 1;
                int indexReda = 2;

                for (int i = 0; i < dataGridViewPrikaz.Columns.Count; i++)
                {
                    if (!(i == 1 || i == 4))
                    {
                        worksheet.Cells[1, indexKolone] = dataGridViewPrikaz.Columns[i].HeaderText;
                        indexKolone++;
                    }
                }

                indexKolone = 1;

                for (int i = 0; i < dataGridViewPrikaz.Rows.Count; i++)
                {
                    for (int j = 0; j < dataGridViewPrikaz.Columns.Count; j++)
                    {
                        if (!(j == 1 || j == 4))
                        {
                            worksheet.Cells[indexReda, indexKolone] = dataGridViewPrikaz.Rows[i].Cells[j].Value.ToString();
                            indexKolone++;
                        }
                    }
                    indexKolone = 1;
                    indexReda++;
                }


                //Getting the location and file name of the excel to save from user. 
                SaveFileDialog saveDialog = new SaveFileDialog();
                saveDialog.Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
                saveDialog.FilterIndex = 2;

                if (saveDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    workbook.SaveAs(saveDialog.FileName);
                    MessageBox.Show("Excel datoteka je uspeno izvezena");
                    putanja = saveDialog.FileName;
                }

            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                workbook.Close(false);
                excel.Quit();
                workbook = null;
                excel = null;
                GC.Collect();
                UbiProces(putanja);
            }
        }

        private void UbiProces(string putanja)
        {
            System.Diagnostics.Process[] process = System.Diagnostics.Process.GetProcessesByName("Excel");
            foreach (System.Diagnostics.Process p in process)
            {
                if (!string.IsNullOrEmpty(p.ProcessName))
                {
                    try
                    {
                        p.Kill();
                    }
                    catch { }
                }
            }
        }

        private void frmLogovanja_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F12)
            {
                ExportGridaExcel(this.dataGridViewNadzorniciLogovi);
            }
            else if (e.KeyCode == Keys.F11)
            {
                ExportGridaExcel(this.dataGridViewOperateriLogovi);
            }
        }
    }
}
