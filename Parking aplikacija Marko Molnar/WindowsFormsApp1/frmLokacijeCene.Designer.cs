﻿namespace WindowsFormsApp1
{
    partial class frmLokacijeCene
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabControlGlavni = new System.Windows.Forms.TabControl();
            this.tabPageLokacija = new System.Windows.Forms.TabPage();
            this.tableLayoutPanelGlavniLokacije = new System.Windows.Forms.TableLayoutPanel();
            this.panelPoljaZaUnosLokacija = new System.Windows.Forms.Panel();
            this.txtCenaStana = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtBrojMesta = new System.Windows.Forms.TextBox();
            this.txtImeMasine = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNazivLokacije = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.lokacijeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dodajToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.izmeniToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.obrišiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.excelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panelDugmadLokacije = new System.Windows.Forms.Panel();
            this.btnDodaj = new System.Windows.Forms.Button();
            this.btnIzmeni = new System.Windows.Forms.Button();
            this.btnObrisi = new System.Windows.Forms.Button();
            this.btnIzlaz = new System.Windows.Forms.Button();
            this.panelGridLokacije = new System.Windows.Forms.Panel();
            this.dataGridViewLokacije = new System.Windows.Forms.DataGridView();
            this.contextMenuStripMeni = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.dodajToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.obrišiToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tabPageCene = new System.Windows.Forms.TabPage();
            this.tableLayoutPanelCene = new System.Windows.Forms.TableLayoutPanel();
            this.panelGornjiDeoCene = new System.Windows.Forms.Panel();
            this.btnUnos = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.txtCena = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.menuStripCene = new System.Windows.Forms.MenuStrip();
            this.ceneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.excelToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.panelZaGridCene = new System.Windows.Forms.Panel();
            this.dataGridViewCene = new System.Windows.Forms.DataGridView();
            this.tabControlGlavni.SuspendLayout();
            this.tabPageLokacija.SuspendLayout();
            this.tableLayoutPanelGlavniLokacije.SuspendLayout();
            this.panelPoljaZaUnosLokacija.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.panelDugmadLokacije.SuspendLayout();
            this.panelGridLokacije.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewLokacije)).BeginInit();
            this.contextMenuStripMeni.SuspendLayout();
            this.tabPageCene.SuspendLayout();
            this.tableLayoutPanelCene.SuspendLayout();
            this.panelGornjiDeoCene.SuspendLayout();
            this.menuStripCene.SuspendLayout();
            this.panelZaGridCene.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCene)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControlGlavni
            // 
            this.tabControlGlavni.Controls.Add(this.tabPageLokacija);
            this.tabControlGlavni.Controls.Add(this.tabPageCene);
            this.tabControlGlavni.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlGlavni.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControlGlavni.Location = new System.Drawing.Point(0, 0);
            this.tabControlGlavni.Name = "tabControlGlavni";
            this.tabControlGlavni.SelectedIndex = 0;
            this.tabControlGlavni.Size = new System.Drawing.Size(1276, 694);
            this.tabControlGlavni.TabIndex = 0;
            this.tabControlGlavni.SelectedIndexChanged += new System.EventHandler(this.tabControlGlavni_SelectedIndexChanged);
            // 
            // tabPageLokacija
            // 
            this.tabPageLokacija.Controls.Add(this.tableLayoutPanelGlavniLokacije);
            this.tabPageLokacija.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPageLokacija.Location = new System.Drawing.Point(4, 29);
            this.tabPageLokacija.Name = "tabPageLokacija";
            this.tabPageLokacija.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageLokacija.Size = new System.Drawing.Size(1268, 661);
            this.tabPageLokacija.TabIndex = 0;
            this.tabPageLokacija.Text = "Lokacije";
            this.tabPageLokacija.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanelGlavniLokacije
            // 
            this.tableLayoutPanelGlavniLokacije.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.tableLayoutPanelGlavniLokacije.ColumnCount = 1;
            this.tableLayoutPanelGlavniLokacije.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelGlavniLokacije.Controls.Add(this.panelPoljaZaUnosLokacija, 0, 0);
            this.tableLayoutPanelGlavniLokacije.Controls.Add(this.panelDugmadLokacije, 0, 1);
            this.tableLayoutPanelGlavniLokacije.Controls.Add(this.panelGridLokacije, 0, 2);
            this.tableLayoutPanelGlavniLokacije.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelGlavniLokacije.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanelGlavniLokacije.Name = "tableLayoutPanelGlavniLokacije";
            this.tableLayoutPanelGlavniLokacije.RowCount = 3;
            this.tableLayoutPanelGlavniLokacije.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 140F));
            this.tableLayoutPanelGlavniLokacije.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanelGlavniLokacije.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelGlavniLokacije.Size = new System.Drawing.Size(1262, 655);
            this.tableLayoutPanelGlavniLokacije.TabIndex = 0;
            // 
            // panelPoljaZaUnosLokacija
            // 
            this.panelPoljaZaUnosLokacija.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panelPoljaZaUnosLokacija.Controls.Add(this.txtCenaStana);
            this.panelPoljaZaUnosLokacija.Controls.Add(this.label4);
            this.panelPoljaZaUnosLokacija.Controls.Add(this.txtBrojMesta);
            this.panelPoljaZaUnosLokacija.Controls.Add(this.txtImeMasine);
            this.panelPoljaZaUnosLokacija.Controls.Add(this.label3);
            this.panelPoljaZaUnosLokacija.Controls.Add(this.label2);
            this.panelPoljaZaUnosLokacija.Controls.Add(this.label1);
            this.panelPoljaZaUnosLokacija.Controls.Add(this.txtNazivLokacije);
            this.panelPoljaZaUnosLokacija.Controls.Add(this.menuStrip1);
            this.panelPoljaZaUnosLokacija.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelPoljaZaUnosLokacija.Location = new System.Drawing.Point(3, 3);
            this.panelPoljaZaUnosLokacija.Name = "panelPoljaZaUnosLokacija";
            this.panelPoljaZaUnosLokacija.Size = new System.Drawing.Size(1256, 134);
            this.panelPoljaZaUnosLokacija.TabIndex = 0;
            this.panelPoljaZaUnosLokacija.Paint += new System.Windows.Forms.PaintEventHandler(this.panelPoljaZaUnosLokacija_Paint);
            // 
            // txtCenaStana
            // 
            this.txtCenaStana.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCenaStana.Location = new System.Drawing.Point(492, 78);
            this.txtCenaStana.Name = "txtCenaStana";
            this.txtCenaStana.Size = new System.Drawing.Size(100, 22);
            this.txtCenaStana.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(390, 84);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 16);
            this.label4.TabIndex = 7;
            this.label4.Text = "Cena sata";
            // 
            // txtBrojMesta
            // 
            this.txtBrojMesta.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBrojMesta.Location = new System.Drawing.Point(492, 32);
            this.txtBrojMesta.Name = "txtBrojMesta";
            this.txtBrojMesta.Size = new System.Drawing.Size(100, 22);
            this.txtBrojMesta.TabIndex = 2;
            // 
            // txtImeMasine
            // 
            this.txtImeMasine.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtImeMasine.Location = new System.Drawing.Point(203, 81);
            this.txtImeMasine.Name = "txtImeMasine";
            this.txtImeMasine.Size = new System.Drawing.Size(146, 22);
            this.txtImeMasine.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(71, 84);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(103, 16);
            this.label3.TabIndex = 4;
            this.label3.Text = "Ime kompljutera";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(389, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 16);
            this.label2.TabIndex = 3;
            this.label2.Text = "Broj mesta";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(71, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 16);
            this.label1.TabIndex = 2;
            this.label1.Text = "Naziv lokacije";
            // 
            // txtNazivLokacije
            // 
            this.txtNazivLokacije.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNazivLokacije.Location = new System.Drawing.Point(203, 31);
            this.txtNazivLokacije.Name = "txtNazivLokacije";
            this.txtNazivLokacije.Size = new System.Drawing.Size(146, 22);
            this.txtNazivLokacije.TabIndex = 0;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lokacijeToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1256, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // lokacijeToolStripMenuItem
            // 
            this.lokacijeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dodajToolStripMenuItem,
            this.izmeniToolStripMenuItem,
            this.obrišiToolStripMenuItem,
            this.excelToolStripMenuItem});
            this.lokacijeToolStripMenuItem.Name = "lokacijeToolStripMenuItem";
            this.lokacijeToolStripMenuItem.Size = new System.Drawing.Size(62, 20);
            this.lokacijeToolStripMenuItem.Text = "Lokacije";
            // 
            // dodajToolStripMenuItem
            // 
            this.dodajToolStripMenuItem.Name = "dodajToolStripMenuItem";
            this.dodajToolStripMenuItem.Size = new System.Drawing.Size(109, 22);
            this.dodajToolStripMenuItem.Text = "Dodaj";
            this.dodajToolStripMenuItem.Click += new System.EventHandler(this.dodajToolStripMenuItem_Click);
            // 
            // izmeniToolStripMenuItem
            // 
            this.izmeniToolStripMenuItem.Name = "izmeniToolStripMenuItem";
            this.izmeniToolStripMenuItem.Size = new System.Drawing.Size(109, 22);
            this.izmeniToolStripMenuItem.Text = "Izmeni";
            this.izmeniToolStripMenuItem.Click += new System.EventHandler(this.izmeniToolStripMenuItem_Click);
            // 
            // obrišiToolStripMenuItem
            // 
            this.obrišiToolStripMenuItem.Name = "obrišiToolStripMenuItem";
            this.obrišiToolStripMenuItem.Size = new System.Drawing.Size(109, 22);
            this.obrišiToolStripMenuItem.Text = "Obriši";
            this.obrišiToolStripMenuItem.Click += new System.EventHandler(this.obrišiToolStripMenuItem_Click);
            // 
            // excelToolStripMenuItem
            // 
            this.excelToolStripMenuItem.Name = "excelToolStripMenuItem";
            this.excelToolStripMenuItem.Size = new System.Drawing.Size(109, 22);
            this.excelToolStripMenuItem.Text = "Excel";
            this.excelToolStripMenuItem.Click += new System.EventHandler(this.excelToolStripMenuItem_Click);
            // 
            // panelDugmadLokacije
            // 
            this.panelDugmadLokacije.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panelDugmadLokacije.Controls.Add(this.btnDodaj);
            this.panelDugmadLokacije.Controls.Add(this.btnIzmeni);
            this.panelDugmadLokacije.Controls.Add(this.btnObrisi);
            this.panelDugmadLokacije.Controls.Add(this.btnIzlaz);
            this.panelDugmadLokacije.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelDugmadLokacije.Location = new System.Drawing.Point(3, 143);
            this.panelDugmadLokacije.Name = "panelDugmadLokacije";
            this.panelDugmadLokacije.Size = new System.Drawing.Size(1256, 44);
            this.panelDugmadLokacije.TabIndex = 1;
            // 
            // btnDodaj
            // 
            this.btnDodaj.Location = new System.Drawing.Point(735, 0);
            this.btnDodaj.Name = "btnDodaj";
            this.btnDodaj.Size = new System.Drawing.Size(125, 41);
            this.btnDodaj.TabIndex = 4;
            this.btnDodaj.Text = "Dodaj";
            this.btnDodaj.UseVisualStyleBackColor = true;
            this.btnDodaj.Click += new System.EventHandler(this.btnDodaj_Click);
            // 
            // btnIzmeni
            // 
            this.btnIzmeni.Location = new System.Drawing.Point(866, 0);
            this.btnIzmeni.Name = "btnIzmeni";
            this.btnIzmeni.Size = new System.Drawing.Size(125, 41);
            this.btnIzmeni.TabIndex = 5;
            this.btnIzmeni.Text = "Izmeni";
            this.btnIzmeni.UseVisualStyleBackColor = true;
            this.btnIzmeni.Click += new System.EventHandler(this.btnIzmeni_Click);
            // 
            // btnObrisi
            // 
            this.btnObrisi.Location = new System.Drawing.Point(997, 0);
            this.btnObrisi.Name = "btnObrisi";
            this.btnObrisi.Size = new System.Drawing.Size(125, 41);
            this.btnObrisi.TabIndex = 6;
            this.btnObrisi.Text = "Obriši";
            this.btnObrisi.UseVisualStyleBackColor = true;
            this.btnObrisi.Click += new System.EventHandler(this.btnObrisi_Click);
            // 
            // btnIzlaz
            // 
            this.btnIzlaz.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnIzlaz.BackColor = System.Drawing.Color.Transparent;
            this.btnIzlaz.Location = new System.Drawing.Point(1128, 0);
            this.btnIzlaz.Name = "btnIzlaz";
            this.btnIzlaz.Size = new System.Drawing.Size(125, 41);
            this.btnIzlaz.TabIndex = 7;
            this.btnIzlaz.Text = "Izlaz";
            this.btnIzlaz.UseVisualStyleBackColor = false;
            this.btnIzlaz.Click += new System.EventHandler(this.btnIzlaz_Click);
            // 
            // panelGridLokacije
            // 
            this.panelGridLokacije.Controls.Add(this.dataGridViewLokacije);
            this.panelGridLokacije.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelGridLokacije.Location = new System.Drawing.Point(3, 193);
            this.panelGridLokacije.Name = "panelGridLokacije";
            this.panelGridLokacije.Size = new System.Drawing.Size(1256, 459);
            this.panelGridLokacije.TabIndex = 2;
            // 
            // dataGridViewLokacije
            // 
            this.dataGridViewLokacije.AllowUserToAddRows = false;
            this.dataGridViewLokacije.AllowUserToDeleteRows = false;
            this.dataGridViewLokacije.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewLokacije.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewLokacije.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewLokacije.ContextMenuStrip = this.contextMenuStripMeni;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewLokacije.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewLokacije.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewLokacije.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewLokacije.Name = "dataGridViewLokacije";
            this.dataGridViewLokacije.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewLokacije.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewLokacije.Size = new System.Drawing.Size(1256, 459);
            this.dataGridViewLokacije.TabIndex = 8;
            this.dataGridViewLokacije.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewLokacije_CellClick);
            this.dataGridViewLokacije.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridViewLokacije_CellMouseDown);
            this.dataGridViewLokacije.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dataGridViewLokacije_MouseDown);
            // 
            // contextMenuStripMeni
            // 
            this.contextMenuStripMeni.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dodajToolStripMenuItem1,
            this.obrišiToolStripMenuItem1});
            this.contextMenuStripMeni.Name = "contextMenuStripMeni";
            this.contextMenuStripMeni.Size = new System.Drawing.Size(106, 48);
            // 
            // dodajToolStripMenuItem1
            // 
            this.dodajToolStripMenuItem1.Name = "dodajToolStripMenuItem1";
            this.dodajToolStripMenuItem1.Size = new System.Drawing.Size(105, 22);
            this.dodajToolStripMenuItem1.Text = "Dodaj";
            this.dodajToolStripMenuItem1.Click += new System.EventHandler(this.dodajToolStripMenuItem1_Click);
            // 
            // obrišiToolStripMenuItem1
            // 
            this.obrišiToolStripMenuItem1.Name = "obrišiToolStripMenuItem1";
            this.obrišiToolStripMenuItem1.Size = new System.Drawing.Size(105, 22);
            this.obrišiToolStripMenuItem1.Text = "Obriši";
            this.obrišiToolStripMenuItem1.Click += new System.EventHandler(this.obrišiToolStripMenuItem1_Click);
            // 
            // tabPageCene
            // 
            this.tabPageCene.Controls.Add(this.tableLayoutPanelCene);
            this.tabPageCene.Location = new System.Drawing.Point(4, 29);
            this.tabPageCene.Name = "tabPageCene";
            this.tabPageCene.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageCene.Size = new System.Drawing.Size(1268, 661);
            this.tabPageCene.TabIndex = 1;
            this.tabPageCene.Text = "Cene";
            this.tabPageCene.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanelCene
            // 
            this.tableLayoutPanelCene.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.tableLayoutPanelCene.ColumnCount = 1;
            this.tableLayoutPanelCene.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelCene.Controls.Add(this.panelGornjiDeoCene, 0, 0);
            this.tableLayoutPanelCene.Controls.Add(this.panelZaGridCene, 0, 1);
            this.tableLayoutPanelCene.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelCene.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanelCene.Name = "tableLayoutPanelCene";
            this.tableLayoutPanelCene.RowCount = 2;
            this.tableLayoutPanelCene.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanelCene.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelCene.Size = new System.Drawing.Size(1262, 655);
            this.tableLayoutPanelCene.TabIndex = 0;
            // 
            // panelGornjiDeoCene
            // 
            this.panelGornjiDeoCene.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panelGornjiDeoCene.Controls.Add(this.btnUnos);
            this.panelGornjiDeoCene.Controls.Add(this.label6);
            this.panelGornjiDeoCene.Controls.Add(this.txtCena);
            this.panelGornjiDeoCene.Controls.Add(this.label5);
            this.panelGornjiDeoCene.Controls.Add(this.menuStripCene);
            this.panelGornjiDeoCene.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelGornjiDeoCene.Location = new System.Drawing.Point(3, 3);
            this.panelGornjiDeoCene.Name = "panelGornjiDeoCene";
            this.panelGornjiDeoCene.Size = new System.Drawing.Size(1256, 114);
            this.panelGornjiDeoCene.TabIndex = 0;
            // 
            // btnUnos
            // 
            this.btnUnos.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUnos.Location = new System.Drawing.Point(321, 34);
            this.btnUnos.Name = "btnUnos";
            this.btnUnos.Size = new System.Drawing.Size(127, 32);
            this.btnUnos.TabIndex = 1;
            this.btnUnos.Text = "Unos";
            this.btnUnos.UseVisualStyleBackColor = true;
            this.btnUnos.Click += new System.EventHandler(this.btnUnos_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(44, 81);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(172, 20);
            this.label6.TabIndex = 3;
            this.label6.Text = "Istorija cena za lokaciju";
            // 
            // txtCena
            // 
            this.txtCena.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCena.Location = new System.Drawing.Point(167, 38);
            this.txtCena.Name = "txtCena";
            this.txtCena.Size = new System.Drawing.Size(127, 24);
            this.txtCena.TabIndex = 0;
            this.txtCena.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCena_KeyDown);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(45, 41);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(79, 18);
            this.label5.TabIndex = 1;
            this.label5.Text = "Nova cena";
            // 
            // menuStripCene
            // 
            this.menuStripCene.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ceneToolStripMenuItem});
            this.menuStripCene.Location = new System.Drawing.Point(0, 0);
            this.menuStripCene.Name = "menuStripCene";
            this.menuStripCene.Size = new System.Drawing.Size(1256, 24);
            this.menuStripCene.TabIndex = 0;
            this.menuStripCene.Text = "menuStrip2";
            // 
            // ceneToolStripMenuItem
            // 
            this.ceneToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.excelToolStripMenuItem1});
            this.ceneToolStripMenuItem.Name = "ceneToolStripMenuItem";
            this.ceneToolStripMenuItem.Size = new System.Drawing.Size(46, 20);
            this.ceneToolStripMenuItem.Text = "Cene";
            // 
            // excelToolStripMenuItem1
            // 
            this.excelToolStripMenuItem1.Name = "excelToolStripMenuItem1";
            this.excelToolStripMenuItem1.Size = new System.Drawing.Size(100, 22);
            this.excelToolStripMenuItem1.Text = "Excel";
            this.excelToolStripMenuItem1.Click += new System.EventHandler(this.excelToolStripMenuItem1_Click);
            // 
            // panelZaGridCene
            // 
            this.panelZaGridCene.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panelZaGridCene.Controls.Add(this.dataGridViewCene);
            this.panelZaGridCene.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelZaGridCene.Location = new System.Drawing.Point(3, 123);
            this.panelZaGridCene.Name = "panelZaGridCene";
            this.panelZaGridCene.Size = new System.Drawing.Size(1256, 529);
            this.panelZaGridCene.TabIndex = 1;
            // 
            // dataGridViewCene
            // 
            this.dataGridViewCene.AllowUserToAddRows = false;
            this.dataGridViewCene.AllowUserToDeleteRows = false;
            this.dataGridViewCene.BackgroundColor = System.Drawing.SystemColors.ActiveCaption;
            this.dataGridViewCene.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewCene.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewCene.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewCene.Name = "dataGridViewCene";
            this.dataGridViewCene.ReadOnly = true;
            this.dataGridViewCene.Size = new System.Drawing.Size(1256, 529);
            this.dataGridViewCene.TabIndex = 2;
            this.dataGridViewCene.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewCene_CellClick);
            // 
            // frmLokacijeCene
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1276, 694);
            this.Controls.Add(this.tabControlGlavni);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmLokacijeCene";
            this.Text = "Lokacije i cene";
            this.Load += new System.EventHandler(this.frmLokacijeCene_Load);
            this.SizeChanged += new System.EventHandler(this.frmLokacijeCene_SizeChanged);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmLokacijeCene_KeyDown);
            this.tabControlGlavni.ResumeLayout(false);
            this.tabPageLokacija.ResumeLayout(false);
            this.tableLayoutPanelGlavniLokacije.ResumeLayout(false);
            this.panelPoljaZaUnosLokacija.ResumeLayout(false);
            this.panelPoljaZaUnosLokacija.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panelDugmadLokacije.ResumeLayout(false);
            this.panelGridLokacije.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewLokacije)).EndInit();
            this.contextMenuStripMeni.ResumeLayout(false);
            this.tabPageCene.ResumeLayout(false);
            this.tableLayoutPanelCene.ResumeLayout(false);
            this.panelGornjiDeoCene.ResumeLayout(false);
            this.panelGornjiDeoCene.PerformLayout();
            this.menuStripCene.ResumeLayout(false);
            this.menuStripCene.PerformLayout();
            this.panelZaGridCene.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCene)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControlGlavni;
        private System.Windows.Forms.TabPage tabPageLokacija;
        private System.Windows.Forms.TabPage tabPageCene;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelGlavniLokacije;
        private System.Windows.Forms.Panel panelPoljaZaUnosLokacija;
        private System.Windows.Forms.Panel panelDugmadLokacije;
        private System.Windows.Forms.Button btnIzlaz;
        private System.Windows.Forms.Panel panelGridLokacije;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem lokacijeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dodajToolStripMenuItem;
        private System.Windows.Forms.Button btnDodaj;
        private System.Windows.Forms.Button btnIzmeni;
        private System.Windows.Forms.Button btnObrisi;
        private System.Windows.Forms.DataGridView dataGridViewLokacije;
        private System.Windows.Forms.TextBox txtNazivLokacije;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtCenaStana;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtBrojMesta;
        private System.Windows.Forms.TextBox txtImeMasine;
        private System.Windows.Forms.ToolStripMenuItem izmeniToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem obrišiToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripMeni;
        private System.Windows.Forms.ToolStripMenuItem dodajToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem obrišiToolStripMenuItem1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelCene;
        private System.Windows.Forms.Panel panelGornjiDeoCene;
        private System.Windows.Forms.MenuStrip menuStripCene;
        private System.Windows.Forms.ToolStripMenuItem ceneToolStripMenuItem;
        private System.Windows.Forms.Panel panelZaGridCene;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtCena;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridView dataGridViewCene;
        private System.Windows.Forms.Button btnUnos;
        private System.Windows.Forms.ToolStripMenuItem excelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem excelToolStripMenuItem1;
    }
}