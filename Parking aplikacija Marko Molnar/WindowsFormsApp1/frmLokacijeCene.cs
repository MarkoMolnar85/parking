﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BusinessLayer;
using DataLayer.Models;

namespace WindowsFormsApp1
{
    public partial class frmLokacijeCene : Form
    {
        DataTable dtLokacije = new DataTable();
        DataTable dtCene = new DataTable();
        LokacijaBusiness lb = new LokacijaBusiness();
        CenaBusiness cb = new CenaBusiness();
        int idLokacije = 0;
        public frmLokacijeCene()
        {
            InitializeComponent();
            this.WindowState = FormWindowState.Maximized;
            KeyPreview = true;
        }

        private void frmLokacijeCene_Load(object sender, EventArgs e)
        {
            SrediDugmadLokacije();
            LoadLokacije();
            SakriKoloneLokacija();
            ResajzovanjeKolonaLokacije();

            LoadCene();
            SakriKoloneCene();
            ResajzovanjeKolonaCene();
        }

        private void SrediDugmadLokacije()
        {
            btnObrisi.Left = btnIzlaz.Left - btnObrisi.Width;
            btnIzmeni.Left = btnObrisi.Left - btnIzmeni.Width;
            btnDodaj.Left = btnIzmeni.Left - btnDodaj.Width;
        }

        private void frmLokacijeCene_SizeChanged(object sender, EventArgs e)
        {
            SrediDugmadLokacije();
            ResajzovanjeKolonaLokacije();
            ResajzovanjeKolonaCene();
        }
        private void LoadLokacije()
        {
            dtLokacije = lb.GetLokacijeDT();
            this.dataGridViewLokacije.DataSource = dtLokacije;
        }
        private void SakriKoloneLokacija()
        {
            this.dataGridViewLokacije.Columns["LokacijaID"].Visible = false;
        }
        private void ResajzovanjeKolonaLokacije()
        {
            if (this.dataGridViewLokacije.DataSource != null)
            {
                int brojKolona = 4;
                int sirinaKolone = (this.dataGridViewLokacije.Width - dataGridViewLokacije.RowHeadersWidth - 25) / brojKolona;
                dataGridViewLokacije.Columns["Naziv lokacije"].Width = sirinaKolone;
                dataGridViewLokacije.Columns["Broj mesta"].Width = sirinaKolone;
                dataGridViewLokacije.Columns["Broj slobodnih mesta"].Width = sirinaKolone;
                dataGridViewLokacije.Columns["Ime kompljutera"].Width = sirinaKolone;
            }

        }

        private void tabControlGlavni_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabControlGlavni.SelectedTab == tabControlGlavni.TabPages["TabPageLokacija"])
            {
                
                //LoadLokacije();
            }
            else if (tabControlGlavni.SelectedTab == tabControlGlavni.TabPages["TabPageCene"])
            {
                LoadCene();
                SakriKoloneCene();
                ResajzovanjeKolonaCene();
            }
        }

        private void dataGridViewLokacije_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int index = e.RowIndex;
            if (!(index < 0))
            {
                this.dataGridViewLokacije.Rows[index].Selected = true;
                idLokacije = int.Parse(this.dataGridViewLokacije.Rows[index].Cells["LokacijaID"].FormattedValue.ToString());
                Lokacija lo = lb.GetLokacijaPremaIDu(idLokacije);
                txtImeMasine.Text = lo.ImeMasine;
                txtNazivLokacije.Text = lo.Naziv;
                txtBrojMesta.Text = lo.BrojMesta.ToString();
            }
        }
        private bool ProveraUnosaPoljaLokacije()
        {
            bool sveOKuneto = true;
            if (string.IsNullOrWhiteSpace(txtBrojMesta.Text) || string.IsNullOrWhiteSpace(txtCenaStana.Text)
                || string.IsNullOrWhiteSpace(txtImeMasine.Text) || string.IsNullOrWhiteSpace(txtNazivLokacije.Text))
            {
                MessageBox.Show("Sva polja moraju biti popunjena");
                sveOKuneto = false;
                return sveOKuneto;
            }
            int brojMesta = 0;
            int cenaSata = 0;
            bool brojMestaProvera = int.TryParse(txtBrojMesta.Text, out brojMesta);
            bool cenaSataProvera = int.TryParse(txtCenaStana.Text, out cenaSata);

            if (!brojMestaProvera)
            {
                MessageBox.Show("Broj mesta mora da bude broj");
                sveOKuneto = false;
                return sveOKuneto;
            }
            if (!cenaSataProvera)
            {
                MessageBox.Show("Cena sata mora da bude broj");
                sveOKuneto = false;
                return sveOKuneto;
            }
            return sveOKuneto;
        }
        private bool ProveraUnosaPoljaLokacijeZaUpdate()
        {
            bool sveOKuneto = true;
            if (string.IsNullOrWhiteSpace(txtBrojMesta.Text) || string.IsNullOrWhiteSpace(txtImeMasine.Text) || string.IsNullOrWhiteSpace(txtNazivLokacije.Text))
            {
                MessageBox.Show("Sva polja moraju biti popunjena");
                sveOKuneto = false;
                return sveOKuneto;
            }
            int brojMesta = 0;
            bool brojMestaProvera = int.TryParse(txtBrojMesta.Text, out brojMesta);

            if (!brojMestaProvera)
            {
                MessageBox.Show("Broj mesta mora da bude broj");
                sveOKuneto = false;
                return sveOKuneto;
            }
            return sveOKuneto;
        }

        private void btnDodaj_Click(object sender, EventArgs e)
        {
            DodajLokaciju();
        }
        private void DodajLokaciju()
        {
            if (ProveraUnosaPoljaLokacije())
            {
                Lokacija lok = new Lokacija();
                lok.Naziv = txtNazivLokacije.Text.ToUpper();
                lok.ImeMasine = txtImeMasine.Text.ToUpper();
                lok.BrojMesta = int.Parse(txtBrojMesta.Text); 
                int cenaSata = int.Parse(txtCenaStana.Text);

                bool postojiMasina = lb.PostojiLokacijaSaImenomMasine(lok.ImeMasine);
                bool postojiNazivLokacije = lb.PostojiLokacijaSaImenomLokacije(lok.Naziv);
                if (postojiMasina)
                {
                    MessageBox.Show("Već postoji lokacija sa ovim imenom računara");
                }
                else if (postojiNazivLokacije)
                {
                    MessageBox.Show("Već postoji lokacija sa ovim imenom lokacije");
                }
                else
                {
                    NadzornikBusiness nb = new NadzornikBusiness();
                    //izbrisati blok koda. blok sluzi samo da ne moram da otvaram login stranu
                    //if (Program.korisnickoIme == "" || string.IsNullOrEmpty(Program.korisnickoIme))
                    //{
                    //    Program.korisnickoIme = "MOLNAR";
                    //}
                    Nadzornik nad = nb.GetNadzornikNaOsnovuKorisnickogImena(Program.korisnickoIme);

                    int uspehInserta = lb.InserLokacijeSaCenom(lok, cenaSata, nad.NadzornikID);
                    if (uspehInserta == 0)
                    {
                        MessageBox.Show("Dodavanje nije uspelo");
                    }
                    else
                    {
                        DataRow noviRed = dtLokacije.NewRow();
                        noviRed["LokacijaID"] = lb.GetZadnjiIdentLokacije();
                        noviRed["Naziv lokacije"] = lok.Naziv;
                        noviRed["Broj mesta"] = lok.BrojMesta;
                        noviRed["Broj slobodnih mesta"] = lok.BrojMesta;
                        noviRed["Ime kompljutera"] = lok.ImeMasine;
                        dtLokacije.Rows.InsertAt(noviRed, 0);
                        dtLokacije.AcceptChanges();

                        
                    }
                }
            }
        }

        private void btnIzmeni_Click(object sender, EventArgs e)
        {
            IzmenaLokacije();
        }
        private void IzmenaLokacije()
        {
            if (ProveraUnosaPoljaLokacijeZaUpdate())
            {
                Lokacija lok = new Lokacija();
                lok.ID = idLokacije;
                lok.Naziv = txtNazivLokacije.Text.ToUpper();
                lok.ImeMasine = txtImeMasine.Text.ToUpper();
                lok.BrojMesta = int.Parse(txtBrojMesta.Text);

                if (lb.PostojiLiLokacijaSaImenomMasineIliNazivomLokacijeProveraZaUpdate(lok))
                {
                    MessageBox.Show("Izmena nije moguća, potrebo je da ime računara i naziv lokacije budu jedinstveni");
                }
                else
                {
                    int uspesanUpdate = lb.UpdateLokacije(lok);
                    if (uspesanUpdate == 0)
                    {
                        MessageBox.Show("Izmena nije uspela");
                    }
                    else
                    {
                        for (int i = 0; i < dtLokacije.Rows.Count; i++)
                        {
                            DataRow red = dtLokacije.Rows[i];
                            if (red["LokacijaID"].ToString() == lok.ID.ToString())
                            {
                                red["Naziv lokacije"] = lok.Naziv;
                                red["Broj mesta"] = lok.BrojMesta;
                                red["Ime kompljutera"] = lok.ImeMasine;
                            }
                        }
                        dtLokacije.AcceptChanges();
                    }
                }
            }
        }

        private void btnObrisi_Click(object sender, EventArgs e)
        {
            ObrisiLokaciju();
        }
        private void ObrisiLokaciju()
        {
            if (lb.DaLiJeMoguceBrisanjeLokacijeKolikoImaDece(idLokacije) > 0)
            {
                MessageBox.Show("Brisanje nije moguće");
            }
            else
            {
                int uspesnostBrisanja = lb.ObrisiSveCeneZaLokaciju(idLokacije);
                if (uspesnostBrisanja == 0)
                {
                    MessageBox.Show("Brisanje nije uspelo");
                }
                else
                {
                    int uspesnostBrisanjaLokacije = lb.DeleteLokaciju(idLokacije);
                    if (uspesnostBrisanja > 0)
                    {
                        for (int i = 0; i < dtLokacije.Rows.Count; i++)
                        {
                            DataRow red = dtLokacije.Rows[i];
                            if (red["LokacijaID"].ToString() == idLokacije.ToString())
                            {
                                red.Delete();
                            }
                        }
                        dtLokacije.AcceptChanges();
                    }
                    else
                    {
                        MessageBox.Show("Brisanje nije uspelo");
                    }
                }
            }
        }

        private void btnIzlaz_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dodajToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DodajLokaciju();
        }

        private void izmeniToolStripMenuItem_Click(object sender, EventArgs e)
        {
            IzmenaLokacije();
        }

        private void obrišiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ObrisiLokaciju();
        }

        private void dodajToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            DodajLokaciju();
        }

        private void obrišiToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            ObrisiLokaciju();
        }

        private void dataGridViewLokacije_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                
            }
        }

        private void dataGridViewLokacije_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                int index = e.RowIndex;
                if (!(index < 0))
                {
                    this.dataGridViewLokacije.ClearSelection();
                    dataGridViewLokacije.Rows[index].Selected = true;
                    idLokacije = int.Parse(dataGridViewLokacije.Rows[index].Cells["LokacijaID"].FormattedValue.ToString());
                }
            }
        }

        private void LoadCene()
        {
            dtCene = cb.GetAllCenePremaIDLokacije(Program.idLokacijeIzKomboBoxaKodFormeZaNadzornika);
            this.dataGridViewCene.DataSource = dtCene;
        }

        private void SakriKoloneCene()
        {
            this.dataGridViewCene.Columns["cenaID"].Visible = false;
            this.dataGridViewCene.Columns["lokacijaID"].Visible = false;
            this.dataGridViewCene.Columns["nadzornikID"].Visible = false;
        }
        private void ResajzovanjeKolonaCene()
        {
            if (dataGridViewCene.DataSource != null)
            {
                int brojKolona = 2;
                int sirinaKolone = (this.dataGridViewCene.Width - dataGridViewCene.RowHeadersWidth - 25) / brojKolona;

                dataGridViewCene.Columns["Iznos cene"].Width = sirinaKolone;
                dataGridViewCene.Columns["Nadzornik postavio cenu"].Width = sirinaKolone;
            }
        }

        private void dataGridViewCene_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int red = e.RowIndex;
            if (!(red < 0))
            {
                this.dataGridViewCene.Rows[red].Selected = true;
            }
        }

        private void btnUnos_Click(object sender, EventArgs e)
        {
            UnosCene();
        }
        private void UnosCene()
        {
            Cena cena = new Cena();
            int cenaIznos = 0;
            bool provera = int.TryParse(txtCena.Text, out cenaIznos);
            if (!provera)
            {
                MessageBox.Show("Iznos mora da bude ceo broj");
                return;
            }
            cena.CenaIznos = cenaIznos;
            cena.LokacijaID = Program.idLokacijeIzKomboBoxaKodFormeZaNadzornika;

            NadzornikBusiness nb = new NadzornikBusiness();
            Nadzornik nad = nb.GetNadzornikNaOsnovuKorisnickogImena(Program.korisnickoIme);

            cena.NadzornikID = nad.NadzornikID;
            
            int uspehInserta = cb.InsertCene(cena);
            if (uspehInserta == 0)
            {
                MessageBox.Show("Upis cene nije uspeo");
            }
            else
            {
                DataRow red = dtCene.NewRow();
                red["cenaID"] = cena.CenaID;
                red["Iznos cene"] = cena.CenaIznos;
                red["lokacijaID"] = cena.LokacijaID;
                red["nadzornikID"] = cena.CenaID;
                red["Nadzornik postavio cenu"] = nad.Korisnicko;
                dtCene.Rows.Add(red);
                dtCene.AcceptChanges();
            }
        }

        private void txtCena_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 13)
            {
                UnosCene();
            }
        }

        private void frmLokacijeCene_KeyDown(object sender, KeyEventArgs e)
        {
            if (tabControlGlavni.SelectedTab == tabControlGlavni.TabPages["TabPageLokacija"])
            {
                if (e.KeyCode == Keys.F12)
                {
                    ExportGridaExcelLokacije(dataGridViewLokacije);
                }
            }
            if (tabControlGlavni.SelectedTab == tabControlGlavni.TabPages["tabPageCene"])
            {
                if (e.KeyCode == Keys.F12)
                {
                    ExportGridaExcelCene(this.dataGridViewCene);
                }
            }
        }

        private void excelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ExportGridaExcelLokacije(dataGridViewLokacije);
        }

        private void excelToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            ExportGridaExcelCene(this.dataGridViewCene);
        }

        private void ExportGridaExcelLokacije(DataGridView dataGridViewPrikaz)
        {
            //kolona 1 je nevidljiva odnosno 0

            // Creating a Excel object. 
            
            Microsoft.Office.Interop.Excel._Application excel = new Microsoft.Office.Interop.Excel.Application();
            Microsoft.Office.Interop.Excel._Workbook workbook = excel.Workbooks.Add(Type.Missing);
            Microsoft.Office.Interop.Excel._Worksheet worksheet = workbook.Worksheets.Add();

            string putanja = "";
            try
            {

                worksheet = workbook.ActiveSheet;

                worksheet.Name = "Lokacije cene";

                int indexKolone = 1;
                int indexReda = 2;

                for (int i = 0; i < dataGridViewPrikaz.Columns.Count; i++)
                {
                    if (!(i == 0))
                    {
                        worksheet.Cells[1, indexKolone] = dataGridViewPrikaz.Columns[i].HeaderText;
                        indexKolone++;
                    }
                }

                indexKolone = 1;

                for (int i = 0; i < dataGridViewPrikaz.Rows.Count; i++)
                {
                    for (int j = 0; j < dataGridViewPrikaz.Columns.Count; j++)
                    {
                        if (!(j == 0))
                        {
                            worksheet.Cells[indexReda, indexKolone] = dataGridViewPrikaz.Rows[i].Cells[j].Value.ToString();
                            indexKolone++;
                        }
                    }
                    indexKolone = 1;
                    indexReda++;
                }


                //Getting the location and file name of the excel to save from user. 
                SaveFileDialog saveDialog = new SaveFileDialog();
                saveDialog.Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
                saveDialog.FilterIndex = 2;

                if (saveDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    workbook.SaveAs(saveDialog.FileName);
                    MessageBox.Show("Excel datoteka je uspeno izvezena");
                    putanja = saveDialog.FileName;
                }

            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                workbook.Close(false);
                excel.Quit();
                workbook = null;
                excel = null;
                GC.Collect();
                UbiProces(putanja);
            }
        }

        private void ExportGridaExcelCene(DataGridView dataGridViewPrikaz)
        {
            //nevidljlive kolone su 1, 3, 4 odnosno 0, 2, 3

            // Creating a Excel object. 

            Microsoft.Office.Interop.Excel._Application excel = new Microsoft.Office.Interop.Excel.Application();
            Microsoft.Office.Interop.Excel._Workbook workbook = excel.Workbooks.Add(Type.Missing);
            Microsoft.Office.Interop.Excel._Worksheet worksheet = workbook.Worksheets.Add();

            string putanja = "";
            try
            {

                worksheet = workbook.ActiveSheet;

                worksheet.Name = "Lokacije cene";

                int indexKolone = 1;
                int indexReda = 2;

                for (int i = 0; i < dataGridViewPrikaz.Columns.Count; i++)
                {
                    if (!(i == 0 || i == 2 || i ==3 ))
                    {
                        worksheet.Cells[1, indexKolone] = dataGridViewPrikaz.Columns[i].HeaderText;
                        indexKolone++;
                    }
                }

                indexKolone = 1;

                for (int i = 0; i < dataGridViewPrikaz.Rows.Count; i++)
                {
                    for (int j = 0; j < dataGridViewPrikaz.Columns.Count; j++)
                    {
                        if (!(j == 0 || j == 2 || j ==3))
                        {
                            worksheet.Cells[indexReda, indexKolone] = dataGridViewPrikaz.Rows[i].Cells[j].Value.ToString();
                            indexKolone++;
                        }
                    }
                    indexKolone = 1;
                    indexReda++;
                }


                //Getting the location and file name of the excel to save from user. 
                SaveFileDialog saveDialog = new SaveFileDialog();
                saveDialog.Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
                saveDialog.FilterIndex = 2;

                if (saveDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    workbook.SaveAs(saveDialog.FileName);
                    MessageBox.Show("Excel datoteka je uspeno izvezena");
                    putanja = saveDialog.FileName;
                }

            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                workbook.Close(false);
                excel.Quit();
                workbook = null;
                excel = null;
                GC.Collect();
                UbiProces(putanja);
            }
        }

        private void UbiProces(string putanja)
        {
            System.Diagnostics.Process[] process = System.Diagnostics.Process.GetProcessesByName("Excel");
            foreach (System.Diagnostics.Process p in process)
            {
                if (!string.IsNullOrEmpty(p.ProcessName))
                {
                    try
                    {
                        p.Kill();
                    }
                    catch { }
                }
            }
        }

        private void panelPoljaZaUnosLokacija_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
