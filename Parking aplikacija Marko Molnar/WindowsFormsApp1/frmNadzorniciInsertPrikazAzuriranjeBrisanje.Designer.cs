﻿namespace WindowsFormsApp1
{
    partial class frmNadzorniciInsertPrikazAzuriranjeBrisanje
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.nadzorniciToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dodajToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.izmeniToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.izbrišiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.excelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tableLayoutPanelGlavni = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBoxFilteriZaNadzornika = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblIme = new System.Windows.Forms.Label();
            this.btnPretragaNadzornika = new System.Windows.Forms.Button();
            this.txtTelefon = new System.Windows.Forms.TextBox();
            this.txtMesto = new System.Windows.Forms.TextBox();
            this.txtUlica = new System.Windows.Forms.TextBox();
            this.txtKorisnicko = new System.Windows.Forms.TextBox();
            this.txtIme = new System.Windows.Forms.TextBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.dataGridViewNadzornici = new System.Windows.Forms.DataGridView();
            this.contextMenuStripOperacijaNadNadzornikom = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.dodajToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.izmeniToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.obrišiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tableLayoutSredina = new System.Windows.Forms.TableLayoutPanel();
            this.panelDugmici = new System.Windows.Forms.Panel();
            this.btnIzlaz = new System.Windows.Forms.Button();
            this.btnIzbrisi = new System.Windows.Forms.Button();
            this.btnIzmeni = new System.Windows.Forms.Button();
            this.btnDodaj = new System.Windows.Forms.Button();
            this.panelLevoOdDugmica = new System.Windows.Forms.Panel();
            this.lblPodaciONadyorniku = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.tableLayoutPanelGlavni.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBoxFilteriZaNadzornika.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewNadzornici)).BeginInit();
            this.contextMenuStripOperacijaNadNadzornikom.SuspendLayout();
            this.tableLayoutSredina.SuspendLayout();
            this.panelDugmici.SuspendLayout();
            this.panelLevoOdDugmica.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nadzorniciToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1276, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // nadzorniciToolStripMenuItem
            // 
            this.nadzorniciToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dodajToolStripMenuItem,
            this.izmeniToolStripMenuItem,
            this.izbrišiToolStripMenuItem,
            this.excelToolStripMenuItem});
            this.nadzorniciToolStripMenuItem.Name = "nadzorniciToolStripMenuItem";
            this.nadzorniciToolStripMenuItem.Size = new System.Drawing.Size(76, 20);
            this.nadzorniciToolStripMenuItem.Text = "Nadzornici";
            // 
            // dodajToolStripMenuItem
            // 
            this.dodajToolStripMenuItem.Name = "dodajToolStripMenuItem";
            this.dodajToolStripMenuItem.Size = new System.Drawing.Size(109, 22);
            this.dodajToolStripMenuItem.Text = "Dodaj";
            this.dodajToolStripMenuItem.Click += new System.EventHandler(this.dodajToolStripMenuItem_Click);
            // 
            // izmeniToolStripMenuItem
            // 
            this.izmeniToolStripMenuItem.Name = "izmeniToolStripMenuItem";
            this.izmeniToolStripMenuItem.Size = new System.Drawing.Size(109, 22);
            this.izmeniToolStripMenuItem.Text = "Izmeni";
            this.izmeniToolStripMenuItem.Click += new System.EventHandler(this.izmeniToolStripMenuItem_Click);
            // 
            // izbrišiToolStripMenuItem
            // 
            this.izbrišiToolStripMenuItem.Name = "izbrišiToolStripMenuItem";
            this.izbrišiToolStripMenuItem.Size = new System.Drawing.Size(109, 22);
            this.izbrišiToolStripMenuItem.Text = "Izbriši";
            this.izbrišiToolStripMenuItem.Click += new System.EventHandler(this.izbrišiToolStripMenuItem_Click);
            // 
            // excelToolStripMenuItem
            // 
            this.excelToolStripMenuItem.Name = "excelToolStripMenuItem";
            this.excelToolStripMenuItem.Size = new System.Drawing.Size(109, 22);
            this.excelToolStripMenuItem.Text = "Excel";
            this.excelToolStripMenuItem.Click += new System.EventHandler(this.excelToolStripMenuItem_Click);
            // 
            // tableLayoutPanelGlavni
            // 
            this.tableLayoutPanelGlavni.ColumnCount = 1;
            this.tableLayoutPanelGlavni.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelGlavni.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanelGlavni.Controls.Add(this.panel4, 0, 2);
            this.tableLayoutPanelGlavni.Controls.Add(this.tableLayoutSredina, 0, 1);
            this.tableLayoutPanelGlavni.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelGlavni.Location = new System.Drawing.Point(0, 24);
            this.tableLayoutPanelGlavni.Name = "tableLayoutPanelGlavni";
            this.tableLayoutPanelGlavni.RowCount = 3;
            this.tableLayoutPanelGlavni.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanelGlavni.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanelGlavni.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelGlavni.Size = new System.Drawing.Size(1276, 670);
            this.tableLayoutPanelGlavni.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panel1.Controls.Add(this.groupBoxFilteriZaNadzornika);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1270, 114);
            this.panel1.TabIndex = 1;
            // 
            // groupBoxFilteriZaNadzornika
            // 
            this.groupBoxFilteriZaNadzornika.Controls.Add(this.label5);
            this.groupBoxFilteriZaNadzornika.Controls.Add(this.label4);
            this.groupBoxFilteriZaNadzornika.Controls.Add(this.label3);
            this.groupBoxFilteriZaNadzornika.Controls.Add(this.label2);
            this.groupBoxFilteriZaNadzornika.Controls.Add(this.lblIme);
            this.groupBoxFilteriZaNadzornika.Controls.Add(this.btnPretragaNadzornika);
            this.groupBoxFilteriZaNadzornika.Controls.Add(this.txtTelefon);
            this.groupBoxFilteriZaNadzornika.Controls.Add(this.txtMesto);
            this.groupBoxFilteriZaNadzornika.Controls.Add(this.txtUlica);
            this.groupBoxFilteriZaNadzornika.Controls.Add(this.txtKorisnicko);
            this.groupBoxFilteriZaNadzornika.Controls.Add(this.txtIme);
            this.groupBoxFilteriZaNadzornika.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxFilteriZaNadzornika.Location = new System.Drawing.Point(18, 14);
            this.groupBoxFilteriZaNadzornika.Name = "groupBoxFilteriZaNadzornika";
            this.groupBoxFilteriZaNadzornika.Size = new System.Drawing.Size(998, 87);
            this.groupBoxFilteriZaNadzornika.TabIndex = 0;
            this.groupBoxFilteriZaNadzornika.TabStop = false;
            this.groupBoxFilteriZaNadzornika.Text = "Pretraga nadzornika";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(592, 29);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 18);
            this.label5.TabIndex = 11;
            this.label5.Text = "Telefon";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(329, 63);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 18);
            this.label4.TabIndex = 10;
            this.label4.Text = "Mesto";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(329, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 18);
            this.label3.TabIndex = 9;
            this.label3.Text = "Ulica";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(27, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 18);
            this.label2.TabIndex = 8;
            this.label2.Text = "Korisničko";
            // 
            // lblIme
            // 
            this.lblIme.AutoSize = true;
            this.lblIme.Location = new System.Drawing.Point(27, 29);
            this.lblIme.Name = "lblIme";
            this.lblIme.Size = new System.Drawing.Size(32, 18);
            this.lblIme.TabIndex = 7;
            this.lblIme.Text = "Ime";
            // 
            // btnPretragaNadzornika
            // 
            this.btnPretragaNadzornika.Location = new System.Drawing.Point(709, 55);
            this.btnPretragaNadzornika.Name = "btnPretragaNadzornika";
            this.btnPretragaNadzornika.Size = new System.Drawing.Size(118, 26);
            this.btnPretragaNadzornika.TabIndex = 5;
            this.btnPretragaNadzornika.Text = "Pretraga";
            this.btnPretragaNadzornika.UseVisualStyleBackColor = true;
            this.btnPretragaNadzornika.Click += new System.EventHandler(this.btnPretragaNadzornika_Click);
            // 
            // txtTelefon
            // 
            this.txtTelefon.Location = new System.Drawing.Point(709, 23);
            this.txtTelefon.Name = "txtTelefon";
            this.txtTelefon.Size = new System.Drawing.Size(118, 24);
            this.txtTelefon.TabIndex = 4;
            // 
            // txtMesto
            // 
            this.txtMesto.Location = new System.Drawing.Point(458, 57);
            this.txtMesto.Name = "txtMesto";
            this.txtMesto.Size = new System.Drawing.Size(118, 24);
            this.txtMesto.TabIndex = 3;
            // 
            // txtUlica
            // 
            this.txtUlica.Location = new System.Drawing.Point(458, 23);
            this.txtUlica.Name = "txtUlica";
            this.txtUlica.Size = new System.Drawing.Size(118, 24);
            this.txtUlica.TabIndex = 2;
            // 
            // txtKorisnicko
            // 
            this.txtKorisnicko.Location = new System.Drawing.Point(178, 53);
            this.txtKorisnicko.Name = "txtKorisnicko";
            this.txtKorisnicko.Size = new System.Drawing.Size(118, 24);
            this.txtKorisnicko.TabIndex = 1;
            // 
            // txtIme
            // 
            this.txtIme.Location = new System.Drawing.Point(178, 23);
            this.txtIme.Name = "txtIme";
            this.txtIme.Size = new System.Drawing.Size(118, 24);
            this.txtIme.TabIndex = 0;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.panel4.Controls.Add(this.dataGridViewNadzornici);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(3, 173);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1270, 494);
            this.panel4.TabIndex = 2;
            // 
            // dataGridViewNadzornici
            // 
            this.dataGridViewNadzornici.AllowUserToAddRows = false;
            this.dataGridViewNadzornici.AllowUserToDeleteRows = false;
            this.dataGridViewNadzornici.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataGridViewNadzornici.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewNadzornici.ContextMenuStrip = this.contextMenuStripOperacijaNadNadzornikom;
            this.dataGridViewNadzornici.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewNadzornici.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewNadzornici.Name = "dataGridViewNadzornici";
            this.dataGridViewNadzornici.ReadOnly = true;
            this.dataGridViewNadzornici.Size = new System.Drawing.Size(1270, 494);
            this.dataGridViewNadzornici.TabIndex = 10;
            this.dataGridViewNadzornici.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewNadzornici_CellClick);
            this.dataGridViewNadzornici.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridViewNadzornici_CellMouseDown);
            this.dataGridViewNadzornici.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridViewNadzornici_RowHeaderMouseClick);
            // 
            // contextMenuStripOperacijaNadNadzornikom
            // 
            this.contextMenuStripOperacijaNadNadzornikom.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dodajToolStripMenuItem1,
            this.izmeniToolStripMenuItem1,
            this.obrišiToolStripMenuItem});
            this.contextMenuStripOperacijaNadNadzornikom.Name = "contextMenuStripOperacijaNadNadzornikom";
            this.contextMenuStripOperacijaNadNadzornikom.Size = new System.Drawing.Size(110, 70);
            // 
            // dodajToolStripMenuItem1
            // 
            this.dodajToolStripMenuItem1.Name = "dodajToolStripMenuItem1";
            this.dodajToolStripMenuItem1.Size = new System.Drawing.Size(109, 22);
            this.dodajToolStripMenuItem1.Text = "Dodaj";
            this.dodajToolStripMenuItem1.Click += new System.EventHandler(this.dodajToolStripMenuItem1_Click);
            // 
            // izmeniToolStripMenuItem1
            // 
            this.izmeniToolStripMenuItem1.Name = "izmeniToolStripMenuItem1";
            this.izmeniToolStripMenuItem1.Size = new System.Drawing.Size(109, 22);
            this.izmeniToolStripMenuItem1.Text = "Izmeni";
            this.izmeniToolStripMenuItem1.Click += new System.EventHandler(this.izmeniToolStripMenuItem1_Click);
            // 
            // obrišiToolStripMenuItem
            // 
            this.obrišiToolStripMenuItem.Name = "obrišiToolStripMenuItem";
            this.obrišiToolStripMenuItem.Size = new System.Drawing.Size(109, 22);
            this.obrišiToolStripMenuItem.Text = "Obriši";
            this.obrišiToolStripMenuItem.Click += new System.EventHandler(this.obrišiToolStripMenuItem_Click);
            // 
            // tableLayoutSredina
            // 
            this.tableLayoutSredina.ColumnCount = 2;
            this.tableLayoutSredina.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutSredina.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 500F));
            this.tableLayoutSredina.Controls.Add(this.panelDugmici, 1, 0);
            this.tableLayoutSredina.Controls.Add(this.panelLevoOdDugmica, 0, 0);
            this.tableLayoutSredina.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutSredina.Location = new System.Drawing.Point(3, 123);
            this.tableLayoutSredina.Name = "tableLayoutSredina";
            this.tableLayoutSredina.RowCount = 1;
            this.tableLayoutSredina.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutSredina.Size = new System.Drawing.Size(1270, 44);
            this.tableLayoutSredina.TabIndex = 3;
            // 
            // panelDugmici
            // 
            this.panelDugmici.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panelDugmici.Controls.Add(this.btnIzlaz);
            this.panelDugmici.Controls.Add(this.btnIzbrisi);
            this.panelDugmici.Controls.Add(this.btnIzmeni);
            this.panelDugmici.Controls.Add(this.btnDodaj);
            this.panelDugmici.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelDugmici.Location = new System.Drawing.Point(773, 3);
            this.panelDugmici.Name = "panelDugmici";
            this.panelDugmici.Size = new System.Drawing.Size(494, 38);
            this.panelDugmici.TabIndex = 0;
            // 
            // btnIzlaz
            // 
            this.btnIzlaz.Location = new System.Drawing.Point(369, 0);
            this.btnIzlaz.Name = "btnIzlaz";
            this.btnIzlaz.Size = new System.Drawing.Size(125, 38);
            this.btnIzlaz.TabIndex = 9;
            this.btnIzlaz.Text = "Izlaz";
            this.btnIzlaz.UseVisualStyleBackColor = true;
            this.btnIzlaz.Click += new System.EventHandler(this.btnIzlaz_Click);
            // 
            // btnIzbrisi
            // 
            this.btnIzbrisi.Location = new System.Drawing.Point(249, 0);
            this.btnIzbrisi.Name = "btnIzbrisi";
            this.btnIzbrisi.Size = new System.Drawing.Size(125, 38);
            this.btnIzbrisi.TabIndex = 8;
            this.btnIzbrisi.Text = "Izbriši";
            this.btnIzbrisi.UseVisualStyleBackColor = true;
            this.btnIzbrisi.Click += new System.EventHandler(this.btnIzbrisi_Click);
            // 
            // btnIzmeni
            // 
            this.btnIzmeni.Location = new System.Drawing.Point(118, 0);
            this.btnIzmeni.Name = "btnIzmeni";
            this.btnIzmeni.Size = new System.Drawing.Size(125, 38);
            this.btnIzmeni.TabIndex = 7;
            this.btnIzmeni.Text = "Izmeni";
            this.btnIzmeni.UseVisualStyleBackColor = true;
            this.btnIzmeni.Click += new System.EventHandler(this.btnIzmeni_Click);
            // 
            // btnDodaj
            // 
            this.btnDodaj.Location = new System.Drawing.Point(0, 0);
            this.btnDodaj.Name = "btnDodaj";
            this.btnDodaj.Size = new System.Drawing.Size(125, 38);
            this.btnDodaj.TabIndex = 6;
            this.btnDodaj.Text = "Dodaj";
            this.btnDodaj.UseVisualStyleBackColor = true;
            this.btnDodaj.Click += new System.EventHandler(this.btnDodaj_Click);
            // 
            // panelLevoOdDugmica
            // 
            this.panelLevoOdDugmica.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panelLevoOdDugmica.Controls.Add(this.lblPodaciONadyorniku);
            this.panelLevoOdDugmica.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelLevoOdDugmica.Location = new System.Drawing.Point(3, 3);
            this.panelLevoOdDugmica.Name = "panelLevoOdDugmica";
            this.panelLevoOdDugmica.Size = new System.Drawing.Size(764, 38);
            this.panelLevoOdDugmica.TabIndex = 1;
            // 
            // lblPodaciONadyorniku
            // 
            this.lblPodaciONadyorniku.AutoSize = true;
            this.lblPodaciONadyorniku.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPodaciONadyorniku.Location = new System.Drawing.Point(55, 8);
            this.lblPodaciONadyorniku.Name = "lblPodaciONadyorniku";
            this.lblPodaciONadyorniku.Size = new System.Drawing.Size(152, 20);
            this.lblPodaciONadyorniku.TabIndex = 0;
            this.lblPodaciONadyorniku.Text = "Podaci o nadzorniku";
            // 
            // frmNadzorniciInsertPrikazAzuriranjeBrisanje
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1276, 694);
            this.Controls.Add(this.tableLayoutPanelGlavni);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmNadzorniciInsertPrikazAzuriranjeBrisanje";
            this.Text = "Nadzornici";
            this.Load += new System.EventHandler(this.frmNadzorniciInsertPrikazAzuriranjeBrisanje_Load);
            this.SizeChanged += new System.EventHandler(this.frmNadzorniciInsertPrikazAzuriranjeBrisanje_SizeChanged);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmNadzorniciInsertPrikazAzuriranjeBrisanje_KeyDown);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tableLayoutPanelGlavni.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.groupBoxFilteriZaNadzornika.ResumeLayout(false);
            this.groupBoxFilteriZaNadzornika.PerformLayout();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewNadzornici)).EndInit();
            this.contextMenuStripOperacijaNadNadzornikom.ResumeLayout(false);
            this.tableLayoutSredina.ResumeLayout(false);
            this.panelDugmici.ResumeLayout(false);
            this.panelLevoOdDugmica.ResumeLayout(false);
            this.panelLevoOdDugmica.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem nadzorniciToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dodajToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem izmeniToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem izbrišiToolStripMenuItem;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelGlavni;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btnIzbrisi;
        private System.Windows.Forms.Button btnDodaj;
        private System.Windows.Forms.Button btnIzmeni;
        private System.Windows.Forms.TableLayoutPanel tableLayoutSredina;
        private System.Windows.Forms.Panel panelDugmici;
        private System.Windows.Forms.Button btnIzlaz;
        private System.Windows.Forms.Panel panelLevoOdDugmica;
        private System.Windows.Forms.Label lblPodaciONadyorniku;
        private System.Windows.Forms.DataGridView dataGridViewNadzornici;
        private System.Windows.Forms.GroupBox groupBoxFilteriZaNadzornika;
        private System.Windows.Forms.TextBox txtMesto;
        private System.Windows.Forms.TextBox txtUlica;
        private System.Windows.Forms.TextBox txtKorisnicko;
        private System.Windows.Forms.TextBox txtIme;
        private System.Windows.Forms.TextBox txtTelefon;
        private System.Windows.Forms.Button btnPretragaNadzornika;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblIme;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripOperacijaNadNadzornikom;
        private System.Windows.Forms.ToolStripMenuItem dodajToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem izmeniToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem obrišiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem excelToolStripMenuItem;
    }
}