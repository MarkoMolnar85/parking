﻿using BusinessLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class frmNadzorniciInsertPrikazAzuriranjeBrisanje : Form
    {
        DataTable NadzornikDT = new DataTable();
        int idNadzornikaDobijenIzGrida = 0;
        internal static int uspehInsertaNadzornikaUPartialu = 0;
        internal static Nadzornik DodatiNadzornikUPartialu = new Nadzornik();
        public frmNadzorniciInsertPrikazAzuriranjeBrisanje()
        {
            InitializeComponent();
            this.WindowState = FormWindowState.Maximized;
            KeyPreview = true;
        }

        private void frmNadzorniciInsertPrikazAzuriranjeBrisanje_Load(object sender, EventArgs e)
        {
            UrediDugmad();
            LoadNadzornici();
            PreimenujKolone();
            SakriKolone();
            ResajzovanjeKolona();
        }

        private void UrediDugmad()
        {
            btnDodaj.Width = 124;
            btnIzmeni.Width = 124;
            btnIzbrisi.Width = 124;
            btnIzlaz.Width = 123;
            btnDodaj.Left = 0;
            btnIzmeni.Left = btnDodaj.Right;
            btnIzbrisi.Left = btnIzmeni.Right;
            btnIzlaz.Left = btnIzbrisi.Right;
        }

        private void LoadNadzornici()
        {
            NadzornikBusiness nb = new NadzornikBusiness();
            NadzornikDT = nb.GetSviNadzornici();
            dataGridViewNadzornici.DataSource = NadzornikDT;
        }

        private void PreimenujKolone()
        {
            dataGridViewNadzornici.Columns["NadzornikID"].HeaderText = "ID";
            dataGridViewNadzornici.Columns["Ime"].HeaderText = "Ime";
            dataGridViewNadzornici.Columns["Korisnicko"].HeaderText = "Korisničko";
            dataGridViewNadzornici.Columns["Ulica"].HeaderText = "Ulica";
            dataGridViewNadzornici.Columns["Mesto"].HeaderText = "Mesto";
            dataGridViewNadzornici.Columns["Telefon"].HeaderText = "Telefon";
            dataGridViewNadzornici.Columns["Sifra"].HeaderText = "Šifra";
            dataGridViewNadzornici.Columns["Privilegija"].HeaderText = "Privilegija";

        }

        private void SakriKolone()
        {
            dataGridViewNadzornici.Columns["NadzornikID"].Visible = false;
        }

        private void ResajzovanjeKolona()
        {
            if (!(this.dataGridViewNadzornici.DataSource == null))
            {
                dataGridViewNadzornici.Columns["Ime"].Width = this.dataGridViewNadzornici.Width / 7;
                dataGridViewNadzornici.Columns["Korisnicko"].Width = this.dataGridViewNadzornici.Width / 7;
                dataGridViewNadzornici.Columns["Ulica"].Width = this.dataGridViewNadzornici.Width / 7;
                dataGridViewNadzornici.Columns["Mesto"].Width = this.dataGridViewNadzornici.Width / 7;
                dataGridViewNadzornici.Columns["Telefon"].Width = this.dataGridViewNadzornici.Width / 7;
                dataGridViewNadzornici.Columns["Sifra"].Width = this.dataGridViewNadzornici.Width / 7;
                dataGridViewNadzornici.Columns["Privilegija"].Width = this.dataGridViewNadzornici.Width / 7;
            }
        }

        private void frmNadzorniciInsertPrikazAzuriranjeBrisanje_SizeChanged(object sender, EventArgs e)
        {
            ResajzovanjeKolona();
        }

        private void PretragaNadzornika()
        {
            Nadzornik nadzornik = new Nadzornik();
            nadzornik.Ime = txtIme.Text;
            nadzornik.Korisnicko = txtKorisnicko.Text;
            nadzornik.Ulica = txtUlica.Text;
            nadzornik.Mesto = txtMesto.Text;
            nadzornik.Telefon = txtTelefon.Text;
            
            NadzornikBusiness nb = new NadzornikBusiness();
            NadzornikDT = nb.PretragaNadzornika(nadzornik);
            this.dataGridViewNadzornici.DataSource = NadzornikDT;
               
        }

        private void btnPretragaNadzornika_Click(object sender, EventArgs e)
        {
            PretragaNadzornika();
        }

        private void dataGridViewNadzornici_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0)
            {
                return;
            }
            int indexReda = e.RowIndex;
            this.dataGridViewNadzornici.Rows[indexReda].Selected = true;

            idNadzornikaDobijenIzGrida = int.Parse(this.dataGridViewNadzornici.Rows[indexReda].Cells["NadzornikID"].FormattedValue.ToString());
            //MessageBox.Show(idNadzornikaDobijenIzGrida.ToString());
        }

        private void dataGridViewNadzornici_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int indexReda = e.RowIndex;
            idNadzornikaDobijenIzGrida = int.Parse(this.dataGridViewNadzornici.Rows[indexReda].Cells["NadzornikID"].FormattedValue.ToString());
            //MessageBox.Show(idNadzornikaDobijenIzGrida.ToString());
        }

        private void btnIzmeni_Click(object sender, EventArgs e)
        {
            IzmeniNadzornika();
        }

        private void IzmeniNadzornika()
        {
            if (idNadzornikaDobijenIzGrida <= 0)
            {
                return;
            }
            else
            {
                partialFrmIzmenaNadzornika pfin = new partialFrmIzmenaNadzornika(idNadzornikaDobijenIzGrida);
                pfin.ShowDialog();

                NadzornikBusiness nb = new NadzornikBusiness();
                NadzornikDT = nb.GetSviNadzornici();
                this.dataGridViewNadzornici.DataSource = NadzornikDT;
            }
        }

        private void btnDodaj_Click(object sender, EventArgs e)
        {
            DodajNadzornika();
        }

        private void DodajNadzornika()
        {
            uspehInsertaNadzornikaUPartialu = 0;

            PartialFrmInsertNadzornika pfin = new PartialFrmInsertNadzornika();
            pfin.ShowDialog();

            if (uspehInsertaNadzornikaUPartialu > 0)
            {
                DataRow noviRed = NadzornikDT.NewRow();
                noviRed["NadzornikID"] = DodatiNadzornikUPartialu.NadzornikID;
                noviRed["Ime"] = DodatiNadzornikUPartialu.Ime;
                noviRed["Korisnicko"] = DodatiNadzornikUPartialu.Korisnicko;
                noviRed["Ulica"] = DodatiNadzornikUPartialu.Ulica;
                noviRed["Mesto"] = DodatiNadzornikUPartialu.Mesto;
                noviRed["Telefon"] = DodatiNadzornikUPartialu.Telefon;
                noviRed["Sifra"] = DodatiNadzornikUPartialu.Sifra;
                noviRed["Privilegija"] = DodatiNadzornikUPartialu.Privilegija;
                NadzornikDT.Rows.InsertAt(noviRed, 0);
                NadzornikDT.AcceptChanges();
            }
        }

        private void btnIzbrisi_Click(object sender, EventArgs e)
        {
            IzbrisiNadzornika();
        }

        private void IzbrisiNadzornika()
        {
            int idNadzornika = idNadzornikaDobijenIzGrida;
            NadzornikBusiness nb = new NadzornikBusiness();
            if (nb.ProveriDaLINadzornikImaDecuOsimLogaOdnosnoDaLiJEBrisanjeMoguce(idNadzornika) == true)
            {
                MessageBox.Show("Brisanje nije moguće");
            }
            else
            {
                int uspeh = nb.DeleteNadzornika(idNadzornika);
                if (uspeh == 0)
                {
                    MessageBox.Show("Brisanje nije uspelo");
                }
                else
                {
                    for (int i = 0; i < NadzornikDT.Rows.Count; i ++)
                    {
                        DataRow red = NadzornikDT.Rows[i];
                        if (red["NadzornikID"].ToString() == idNadzornika.ToString())
                        {
                            red.Delete();
                        }
                    }
                    NadzornikDT.AcceptChanges();
                }
            }
        }

        private void btnIzlaz_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dodajToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DodajNadzornika();
        }

        private void izmeniToolStripMenuItem_Click(object sender, EventArgs e)
        {
            IzmeniNadzornika();
        }

        private void izbrišiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            IzbrisiNadzornika();
        }

        private void dataGridViewNadzornici_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                int selektovaniRed = e.RowIndex;
                idNadzornikaDobijenIzGrida = int.Parse(this.dataGridViewNadzornici.Rows[selektovaniRed].Cells["NadzornikID"].FormattedValue.ToString());
                if (!(selektovaniRed < 0))
                {
                    this.dataGridViewNadzornici.ClearSelection(); //odbacuje selektovani red
                    this.dataGridViewNadzornici.Rows[selektovaniRed].Selected = true;
                }
            }
        }

        private void dodajToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            DodajNadzornika();
        }

        private void izmeniToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            IzmeniNadzornika();
        }

        private void obrišiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            IzbrisiNadzornika();
        }

        private void excelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Program.ExportGridaExcel(this.dataGridViewNadzornici);
            ExportGridaExcel(this.dataGridViewNadzornici);
        }

        private void ExportGridaExcel(DataGridView dataGridViewPrikaz)
        {
            //prva i osma kolona su nevidljive odnosno 0 i 7

            // Creating a Excel object. 
            Microsoft.Office.Interop.Excel._Application excel = new Microsoft.Office.Interop.Excel.Application();
            Microsoft.Office.Interop.Excel._Workbook workbook = excel.Workbooks.Add(Type.Missing);
            Microsoft.Office.Interop.Excel._Worksheet worksheet = workbook.Worksheets.Add();


            string putanja = "";
            try
            {

                worksheet = workbook.ActiveSheet;

                worksheet.Name = "Nadzornici";

                int indexKolone = 1;
                int indexReda = 2;

                for (int i = 0; i < dataGridViewPrikaz.Columns.Count; i++)
                {
                    if (!(i == 0 || i == 7))
                    {
                        worksheet.Cells[1, indexKolone] = dataGridViewPrikaz.Columns[i].HeaderText;
                        indexKolone++;
                    }
                }

                indexKolone = 1;

                for (int i = 0; i < dataGridViewPrikaz.Rows.Count; i++)
                {
                    for (int j = 0; j < dataGridViewPrikaz.Columns.Count; j++)
                    {
                        if (!(j == 0 || j == 7))
                        {
                            worksheet.Cells[indexReda, indexKolone] = dataGridViewPrikaz.Rows[i].Cells[j].Value.ToString();
                            indexKolone++;
                        }
                    }
                    indexKolone = 1;
                    indexReda++;
                }


                //Getting the location and file name of the excel to save from user. 
                SaveFileDialog saveDialog = new SaveFileDialog();
                saveDialog.Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
                saveDialog.FilterIndex = 2;

                if (saveDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    workbook.SaveAs(saveDialog.FileName);
                    MessageBox.Show("Excel datoteka je uspeno izvezena");
                    putanja = saveDialog.FileName;
                }

            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                workbook.Close(false);
                excel.Quit();
                workbook = null;
                excel = null;
                GC.Collect();
                UbiProces(putanja);
            }
        }

        private void UbiProces(string putanja)
        {
            System.Diagnostics.Process[] process = System.Diagnostics.Process.GetProcessesByName("Excel");
            foreach (System.Diagnostics.Process p in process)
            {
                if (!string.IsNullOrEmpty(p.ProcessName))
                {
                    try
                    {
                        p.Kill();
                    }
                    catch { }
                }
            }
        }

        private void frmNadzorniciInsertPrikazAzuriranjeBrisanje_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F12)
            {
                ExportGridaExcel(this.dataGridViewNadzornici);
            }
        }
    }
}
