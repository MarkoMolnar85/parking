﻿namespace WindowsFormsApp1
{
    partial class frmOperater
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.operateriToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dodajToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.izmeniToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.izbrišiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.excelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tableLayoutPanelGlavni = new System.Windows.Forms.TableLayoutPanel();
            this.panelFilteri = new System.Windows.Forms.Panel();
            this.groupBoxPretragaOperatera = new System.Windows.Forms.GroupBox();
            this.btnPretraga = new System.Windows.Forms.Button();
            this.txtNadzornik = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtMesto = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtTelefon = new System.Windows.Forms.TextBox();
            this.txtLozinka = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtAdresa = new System.Windows.Forms.TextBox();
            this.txtIme = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtKorisnicko = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tableLayoutPanelOperacije = new System.Windows.Forms.TableLayoutPanel();
            this.panelZaInformaciju = new System.Windows.Forms.Panel();
            this.lblPodaciOperateru = new System.Windows.Forms.Label();
            this.panelZaDugmad = new System.Windows.Forms.Panel();
            this.btnIzlaz = new System.Windows.Forms.Button();
            this.btnDodaj = new System.Windows.Forms.Button();
            this.btnIzbrisi = new System.Windows.Forms.Button();
            this.btnIzmeni = new System.Windows.Forms.Button();
            this.panelZaGrid = new System.Windows.Forms.Panel();
            this.dataGridViewOperateri = new System.Windows.Forms.DataGridView();
            this.contextMenuStripOpcijeNadOeraterom = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.dodajToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.izmeniToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.izbrišiToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.tableLayoutPanelGlavni.SuspendLayout();
            this.panelFilteri.SuspendLayout();
            this.groupBoxPretragaOperatera.SuspendLayout();
            this.tableLayoutPanelOperacije.SuspendLayout();
            this.panelZaInformaciju.SuspendLayout();
            this.panelZaDugmad.SuspendLayout();
            this.panelZaGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewOperateri)).BeginInit();
            this.contextMenuStripOpcijeNadOeraterom.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.operateriToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1276, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // operateriToolStripMenuItem
            // 
            this.operateriToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dodajToolStripMenuItem,
            this.izmeniToolStripMenuItem,
            this.izbrišiToolStripMenuItem,
            this.excelToolStripMenuItem});
            this.operateriToolStripMenuItem.Name = "operateriToolStripMenuItem";
            this.operateriToolStripMenuItem.Size = new System.Drawing.Size(68, 20);
            this.operateriToolStripMenuItem.Text = "Operateri";
            // 
            // dodajToolStripMenuItem
            // 
            this.dodajToolStripMenuItem.Name = "dodajToolStripMenuItem";
            this.dodajToolStripMenuItem.Size = new System.Drawing.Size(109, 22);
            this.dodajToolStripMenuItem.Text = "Dodaj";
            this.dodajToolStripMenuItem.Click += new System.EventHandler(this.dodajToolStripMenuItem_Click);
            // 
            // izmeniToolStripMenuItem
            // 
            this.izmeniToolStripMenuItem.Name = "izmeniToolStripMenuItem";
            this.izmeniToolStripMenuItem.Size = new System.Drawing.Size(109, 22);
            this.izmeniToolStripMenuItem.Text = "Izmeni";
            this.izmeniToolStripMenuItem.Click += new System.EventHandler(this.izmeniToolStripMenuItem_Click);
            // 
            // izbrišiToolStripMenuItem
            // 
            this.izbrišiToolStripMenuItem.Name = "izbrišiToolStripMenuItem";
            this.izbrišiToolStripMenuItem.Size = new System.Drawing.Size(109, 22);
            this.izbrišiToolStripMenuItem.Text = "Izbriši";
            this.izbrišiToolStripMenuItem.Click += new System.EventHandler(this.izbrišiToolStripMenuItem_Click);
            // 
            // excelToolStripMenuItem
            // 
            this.excelToolStripMenuItem.Name = "excelToolStripMenuItem";
            this.excelToolStripMenuItem.Size = new System.Drawing.Size(109, 22);
            this.excelToolStripMenuItem.Text = "Excel";
            this.excelToolStripMenuItem.Click += new System.EventHandler(this.excelToolStripMenuItem_Click);
            // 
            // tableLayoutPanelGlavni
            // 
            this.tableLayoutPanelGlavni.ColumnCount = 1;
            this.tableLayoutPanelGlavni.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelGlavni.Controls.Add(this.panelFilteri, 0, 0);
            this.tableLayoutPanelGlavni.Controls.Add(this.tableLayoutPanelOperacije, 0, 1);
            this.tableLayoutPanelGlavni.Controls.Add(this.panelZaGrid, 0, 2);
            this.tableLayoutPanelGlavni.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelGlavni.Location = new System.Drawing.Point(0, 24);
            this.tableLayoutPanelGlavni.Name = "tableLayoutPanelGlavni";
            this.tableLayoutPanelGlavni.RowCount = 3;
            this.tableLayoutPanelGlavni.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 140F));
            this.tableLayoutPanelGlavni.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanelGlavni.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelGlavni.Size = new System.Drawing.Size(1276, 670);
            this.tableLayoutPanelGlavni.TabIndex = 1;
            // 
            // panelFilteri
            // 
            this.panelFilteri.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panelFilteri.Controls.Add(this.groupBoxPretragaOperatera);
            this.panelFilteri.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelFilteri.Location = new System.Drawing.Point(3, 3);
            this.panelFilteri.Name = "panelFilteri";
            this.panelFilteri.Size = new System.Drawing.Size(1270, 134);
            this.panelFilteri.TabIndex = 0;
            // 
            // groupBoxPretragaOperatera
            // 
            this.groupBoxPretragaOperatera.Controls.Add(this.btnPretraga);
            this.groupBoxPretragaOperatera.Controls.Add(this.txtNadzornik);
            this.groupBoxPretragaOperatera.Controls.Add(this.label1);
            this.groupBoxPretragaOperatera.Controls.Add(this.txtMesto);
            this.groupBoxPretragaOperatera.Controls.Add(this.label7);
            this.groupBoxPretragaOperatera.Controls.Add(this.txtTelefon);
            this.groupBoxPretragaOperatera.Controls.Add(this.txtLozinka);
            this.groupBoxPretragaOperatera.Controls.Add(this.label2);
            this.groupBoxPretragaOperatera.Controls.Add(this.txtAdresa);
            this.groupBoxPretragaOperatera.Controls.Add(this.txtIme);
            this.groupBoxPretragaOperatera.Controls.Add(this.label5);
            this.groupBoxPretragaOperatera.Controls.Add(this.label4);
            this.groupBoxPretragaOperatera.Controls.Add(this.label3);
            this.groupBoxPretragaOperatera.Controls.Add(this.txtKorisnicko);
            this.groupBoxPretragaOperatera.Controls.Add(this.label6);
            this.groupBoxPretragaOperatera.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxPretragaOperatera.Location = new System.Drawing.Point(24, 13);
            this.groupBoxPretragaOperatera.Name = "groupBoxPretragaOperatera";
            this.groupBoxPretragaOperatera.Size = new System.Drawing.Size(900, 100);
            this.groupBoxPretragaOperatera.TabIndex = 0;
            this.groupBoxPretragaOperatera.TabStop = false;
            this.groupBoxPretragaOperatera.Text = "Pretraga operatera";
            // 
            // btnPretraga
            // 
            this.btnPretraga.Location = new System.Drawing.Point(753, 60);
            this.btnPretraga.Name = "btnPretraga";
            this.btnPretraga.Size = new System.Drawing.Size(121, 30);
            this.btnPretraga.TabIndex = 7;
            this.btnPretraga.Text = "Pretraga";
            this.btnPretraga.UseVisualStyleBackColor = true;
            this.btnPretraga.Click += new System.EventHandler(this.btnPretraga_Click);
            // 
            // txtNadzornik
            // 
            this.txtNadzornik.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNadzornik.Location = new System.Drawing.Point(753, 27);
            this.txtNadzornik.Name = "txtNadzornik";
            this.txtNadzornik.Size = new System.Drawing.Size(121, 24);
            this.txtNadzornik.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(20, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 18);
            this.label1.TabIndex = 1;
            this.label1.Text = "Korisnicko";
            // 
            // txtMesto
            // 
            this.txtMesto.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMesto.Location = new System.Drawing.Point(536, 27);
            this.txtMesto.Name = "txtMesto";
            this.txtMesto.Size = new System.Drawing.Size(121, 24);
            this.txtMesto.TabIndex = 4;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(667, 27);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(76, 18);
            this.label7.TabIndex = 7;
            this.label7.Text = "Nadzornik";
            // 
            // txtTelefon
            // 
            this.txtTelefon.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTelefon.Location = new System.Drawing.Point(536, 66);
            this.txtTelefon.Name = "txtTelefon";
            this.txtTelefon.Size = new System.Drawing.Size(121, 24);
            this.txtTelefon.TabIndex = 5;
            // 
            // txtLozinka
            // 
            this.txtLozinka.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLozinka.Location = new System.Drawing.Point(315, 30);
            this.txtLozinka.Name = "txtLozinka";
            this.txtLozinka.Size = new System.Drawing.Size(121, 24);
            this.txtLozinka.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(20, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 18);
            this.label2.TabIndex = 2;
            this.label2.Text = "Ime";
            // 
            // txtAdresa
            // 
            this.txtAdresa.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAdresa.Location = new System.Drawing.Point(315, 66);
            this.txtAdresa.Name = "txtAdresa";
            this.txtAdresa.Size = new System.Drawing.Size(121, 24);
            this.txtAdresa.TabIndex = 3;
            // 
            // txtIme
            // 
            this.txtIme.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIme.Location = new System.Drawing.Point(105, 69);
            this.txtIme.Name = "txtIme";
            this.txtIme.Size = new System.Drawing.Size(121, 24);
            this.txtIme.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(466, 66);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 18);
            this.label5.TabIndex = 5;
            this.label5.Text = "Telefon";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(466, 30);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 18);
            this.label4.TabIndex = 4;
            this.label4.Text = "Mesto";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(249, 30);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 18);
            this.label3.TabIndex = 3;
            this.label3.Text = "Lozinka";
            // 
            // txtKorisnicko
            // 
            this.txtKorisnicko.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKorisnicko.Location = new System.Drawing.Point(105, 30);
            this.txtKorisnicko.Name = "txtKorisnicko";
            this.txtKorisnicko.Size = new System.Drawing.Size(121, 24);
            this.txtKorisnicko.TabIndex = 0;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(249, 69);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 18);
            this.label6.TabIndex = 6;
            this.label6.Text = "Adresa";
            // 
            // tableLayoutPanelOperacije
            // 
            this.tableLayoutPanelOperacije.ColumnCount = 2;
            this.tableLayoutPanelOperacije.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelOperacije.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 500F));
            this.tableLayoutPanelOperacije.Controls.Add(this.panelZaInformaciju, 0, 0);
            this.tableLayoutPanelOperacije.Controls.Add(this.panelZaDugmad, 1, 0);
            this.tableLayoutPanelOperacije.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelOperacije.Location = new System.Drawing.Point(3, 143);
            this.tableLayoutPanelOperacije.Name = "tableLayoutPanelOperacije";
            this.tableLayoutPanelOperacije.RowCount = 1;
            this.tableLayoutPanelOperacije.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelOperacije.Size = new System.Drawing.Size(1270, 44);
            this.tableLayoutPanelOperacije.TabIndex = 1;
            // 
            // panelZaInformaciju
            // 
            this.panelZaInformaciju.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panelZaInformaciju.Controls.Add(this.lblPodaciOperateru);
            this.panelZaInformaciju.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelZaInformaciju.Location = new System.Drawing.Point(3, 3);
            this.panelZaInformaciju.Name = "panelZaInformaciju";
            this.panelZaInformaciju.Size = new System.Drawing.Size(764, 38);
            this.panelZaInformaciju.TabIndex = 0;
            // 
            // lblPodaciOperateru
            // 
            this.lblPodaciOperateru.AutoSize = true;
            this.lblPodaciOperateru.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPodaciOperateru.Location = new System.Drawing.Point(47, 8);
            this.lblPodaciOperateru.Name = "lblPodaciOperateru";
            this.lblPodaciOperateru.Size = new System.Drawing.Size(143, 20);
            this.lblPodaciOperateru.TabIndex = 0;
            this.lblPodaciOperateru.Text = "Podaci o operateru";
            // 
            // panelZaDugmad
            // 
            this.panelZaDugmad.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panelZaDugmad.Controls.Add(this.btnIzlaz);
            this.panelZaDugmad.Controls.Add(this.btnDodaj);
            this.panelZaDugmad.Controls.Add(this.btnIzbrisi);
            this.panelZaDugmad.Controls.Add(this.btnIzmeni);
            this.panelZaDugmad.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelZaDugmad.Location = new System.Drawing.Point(773, 3);
            this.panelZaDugmad.Name = "panelZaDugmad";
            this.panelZaDugmad.Size = new System.Drawing.Size(494, 38);
            this.panelZaDugmad.TabIndex = 1;
            // 
            // btnIzlaz
            // 
            this.btnIzlaz.Location = new System.Drawing.Point(369, 0);
            this.btnIzlaz.Name = "btnIzlaz";
            this.btnIzlaz.Size = new System.Drawing.Size(125, 38);
            this.btnIzlaz.TabIndex = 11;
            this.btnIzlaz.Text = "Izlaz";
            this.btnIzlaz.UseVisualStyleBackColor = true;
            this.btnIzlaz.Click += new System.EventHandler(this.btnIzlaz_Click);
            // 
            // btnDodaj
            // 
            this.btnDodaj.Location = new System.Drawing.Point(0, 0);
            this.btnDodaj.Name = "btnDodaj";
            this.btnDodaj.Size = new System.Drawing.Size(125, 38);
            this.btnDodaj.TabIndex = 8;
            this.btnDodaj.Text = "Dodaj";
            this.btnDodaj.UseVisualStyleBackColor = true;
            this.btnDodaj.Click += new System.EventHandler(this.btnDodaj_Click);
            // 
            // btnIzbrisi
            // 
            this.btnIzbrisi.Location = new System.Drawing.Point(249, 0);
            this.btnIzbrisi.Name = "btnIzbrisi";
            this.btnIzbrisi.Size = new System.Drawing.Size(125, 38);
            this.btnIzbrisi.TabIndex = 10;
            this.btnIzbrisi.Text = "Izbriši";
            this.btnIzbrisi.UseVisualStyleBackColor = true;
            this.btnIzbrisi.Click += new System.EventHandler(this.btnIzbrisi_Click);
            // 
            // btnIzmeni
            // 
            this.btnIzmeni.Location = new System.Drawing.Point(131, 0);
            this.btnIzmeni.Name = "btnIzmeni";
            this.btnIzmeni.Size = new System.Drawing.Size(125, 38);
            this.btnIzmeni.TabIndex = 9;
            this.btnIzmeni.Text = "Izmeni";
            this.btnIzmeni.UseVisualStyleBackColor = true;
            this.btnIzmeni.Click += new System.EventHandler(this.btnIzmeni_Click);
            // 
            // panelZaGrid
            // 
            this.panelZaGrid.Controls.Add(this.dataGridViewOperateri);
            this.panelZaGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelZaGrid.Location = new System.Drawing.Point(3, 193);
            this.panelZaGrid.Name = "panelZaGrid";
            this.panelZaGrid.Size = new System.Drawing.Size(1270, 474);
            this.panelZaGrid.TabIndex = 2;
            // 
            // dataGridViewOperateri
            // 
            this.dataGridViewOperateri.AllowUserToAddRows = false;
            this.dataGridViewOperateri.AllowUserToDeleteRows = false;
            this.dataGridViewOperateri.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataGridViewOperateri.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewOperateri.ContextMenuStrip = this.contextMenuStripOpcijeNadOeraterom;
            this.dataGridViewOperateri.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewOperateri.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewOperateri.Name = "dataGridViewOperateri";
            this.dataGridViewOperateri.ReadOnly = true;
            this.dataGridViewOperateri.Size = new System.Drawing.Size(1270, 474);
            this.dataGridViewOperateri.TabIndex = 12;
            this.dataGridViewOperateri.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewOperateri_CellClick);
            this.dataGridViewOperateri.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridViewOperateri_CellMouseDown);
            this.dataGridViewOperateri.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridViewOperateri_RowHeaderMouseClick);
            // 
            // contextMenuStripOpcijeNadOeraterom
            // 
            this.contextMenuStripOpcijeNadOeraterom.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dodajToolStripMenuItem1,
            this.izmeniToolStripMenuItem1,
            this.izbrišiToolStripMenuItem1});
            this.contextMenuStripOpcijeNadOeraterom.Name = "contextMenuStripOpcijeNadOeraterom";
            this.contextMenuStripOpcijeNadOeraterom.Size = new System.Drawing.Size(110, 70);
            // 
            // dodajToolStripMenuItem1
            // 
            this.dodajToolStripMenuItem1.Name = "dodajToolStripMenuItem1";
            this.dodajToolStripMenuItem1.Size = new System.Drawing.Size(109, 22);
            this.dodajToolStripMenuItem1.Text = "Dodaj";
            this.dodajToolStripMenuItem1.Click += new System.EventHandler(this.dodajToolStripMenuItem1_Click);
            // 
            // izmeniToolStripMenuItem1
            // 
            this.izmeniToolStripMenuItem1.Name = "izmeniToolStripMenuItem1";
            this.izmeniToolStripMenuItem1.Size = new System.Drawing.Size(109, 22);
            this.izmeniToolStripMenuItem1.Text = "Izmeni";
            this.izmeniToolStripMenuItem1.Click += new System.EventHandler(this.izmeniToolStripMenuItem1_Click);
            // 
            // izbrišiToolStripMenuItem1
            // 
            this.izbrišiToolStripMenuItem1.Name = "izbrišiToolStripMenuItem1";
            this.izbrišiToolStripMenuItem1.Size = new System.Drawing.Size(109, 22);
            this.izbrišiToolStripMenuItem1.Text = "Izbriši";
            this.izbrišiToolStripMenuItem1.Click += new System.EventHandler(this.izbrišiToolStripMenuItem1_Click);
            // 
            // frmOperater
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1276, 694);
            this.Controls.Add(this.tableLayoutPanelGlavni);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmOperater";
            this.Text = "Operater";
            this.Load += new System.EventHandler(this.frmOperater_Load);
            this.SizeChanged += new System.EventHandler(this.frmOperater_SizeChanged);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmOperater_KeyDown);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tableLayoutPanelGlavni.ResumeLayout(false);
            this.panelFilteri.ResumeLayout(false);
            this.groupBoxPretragaOperatera.ResumeLayout(false);
            this.groupBoxPretragaOperatera.PerformLayout();
            this.tableLayoutPanelOperacije.ResumeLayout(false);
            this.panelZaInformaciju.ResumeLayout(false);
            this.panelZaInformaciju.PerformLayout();
            this.panelZaDugmad.ResumeLayout(false);
            this.panelZaGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewOperateri)).EndInit();
            this.contextMenuStripOpcijeNadOeraterom.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem operateriToolStripMenuItem;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelGlavni;
        private System.Windows.Forms.Panel panelFilteri;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelOperacije;
        private System.Windows.Forms.Panel panelZaInformaciju;
        private System.Windows.Forms.Panel panelZaDugmad;
        private System.Windows.Forms.Panel panelZaGrid;
        private System.Windows.Forms.Button btnIzmeni;
        private System.Windows.Forms.DataGridView dataGridViewOperateri;
        private System.Windows.Forms.Button btnDodaj;
        private System.Windows.Forms.Button btnIzlaz;
        private System.Windows.Forms.Button btnIzbrisi;
        private System.Windows.Forms.Label lblPodaciOperateru;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtMesto;
        private System.Windows.Forms.TextBox txtNadzornik;
        private System.Windows.Forms.TextBox txtLozinka;
        private System.Windows.Forms.TextBox txtTelefon;
        private System.Windows.Forms.TextBox txtIme;
        private System.Windows.Forms.TextBox txtKorisnicko;
        private System.Windows.Forms.TextBox txtAdresa;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBoxPretragaOperatera;
        private System.Windows.Forms.Button btnPretraga;
        private System.Windows.Forms.ToolStripMenuItem dodajToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem izmeniToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem izbrišiToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripOpcijeNadOeraterom;
        private System.Windows.Forms.ToolStripMenuItem dodajToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem izmeniToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem izbrišiToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem excelToolStripMenuItem;
    }
}