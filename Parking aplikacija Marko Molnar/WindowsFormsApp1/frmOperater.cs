﻿using BusinessLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class frmOperater : Form
    {
        DataTable OperateriDT = new DataTable();
        int idOperateraGlobal = 0;
        public frmOperater()
        {
            InitializeComponent();
            this.WindowState = FormWindowState.Maximized;
            KeyPreview = true;
        }

        private void frmOperater_Load(object sender, EventArgs e)
        {
            UrediDugmad();
            LoadOPerateri();
            PreimenujKolone();
            SakriKolone();
            ResajzovanjeKolona();

        }
        private void UrediDugmad()
        {
            btnDodaj.Width = 124;
            btnIzmeni.Width = 124;
            btnIzbrisi.Width = 124;
            btnIzlaz.Width = 123;
            btnDodaj.Left = 0;
            btnIzmeni.Left = btnDodaj.Right;
            btnIzbrisi.Left = btnIzmeni.Right;
            btnIzlaz.Left = btnIzbrisi.Right;
        }
        private void LoadOPerateri()
        {
            OperaterBusiness ob = new OperaterBusiness();
            OperateriDT = ob.GetSviOperateriSaNadzornikom();
            this.dataGridViewOperateri.DataSource = OperateriDT;
        }

        private void PreimenujKolone()
        {
            dataGridViewOperateri.Columns["OperaterID"].HeaderText = "ID";
            dataGridViewOperateri.Columns["Korisnicko"].HeaderText = "Korisničko ime";
            dataGridViewOperateri.Columns["Ime"].HeaderText = "Ime";
            dataGridViewOperateri.Columns["Sifra"].HeaderText = "Lozinka";
            dataGridViewOperateri.Columns["Adresa"].HeaderText = "Adresa";
            dataGridViewOperateri.Columns["Mesto"].HeaderText = "Mesto";
            dataGridViewOperateri.Columns["Telefon"].HeaderText = "Telefon";
            dataGridViewOperateri.Columns["NadzornikID"].HeaderText = "NadzornikID";
            dataGridViewOperateri.Columns["NadzornikKorisnicko"].HeaderText = "Korisničko ime nadzornika";

        }

        private void SakriKolone()
        {
            dataGridViewOperateri.Columns["OperaterID"].Visible = false;
            dataGridViewOperateri.Columns["NadzornikID"].Visible = false;
        }

        private void dataGridViewOperateri_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0)
            {
                return;
            }
            int indexReda = e.RowIndex;
            this.dataGridViewOperateri.Rows[indexReda].Selected = true;

            idOperateraGlobal = int.Parse(this.dataGridViewOperateri.Rows[indexReda].Cells["OperaterID"].FormattedValue.ToString());
            PromenaNaLabelu();
        }

        private void dataGridViewOperateri_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex < 0)
            {
                return;
            }
            int indexReda = e.RowIndex;

            idOperateraGlobal = int.Parse(this.dataGridViewOperateri.Rows[indexReda].Cells["OperaterID"].FormattedValue.ToString());
            PromenaNaLabelu();
        }

        private void ResajzovanjeKolona()
        {
            if (!(this.dataGridViewOperateri.DataSource == null))
            {
                dataGridViewOperateri.Columns["Korisnicko"].Width = this.dataGridViewOperateri.Width / 7;
                dataGridViewOperateri.Columns["Ime"].Width = this.dataGridViewOperateri.Width / 7;
                dataGridViewOperateri.Columns["Sifra"].Width = this.dataGridViewOperateri.Width / 7;
                dataGridViewOperateri.Columns["Adresa"].Width = this.dataGridViewOperateri.Width / 7;
                dataGridViewOperateri.Columns["Mesto"].Width = this.dataGridViewOperateri.Width / 7;
                dataGridViewOperateri.Columns["Telefon"].Width = this.dataGridViewOperateri.Width / 7;
                dataGridViewOperateri.Columns["NadzornikKorisnicko"].Width = this.dataGridViewOperateri.Width / 7;
            }
        }

        private void frmOperater_SizeChanged(object sender, EventArgs e)
        {
            ResajzovanjeKolona();
        }

        private void btnPretraga_Click(object sender, EventArgs e)
        {
            PretragaOperatera();
        }

        private void PretragaOperatera()
        {
            OperaterSaNadzornikom osn = new OperaterSaNadzornikom();
            osn.Korisnicko = txtKorisnicko.Text;
            osn.Ime = txtIme.Text;
            osn.Sifra = txtLozinka.Text;
            osn.Adresa = txtAdresa.Text;
            osn.Mesto = txtMesto.Text;
            osn.Telefon = txtTelefon.Text;
            osn.KorisnickoNadzornik = txtNadzornik.Text;

            OperaterBusiness ob = new OperaterBusiness();
            OperateriDT = ob.GetPretrazeneOperatereNaOnsovuSvihParametaraIzKlase(osn);
            this.dataGridViewOperateri.DataSource = OperateriDT;
        }

        //private bool PopunjenaSvaPolja()
        //{
        //    if (txtKorisnicko.Text == "" || string.IsNullOrEmpty(txtKorisnicko.Text)
        //        || txtIme.Text == "" || string.IsNullOrEmpty(txtIme.Text)
        //        || txtLozinka.Text == "" || string.IsNullOrEmpty(txtLozinka.Text)
        //        || txtAdresa.Text == "" || string.IsNullOrEmpty(txtAdresa.Text)
        //        || txtMesto.Text == "" || string.IsNullOrEmpty(txtMesto.Text)
        //        || txtTelefon.Text == "" || string.IsNullOrEmpty(txtTelefon.Text)
        //        || txtNadzornik.Text == "" || string.IsNullOrEmpty(txtNadzornik.Text))
        //    {
        //        return false;
        //    }
        //    else
        //    {
        //        return true;
        //    }
        //}

        private void PromenaNaLabelu()
        {
            OperaterBusiness ob = new OperaterBusiness();
            Operater o = ob.GetOperaterPoIDu(idOperateraGlobal);
            lblPodaciOperateru.Text = "Podaci o operateru: " + o.Ime; 
        }

        private void btnDodaj_Click(object sender, EventArgs e)
        {
            InsertovanjeOperatera();
        }

        private void InsertovanjeOperatera()
        {
            PartialInsertOperatera pio = new PartialInsertOperatera();
            pio.ShowDialog();

            OperaterBusiness ob = new OperaterBusiness();
            OperateriDT = ob.GetSviOperateriSaNadzornikom();
            this.dataGridViewOperateri.DataSource = OperateriDT;
        }

        private void btnIzmeni_Click(object sender, EventArgs e)
        {
            IzmenaOperatera();
        }
        private void IzmenaOperatera()
        {
            if (idOperateraGlobal <= 0)
            {
                return;
            }
            else
            {
                PartialFrmUpdateOperatera pfuo = new PartialFrmUpdateOperatera(idOperateraGlobal);
                pfuo.ShowDialog();

                OperaterBusiness ob = new OperaterBusiness();
                OperateriDT = ob.GetSviOperateriSaNadzornikom();
                this.dataGridViewOperateri.DataSource = OperateriDT;
            }
        }

        private void btnIzlaz_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnIzbrisi_Click(object sender, EventArgs e)
        {
            IzbrisiOperatera();
        }
        private void IzbrisiOperatera()
        {
            if (idOperateraGlobal <= 0)
            {
                return;
            }
            else
            {
                OperaterBusiness ob = new OperaterBusiness();

                if (ob.DaLijeMoguceBrisanjeOperateraPostojeLiNjegovaDecaTrueAkoJeste(idOperateraGlobal) == true)
                {
                    int uspehBrisanjaLogova = ob.BrisanjeLogovaOperatera(idOperateraGlobal);

                    OperaterLogInBusiness olb = new OperaterLogInBusiness();

                    if (uspehBrisanjaLogova > 0 || olb.BrojLogovaOperatera(idOperateraGlobal) == 0)
                    {

                        int uspeh = ob.DeleteOperateraNaOsnovuIDa(idOperateraGlobal);
                        if (uspeh > 0)
                        {
                            for (int i = 0; i < OperateriDT.Rows.Count; i++)
                            {
                                DataRow red = OperateriDT.Rows[i];
                                if (red["OperaterID"].ToString() == idOperateraGlobal.ToString())
                                {
                                    red.Delete();
                                }
                            }
                            OperateriDT.AcceptChanges();
                        }
                        else
                        {
                            MessageBox.Show("Brisanje nije uspelo");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Brisanje logova nije uspelo");
                    }
                }
                else
                {
                    MessageBox.Show("Brisanje nije moguce");
                }
            }
        }

        private void dataGridViewOperateri_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                int redSelektovani = e.RowIndex;
                if (!(redSelektovani < 0))
                {
                    this.dataGridViewOperateri.ClearSelection();
                    this.dataGridViewOperateri.Rows[redSelektovani].Selected = true;
                    idOperateraGlobal = int.Parse(this.dataGridViewOperateri.Rows[redSelektovani].Cells["OperaterID"].FormattedValue.ToString());
                    PromenaNaLabelu();
                }
            }
            
        }

        private void dodajToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            InsertovanjeOperatera();
        }

        private void izmeniToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            IzmenaOperatera();
        }

        private void izbrišiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            IzbrisiOperatera();
        }

        private void dodajToolStripMenuItem_Click(object sender, EventArgs e)
        {
            InsertovanjeOperatera();
        }

        private void izmeniToolStripMenuItem_Click(object sender, EventArgs e)
        {
            IzmenaOperatera();
        }

        private void izbrišiToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            IzbrisiOperatera();
        }

        private void ExportGridaExcel(DataGridView dataGridViewPrikaz)
        {
            //nevidljive kolone su 1 i 8 odnosno 0 i 7

            // Creating a Excel object. 
            Microsoft.Office.Interop.Excel._Application excel = new Microsoft.Office.Interop.Excel.Application();
            Microsoft.Office.Interop.Excel._Workbook workbook = excel.Workbooks.Add(Type.Missing);
            Microsoft.Office.Interop.Excel._Worksheet worksheet = workbook.Worksheets.Add();


            string putanja = "";
            try
            {

                worksheet = workbook.ActiveSheet;

                worksheet.Name = "Operateri";

                int indexKolone = 1;
                int indexReda = 2;

                for (int i = 0; i < dataGridViewPrikaz.Columns.Count; i++)
                {
                    if (!(i == 0 || i == 7))
                    {
                        worksheet.Cells[1, indexKolone] = dataGridViewPrikaz.Columns[i].HeaderText;
                        indexKolone++;
                    }
                }

                indexKolone = 1;

                for (int i = 0; i < dataGridViewPrikaz.Rows.Count; i++)
                {
                    for (int j = 0; j < dataGridViewPrikaz.Columns.Count; j++)
                    {
                        if (!(j == 0 || j == 7))
                        {
                            worksheet.Cells[indexReda, indexKolone] = dataGridViewPrikaz.Rows[i].Cells[j].Value.ToString();
                            indexKolone++;
                        }
                    }
                    indexKolone = 1;
                    indexReda++;
                }


                //Getting the location and file name of the excel to save from user. 
                SaveFileDialog saveDialog = new SaveFileDialog();
                saveDialog.Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
                saveDialog.FilterIndex = 2;

                if (saveDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    workbook.SaveAs(saveDialog.FileName);
                    MessageBox.Show("Excel datoteka je uspeno izvezena");
                    putanja = saveDialog.FileName;
                }

            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                workbook.Close(false);
                excel.Quit();
                workbook = null;
                excel = null;
                GC.Collect();
                UbiProces(putanja);
            }
        }

        private void UbiProces(string putanja)
        {
            System.Diagnostics.Process[] process = System.Diagnostics.Process.GetProcessesByName("Excel");
            foreach (System.Diagnostics.Process p in process)
            {
                if (!string.IsNullOrEmpty(p.ProcessName))
                {
                    try
                    {
                        p.Kill();
                    }
                    catch { }
                }
            }
        }

        private void excelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ExportGridaExcel(this.dataGridViewOperateri);
        }

        private void frmOperater_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F12)
            {
                ExportGridaExcel(this.dataGridViewOperateri);
            }
        }
    }

}
