﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BusinessLayer;
using DataLayer.Models;

namespace WindowsFormsApp1
{
    public partial class frmPazari : Form
    {
        RacunBusiness rb = new RacunBusiness();
        public frmPazari()
        {
            InitializeComponent();
            this.WindowState = FormWindowState.Maximized;
            KeyPreview = true;
        }

        private void frmPazari_Load(object sender, EventArgs e)
        {
            PodesiDateTimePickere();
            LoadPazari();
            ResajzovanjeKolona();
            PreimenovanjeKolona();
            PopunjavanjeKomboBoxeva();
        }
        private void PodesiDateTimePickere()
        {
            this.dateTimePickerOD.Format = DateTimePickerFormat.Custom;
            this.dateTimePickerOD.CustomFormat = "dd-MM-yyyy";
            this.dateTimePickerDO.Format = DateTimePickerFormat.Custom;
            this.dateTimePickerDO.CustomFormat = "dd-MM-yyyy";
            dateTimePickerOD.Value = DateTime.Today.AddMonths(-1);
        }

        private void LoadPazari()
        {
            //izbrisati blok koda koji je tu samo da nebi otvarao login formu svaki put
            if (Program.idLokacijeIzKomboBoxaKodFormeZaNadzornika == 0)
            {
                Program.idLokacijeIzKomboBoxaKodFormeZaNadzornika = 1002;
            }
            this.dataGridViewPazari.DataSource = rb.GetAllPazariPremaIdeuLokacije(Program.idLokacijeIzKomboBoxaKodFormeZaNadzornika);
        }
        private void ResajzovanjeKolona()
        {
            if (this.dataGridViewPazari.DataSource != null)
            {
                int brojKolona = 9;
                int sirinaKolona = (this.dataGridViewPazari.Width - this.dataGridViewPazari.RowHeadersWidth - 25) / brojKolona;
                this.dataGridViewPazari.Columns["Korisnicko"].Width = sirinaKolona;
                this.dataGridViewPazari.Columns["datum"].Width = sirinaKolona;
                this.dataGridViewPazari.Columns["NazivSmene"].Width = sirinaKolona;
                this.dataGridViewPazari.Columns["Suma"].Width = sirinaKolona;
                this.dataGridViewPazari.Columns["Sati"].Width = sirinaKolona;
                //kolone ispod imaju spojena imena bez obzira sto je u datalayeru razdvojeno
                this.dataGridViewPazari.Columns["ProsecnoPlacanje"].Width = sirinaKolona;
                this.dataGridViewPazari.Columns["ProsecnoSati"].Width = sirinaKolona;
                this.dataGridViewPazari.Columns["MaksimalnoSati"].Width = sirinaKolona;
                this.dataGridViewPazari.Columns["MinimalnoSati"].Width = sirinaKolona;
            }
        }
        private void PreimenovanjeKolona()
        {
            this.dataGridViewPazari.Columns["Korisnicko"].HeaderText = "Operater";
            this.dataGridViewPazari.Columns["datum"].HeaderText = "Datum";
            this.dataGridViewPazari.Columns["NazivSmene"].HeaderText = "Smena";
            this.dataGridViewPazari.Columns["Suma"].HeaderText = "Suma";
            this.dataGridViewPazari.Columns["Sati"].HeaderText = "Sati";
            //kolone ispod imaju spojena imena bez obzira sto je u datalayeru razdvojeno
            this.dataGridViewPazari.Columns["ProsecnoPlacanje"].HeaderText = "Prosečno plaćanje";
            this.dataGridViewPazari.Columns["ProsecnoSati"].HeaderText = "Proseečno sati";
            this.dataGridViewPazari.Columns["MaksimalnoSati"].HeaderText = "Maksimalno sati";
            this.dataGridViewPazari.Columns["MinimalnoSati"].HeaderText = "Minimalno sati";
        }

        private void frmPazari_SizeChanged(object sender, EventArgs e)
        {
            ResajzovanjeKolona();
        }

        private void PopunjavanjeKomboBoxeva()
        {
            cmbOperater.Items.Clear();
            OperaterBusiness ob = new OperaterBusiness();
            foreach (var v in ob.GetAllKorisnickaImenaZaKomboBox())
            {
                cmbOperater.Items.Add(v);
            }
            cmbSmena.Items.Clear();
            SmenaBusiness sb = new SmenaBusiness();
            foreach (var v in sb.GetAllNaziviSmenaZaKomboBox())
            {
                cmbSmena.Items.Add(v);
            }
        }

        private void btnPretraga_Click(object sender, EventArgs e)
        {
            Pretraga();
        }

        private void Pretraga()
        {
            Pazar p = new Pazar();
            p.Korisnicko = cmbOperater.Text;
            p.NazivSmene = cmbSmena.Text;
            DateTime datumOD = dateTimePickerOD.Value;
            DateTime datumDO = dateTimePickerDO.Value;
            double zaradaOD = 0;
            double zaradaDO = double.MaxValue;

            if (!(txtZaradaOD.Text == "" || string.IsNullOrEmpty(txtZaradaOD.Text) || string.IsNullOrWhiteSpace(txtZaradaOD.Text)))
            {
                bool zaradaOdProvera = double.TryParse(txtZaradaOD.Text, out zaradaOD);
                if (zaradaOdProvera == false)
                {
                    MessageBox.Show("Zarada od mora da bude broj");
                    return;
                }
            }
            if (!(txtZaradaDO.Text == "" || string.IsNullOrEmpty(txtZaradaDO.Text) || string.IsNullOrWhiteSpace(txtZaradaDO.Text)))
            {
                bool zaradaDoProvera = double.TryParse(txtZaradaDO.Text, out zaradaDO);
                if (zaradaDoProvera == false)
                {
                    MessageBox.Show("Zarada do mora da bude broj");
                    return;
                }
            }
            this.dataGridViewPazari.DataSource = rb.PretragaPazaraPoParametrima(p, datumOD, datumDO, zaradaOD, zaradaDO, Program.idLokacijeIzKomboBoxaKodFormeZaNadzornika);

        }

        private void dataGridViewPazari_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int index = e.RowIndex;
            if (!(index < 0))
            {
                this.dataGridViewPazari.Rows[index].Selected = true;
            }
        }

        private void frmPazari_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F12)
            {
                ExportGridaExcel(this.dataGridViewPazari);
            }
        }

        private void ExportGridaExcel(DataGridView dataGridViewPrikaz)
        {
            //nema nevidljivih kolona

            // Creating a Excel object. 
            Microsoft.Office.Interop.Excel._Application excel = new Microsoft.Office.Interop.Excel.Application();
            Microsoft.Office.Interop.Excel._Workbook workbook = excel.Workbooks.Add(Type.Missing);
            Microsoft.Office.Interop.Excel._Worksheet worksheet = workbook.Worksheets.Add();


            string putanja = "";
            try
            {

                worksheet = workbook.ActiveSheet;

                worksheet.Name = "Pazari";

                int indexKolone = 1;
                int indexReda = 2;

                for (int i = 0; i < dataGridViewPrikaz.Columns.Count; i++)
                {
                    //if (!(i == 0 || i == 4))
                    {
                        worksheet.Cells[1, indexKolone] = dataGridViewPrikaz.Columns[i].HeaderText;
                        indexKolone++;
                    }
                }

                indexKolone = 1;

                for (int i = 0; i < dataGridViewPrikaz.Rows.Count; i++)
                {
                    for (int j = 0; j < dataGridViewPrikaz.Columns.Count; j++)
                    {
                        //if (!(j == 0 || j == 4))
                        {
                            worksheet.Cells[indexReda, indexKolone] = dataGridViewPrikaz.Rows[i].Cells[j].Value.ToString();
                            indexKolone++;
                        }
                    }
                    indexKolone = 1;
                    indexReda++;
                }


                //Getting the location and file name of the excel to save from user. 
                SaveFileDialog saveDialog = new SaveFileDialog();
                saveDialog.Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
                saveDialog.FilterIndex = 2;

                if (saveDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    workbook.SaveAs(saveDialog.FileName);
                    MessageBox.Show("Excel datoteka je uspeno izvezena");
                    putanja = saveDialog.FileName;
                }

            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                workbook.Close(false);
                excel.Quit();
                workbook = null;
                excel = null;
                GC.Collect();
                UbiProces(putanja);
            }
        }

        private void UbiProces(string putanja)
        {
            System.Diagnostics.Process[] process = System.Diagnostics.Process.GetProcessesByName("Excel");
            foreach (System.Diagnostics.Process p in process)
            {
                if (!string.IsNullOrEmpty(p.ProcessName))
                {
                    try
                    {
                        p.Kill();
                    }
                    catch { }
                }
            }
        }
    }
}
