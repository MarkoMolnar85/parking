﻿namespace WindowsFormsApp1
{
    partial class frmPretplatneKarteInsertUpdatePretragacs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tableLayoutPanelGlavni = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanelZaDugmad = new System.Windows.Forms.TableLayoutPanel();
            this.panelZaLabelInformaciju = new System.Windows.Forms.Panel();
            this.lblInfo = new System.Windows.Forms.Label();
            this.panelZaDugmad = new System.Windows.Forms.Panel();
            this.btnIzlaz = new System.Windows.Forms.Button();
            this.btnPretraga = new System.Windows.Forms.Button();
            this.btnObrisi = new System.Windows.Forms.Button();
            this.btnOcisti = new System.Windows.Forms.Button();
            this.btnDodaj = new System.Windows.Forms.Button();
            this.btnIzmeni = new System.Windows.Forms.Button();
            this.panelPoljaZaUnos = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.dateTimePickerDO = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerOD = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtCena = new System.Windows.Forms.TextBox();
            this.txtRegistracija = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.pretplatneKarteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dodajToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.očistiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dodajToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.izmeniToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.obrišiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.izlazToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.excelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panelZaGrid = new System.Windows.Forms.Panel();
            this.dataGridViewPretplatneKarte = new System.Windows.Forms.DataGridView();
            this.contextMenuStripMeni = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.pretragaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.očistiToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.dodajToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.izmeniToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.obrišiToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.izlazToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tableLayoutPanelGlavni.SuspendLayout();
            this.tableLayoutPanelZaDugmad.SuspendLayout();
            this.panelZaLabelInformaciju.SuspendLayout();
            this.panelZaDugmad.SuspendLayout();
            this.panelPoljaZaUnos.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.panelZaGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPretplatneKarte)).BeginInit();
            this.contextMenuStripMeni.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanelGlavni
            // 
            this.tableLayoutPanelGlavni.ColumnCount = 1;
            this.tableLayoutPanelGlavni.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelGlavni.Controls.Add(this.tableLayoutPanelZaDugmad, 0, 1);
            this.tableLayoutPanelGlavni.Controls.Add(this.panelPoljaZaUnos, 0, 0);
            this.tableLayoutPanelGlavni.Controls.Add(this.panelZaGrid, 0, 2);
            this.tableLayoutPanelGlavni.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelGlavni.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanelGlavni.Name = "tableLayoutPanelGlavni";
            this.tableLayoutPanelGlavni.RowCount = 3;
            this.tableLayoutPanelGlavni.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 140F));
            this.tableLayoutPanelGlavni.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanelGlavni.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelGlavni.Size = new System.Drawing.Size(1276, 694);
            this.tableLayoutPanelGlavni.TabIndex = 0;
            // 
            // tableLayoutPanelZaDugmad
            // 
            this.tableLayoutPanelZaDugmad.ColumnCount = 2;
            this.tableLayoutPanelZaDugmad.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelZaDugmad.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 750F));
            this.tableLayoutPanelZaDugmad.Controls.Add(this.panelZaLabelInformaciju, 0, 0);
            this.tableLayoutPanelZaDugmad.Controls.Add(this.panelZaDugmad, 1, 0);
            this.tableLayoutPanelZaDugmad.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelZaDugmad.Location = new System.Drawing.Point(3, 143);
            this.tableLayoutPanelZaDugmad.Name = "tableLayoutPanelZaDugmad";
            this.tableLayoutPanelZaDugmad.RowCount = 1;
            this.tableLayoutPanelZaDugmad.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelZaDugmad.Size = new System.Drawing.Size(1270, 44);
            this.tableLayoutPanelZaDugmad.TabIndex = 0;
            // 
            // panelZaLabelInformaciju
            // 
            this.panelZaLabelInformaciju.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panelZaLabelInformaciju.Controls.Add(this.lblInfo);
            this.panelZaLabelInformaciju.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelZaLabelInformaciju.Location = new System.Drawing.Point(3, 3);
            this.panelZaLabelInformaciju.Name = "panelZaLabelInformaciju";
            this.panelZaLabelInformaciju.Size = new System.Drawing.Size(514, 38);
            this.panelZaLabelInformaciju.TabIndex = 0;
            // 
            // lblInfo
            // 
            this.lblInfo.AutoSize = true;
            this.lblInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInfo.Location = new System.Drawing.Point(6, 11);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(260, 16);
            this.lblInfo.TabIndex = 12;
            this.lblInfo.Text = "Info o karti za vozilo sa registracijom";
            // 
            // panelZaDugmad
            // 
            this.panelZaDugmad.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panelZaDugmad.Controls.Add(this.btnIzlaz);
            this.panelZaDugmad.Controls.Add(this.btnPretraga);
            this.panelZaDugmad.Controls.Add(this.btnObrisi);
            this.panelZaDugmad.Controls.Add(this.btnOcisti);
            this.panelZaDugmad.Controls.Add(this.btnDodaj);
            this.panelZaDugmad.Controls.Add(this.btnIzmeni);
            this.panelZaDugmad.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelZaDugmad.Location = new System.Drawing.Point(523, 3);
            this.panelZaDugmad.Name = "panelZaDugmad";
            this.panelZaDugmad.Size = new System.Drawing.Size(744, 38);
            this.panelZaDugmad.TabIndex = 1;
            // 
            // btnIzlaz
            // 
            this.btnIzlaz.Location = new System.Drawing.Point(613, 0);
            this.btnIzlaz.Name = "btnIzlaz";
            this.btnIzlaz.Size = new System.Drawing.Size(125, 38);
            this.btnIzlaz.TabIndex = 9;
            this.btnIzlaz.Text = "Izlaz";
            this.btnIzlaz.UseVisualStyleBackColor = true;
            this.btnIzlaz.Click += new System.EventHandler(this.btnIzlaz_Click);
            // 
            // btnPretraga
            // 
            this.btnPretraga.Location = new System.Drawing.Point(0, 0);
            this.btnPretraga.Name = "btnPretraga";
            this.btnPretraga.Size = new System.Drawing.Size(125, 38);
            this.btnPretraga.TabIndex = 4;
            this.btnPretraga.Text = "Pretraga";
            this.btnPretraga.UseVisualStyleBackColor = true;
            this.btnPretraga.Click += new System.EventHandler(this.btnPretraga_Click);
            // 
            // btnObrisi
            // 
            this.btnObrisi.Location = new System.Drawing.Point(482, 0);
            this.btnObrisi.Name = "btnObrisi";
            this.btnObrisi.Size = new System.Drawing.Size(125, 38);
            this.btnObrisi.TabIndex = 8;
            this.btnObrisi.Text = "Obriši";
            this.btnObrisi.UseVisualStyleBackColor = true;
            this.btnObrisi.Click += new System.EventHandler(this.btnObrisi_Click);
            // 
            // btnOcisti
            // 
            this.btnOcisti.Location = new System.Drawing.Point(122, 0);
            this.btnOcisti.Name = "btnOcisti";
            this.btnOcisti.Size = new System.Drawing.Size(125, 38);
            this.btnOcisti.TabIndex = 5;
            this.btnOcisti.Text = "Očisti";
            this.btnOcisti.UseVisualStyleBackColor = true;
            this.btnOcisti.Click += new System.EventHandler(this.btnOcisti_Click);
            // 
            // btnDodaj
            // 
            this.btnDodaj.Location = new System.Drawing.Point(242, 0);
            this.btnDodaj.Name = "btnDodaj";
            this.btnDodaj.Size = new System.Drawing.Size(125, 38);
            this.btnDodaj.TabIndex = 6;
            this.btnDodaj.Text = "Dodaj";
            this.btnDodaj.UseVisualStyleBackColor = true;
            this.btnDodaj.Click += new System.EventHandler(this.btnDodaj_Click);
            // 
            // btnIzmeni
            // 
            this.btnIzmeni.Location = new System.Drawing.Point(363, 0);
            this.btnIzmeni.Name = "btnIzmeni";
            this.btnIzmeni.Size = new System.Drawing.Size(125, 38);
            this.btnIzmeni.TabIndex = 7;
            this.btnIzmeni.Text = "Izmeni";
            this.btnIzmeni.UseVisualStyleBackColor = true;
            this.btnIzmeni.Click += new System.EventHandler(this.btnIzmeni_Click);
            // 
            // panelPoljaZaUnos
            // 
            this.panelPoljaZaUnos.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panelPoljaZaUnos.Controls.Add(this.label6);
            this.panelPoljaZaUnos.Controls.Add(this.label5);
            this.panelPoljaZaUnos.Controls.Add(this.dateTimePickerDO);
            this.panelPoljaZaUnos.Controls.Add(this.dateTimePickerOD);
            this.panelPoljaZaUnos.Controls.Add(this.label4);
            this.panelPoljaZaUnos.Controls.Add(this.label3);
            this.panelPoljaZaUnos.Controls.Add(this.label2);
            this.panelPoljaZaUnos.Controls.Add(this.label1);
            this.panelPoljaZaUnos.Controls.Add(this.txtCena);
            this.panelPoljaZaUnos.Controls.Add(this.txtRegistracija);
            this.panelPoljaZaUnos.Controls.Add(this.menuStrip1);
            this.panelPoljaZaUnos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelPoljaZaUnos.Location = new System.Drawing.Point(3, 3);
            this.panelPoljaZaUnos.Name = "panelPoljaZaUnos";
            this.panelPoljaZaUnos.Size = new System.Drawing.Size(1270, 134);
            this.panelPoljaZaUnos.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(572, 93);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(32, 24);
            this.label6.TabIndex = 12;
            this.label6.Text = "<=";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(572, 33);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(32, 24);
            this.label5.TabIndex = 11;
            this.label5.Text = "<=";
            // 
            // dateTimePickerDO
            // 
            this.dateTimePickerDO.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePickerDO.Location = new System.Drawing.Point(645, 93);
            this.dateTimePickerDO.Name = "dateTimePickerDO";
            this.dateTimePickerDO.Size = new System.Drawing.Size(178, 24);
            this.dateTimePickerDO.TabIndex = 3;
            // 
            // dateTimePickerOD
            // 
            this.dateTimePickerOD.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePickerOD.Location = new System.Drawing.Point(645, 32);
            this.dateTimePickerOD.Name = "dateTimePickerOD";
            this.dateTimePickerOD.Size = new System.Drawing.Size(178, 24);
            this.dateTimePickerOD.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(423, 101);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(95, 18);
            this.label4.TabIndex = 7;
            this.label4.Text = "Datum isteka";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(423, 39);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(109, 18);
            this.label3.TabIndex = 6;
            this.label3.Text = "Datum početka";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(46, 102);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 18);
            this.label2.TabIndex = 5;
            this.label2.Text = "Cena";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(46, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 18);
            this.label1.TabIndex = 4;
            this.label1.Text = "Registracija";
            // 
            // txtCena
            // 
            this.txtCena.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCena.Location = new System.Drawing.Point(172, 95);
            this.txtCena.Name = "txtCena";
            this.txtCena.Size = new System.Drawing.Size(178, 24);
            this.txtCena.TabIndex = 1;
            // 
            // txtRegistracija
            // 
            this.txtRegistracija.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRegistracija.Location = new System.Drawing.Point(172, 36);
            this.txtRegistracija.Name = "txtRegistracija";
            this.txtRegistracija.Size = new System.Drawing.Size(178, 24);
            this.txtRegistracija.TabIndex = 0;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pretplatneKarteToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1270, 24);
            this.menuStrip1.TabIndex = 10;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // pretplatneKarteToolStripMenuItem
            // 
            this.pretplatneKarteToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dodajToolStripMenuItem,
            this.očistiToolStripMenuItem,
            this.dodajToolStripMenuItem1,
            this.izmeniToolStripMenuItem,
            this.obrišiToolStripMenuItem,
            this.izlazToolStripMenuItem,
            this.excelToolStripMenuItem});
            this.pretplatneKarteToolStripMenuItem.Name = "pretplatneKarteToolStripMenuItem";
            this.pretplatneKarteToolStripMenuItem.Size = new System.Drawing.Size(102, 20);
            this.pretplatneKarteToolStripMenuItem.Text = "Pretplatne karte";
            // 
            // dodajToolStripMenuItem
            // 
            this.dodajToolStripMenuItem.Name = "dodajToolStripMenuItem";
            this.dodajToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.dodajToolStripMenuItem.Text = "Pretraga";
            this.dodajToolStripMenuItem.Click += new System.EventHandler(this.PretragaToolStripMenuItem_Click);
            // 
            // očistiToolStripMenuItem
            // 
            this.očistiToolStripMenuItem.Name = "očistiToolStripMenuItem";
            this.očistiToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.očistiToolStripMenuItem.Text = "Očisti";
            this.očistiToolStripMenuItem.Click += new System.EventHandler(this.očistiToolStripMenuItem_Click);
            // 
            // dodajToolStripMenuItem1
            // 
            this.dodajToolStripMenuItem1.Name = "dodajToolStripMenuItem1";
            this.dodajToolStripMenuItem1.Size = new System.Drawing.Size(118, 22);
            this.dodajToolStripMenuItem1.Text = "Dodaj";
            this.dodajToolStripMenuItem1.Click += new System.EventHandler(this.dodajToolStripMenuItem1_Click);
            // 
            // izmeniToolStripMenuItem
            // 
            this.izmeniToolStripMenuItem.Name = "izmeniToolStripMenuItem";
            this.izmeniToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.izmeniToolStripMenuItem.Text = "Izmeni";
            this.izmeniToolStripMenuItem.Click += new System.EventHandler(this.izmeniToolStripMenuItem_Click);
            // 
            // obrišiToolStripMenuItem
            // 
            this.obrišiToolStripMenuItem.Name = "obrišiToolStripMenuItem";
            this.obrišiToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.obrišiToolStripMenuItem.Text = "Obriši";
            this.obrišiToolStripMenuItem.Click += new System.EventHandler(this.obrišiToolStripMenuItem_Click);
            // 
            // izlazToolStripMenuItem
            // 
            this.izlazToolStripMenuItem.Name = "izlazToolStripMenuItem";
            this.izlazToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.izlazToolStripMenuItem.Text = "Izlaz";
            this.izlazToolStripMenuItem.Click += new System.EventHandler(this.izlazToolStripMenuItem_Click);
            // 
            // excelToolStripMenuItem
            // 
            this.excelToolStripMenuItem.Name = "excelToolStripMenuItem";
            this.excelToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.excelToolStripMenuItem.Text = "Excel";
            this.excelToolStripMenuItem.Click += new System.EventHandler(this.excelToolStripMenuItem_Click);
            // 
            // panelZaGrid
            // 
            this.panelZaGrid.Controls.Add(this.dataGridViewPretplatneKarte);
            this.panelZaGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelZaGrid.Location = new System.Drawing.Point(3, 193);
            this.panelZaGrid.Name = "panelZaGrid";
            this.panelZaGrid.Size = new System.Drawing.Size(1270, 498);
            this.panelZaGrid.TabIndex = 2;
            // 
            // dataGridViewPretplatneKarte
            // 
            this.dataGridViewPretplatneKarte.AllowUserToAddRows = false;
            this.dataGridViewPretplatneKarte.AllowUserToDeleteRows = false;
            this.dataGridViewPretplatneKarte.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataGridViewPretplatneKarte.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewPretplatneKarte.ContextMenuStrip = this.contextMenuStripMeni;
            this.dataGridViewPretplatneKarte.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewPretplatneKarte.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewPretplatneKarte.Name = "dataGridViewPretplatneKarte";
            this.dataGridViewPretplatneKarte.ReadOnly = true;
            this.dataGridViewPretplatneKarte.Size = new System.Drawing.Size(1270, 498);
            this.dataGridViewPretplatneKarte.TabIndex = 11;
            this.dataGridViewPretplatneKarte.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewPretplatneKarte_CellClick);
            this.dataGridViewPretplatneKarte.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridViewPretplatneKarte_CellMouseDown);
            this.dataGridViewPretplatneKarte.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridViewPretplatneKarte_RowHeaderMouseClick);
            this.dataGridViewPretplatneKarte.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dataGridViewPretplatneKarte_MouseDown);
            // 
            // contextMenuStripMeni
            // 
            this.contextMenuStripMeni.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pretragaToolStripMenuItem,
            this.očistiToolStripMenuItem1,
            this.dodajToolStripMenuItem2,
            this.izmeniToolStripMenuItem1,
            this.obrišiToolStripMenuItem1,
            this.izlazToolStripMenuItem1});
            this.contextMenuStripMeni.Name = "contextMenuStripMeni";
            this.contextMenuStripMeni.Size = new System.Drawing.Size(119, 136);
            // 
            // pretragaToolStripMenuItem
            // 
            this.pretragaToolStripMenuItem.Name = "pretragaToolStripMenuItem";
            this.pretragaToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.pretragaToolStripMenuItem.Text = "Pretraga";
            this.pretragaToolStripMenuItem.Click += new System.EventHandler(this.pretragaToolStripMenuItem_Click_1);
            // 
            // očistiToolStripMenuItem1
            // 
            this.očistiToolStripMenuItem1.Name = "očistiToolStripMenuItem1";
            this.očistiToolStripMenuItem1.Size = new System.Drawing.Size(118, 22);
            this.očistiToolStripMenuItem1.Text = "Očisti";
            this.očistiToolStripMenuItem1.Click += new System.EventHandler(this.očistiToolStripMenuItem1_Click);
            // 
            // dodajToolStripMenuItem2
            // 
            this.dodajToolStripMenuItem2.Name = "dodajToolStripMenuItem2";
            this.dodajToolStripMenuItem2.Size = new System.Drawing.Size(118, 22);
            this.dodajToolStripMenuItem2.Text = "Dodaj";
            this.dodajToolStripMenuItem2.Click += new System.EventHandler(this.dodajToolStripMenuItem2_Click);
            // 
            // izmeniToolStripMenuItem1
            // 
            this.izmeniToolStripMenuItem1.Name = "izmeniToolStripMenuItem1";
            this.izmeniToolStripMenuItem1.Size = new System.Drawing.Size(118, 22);
            this.izmeniToolStripMenuItem1.Text = "Izmeni";
            this.izmeniToolStripMenuItem1.Click += new System.EventHandler(this.izmeniToolStripMenuItem1_Click);
            // 
            // obrišiToolStripMenuItem1
            // 
            this.obrišiToolStripMenuItem1.Name = "obrišiToolStripMenuItem1";
            this.obrišiToolStripMenuItem1.Size = new System.Drawing.Size(118, 22);
            this.obrišiToolStripMenuItem1.Text = "Obriši";
            this.obrišiToolStripMenuItem1.Click += new System.EventHandler(this.obrišiToolStripMenuItem1_Click);
            // 
            // izlazToolStripMenuItem1
            // 
            this.izlazToolStripMenuItem1.Name = "izlazToolStripMenuItem1";
            this.izlazToolStripMenuItem1.Size = new System.Drawing.Size(118, 22);
            this.izlazToolStripMenuItem1.Text = "Izlaz";
            this.izlazToolStripMenuItem1.Click += new System.EventHandler(this.izlazToolStripMenuItem1_Click);
            // 
            // frmPretplatneKarteInsertUpdatePretragacs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1276, 694);
            this.Controls.Add(this.tableLayoutPanelGlavni);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmPretplatneKarteInsertUpdatePretragacs";
            this.Text = "Pretplatne karte";
            this.Load += new System.EventHandler(this.frmPretplatneKarteInsertUpdatePretragacs_Load);
            this.SizeChanged += new System.EventHandler(this.frmPretplatneKarteInsertUpdatePretragacs_SizeChanged);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmPretplatneKarteInsertUpdatePretragacs_KeyDown);
            this.tableLayoutPanelGlavni.ResumeLayout(false);
            this.tableLayoutPanelZaDugmad.ResumeLayout(false);
            this.panelZaLabelInformaciju.ResumeLayout(false);
            this.panelZaLabelInformaciju.PerformLayout();
            this.panelZaDugmad.ResumeLayout(false);
            this.panelPoljaZaUnos.ResumeLayout(false);
            this.panelPoljaZaUnos.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panelZaGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPretplatneKarte)).EndInit();
            this.contextMenuStripMeni.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelGlavni;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelZaDugmad;
        private System.Windows.Forms.Panel panelZaLabelInformaciju;
        private System.Windows.Forms.Panel panelZaDugmad;
        private System.Windows.Forms.Panel panelPoljaZaUnos;
        private System.Windows.Forms.Panel panelZaGrid;
        private System.Windows.Forms.DataGridView dataGridViewPretplatneKarte;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtCena;
        private System.Windows.Forms.TextBox txtRegistracija;
        private System.Windows.Forms.DateTimePicker dateTimePickerDO;
        private System.Windows.Forms.DateTimePicker dateTimePickerOD;
        private System.Windows.Forms.Button btnPretraga;
        private System.Windows.Forms.Button btnOcisti;
        private System.Windows.Forms.Button btnObrisi;
        private System.Windows.Forms.Button btnIzmeni;
        private System.Windows.Forms.Button btnDodaj;
        private System.Windows.Forms.Button btnIzlaz;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem pretplatneKarteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dodajToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem očistiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dodajToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem izmeniToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem obrišiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem izlazToolStripMenuItem;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblInfo;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripMeni;
        private System.Windows.Forms.ToolStripMenuItem pretragaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem očistiToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem dodajToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem izmeniToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem obrišiToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem izlazToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem excelToolStripMenuItem;
    }
}