﻿using BusinessLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class frmPretplatneKarteInsertUpdatePretragacs : Form
    {
        int idPretplatneKarteIzabraneNaGridu = 0;
        DataTable dtPretplatneKarte = new DataTable();
        PretplatneKarteBusiness pkb;
        public frmPretplatneKarteInsertUpdatePretragacs()
        {
            InitializeComponent();
            this.WindowState = FormWindowState.Maximized;
            KeyPreview = true;
        }

        private void frmPretplatneKarteInsertUpdatePretragacs_Load(object sender, EventArgs e)
        {
            pkb = new PretplatneKarteBusiness();
            FormatDateTimePicker();
            UrediDugmad();
            LoadPretplatnihKarata();
            ResajzovanjeKolona();
            SakrivanjeKolona();
            PreimenovanjeKolona();
        }

        private void btnIzlaz_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FormatDateTimePicker()
        {
            this.dateTimePickerOD.Format = DateTimePickerFormat.Custom;
            this.dateTimePickerOD.CustomFormat = "dd-MM-yyyy";
            this.dateTimePickerDO.Format = DateTimePickerFormat.Custom;
            this.dateTimePickerDO.CustomFormat = "dd-MM-yyyy";
        }
        private void UrediDugmad()
        {
            btnPretraga.Width = 125;
            btnOcisti.Width = 125;
            btnDodaj.Width = 125;
            btnIzmeni.Width = 123;
            btnObrisi.Width = 122;
            btnIzlaz.Width = 122;

            btnPretraga.Left = 0;
            btnOcisti.Left = btnPretraga.Right;
            btnDodaj.Left = btnOcisti.Right;
            btnIzmeni.Left = btnDodaj.Right;
            btnObrisi.Left = btnIzmeni.Right;
            btnIzlaz.Left = btnObrisi.Right;
        }

        private void LoadPretplatnihKarata()
        {
            dtPretplatneKarte = pkb.GetAllPretplatneKarteSaNadzornikom();
            this.dataGridViewPretplatneKarte.DataSource = dtPretplatneKarte;
        }
        private void ResajzovanjeKolona()
        {
            if (this.dataGridViewPretplatneKarte.DataSource != null)
            {
                int sirinaSvihKolonaKojeSuVidljive = this.dataGridViewPretplatneKarte.Width;
                int sirinaJedneKolone = (sirinaSvihKolonaKojeSuVidljive - this.dataGridViewPretplatneKarte.RowHeadersWidth) / 6;

                this.dataGridViewPretplatneKarte.Columns["Registracija"].Width = sirinaJedneKolone;
                this.dataGridViewPretplatneKarte.Columns["VaziOD"].Width = sirinaJedneKolone;
                this.dataGridViewPretplatneKarte.Columns["VaziDO"].Width = sirinaJedneKolone;
                this.dataGridViewPretplatneKarte.Columns["Cena"].Width = sirinaJedneKolone;
                this.dataGridViewPretplatneKarte.Columns["DatumIzdavanja"].Width = sirinaJedneKolone;
                this.dataGridViewPretplatneKarte.Columns["KorisnickoImeNadzornika"].Width = sirinaJedneKolone;
            }
        }

        private void SakrivanjeKolona()
        {
            this.dataGridViewPretplatneKarte.Columns["PretplatneKarteID"].Visible = false;
            this.dataGridViewPretplatneKarte.Columns["NadzornikID"].Visible = false;
        }

        private void PreimenovanjeKolona()
        {
            this.dataGridViewPretplatneKarte.Columns["PretplatneKarteID"].HeaderText = "PretplatneKarteID";
            this.dataGridViewPretplatneKarte.Columns["Registracija"].HeaderText = "Registracija";
            this.dataGridViewPretplatneKarte.Columns["VaziOD"].HeaderText = "Važi od";
            this.dataGridViewPretplatneKarte.Columns["VaziDO"].HeaderText = "Važi do";
            this.dataGridViewPretplatneKarte.Columns["NadzornikID"].HeaderText = "NadzornikID";
            this.dataGridViewPretplatneKarte.Columns["Cena"].HeaderText = "Cena";
            this.dataGridViewPretplatneKarte.Columns["DatumIzdavanja"].HeaderText = "Datum izdavanja";
            this.dataGridViewPretplatneKarte.Columns["KorisnickoImeNadzornika"].HeaderText = "Korisničko ime nadzornika";
        }

        private void frmPretplatneKarteInsertUpdatePretragacs_SizeChanged(object sender, EventArgs e)
        {
            ResajzovanjeKolona();
        }

        private void btnPretraga_Click(object sender, EventArgs e)
        {
            PretragaPoSvimParametrima();
        }
        private void PretragaPoSvimParametrima()
        {
            PretplatnaKarta pk = new PretplatnaKarta();
            pk.Registracija = txtRegistracija.Text;
            string cenaStr = txtCena.Text;
            DateTime datumOD = this.dateTimePickerOD.Value;
            DateTime datumDO = this.dateTimePickerDO.Value;
            double a = 0;
            double b = 0;

            int brojCrtica = PreuzimanjeBrojevaIzCene(ref a, ref b, cenaStr);
            if (brojCrtica > 1)
            {
                MessageBox.Show("U polju cena je potrebno upisati jednu crticu (-) izmedju dve cene radi pretrage");
                return;
            }
            else
            {
                //var v = new { RegistracijaVozila = pk.Registracija, DatumOd = datumOD, DatumDo = datumDO, ManjiBroj = a, VeciBroj = b };
                dtPretplatneKarte = pkb.GetPretplatneKarteSaNadzornikomKaoRezultatPretragePosSvimParametrimaDT(pk.Registracija, datumOD, datumDO, a, b);
                this.dataGridViewPretplatneKarte.DataSource = dtPretplatneKarte;
                //dtPretplatneKarte = pkb.GetPretplatneKarteSaNadzornikomKaoRezultatPretragePosSvimParametrimaDT(v);
            }
        }
        //ako je broj crtica manji od 1 onda se uzima u obzir samo jedan broj(1) ako je broj crtica 1 onda se uzimaju u obzir oba. Ako ima vise od jedne crtice onda je greska
        private int PreuzimanjeBrojevaIzCene(ref double a, ref double b, string str)
        {
            if (str == "" || string.IsNullOrEmpty(str))
            {
                a = double.MaxValue;
                if (a > b)
                {
                    double temp = a;
                    a = b;
                    b = temp;
                }
                return 0;
            }
            int brojCrtica = str.Count(br => br == '-');
            if (brojCrtica == 1)
            {
                string text = "";
                foreach (var s in str)
                {
                    if (!(s.Equals(" ")))
                    {
                        text += s.ToString();
                    }
                }
                int index = text.IndexOf('-');
                string strA = text.Substring(0, index);
                string strB = text.Substring(index + 1, text.Length - 1 - index);
                a = double.Parse(strA);
                b = double.Parse(strB);
                if (a > b)
                {
                    double temp = a;
                    a = b;
                    b = temp;
                }
            }
            else if (brojCrtica == 0)
            {
                string text = "";
                foreach (var s in str)
                {
                    if (!(s.Equals(" ")))
                    {
                        text += s.ToString();
                    }
                }
                //int index = text.IndexOf('-');
                string strA = text.Substring(0, str.Length);
                a = double.Parse(strA);
                b = 0;
                if (a > b)
                {
                    double temp = a;
                    a = b;
                    b = temp;
                }
            }
            else
            {
                a = 0;
                b = 0;
            }
            return brojCrtica;
        }

        private void btnDodaj_Click(object sender, EventArgs e)
        {
            InsertPretplatneKarte();
        }
        private void InsertPretplatneKarte()
        {
            if (!PopunjenaSvaPolja())
            {
                MessageBox.Show("Potrebno je popuniti sva polja");
                return;
            }
            string registracija = txtRegistracija.Text;
            double cena = 0;
            bool uspesanParseCene = double.TryParse(txtCena.Text, out cena);
            if (uspesanParseCene == false)
            {
                MessageBox.Show("Cena mora da bude broj");
                return;
            }
            DateTime datumOD = dateTimePickerOD.Value;
            DateTime datumDO = dateTimePickerDO.Value;
            DateTime datumIzdavanja = DateTime.Now;

            NadzornikBusiness nb = new NadzornikBusiness();
            //IF blok ispod inicijalicuje korisnicko ime ako nisam usao na stranicu preko logina
            if (Program.korisnickoIme == "" || string.IsNullOrEmpty(Program.korisnickoIme))
            {
                Program.korisnickoIme = "MOLNAR";
            }
            Nadzornik nadzornik = nb.GetNadzornikNaOsnovuKorisnickogImena(Program.korisnickoIme);

            int idNadzornika = nadzornik.NadzornikID;
            if (datumOD > datumDO)
            {
                MessageBox.Show("Datum do mora biti veći od datuma od");
                return;
            }

            PretplatnaKarta pretplatnaKarta = new PretplatnaKarta();
            pretplatnaKarta.Registracija = registracija.ToUpper();
            pretplatnaKarta.Cena = cena;
            pretplatnaKarta.OD = datumOD;
            pretplatnaKarta.DO = datumDO;
            pretplatnaKarta.DatumIzdavanja = datumIzdavanja;
            pretplatnaKarta.NadzornikId = idNadzornika;


            bool ispravanDatumKarte = pkb.DaLiJeDatumNoveKarteVeciOdStarihZaIstiRegistarskiBroj(pretplatnaKarta);
            if (!ispravanDatumKarte)
            {
                MessageBox.Show("Datum karte nije ispravan, potrebno je uneti veći datum od datuma stare karte za ovo vozilo");
                return;
            }

            int uspeh = pkb.InsertPretplatneKarte(pretplatnaKarta);
            if (uspeh <= 0)
            {
                MessageBox.Show("Upis nije uspeo");
            }
            else
            {
                DataRow red = dtPretplatneKarte.NewRow();
                red["PretplatneKarteID"] = pkb.ZadnjInsertovaniID();
                red["Registracija"] = pretplatnaKarta.Registracija;
                red["VaziOD"] = pretplatnaKarta.OD;
                red["VaziDO"] = pretplatnaKarta.DO;
                red["Cena"] = pretplatnaKarta.Cena;
                red["DatumIzdavanja"] = pretplatnaKarta.DatumIzdavanja;
                red["KorisnickoImeNadzornika"] = Program.korisnickoIme;
                dtPretplatneKarte.Rows.InsertAt(red, 0);
                dtPretplatneKarte.AcceptChanges();
            }
        }
        private bool PopunjenaSvaPolja()
        {
            bool popunjenaSvaPolja = false;
            if (txtRegistracija.Text == "" || string.IsNullOrEmpty(txtRegistracija.Text)
                || txtCena.Text == "" || string.IsNullOrEmpty(txtCena.Text))
            {
                popunjenaSvaPolja = false;
            }
            else
            {
                popunjenaSvaPolja = true; 
            }
            return popunjenaSvaPolja;
        }

        private void btnOcisti_Click(object sender, EventArgs e)
        {
            Ocisti();
        }
        private void Ocisti()
        {
            txtCena.Text = "";
            txtRegistracija.Text = "";
            dateTimePickerDO.Value = DateTime.Now;
            dateTimePickerOD.Value = DateTime.Now;
        }

        private void dataGridViewPretplatneKarte_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0)
            {
                return;
            }
            int index = e.RowIndex;
            this.dataGridViewPretplatneKarte.Rows[index].Selected = true;
            idPretplatneKarteIzabraneNaGridu = int.Parse(this.dataGridViewPretplatneKarte.Rows[index].Cells["PretplatneKarteID"].FormattedValue.ToString());
            //MessageBox.Show("" + idPretplatneKarteIzabraneNaGridu.ToString());
            PopuniPoljaNaKLikReda();
            lblInfo.Text = "Info o karti za vozilo sa registracijom: " + this.dataGridViewPretplatneKarte.Rows[index].Cells["Registracija"].FormattedValue.ToString();
        }

        private void dataGridViewPretplatneKarte_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex < 0)
            {
                return;
            }
            int index = e.RowIndex;
            idPretplatneKarteIzabraneNaGridu = int.Parse(this.dataGridViewPretplatneKarte.Rows[index].Cells["PretplatneKarteID"].FormattedValue.ToString());
            //MessageBox.Show("" + idPretplatneKarteIzabraneNaGridu.ToString());
            PopuniPoljaNaKLikReda();
            lblInfo.Text = "Info o karti za vozilo sa registracijom: " + this.dataGridViewPretplatneKarte.Rows[index].Cells["Registracija"].FormattedValue.ToString();
        }

        private void dataGridViewPretplatneKarte_MouseDown(object sender, MouseEventArgs e)
        {
            
        }

        private void dataGridViewPretplatneKarte_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                int index = e.RowIndex;
                this.dataGridViewPretplatneKarte.ClearSelection();
                this.dataGridViewPretplatneKarte.Rows[index].Selected = true;
                idPretplatneKarteIzabraneNaGridu = int.Parse(this.dataGridViewPretplatneKarte.Rows[index].Cells["PretplatneKarteID"].FormattedValue.ToString());
                //MessageBox.Show("" + idPretplatneKarteIzabraneNaGridu.ToString());
                PopuniPoljaNaKLikReda();
            }
        }

        private void btnObrisi_Click(object sender, EventArgs e)
        {
            BrisanjeReda();
        }
        private void BrisanjeReda()
        {
            int uspehBrisanja = pkb.DeletePretplatneKarte(idPretplatneKarteIzabraneNaGridu);
            if (uspehBrisanja > 0)
            {
                for (int i = 0; i < dtPretplatneKarte.Rows.Count; i++)
                {
                    DataRow red = dtPretplatneKarte.Rows[i];
                    if (red["PretplatneKarteID"].ToString() == idPretplatneKarteIzabraneNaGridu.ToString())
                    {
                        red.Delete();
                    }
                }
                dtPretplatneKarte.AcceptChanges();
            }
        }

        private void btnIzmeni_Click(object sender, EventArgs e)
        {
            Izmeni();
        }
        private void Izmeni()
        {
            if (!PopunjenaSvaPolja())
            {
                MessageBox.Show("Potrebno je popuniti sva polja");
                return;
            }
            string registracija = txtRegistracija.Text;
            double cena = 0;
            bool uspesanParseCene = double.TryParse(txtCena.Text, out cena);
            if (uspesanParseCene == false)
            {
                MessageBox.Show("Cena mora da bude broj");
                return;
            }
            DateTime datumOD = dateTimePickerOD.Value;
            DateTime datumDO = dateTimePickerDO.Value;
            DateTime datumIzdavanja = DateTime.Now;

            NadzornikBusiness nb = new NadzornikBusiness();
            if (Program.korisnickoIme == "" || string.IsNullOrEmpty(Program.korisnickoIme))
            {
                Program.korisnickoIme = "MOLNAR";
            }
            Nadzornik nadzornik = nb.GetNadzornikNaOsnovuKorisnickogImena(Program.korisnickoIme);

            int idNadzornika = nadzornik.NadzornikID;
            if (datumOD > datumDO)
            {
                MessageBox.Show("Datum do mora biti veći od datuma od");
                return;
            }

            PretplatnaKarta pretplatnaKarta = new PretplatnaKarta();
            pretplatnaKarta.ID = idPretplatneKarteIzabraneNaGridu;
            pretplatnaKarta.Registracija = registracija.ToUpper();
            pretplatnaKarta.Cena = cena;
            pretplatnaKarta.OD = datumOD;
            pretplatnaKarta.DO = datumDO;
            pretplatnaKarta.DatumIzdavanja = datumIzdavanja;
            pretplatnaKarta.NadzornikId = idNadzornika;


            bool ispravanDatumKarte = pkb.DaLiJeDatumNoveKarteVeciOdStarihZaIstiRegistarskiBroj(pretplatnaKarta);
            if (!ispravanDatumKarte)
            {
                MessageBox.Show("Datum karte nije ispravan, potrebno je uneti veći datum od datuma stare karte za ovo vozilo");
                return;
            }

            int uspeh = pkb.UpdatePretplatneKarte(pretplatnaKarta);
            if (uspeh <= 0)
            {
                MessageBox.Show("Upis nije uspeo");
            }
            else
            {
                for (int i =0; i < dtPretplatneKarte.Rows.Count; i++)
                {
                    DataRow red = dtPretplatneKarte.Rows[i];
                    if (red["PretplatneKarteID"].ToString() == idPretplatneKarteIzabraneNaGridu.ToString())
                    {
                        red["Registracija"] = pretplatnaKarta.Registracija;
                        red["VaziOD"] = pretplatnaKarta.OD;
                        red["VaziDO"] = pretplatnaKarta.DO;
                        red["Cena"] = pretplatnaKarta.Cena;
                        red["NadzornikID"] = pretplatnaKarta.NadzornikId;
                        red["DatumIzdavanja"] = pretplatnaKarta.DatumIzdavanja;
                    }
                }
            }
        }
        private void PopuniPoljaNaKLikReda()
        {
            PretplatnaKarta pk = pkb.GetPretplatnaKartaNaOsnovuIDa(idPretplatneKarteIzabraneNaGridu);
            txtRegistracija.Text = pk.Registracija;
            txtCena.Text = pk.Cena.ToString();
            dateTimePickerOD.Value = pk.OD;
            dateTimePickerDO.Value = pk.DO;
        }

        private void PretragaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PretragaPoSvimParametrima();
        }

        private void očistiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Ocisti();
        }

        private void dodajToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            InsertPretplatneKarte();
        }

        private void izmeniToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Izmeni();
        }

        private void obrišiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BrisanjeReda();
        }

        private void izlazToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pretragaToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            PretragaPoSvimParametrima();
        }

        private void očistiToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Ocisti();
        }

        private void dodajToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            InsertPretplatneKarte();
        }

        private void izmeniToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Izmeni();
        }

        private void obrišiToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            BrisanjeReda();
        }

        private void izlazToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ExportGridaExcel(DataGridView dataGridViewPrikaz)
        {
            //nevidljive kolone su 1 i 5 odnosno 0 i 4

            // Creating a Excel object. 
            Microsoft.Office.Interop.Excel._Application excel = new Microsoft.Office.Interop.Excel.Application();
            Microsoft.Office.Interop.Excel._Workbook workbook = excel.Workbooks.Add(Type.Missing);
            Microsoft.Office.Interop.Excel._Worksheet worksheet = workbook.Worksheets.Add();


            string putanja = "";
            try
            {

                worksheet = workbook.ActiveSheet;

                worksheet.Name = "Pretplatne karte";

                int indexKolone = 1;
                int indexReda = 2;

                for (int i = 0; i < dataGridViewPrikaz.Columns.Count; i++)
                {
                    if (!(i == 0 || i == 4))
                    {
                        worksheet.Cells[1, indexKolone] = dataGridViewPrikaz.Columns[i].HeaderText;
                        indexKolone++;
                    }
                }

                indexKolone = 1;

                for (int i = 0; i < dataGridViewPrikaz.Rows.Count; i++)
                {
                    for (int j = 0; j < dataGridViewPrikaz.Columns.Count; j++)
                    {
                        if (!(j == 0 || j == 4))
                        {
                            worksheet.Cells[indexReda, indexKolone] = dataGridViewPrikaz.Rows[i].Cells[j].Value.ToString();
                            indexKolone++;
                        }
                    }
                    indexKolone = 1;
                    indexReda++;
                }


                //Getting the location and file name of the excel to save from user. 
                SaveFileDialog saveDialog = new SaveFileDialog();
                saveDialog.Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
                saveDialog.FilterIndex = 2;

                if (saveDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    workbook.SaveAs(saveDialog.FileName);
                    MessageBox.Show("Excel datoteka je uspeno izvezena");
                    putanja = saveDialog.FileName;
                }

            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                workbook.Close(false);
                excel.Quit();
                workbook = null;
                excel = null;
                GC.Collect();
                UbiProces(putanja);
            }
        }

        private void UbiProces(string putanja)
        {
            System.Diagnostics.Process[] process = System.Diagnostics.Process.GetProcessesByName("Excel");
            foreach (System.Diagnostics.Process p in process)
            {
                if (!string.IsNullOrEmpty(p.ProcessName))
                {
                    try
                    {
                        p.Kill();
                    }
                    catch { }
                }
            }
        }

        private void excelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ExportGridaExcel(this.dataGridViewPretplatneKarte);
        }

        private void frmPretplatneKarteInsertUpdatePretragacs_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F12)
            {
                ExportGridaExcel(this.dataGridViewPretplatneKarte);
            }
        }
    }
}
