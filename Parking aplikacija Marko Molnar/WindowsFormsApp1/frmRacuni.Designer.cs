﻿namespace WindowsFormsApp1
{
    partial class frmRacuni
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tableLayoutPanelGlavni = new System.Windows.Forms.TableLayoutPanel();
            this.panelFilter = new System.Windows.Forms.Panel();
            this.btnPretraga = new System.Windows.Forms.Button();
            this.lblVremeIzlaza = new System.Windows.Forms.Label();
            this.lblVremeUlaza = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtUkupnaCenaDO = new System.Windows.Forms.TextBox();
            this.txtUkupnaCenaOD = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtSatiDO = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtSatiOD = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtTablice = new System.Windows.Forms.TextBox();
            this.txtBrojRacuna = new System.Windows.Forms.TextBox();
            this.panelZaGrid = new System.Windows.Forms.Panel();
            this.dataTableViewRacuni = new System.Windows.Forms.DataGridView();
            this.tableLayoutPanelGlavni.SuspendLayout();
            this.panelFilter.SuspendLayout();
            this.panelZaGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataTableViewRacuni)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanelGlavni
            // 
            this.tableLayoutPanelGlavni.ColumnCount = 1;
            this.tableLayoutPanelGlavni.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelGlavni.Controls.Add(this.panelFilter, 0, 0);
            this.tableLayoutPanelGlavni.Controls.Add(this.panelZaGrid, 0, 1);
            this.tableLayoutPanelGlavni.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelGlavni.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanelGlavni.Name = "tableLayoutPanelGlavni";
            this.tableLayoutPanelGlavni.RowCount = 2;
            this.tableLayoutPanelGlavni.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanelGlavni.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelGlavni.Size = new System.Drawing.Size(1276, 694);
            this.tableLayoutPanelGlavni.TabIndex = 0;
            // 
            // panelFilter
            // 
            this.panelFilter.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panelFilter.Controls.Add(this.btnPretraga);
            this.panelFilter.Controls.Add(this.lblVremeIzlaza);
            this.panelFilter.Controls.Add(this.lblVremeUlaza);
            this.panelFilter.Controls.Add(this.label6);
            this.panelFilter.Controls.Add(this.label5);
            this.panelFilter.Controls.Add(this.txtUkupnaCenaDO);
            this.panelFilter.Controls.Add(this.txtUkupnaCenaOD);
            this.panelFilter.Controls.Add(this.label4);
            this.panelFilter.Controls.Add(this.txtSatiDO);
            this.panelFilter.Controls.Add(this.label3);
            this.panelFilter.Controls.Add(this.txtSatiOD);
            this.panelFilter.Controls.Add(this.label2);
            this.panelFilter.Controls.Add(this.label1);
            this.panelFilter.Controls.Add(this.txtTablice);
            this.panelFilter.Controls.Add(this.txtBrojRacuna);
            this.panelFilter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelFilter.Location = new System.Drawing.Point(3, 3);
            this.panelFilter.Name = "panelFilter";
            this.panelFilter.Size = new System.Drawing.Size(1270, 194);
            this.panelFilter.TabIndex = 0;
            // 
            // btnPretraga
            // 
            this.btnPretraga.Location = new System.Drawing.Point(865, 54);
            this.btnPretraga.Name = "btnPretraga";
            this.btnPretraga.Size = new System.Drawing.Size(125, 30);
            this.btnPretraga.TabIndex = 6;
            this.btnPretraga.Text = "Pretraga";
            this.btnPretraga.UseVisualStyleBackColor = true;
            this.btnPretraga.Click += new System.EventHandler(this.btnPretraga_Click);
            // 
            // lblVremeIzlaza
            // 
            this.lblVremeIzlaza.AutoSize = true;
            this.lblVremeIzlaza.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVremeIzlaza.Location = new System.Drawing.Point(15, 148);
            this.lblVremeIzlaza.Name = "lblVremeIzlaza";
            this.lblVremeIzlaza.Size = new System.Drawing.Size(97, 18);
            this.lblVremeIzlaza.TabIndex = 13;
            this.lblVremeIzlaza.Text = "Vreme izlaza:";
            // 
            // lblVremeUlaza
            // 
            this.lblVremeUlaza.AutoSize = true;
            this.lblVremeUlaza.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVremeUlaza.Location = new System.Drawing.Point(14, 103);
            this.lblVremeUlaza.Name = "lblVremeUlaza";
            this.lblVremeUlaza.Size = new System.Drawing.Size(94, 18);
            this.lblVremeUlaza.TabIndex = 12;
            this.lblVremeUlaza.Text = "Vreme ulaza:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(603, 61);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(116, 18);
            this.label6.TabIndex = 11;
            this.label6.Text = "Ukupna cena do";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(603, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(116, 18);
            this.label5.TabIndex = 10;
            this.label5.Text = "Ukupna cena od";
            // 
            // txtUkupnaCenaDO
            // 
            this.txtUkupnaCenaDO.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUkupnaCenaDO.Location = new System.Drawing.Point(727, 58);
            this.txtUkupnaCenaDO.Name = "txtUkupnaCenaDO";
            this.txtUkupnaCenaDO.Size = new System.Drawing.Size(100, 24);
            this.txtUkupnaCenaDO.TabIndex = 5;
            // 
            // txtUkupnaCenaOD
            // 
            this.txtUkupnaCenaOD.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUkupnaCenaOD.Location = new System.Drawing.Point(727, 19);
            this.txtUkupnaCenaOD.Name = "txtUkupnaCenaOD";
            this.txtUkupnaCenaOD.Size = new System.Drawing.Size(100, 24);
            this.txtUkupnaCenaOD.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(375, 61);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 18);
            this.label4.TabIndex = 7;
            this.label4.Text = "Broj sati do";
            // 
            // txtSatiDO
            // 
            this.txtSatiDO.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSatiDO.Location = new System.Drawing.Point(471, 54);
            this.txtSatiDO.Name = "txtSatiDO";
            this.txtSatiDO.Size = new System.Drawing.Size(100, 24);
            this.txtSatiDO.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(375, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 18);
            this.label3.TabIndex = 5;
            this.label3.Text = "Broj sati od";
            // 
            // txtSatiOD
            // 
            this.txtSatiOD.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSatiOD.Location = new System.Drawing.Point(471, 19);
            this.txtSatiOD.Name = "txtSatiOD";
            this.txtSatiOD.Size = new System.Drawing.Size(100, 24);
            this.txtSatiOD.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(14, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 18);
            this.label2.TabIndex = 3;
            this.label2.Text = "Broj tablice";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(14, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 18);
            this.label1.TabIndex = 2;
            this.label1.Text = "Broj računa";
            // 
            // txtTablice
            // 
            this.txtTablice.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTablice.Location = new System.Drawing.Point(144, 54);
            this.txtTablice.Name = "txtTablice";
            this.txtTablice.Size = new System.Drawing.Size(178, 24);
            this.txtTablice.TabIndex = 1;
            // 
            // txtBrojRacuna
            // 
            this.txtBrojRacuna.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBrojRacuna.Location = new System.Drawing.Point(144, 19);
            this.txtBrojRacuna.Name = "txtBrojRacuna";
            this.txtBrojRacuna.Size = new System.Drawing.Size(178, 24);
            this.txtBrojRacuna.TabIndex = 0;
            // 
            // panelZaGrid
            // 
            this.panelZaGrid.Controls.Add(this.dataTableViewRacuni);
            this.panelZaGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelZaGrid.Location = new System.Drawing.Point(3, 203);
            this.panelZaGrid.Name = "panelZaGrid";
            this.panelZaGrid.Size = new System.Drawing.Size(1270, 488);
            this.panelZaGrid.TabIndex = 1;
            // 
            // dataTableViewRacuni
            // 
            this.dataTableViewRacuni.AllowUserToAddRows = false;
            this.dataTableViewRacuni.AllowUserToDeleteRows = false;
            this.dataTableViewRacuni.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataTableViewRacuni.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataTableViewRacuni.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataTableViewRacuni.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataTableViewRacuni.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataTableViewRacuni.Location = new System.Drawing.Point(0, 0);
            this.dataTableViewRacuni.Name = "dataTableViewRacuni";
            this.dataTableViewRacuni.ReadOnly = true;
            this.dataTableViewRacuni.Size = new System.Drawing.Size(1270, 488);
            this.dataTableViewRacuni.TabIndex = 7;
            this.dataTableViewRacuni.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewRacuni_CellClick);
            // 
            // frmRacuni
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1276, 694);
            this.Controls.Add(this.tableLayoutPanelGlavni);
            this.Name = "frmRacuni";
            this.Text = "Računi";
            this.Load += new System.EventHandler(this.frmRacuni_Load);
            this.SizeChanged += new System.EventHandler(this.frmRacuni_SizeChanged);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmRacuni_KeyDown);
            this.tableLayoutPanelGlavni.ResumeLayout(false);
            this.panelFilter.ResumeLayout(false);
            this.panelFilter.PerformLayout();
            this.panelZaGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataTableViewRacuni)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelGlavni;
        private System.Windows.Forms.Panel panelFilter;
        private System.Windows.Forms.Panel panelZaGrid;
        private System.Windows.Forms.DataGridView dataTableViewRacuni;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtTablice;
        private System.Windows.Forms.TextBox txtBrojRacuna;
        private System.Windows.Forms.Label lblVremeIzlaza;
        private System.Windows.Forms.Label lblVremeUlaza;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtUkupnaCenaDO;
        private System.Windows.Forms.TextBox txtUkupnaCenaOD;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtSatiDO;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtSatiOD;
        private System.Windows.Forms.Button btnPretraga;
    }
}