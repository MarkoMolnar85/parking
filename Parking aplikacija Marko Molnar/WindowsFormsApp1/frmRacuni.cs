﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BusinessLayer;
using DataLayer.Models;

namespace WindowsFormsApp1
{
    public partial class frmRacuni : Form
    {
        DataTable dtRacuni = new DataTable();
        RacunBusiness rb = new RacunBusiness();
        
        public frmRacuni()
        {
            InitializeComponent();
            this.WindowState = FormWindowState.Maximized;
            KeyPreview = true;
        }

        private void frmRacuni_Load(object sender, EventArgs e)
        {
            LoadRacuni();
            SakrivanjeKolona();
            ResajzovanjeKolona();
        }
        private void LoadRacuni()
        {
            //izbrisati blok koda. sluzi da nebi morao da otvaram login dformu
            if (Program.idLokacijeIzKomboBoxaKodFormeZaNadzornika == 0)
            {
                Program.idLokacijeIzKomboBoxaKodFormeZaNadzornika = 1002;
            }
            dtRacuni = rb.GetSviRacuniNaOsnovuLokacije(Program.idLokacijeIzKomboBoxaKodFormeZaNadzornika);
            this.dataTableViewRacuni.DataSource = dtRacuni;
        }
        private void SakrivanjeKolona()
        {
            this.dataTableViewRacuni.Columns["IzlazID"].Visible = false;
            this.dataTableViewRacuni.Columns["UlazID"].Visible = false;
            this.dataTableViewRacuni.Columns["Vreme na parkingu"].Visible = false;
            this.dataTableViewRacuni.Columns["CenaID"].Visible = false;
        }
        private void ResajzovanjeKolona()
        {
            if (this.dataTableViewRacuni.DataSource != null)
            {
                int brojKolona = 4;
                int sirinaKolone = (this.dataTableViewRacuni.Width - this.dataTableViewRacuni.RowHeadersWidth - 25) / brojKolona;
                this.dataTableViewRacuni.Columns["Broj računa"].Width = sirinaKolone;
                this.dataTableViewRacuni.Columns["Tablica"].Width = sirinaKolone;
                this.dataTableViewRacuni.Columns["Ukupna cena"].Width = sirinaKolone;
                this.dataTableViewRacuni.Columns["Sati na parkingu"].Width = sirinaKolone;
            }
        }

        private void frmRacuni_SizeChanged(object sender, EventArgs e)
        {
            ResajzovanjeKolona();
        }

        private void dataGridViewRacuni_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int index = e.RowIndex;
            if (!(index < 0))
            {
                this.dataTableViewRacuni.Rows[index].Selected = true;
                UlazBusiness ub = new UlazBusiness();
                int idUlaza = int.Parse(this.dataTableViewRacuni.Rows[index].Cells["UlazID"].FormattedValue.ToString());
                Ulaz u = ub.GetUlazPremaIDu(idUlaza);
                lblVremeUlaza.Text = "Vreme ulaza: " + u.Vreme;

                IzlazBusiness ib = new IzlazBusiness();
                int idIzlaza = int.Parse(this.dataTableViewRacuni.Rows[index].Cells["IzlazID"].FormattedValue.ToString());
                Izlaz iz = ib.GetIzlazPremaIDeu(idIzlaza);
                lblVremeIzlaza.Text = "Vreme izlaza: " + iz.VremeP;
            }
        }
        private void PretragaPoSvimParametrima()
        {
            Racun r = new Racun();

            int idRacuna = 0;
            bool brojRacunaProvera = int.TryParse(txtBrojRacuna.Text, out idRacuna);
            if (brojRacunaProvera == false)
            {
                MessageBox.Show("Broj računa mora da bude broj");
                return;
            }
            r.RacunID = idRacuna;
            r.Tablica = txtTablice.Text;

            bool satiODProvera = true;
            bool satiDOProvera = true;
            bool cenaODProvera = true;
            bool cenaDOProvera = true;
            int satiOD = 0;
            int satiDO = 0;
            double ukupnaCenaOD = 0;
            double ukupnaCenaDO = 0;
            if (txtSatiOD.Text == "" || string.IsNullOrEmpty(txtSatiOD.Text))
            {
                satiOD = 0;
            }
            else
            {
                satiODProvera = int.TryParse(txtSatiOD.Text, out satiOD);
            }
            if (txtSatiDO.Text == "" || string.IsNullOrEmpty(txtSatiDO.Text))
            {
                satiDO = int.MaxValue;
            }
            else
            {
                satiDOProvera = int.TryParse(txtSatiDO.Text, out satiDO);
            }
            if (txtUkupnaCenaOD.Text == "" || string.IsNullOrEmpty(txtUkupnaCenaOD.Text))
            {
                ukupnaCenaOD = 0;
            }
            else
            {
                cenaODProvera = double.TryParse(txtUkupnaCenaOD.Text, out ukupnaCenaOD);
            }
            if (txtUkupnaCenaDO.Text == "" || string.IsNullOrEmpty(txtUkupnaCenaDO.Text))
            {
                ukupnaCenaDO = double.MaxValue;
            }
            else
            {
                cenaDOProvera = double.TryParse(txtUkupnaCenaDO.Text, out ukupnaCenaDO);
            }
            if (satiODProvera && satiDOProvera && cenaODProvera && cenaDOProvera)
            {
                dtRacuni = rb.PretragaRacunaPoSvimParametrima(r, satiOD, satiDO, ukupnaCenaOD, ukupnaCenaDO, Program.idLokacijeIzKomboBoxaKodFormeZaNadzornika);
                this.dataTableViewRacuni.DataSource = dtRacuni;
            }
            else
            {
                MessageBox.Show("Polja za cenu i sate moraju biti brojevi");
            }

        }

        private void btnPretraga_Click(object sender, EventArgs e)
        {
            PretragaPoSvimParametrima();
        }

        private void ExportGridaExcel(DataGridView dataGridViewPrikaz)
        {
            //nevidljive kolone su 3, 4, 7 odnosno 2, 3, 6

            // Creating a Excel object. 
            Microsoft.Office.Interop.Excel._Application excel = new Microsoft.Office.Interop.Excel.Application();
            Microsoft.Office.Interop.Excel._Workbook workbook = excel.Workbooks.Add(Type.Missing);
            Microsoft.Office.Interop.Excel._Worksheet worksheet = workbook.Worksheets.Add();


            string putanja = "";
            try
            {

                worksheet = workbook.ActiveSheet;

                worksheet.Name = "Računi";

                int indexKolone = 1;
                int indexReda = 2;

                for (int i = 0; i < dataGridViewPrikaz.Columns.Count; i++)
                {
                    if (!(i == 2 || i == 3 || i == 6))
                    {
                        worksheet.Cells[1, indexKolone] = dataGridViewPrikaz.Columns[i].HeaderText;
                        indexKolone++;
                    }
                }

                indexKolone = 1;

                for (int i = 0; i < dataGridViewPrikaz.Rows.Count; i++)
                {
                    for (int j = 0; j < dataGridViewPrikaz.Columns.Count; j++)
                    {
                        if (!(j == 2 || j == 3 || j == 6))
                        {
                            worksheet.Cells[indexReda, indexKolone] = dataGridViewPrikaz.Rows[i].Cells[j].Value.ToString();
                            indexKolone++;
                        }
                    }
                    indexKolone = 1;
                    indexReda++;
                }


                //Getting the location and file name of the excel to save from user. 
                SaveFileDialog saveDialog = new SaveFileDialog();
                saveDialog.Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
                saveDialog.FilterIndex = 2;

                if (saveDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    workbook.SaveAs(saveDialog.FileName);
                    MessageBox.Show("Excel datoteka je uspeno izvezena");
                    putanja = saveDialog.FileName;
                }

            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                workbook.Close(false);
                excel.Quit();
                workbook = null;
                excel = null;
                GC.Collect();
                UbiProces(putanja);
            }
        }

        private void UbiProces(string putanja)
        {
            System.Diagnostics.Process[] process = System.Diagnostics.Process.GetProcessesByName("Excel");
            foreach (System.Diagnostics.Process p in process)
            {
                if (!string.IsNullOrEmpty(p.ProcessName))
                {
                    try
                    {
                        p.Kill();
                    }
                    catch { }
                }
            }
        }

        private void frmRacuni_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F12)
            {
                ExportGridaExcel(this.dataTableViewRacuni);
            }
        }
    }
}
