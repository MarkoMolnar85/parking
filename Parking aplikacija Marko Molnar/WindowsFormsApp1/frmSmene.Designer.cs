﻿namespace WindowsFormsApp1
{
    partial class frmSmene
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.dateTimePickerPocetakPrveSmene = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerPocetakDrugeSmene = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerPocetakTreceSmene = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerKrajPrveSmene = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerKrajTreceSmene = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerKrajDrugeSmene = new System.Windows.Forms.DateTimePicker();
            this.btnUnos = new System.Windows.Forms.Button();
            this.btnManjeSati = new System.Windows.Forms.Button();
            this.btnVeceSati = new System.Windows.Forms.Button();
            this.btnManjeMinuti = new System.Windows.Forms.Button();
            this.btnVeceMinuti = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(230, 155);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(153, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Početak prve smene";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(230, 216);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(164, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Početak druge smene";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(230, 272);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(159, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "Početak treće smene";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(651, 154);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(122, 20);
            this.label4.TabIndex = 3;
            this.label4.Text = "Kraj prve smene";
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(651, 216);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(133, 20);
            this.label5.TabIndex = 4;
            this.label5.Text = "Kraj druge smene";
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(651, 272);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(128, 20);
            this.label6.TabIndex = 5;
            this.label6.Text = "Kraj treće smene";
            // 
            // dateTimePickerPocetakPrveSmene
            // 
            this.dateTimePickerPocetakPrveSmene.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dateTimePickerPocetakPrveSmene.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePickerPocetakPrveSmene.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePickerPocetakPrveSmene.Location = new System.Drawing.Point(419, 148);
            this.dateTimePickerPocetakPrveSmene.Name = "dateTimePickerPocetakPrveSmene";
            this.dateTimePickerPocetakPrveSmene.Size = new System.Drawing.Size(151, 26);
            this.dateTimePickerPocetakPrveSmene.TabIndex = 0;
            // 
            // dateTimePickerPocetakDrugeSmene
            // 
            this.dateTimePickerPocetakDrugeSmene.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dateTimePickerPocetakDrugeSmene.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePickerPocetakDrugeSmene.Location = new System.Drawing.Point(419, 210);
            this.dateTimePickerPocetakDrugeSmene.Name = "dateTimePickerPocetakDrugeSmene";
            this.dateTimePickerPocetakDrugeSmene.Size = new System.Drawing.Size(151, 26);
            this.dateTimePickerPocetakDrugeSmene.TabIndex = 2;
            // 
            // dateTimePickerPocetakTreceSmene
            // 
            this.dateTimePickerPocetakTreceSmene.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dateTimePickerPocetakTreceSmene.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePickerPocetakTreceSmene.Location = new System.Drawing.Point(419, 266);
            this.dateTimePickerPocetakTreceSmene.Name = "dateTimePickerPocetakTreceSmene";
            this.dateTimePickerPocetakTreceSmene.Size = new System.Drawing.Size(151, 26);
            this.dateTimePickerPocetakTreceSmene.TabIndex = 4;
            // 
            // dateTimePickerKrajPrveSmene
            // 
            this.dateTimePickerKrajPrveSmene.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dateTimePickerKrajPrveSmene.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePickerKrajPrveSmene.Location = new System.Drawing.Point(812, 149);
            this.dateTimePickerKrajPrveSmene.Name = "dateTimePickerKrajPrveSmene";
            this.dateTimePickerKrajPrveSmene.Size = new System.Drawing.Size(151, 26);
            this.dateTimePickerKrajPrveSmene.TabIndex = 1;
            // 
            // dateTimePickerKrajTreceSmene
            // 
            this.dateTimePickerKrajTreceSmene.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dateTimePickerKrajTreceSmene.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePickerKrajTreceSmene.Location = new System.Drawing.Point(812, 266);
            this.dateTimePickerKrajTreceSmene.Name = "dateTimePickerKrajTreceSmene";
            this.dateTimePickerKrajTreceSmene.Size = new System.Drawing.Size(151, 26);
            this.dateTimePickerKrajTreceSmene.TabIndex = 5;
            // 
            // dateTimePickerKrajDrugeSmene
            // 
            this.dateTimePickerKrajDrugeSmene.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dateTimePickerKrajDrugeSmene.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePickerKrajDrugeSmene.Location = new System.Drawing.Point(812, 210);
            this.dateTimePickerKrajDrugeSmene.Name = "dateTimePickerKrajDrugeSmene";
            this.dateTimePickerKrajDrugeSmene.Size = new System.Drawing.Size(151, 26);
            this.dateTimePickerKrajDrugeSmene.TabIndex = 3;
            // 
            // btnUnos
            // 
            this.btnUnos.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnUnos.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUnos.Location = new System.Drawing.Point(846, 467);
            this.btnUnos.Name = "btnUnos";
            this.btnUnos.Size = new System.Drawing.Size(117, 30);
            this.btnUnos.TabIndex = 10;
            this.btnUnos.Text = "Unos";
            this.btnUnos.UseVisualStyleBackColor = true;
            this.btnUnos.Click += new System.EventHandler(this.btnUnos_Click);
            // 
            // btnManjeSati
            // 
            this.btnManjeSati.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnManjeSati.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnManjeSati.Location = new System.Drawing.Point(790, 344);
            this.btnManjeSati.Name = "btnManjeSati";
            this.btnManjeSati.Size = new System.Drawing.Size(73, 30);
            this.btnManjeSati.TabIndex = 6;
            this.btnManjeSati.Text = "<";
            this.btnManjeSati.UseVisualStyleBackColor = true;
            this.btnManjeSati.Click += new System.EventHandler(this.btnManjeSati_Click);
            // 
            // btnVeceSati
            // 
            this.btnVeceSati.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnVeceSati.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVeceSati.Location = new System.Drawing.Point(890, 344);
            this.btnVeceSati.Name = "btnVeceSati";
            this.btnVeceSati.Size = new System.Drawing.Size(73, 30);
            this.btnVeceSati.TabIndex = 7;
            this.btnVeceSati.Text = ">";
            this.btnVeceSati.UseVisualStyleBackColor = true;
            this.btnVeceSati.Click += new System.EventHandler(this.btnVeceSati_Click);
            // 
            // btnManjeMinuti
            // 
            this.btnManjeMinuti.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnManjeMinuti.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnManjeMinuti.Location = new System.Drawing.Point(790, 400);
            this.btnManjeMinuti.Name = "btnManjeMinuti";
            this.btnManjeMinuti.Size = new System.Drawing.Size(73, 30);
            this.btnManjeMinuti.TabIndex = 8;
            this.btnManjeMinuti.Text = "<";
            this.btnManjeMinuti.UseVisualStyleBackColor = true;
            this.btnManjeMinuti.Click += new System.EventHandler(this.btnManjeMinuti_Click);
            // 
            // btnVeceMinuti
            // 
            this.btnVeceMinuti.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnVeceMinuti.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVeceMinuti.Location = new System.Drawing.Point(890, 400);
            this.btnVeceMinuti.Name = "btnVeceMinuti";
            this.btnVeceMinuti.Size = new System.Drawing.Size(73, 30);
            this.btnVeceMinuti.TabIndex = 9;
            this.btnVeceMinuti.Text = ">";
            this.btnVeceMinuti.UseVisualStyleBackColor = true;
            this.btnVeceMinuti.Click += new System.EventHandler(this.btnVeceMinuti_Click);
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(707, 349);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(37, 20);
            this.label7.TabIndex = 17;
            this.label7.Text = "Sati";
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(707, 405);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(51, 20);
            this.label8.TabIndex = 18;
            this.label8.Text = "Minuti";
            // 
            // frmSmene
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1276, 694);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.btnVeceMinuti);
            this.Controls.Add(this.btnManjeMinuti);
            this.Controls.Add(this.btnVeceSati);
            this.Controls.Add(this.btnManjeSati);
            this.Controls.Add(this.btnUnos);
            this.Controls.Add(this.dateTimePickerKrajDrugeSmene);
            this.Controls.Add(this.dateTimePickerKrajTreceSmene);
            this.Controls.Add(this.dateTimePickerKrajPrveSmene);
            this.Controls.Add(this.dateTimePickerPocetakTreceSmene);
            this.Controls.Add(this.dateTimePickerPocetakDrugeSmene);
            this.Controls.Add(this.dateTimePickerPocetakPrveSmene);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "frmSmene";
            this.Text = "Smene";
            this.Load += new System.EventHandler(this.frmSmene_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker dateTimePickerPocetakPrveSmene;
        private System.Windows.Forms.DateTimePicker dateTimePickerPocetakDrugeSmene;
        private System.Windows.Forms.DateTimePicker dateTimePickerPocetakTreceSmene;
        private System.Windows.Forms.DateTimePicker dateTimePickerKrajPrveSmene;
        private System.Windows.Forms.DateTimePicker dateTimePickerKrajTreceSmene;
        private System.Windows.Forms.DateTimePicker dateTimePickerKrajDrugeSmene;
        private System.Windows.Forms.Button btnUnos;
        private System.Windows.Forms.Button btnManjeSati;
        private System.Windows.Forms.Button btnVeceSati;
        private System.Windows.Forms.Button btnManjeMinuti;
        private System.Windows.Forms.Button btnVeceMinuti;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
    }
}