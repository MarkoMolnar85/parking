﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BusinessLayer;
using DataLayer.Models;

namespace WindowsFormsApp1
{
    public partial class frmSmene : Form
    {
        SmenaBusiness sb = new SmenaBusiness();
        public frmSmene()
        {
            InitializeComponent();
            this.WindowState = FormWindowState.Maximized;
            //KeyPreview = true;
        }

        private void frmSmene_Load(object sender, EventArgs e)
        {
            UrediDateTimePickere();
            LoadSmene();
        }
        private void UrediDateTimePickere()
        {
            this.dateTimePickerPocetakPrveSmene.Format = DateTimePickerFormat.Custom;
            dateTimePickerPocetakPrveSmene.CustomFormat = "HH:mm";
            dateTimePickerPocetakPrveSmene.ShowUpDown = true;

            this.dateTimePickerPocetakDrugeSmene.Format = DateTimePickerFormat.Custom;
            dateTimePickerPocetakDrugeSmene.CustomFormat = "HH:mm";
            dateTimePickerPocetakDrugeSmene.ShowUpDown = true;

            this.dateTimePickerPocetakTreceSmene.Format = DateTimePickerFormat.Custom;
            dateTimePickerPocetakTreceSmene.CustomFormat = "HH:mm";
            dateTimePickerPocetakTreceSmene.ShowUpDown = true;

            this.dateTimePickerKrajPrveSmene.Format = DateTimePickerFormat.Custom;
            dateTimePickerKrajPrveSmene.CustomFormat = "HH:mm";
            dateTimePickerKrajPrveSmene.ShowUpDown = true;

            this.dateTimePickerKrajDrugeSmene.Format = DateTimePickerFormat.Custom;
            dateTimePickerKrajDrugeSmene.CustomFormat = "HH:mm";
            dateTimePickerKrajDrugeSmene.ShowUpDown = true;

            this.dateTimePickerKrajTreceSmene.Format = DateTimePickerFormat.Custom;
            dateTimePickerKrajTreceSmene.CustomFormat = "HH:mm";
            dateTimePickerKrajTreceSmene.ShowUpDown = true;

            dateTimePickerPocetakPrveSmene.Enabled = false;
            dateTimePickerPocetakDrugeSmene.Enabled = false;
            dateTimePickerPocetakTreceSmene.Enabled = false;
            dateTimePickerKrajPrveSmene.Enabled = false;
            dateTimePickerKrajDrugeSmene.Enabled = false;
            dateTimePickerKrajTreceSmene.Enabled = false;
        }

        private void btnUnos_Click(object sender, EventArgs e)
        {
            //MessageBox.Show(dateTimePickerPocetakPrveSmene.Value.ToString());
            //TimeSpan ts = TimeSpan.Parse(dateTimePickerPocetakPrveSmene.Value.ToString("HH:mm"));
            //MessageBox.Show(ts.ToString());
            ////DateTime dt = DateTime.Parse(ts.ToString("HH:mm:ss"));
            //DateTime dt = DateTime.Today;
            //dateTimePickerPocetakPrveSmene.Value = dt.Add(ts);
            //MessageBox.Show(dateTimePickerPocetakPrveSmene.Value.ToLongTimeString());

            List<Smena> listaSmenaZaInsert = new List<Smena>();

            DateTime pocetakPrve = dateTimePickerPocetakPrveSmene.Value;
            DateTime pocetakDruge = dateTimePickerPocetakDrugeSmene.Value;
            DateTime pocetakTrece = dateTimePickerPocetakTreceSmene.Value;

            DateTime krajPrve = dateTimePickerKrajPrveSmene.Value;
            DateTime krajDruge = dateTimePickerKrajDrugeSmene.Value;
            DateTime krajTrece = dateTimePickerKrajTreceSmene.Value;

            TimeSpan tsPocetakPrve = new TimeSpan(pocetakPrve.Hour, pocetakPrve.Minute, 0);
            TimeSpan tsPocetakDruge = new TimeSpan(pocetakDruge.Hour, pocetakDruge.Minute, 0);
            TimeSpan tsPocetakTrece = new TimeSpan(pocetakTrece.Hour, pocetakTrece.Minute, 0);

            TimeSpan tsKrajPrve = new TimeSpan(krajPrve.Hour, krajPrve.Minute, 0);
            TimeSpan tsKrajDruge = new TimeSpan(krajDruge.Hour, krajDruge.Minute, 0);
            TimeSpan tsKrajTrece = new TimeSpan(krajTrece.Hour, krajTrece.Minute, 0);

            Smena prva = new Smena();
            prva.NazivSmene = "PRVA".ToUpper();
            prva.OD = tsPocetakPrve;
            prva.DO = tsKrajPrve;
            prva.TrenutnoVazeca = 1;

            Smena druga = new Smena();
            druga.NazivSmene = "DRUGA".ToUpper();
            druga.OD = tsPocetakDruge;
            druga.DO = tsKrajDruge;
            druga.TrenutnoVazeca = 1;

            Smena treca = new Smena();
            treca.NazivSmene = "TREĆA".ToUpper();
            treca.OD = tsPocetakTrece;
            treca.DO = tsKrajTrece;
            treca.TrenutnoVazeca = 1;

            listaSmenaZaInsert.Add(prva);
            listaSmenaZaInsert.Add(druga);
            listaSmenaZaInsert.Add(treca);

            int uspehInserta = sb.InsertovanjeSmeneUpdateovanjeStarih(listaSmenaZaInsert);
            if (uspehInserta > 0)
            {
                MessageBox.Show("Nove smene su upisane");
            }
            else
            {
                MessageBox.Show("Nova smene nisu upisane");
            }
        }

        private void btnVeceSati_Click(object sender, EventArgs e)
        {
            DateTime dt = dateTimePickerPocetakPrveSmene.Value;
            var ts = dt.AddHours(1);
            dateTimePickerPocetakPrveSmene.Value = ts;

            dt = dateTimePickerPocetakDrugeSmene.Value;
            ts = dt.AddHours(1);
            dateTimePickerPocetakDrugeSmene.Value = ts;

            dt = dateTimePickerPocetakTreceSmene.Value;
            ts = dt.AddHours(1);
            dateTimePickerPocetakTreceSmene.Value = ts;

            dt = dateTimePickerKrajPrveSmene.Value;
            ts = dt.AddHours(1);
            dateTimePickerKrajPrveSmene.Value = ts;

            dt = dateTimePickerKrajDrugeSmene.Value;
            ts = dt.AddHours(1);
            dateTimePickerKrajDrugeSmene.Value = ts;

            dt = dateTimePickerKrajTreceSmene.Value;
            ts = dt.AddHours(1);
            dateTimePickerKrajTreceSmene.Value = ts;

        }

        private void btnVeceMinuti_Click(object sender, EventArgs e)
        {
            DateTime dt = dateTimePickerPocetakPrveSmene.Value;
            var ts = dt.AddMinutes(1);
            dateTimePickerPocetakPrveSmene.Value = ts;

            dt = dateTimePickerPocetakDrugeSmene.Value;
            ts = dt.AddMinutes(1);
            dateTimePickerPocetakDrugeSmene.Value = ts;

            dt = dateTimePickerPocetakTreceSmene.Value;
            ts = dt.AddMinutes(1);
            dateTimePickerPocetakTreceSmene.Value = ts;

            dt = dateTimePickerKrajPrveSmene.Value;
            ts = dt.AddMinutes(1);
            dateTimePickerKrajPrveSmene.Value = ts;

            dt = dateTimePickerKrajDrugeSmene.Value;
            ts = dt.AddMinutes(1);
            dateTimePickerKrajDrugeSmene.Value = ts;

            dt = dateTimePickerKrajTreceSmene.Value;
            ts = dt.AddMinutes(1);
            dateTimePickerKrajTreceSmene.Value = ts;
        }

        private void btnManjeSati_Click(object sender, EventArgs e)
        {
            DateTime dt = dateTimePickerPocetakPrveSmene.Value;
            var ts = dt.AddHours(-1);
            dateTimePickerPocetakPrveSmene.Value = ts;

            dt = dateTimePickerPocetakDrugeSmene.Value;
            ts = dt.AddHours(-1);
            dateTimePickerPocetakDrugeSmene.Value = ts;

            dt = dateTimePickerPocetakTreceSmene.Value;
            ts = dt.AddHours(-1);
            dateTimePickerPocetakTreceSmene.Value = ts;

            dt = dateTimePickerKrajPrveSmene.Value;
            ts = dt.AddHours(-1);
            dateTimePickerKrajPrveSmene.Value = ts;

            dt = dateTimePickerKrajDrugeSmene.Value;
            ts = dt.AddHours(-1);
            dateTimePickerKrajDrugeSmene.Value = ts;

            dt = dateTimePickerKrajTreceSmene.Value;
            ts = dt.AddHours(-1);
            dateTimePickerKrajTreceSmene.Value = ts;
        }

        private void btnManjeMinuti_Click(object sender, EventArgs e)
        {
            DateTime dt = dateTimePickerPocetakPrveSmene.Value;
            var ts = dt.AddMinutes(-1);
            dateTimePickerPocetakPrveSmene.Value = ts;

            dt = dateTimePickerPocetakDrugeSmene.Value;
            ts = dt.AddMinutes(-1);
            dateTimePickerPocetakDrugeSmene.Value = ts;

            dt = dateTimePickerPocetakTreceSmene.Value;
            ts = dt.AddMinutes(-1);
            dateTimePickerPocetakTreceSmene.Value = ts;

            dt = dateTimePickerKrajPrveSmene.Value;
            ts = dt.AddMinutes(-1);
            dateTimePickerKrajPrveSmene.Value = ts;

            dt = dateTimePickerKrajDrugeSmene.Value;
            ts = dt.AddMinutes(-1);
            dateTimePickerKrajDrugeSmene.Value = ts;

            dt = dateTimePickerKrajTreceSmene.Value;
            ts = dt.AddMinutes(-1);
            dateTimePickerKrajTreceSmene.Value = ts;
        }

        private void LoadSmene()
        {
            List<Smena> listaAktuelnihSmena = sb.GetAktuelneSmene();
            foreach (var v in listaAktuelnihSmena)
            {
                if (v.NazivSmene == "PRVA".ToUpper())
                {
                    this.dateTimePickerPocetakPrveSmene.Value = DateTime.Today.Add(v.OD);
                    this.dateTimePickerKrajPrveSmene.Value = DateTime.Today.Add(v.DO);
                }
                if (v.NazivSmene == "Druga".ToUpper())
                {
                    this.dateTimePickerPocetakDrugeSmene.Value = DateTime.Today.Add(v.OD);
                    this.dateTimePickerKrajDrugeSmene.Value = DateTime.Today.Add(v.DO);
                }
                if (v.NazivSmene == "TREĆA".ToUpper())
                {
                    this.dateTimePickerPocetakTreceSmene.Value = DateTime.Today.Add(v.OD);
                    this.dateTimePickerKrajTreceSmene.Value = DateTime.Today.Add(v.DO);
                }
            }
        }
    }
}
