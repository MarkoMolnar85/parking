﻿namespace WindowsFormsApp1
{
    partial class frmUlazi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanelGlavni = new System.Windows.Forms.TableLayoutPanel();
            this.panelFilter = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cmbSmene = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtOperaterKorisnicko = new System.Windows.Forms.TextBox();
            this.cmbPretplatnaObicna = new System.Windows.Forms.ComboBox();
            this.cmbNaParkingu = new System.Windows.Forms.CheckBox();
            this.dateTimePickerDO = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerOD = new System.Windows.Forms.DateTimePicker();
            this.txtTablice = new System.Windows.Forms.TextBox();
            this.btnPretrazi = new System.Windows.Forms.Button();
            this.panelZaGrid = new System.Windows.Forms.Panel();
            this.dataGridViewUlazi = new System.Windows.Forms.DataGridView();
            this.tableLayoutPanelGlavni.SuspendLayout();
            this.panelFilter.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panelZaGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewUlazi)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanelGlavni
            // 
            this.tableLayoutPanelGlavni.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.tableLayoutPanelGlavni.ColumnCount = 1;
            this.tableLayoutPanelGlavni.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelGlavni.Controls.Add(this.panelFilter, 0, 0);
            this.tableLayoutPanelGlavni.Controls.Add(this.panelZaGrid, 0, 1);
            this.tableLayoutPanelGlavni.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelGlavni.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanelGlavni.Name = "tableLayoutPanelGlavni";
            this.tableLayoutPanelGlavni.RowCount = 2;
            this.tableLayoutPanelGlavni.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanelGlavni.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelGlavni.Size = new System.Drawing.Size(1276, 694);
            this.tableLayoutPanelGlavni.TabIndex = 0;
            // 
            // panelFilter
            // 
            this.panelFilter.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panelFilter.Controls.Add(this.groupBox1);
            this.panelFilter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelFilter.Location = new System.Drawing.Point(3, 3);
            this.panelFilter.Name = "panelFilter";
            this.panelFilter.Size = new System.Drawing.Size(1270, 144);
            this.panelFilter.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cmbSmene);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtOperaterKorisnicko);
            this.groupBox1.Controls.Add(this.cmbPretplatnaObicna);
            this.groupBox1.Controls.Add(this.cmbNaParkingu);
            this.groupBox1.Controls.Add(this.dateTimePickerDO);
            this.groupBox1.Controls.Add(this.dateTimePickerOD);
            this.groupBox1.Controls.Add(this.txtTablice);
            this.groupBox1.Controls.Add(this.btnPretrazi);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(9, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1156, 123);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Ulazi pretraga";
            // 
            // cmbSmene
            // 
            this.cmbSmene.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSmene.FormattingEnabled = true;
            this.cmbSmene.Items.AddRange(new object[] {
            "PRVA",
            "DRUGA",
            "TREĆA"});
            this.cmbSmene.Location = new System.Drawing.Point(756, 73);
            this.cmbSmene.Name = "cmbSmene";
            this.cmbSmene.Size = new System.Drawing.Size(133, 26);
            this.cmbSmene.TabIndex = 5;
            this.cmbSmene.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbSmene_KeyDown);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(611, 81);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(98, 18);
            this.label6.TabIndex = 14;
            this.label6.Text = "Naziv smene ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(611, 27);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(139, 18);
            this.label5.TabIndex = 13;
            this.label5.Text = "Operater korisničko";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(263, 84);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 18);
            this.label4.TabIndex = 12;
            this.label4.Text = "DO";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(263, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 18);
            this.label3.TabIndex = 11;
            this.label3.Text = "OD";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(28, 84);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 18);
            this.label2.TabIndex = 10;
            this.label2.Text = "Tip karte";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 18);
            this.label1.TabIndex = 9;
            this.label1.Text = "Tablice";
            // 
            // txtOperaterKorisnicko
            // 
            this.txtOperaterKorisnicko.Location = new System.Drawing.Point(756, 24);
            this.txtOperaterKorisnicko.Name = "txtOperaterKorisnicko";
            this.txtOperaterKorisnicko.Size = new System.Drawing.Size(133, 24);
            this.txtOperaterKorisnicko.TabIndex = 4;
            this.txtOperaterKorisnicko.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtOperaterKorisnicko_KeyDown);
            // 
            // cmbPretplatnaObicna
            // 
            this.cmbPretplatnaObicna.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPretplatnaObicna.FormattingEnabled = true;
            this.cmbPretplatnaObicna.Items.AddRange(new object[] {
            "Obična",
            "Pretplatna"});
            this.cmbPretplatnaObicna.Location = new System.Drawing.Point(109, 81);
            this.cmbPretplatnaObicna.Name = "cmbPretplatnaObicna";
            this.cmbPretplatnaObicna.Size = new System.Drawing.Size(133, 26);
            this.cmbPretplatnaObicna.TabIndex = 1;
            this.cmbPretplatnaObicna.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbPretplatnaObicna_KeyDown);
            // 
            // cmbNaParkingu
            // 
            this.cmbNaParkingu.AutoSize = true;
            this.cmbNaParkingu.Location = new System.Drawing.Point(993, 23);
            this.cmbNaParkingu.Name = "cmbNaParkingu";
            this.cmbNaParkingu.Size = new System.Drawing.Size(106, 22);
            this.cmbNaParkingu.TabIndex = 6;
            this.cmbNaParkingu.Text = "Na parkingu";
            this.cmbNaParkingu.UseVisualStyleBackColor = true;
            this.cmbNaParkingu.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbNaParkingu_KeyDown);
            // 
            // dateTimePickerDO
            // 
            this.dateTimePickerDO.Location = new System.Drawing.Point(319, 81);
            this.dateTimePickerDO.Name = "dateTimePickerDO";
            this.dateTimePickerDO.Size = new System.Drawing.Size(261, 24);
            this.dateTimePickerDO.TabIndex = 3;
            this.dateTimePickerDO.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dateTimePickerDO_KeyDown);
            // 
            // dateTimePickerOD
            // 
            this.dateTimePickerOD.Location = new System.Drawing.Point(319, 24);
            this.dateTimePickerOD.Name = "dateTimePickerOD";
            this.dateTimePickerOD.Size = new System.Drawing.Size(261, 24);
            this.dateTimePickerOD.TabIndex = 2;
            this.dateTimePickerOD.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dateTimePickerOD_KeyDown);
            // 
            // txtTablice
            // 
            this.txtTablice.Location = new System.Drawing.Point(109, 27);
            this.txtTablice.Name = "txtTablice";
            this.txtTablice.Size = new System.Drawing.Size(133, 24);
            this.txtTablice.TabIndex = 0;
            this.txtTablice.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtTablice_KeyDown);
            // 
            // btnPretrazi
            // 
            this.btnPretrazi.Location = new System.Drawing.Point(993, 68);
            this.btnPretrazi.Name = "btnPretrazi";
            this.btnPretrazi.Size = new System.Drawing.Size(133, 34);
            this.btnPretrazi.TabIndex = 7;
            this.btnPretrazi.Text = "Pretraži";
            this.btnPretrazi.UseVisualStyleBackColor = true;
            this.btnPretrazi.Click += new System.EventHandler(this.btnPretrazi_Click);
            // 
            // panelZaGrid
            // 
            this.panelZaGrid.Controls.Add(this.dataGridViewUlazi);
            this.panelZaGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelZaGrid.Location = new System.Drawing.Point(3, 153);
            this.panelZaGrid.Name = "panelZaGrid";
            this.panelZaGrid.Size = new System.Drawing.Size(1270, 538);
            this.panelZaGrid.TabIndex = 1;
            // 
            // dataGridViewUlazi
            // 
            this.dataGridViewUlazi.AllowUserToAddRows = false;
            this.dataGridViewUlazi.AllowUserToDeleteRows = false;
            this.dataGridViewUlazi.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dataGridViewUlazi.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewUlazi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewUlazi.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewUlazi.Name = "dataGridViewUlazi";
            this.dataGridViewUlazi.ReadOnly = true;
            this.dataGridViewUlazi.Size = new System.Drawing.Size(1270, 538);
            this.dataGridViewUlazi.TabIndex = 8;
            this.dataGridViewUlazi.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewUlazi_CellClick);
            // 
            // frmUlazi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1276, 694);
            this.Controls.Add(this.tableLayoutPanelGlavni);
            this.Name = "frmUlazi";
            this.Text = "Ulazi";
            this.Load += new System.EventHandler(this.frmUlazi_Load);
            this.SizeChanged += new System.EventHandler(this.frmUlazi_SizeChanged);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmUlazi_KeyDown);
            this.tableLayoutPanelGlavni.ResumeLayout(false);
            this.panelFilter.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panelZaGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewUlazi)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelGlavni;
        private System.Windows.Forms.Panel panelFilter;
        private System.Windows.Forms.Panel panelZaGrid;
        private System.Windows.Forms.DataGridView dataGridViewUlazi;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DateTimePicker dateTimePickerDO;
        private System.Windows.Forms.DateTimePicker dateTimePickerOD;
        private System.Windows.Forms.TextBox txtTablice;
        private System.Windows.Forms.CheckBox cmbNaParkingu;
        private System.Windows.Forms.ComboBox cmbPretplatnaObicna;
        private System.Windows.Forms.TextBox txtOperaterKorisnicko;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnPretrazi;
        private System.Windows.Forms.ComboBox cmbSmene;
    }
}