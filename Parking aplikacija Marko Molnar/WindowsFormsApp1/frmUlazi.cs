﻿using BusinessLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class frmUlazi : Form
    {
        DataTable dtUlazi;
        UlazBusiness ub;
        public frmUlazi()
        {
            InitializeComponent();
            this.WindowState = FormWindowState.Maximized;
            KeyPreview = true;
        }

        private void frmUlazi_Load(object sender, EventArgs e)
        {
            ub = new UlazBusiness();
            dtUlazi = new DataTable();
            PodesavanjeDateTimePickera();
            LoadUlazi();
            ResajzKolona();
        }
        private void LoadUlazi()
        {
            //blok koda izbaciti ovoje za situacije kada nemam id lokacije
            if (Program.idLokacijeIzKomboBoxaKodFormeZaNadzornika == 0)
            {
                Program.idLokacijeIzKomboBoxaKodFormeZaNadzornika = 1002;
            }
            dtUlazi = ub.GetAllAutSaDodacimaiNaOsnovuLokacije(Program.idLokacijeIzKomboBoxaKodFormeZaNadzornika);
            this.dataGridViewUlazi.DataSource = dtUlazi;
        }
        private void ResajzKolona()
        {
            if (this.dataGridViewUlazi.DataSource != null)
            {
                int brojKolona = 7;
                int sirinaKolone = (this.dataGridViewUlazi.Width - this.dataGridViewUlazi.RowHeadersWidth) / brojKolona;
                this.dataGridViewUlazi.Columns["Tablica"].Width = sirinaKolone;
                this.dataGridViewUlazi.Columns["Vreme ulaza"].Width = sirinaKolone;
                this.dataGridViewUlazi.Columns["Na parkingu"].Width = sirinaKolone;
                this.dataGridViewUlazi.Columns["Tip karte"].Width = sirinaKolone;
                this.dataGridViewUlazi.Columns["Operater korsničko ime"].Width = sirinaKolone;
                this.dataGridViewUlazi.Columns["Naziv smene"].Width = sirinaKolone;
                this.dataGridViewUlazi.Columns["Naziv lokacije"].Width = sirinaKolone;
            }
        }

        private void dataGridViewUlazi_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (!(e.RowIndex < 0))
            {
                this.dataGridViewUlazi.Rows[e.RowIndex].Selected = true;
            }
        }
        private void PodesavanjeDateTimePickera()
        {
            this.dateTimePickerOD.Format = DateTimePickerFormat.Custom;
            this.dateTimePickerOD.CustomFormat = "dd-MM-yyyy";
            this.dateTimePickerDO.Format = DateTimePickerFormat.Custom;
            this.dateTimePickerDO.CustomFormat = "dd-MM-yyyy";

            this.dateTimePickerOD.Value = DateTime.Today.AddDays(-1);
        }

        private void frmUlazi_SizeChanged(object sender, EventArgs e)
        {
            ResajzKolona();
        }

        private void btnPretrazi_Click(object sender, EventArgs e)
        {
            Pretraga();
        }
        private void Pretraga()
        {
            UlazSaOstalim uso = new UlazSaOstalim();
            uso.Tablica = txtTablice.Text;
            uso.TipKarte = cmbPretplatnaObicna.Text;
            uso.OperaterKorisnicko = txtOperaterKorisnicko.Text;
            uso.SmenaNaziv = cmbSmene.Text;
            //uso.LokacijaNaziv = txtLokacija.Text;
            uso.NaParkingu = cmbNaParkingu.Checked ? 1 : 0;

            DateTime OD = this.dateTimePickerOD.Value;
            DateTime DO = this.dateTimePickerDO.Value;
            dtUlazi = ub.PretragaUlazaNaOsnovuSvihParametaraUlaziIOstaloDT(uso, OD, DO, Program.idLokacijeIzKomboBoxaKodFormeZaNadzornika);
            this.dataGridViewUlazi.DataSource = dtUlazi;
        }

        private void txtTablice_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 13)
            {
                Pretraga();
            }
        }

        private void cmbPretplatnaObicna_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 13)
            {
                Pretraga();
            }
        }

        private void dateTimePickerOD_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 13)
            {
                Pretraga();
            }
        }

        private void dateTimePickerDO_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 13)
            {
                Pretraga();
            }
        }

        private void txtOperaterKorisnicko_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 13)
            {
                Pretraga();
            }
        }

        private void cmbSmene_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 13)
            {
                Pretraga();
            }
        }

        private void cmbNaParkingu_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 13)
            {
                Pretraga();
            }
        }

        private void frmUlazi_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F12)
            {
                ExportGridaExcel(this.dataGridViewUlazi);
            }
        }

        private void ExportGridaExcel(DataGridView dataGridViewPrikaz)
        {
            //nema nevidljivih kolona

            // Creating a Excel object. 
            Microsoft.Office.Interop.Excel._Application excel = new Microsoft.Office.Interop.Excel.Application();
            Microsoft.Office.Interop.Excel._Workbook workbook = excel.Workbooks.Add(Type.Missing);
            Microsoft.Office.Interop.Excel._Worksheet worksheet = workbook.Worksheets.Add();


            string putanja = "";
            try
            {

                worksheet = workbook.ActiveSheet;

                worksheet.Name = "Ulazi";

                int indexKolone = 1;
                int indexReda = 2;

                for (int i = 0; i < dataGridViewPrikaz.Columns.Count; i++)
                {
                    //if (!(i == 0 || i == 7))
                    {
                        worksheet.Cells[1, indexKolone] = dataGridViewPrikaz.Columns[i].HeaderText;
                        indexKolone++;
                    }
                }

                indexKolone = 1;

                for (int i = 0; i < dataGridViewPrikaz.Rows.Count; i++)
                {
                    for (int j = 0; j < dataGridViewPrikaz.Columns.Count; j++)
                    {
                        //if (!(j == 0 || j == 7))
                        {
                            worksheet.Cells[indexReda, indexKolone] = dataGridViewPrikaz.Rows[i].Cells[j].Value.ToString();
                            indexKolone++;
                        }
                    }
                    indexKolone = 1;
                    indexReda++;
                }


                //Getting the location and file name of the excel to save from user. 
                SaveFileDialog saveDialog = new SaveFileDialog();
                saveDialog.Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
                saveDialog.FilterIndex = 2;

                if (saveDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    workbook.SaveAs(saveDialog.FileName);
                    MessageBox.Show("Excel datoteka je uspeno izvezena");
                    putanja = saveDialog.FileName;
                }

            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                workbook.Close(false);
                excel.Quit();
                workbook = null;
                excel = null;
                GC.Collect();
                UbiProces(putanja);
            }
        }

        private void UbiProces(string putanja)
        {
            System.Diagnostics.Process[] process = System.Diagnostics.Process.GetProcessesByName("Excel");
            foreach (System.Diagnostics.Process p in process)
            {
                if (!string.IsNullOrEmpty(p.ProcessName))
                {
                    try
                    {
                        p.Kill();
                    }
                    catch { }
                }
            }
        }
    }
}
