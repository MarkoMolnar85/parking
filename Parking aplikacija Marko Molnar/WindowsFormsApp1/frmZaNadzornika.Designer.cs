﻿namespace WindowsFormsApp1
{
    partial class frmZaNadzornika
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanelGlavneFormeNadzornika = new System.Windows.Forms.TableLayoutPanel();
            this.panelZaDugmadKojaVodeKaFormama = new System.Windows.Forms.Panel();
            this.panelZaPrikazDugmadi = new System.Windows.Forms.Panel();
            this.lblText10 = new System.Windows.Forms.Label();
            this.lblText9 = new System.Windows.Forms.Label();
            this.lblText8 = new System.Windows.Forms.Label();
            this.lblText7 = new System.Windows.Forms.Label();
            this.lblText5 = new System.Windows.Forms.Label();
            this.lblText6 = new System.Windows.Forms.Label();
            this.lblText4 = new System.Windows.Forms.Label();
            this.lblText3 = new System.Windows.Forms.Label();
            this.lblText2 = new System.Windows.Forms.Label();
            this.lblText1 = new System.Windows.Forms.Label();
            this.lblRedni10 = new System.Windows.Forms.Label();
            this.lblRedni9 = new System.Windows.Forms.Label();
            this.lblRedni8 = new System.Windows.Forms.Label();
            this.lblRedni7 = new System.Windows.Forms.Label();
            this.lblRedni6 = new System.Windows.Forms.Label();
            this.lblRedni5 = new System.Windows.Forms.Label();
            this.lblRedni4 = new System.Windows.Forms.Label();
            this.lblRedni3 = new System.Windows.Forms.Label();
            this.lblRedni2 = new System.Windows.Forms.Label();
            this.lblRedni1 = new System.Windows.Forms.Label();
            this.panelZaIzborStanice = new System.Windows.Forms.Panel();
            this.cmbIzborStanice = new System.Windows.Forms.ComboBox();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.tableLayoutPanelGlavneFormeNadzornika.SuspendLayout();
            this.panelZaDugmadKojaVodeKaFormama.SuspendLayout();
            this.panelZaPrikazDugmadi.SuspendLayout();
            this.panelZaIzborStanice.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanelGlavneFormeNadzornika
            // 
            this.tableLayoutPanelGlavneFormeNadzornika.ColumnCount = 1;
            this.tableLayoutPanelGlavneFormeNadzornika.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelGlavneFormeNadzornika.Controls.Add(this.panelZaDugmadKojaVodeKaFormama, 0, 1);
            this.tableLayoutPanelGlavneFormeNadzornika.Controls.Add(this.panelZaIzborStanice, 0, 0);
            this.tableLayoutPanelGlavneFormeNadzornika.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelGlavneFormeNadzornika.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanelGlavneFormeNadzornika.Name = "tableLayoutPanelGlavneFormeNadzornika";
            this.tableLayoutPanelGlavneFormeNadzornika.RowCount = 2;
            this.tableLayoutPanelGlavneFormeNadzornika.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanelGlavneFormeNadzornika.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelGlavneFormeNadzornika.Size = new System.Drawing.Size(1276, 694);
            this.tableLayoutPanelGlavneFormeNadzornika.TabIndex = 0;
            // 
            // panelZaDugmadKojaVodeKaFormama
            // 
            this.panelZaDugmadKojaVodeKaFormama.BackColor = System.Drawing.Color.DodgerBlue;
            this.panelZaDugmadKojaVodeKaFormama.Controls.Add(this.panelZaPrikazDugmadi);
            this.panelZaDugmadKojaVodeKaFormama.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelZaDugmadKojaVodeKaFormama.Location = new System.Drawing.Point(3, 113);
            this.panelZaDugmadKojaVodeKaFormama.Name = "panelZaDugmadKojaVodeKaFormama";
            this.panelZaDugmadKojaVodeKaFormama.Size = new System.Drawing.Size(1270, 578);
            this.panelZaDugmadKojaVodeKaFormama.TabIndex = 0;
            // 
            // panelZaPrikazDugmadi
            // 
            this.panelZaPrikazDugmadi.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelZaPrikazDugmadi.BackColor = System.Drawing.Color.RoyalBlue;
            this.panelZaPrikazDugmadi.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelZaPrikazDugmadi.Controls.Add(this.lblText10);
            this.panelZaPrikazDugmadi.Controls.Add(this.lblText9);
            this.panelZaPrikazDugmadi.Controls.Add(this.lblText8);
            this.panelZaPrikazDugmadi.Controls.Add(this.lblText7);
            this.panelZaPrikazDugmadi.Controls.Add(this.lblText5);
            this.panelZaPrikazDugmadi.Controls.Add(this.lblText6);
            this.panelZaPrikazDugmadi.Controls.Add(this.lblText4);
            this.panelZaPrikazDugmadi.Controls.Add(this.lblText3);
            this.panelZaPrikazDugmadi.Controls.Add(this.lblText2);
            this.panelZaPrikazDugmadi.Controls.Add(this.lblText1);
            this.panelZaPrikazDugmadi.Controls.Add(this.lblRedni10);
            this.panelZaPrikazDugmadi.Controls.Add(this.lblRedni9);
            this.panelZaPrikazDugmadi.Controls.Add(this.lblRedni8);
            this.panelZaPrikazDugmadi.Controls.Add(this.lblRedni7);
            this.panelZaPrikazDugmadi.Controls.Add(this.lblRedni6);
            this.panelZaPrikazDugmadi.Controls.Add(this.lblRedni5);
            this.panelZaPrikazDugmadi.Controls.Add(this.lblRedni4);
            this.panelZaPrikazDugmadi.Controls.Add(this.lblRedni3);
            this.panelZaPrikazDugmadi.Controls.Add(this.lblRedni2);
            this.panelZaPrikazDugmadi.Controls.Add(this.lblRedni1);
            this.panelZaPrikazDugmadi.Controls.Add(this.button6);
            this.panelZaPrikazDugmadi.Controls.Add(this.button5);
            this.panelZaPrikazDugmadi.Controls.Add(this.button10);
            this.panelZaPrikazDugmadi.Controls.Add(this.button4);
            this.panelZaPrikazDugmadi.Controls.Add(this.button7);
            this.panelZaPrikazDugmadi.Controls.Add(this.button3);
            this.panelZaPrikazDugmadi.Controls.Add(this.button9);
            this.panelZaPrikazDugmadi.Controls.Add(this.button2);
            this.panelZaPrikazDugmadi.Controls.Add(this.button8);
            this.panelZaPrikazDugmadi.Controls.Add(this.button1);
            this.panelZaPrikazDugmadi.Location = new System.Drawing.Point(36, 51);
            this.panelZaPrikazDugmadi.Name = "panelZaPrikazDugmadi";
            this.panelZaPrikazDugmadi.Size = new System.Drawing.Size(1192, 481);
            this.panelZaPrikazDugmadi.TabIndex = 10;
            // 
            // lblText10
            // 
            this.lblText10.AutoSize = true;
            this.lblText10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblText10.Location = new System.Drawing.Point(867, 324);
            this.lblText10.Name = "lblText10";
            this.lblText10.Size = new System.Drawing.Size(55, 20);
            this.lblText10.TabIndex = 29;
            this.lblText10.Text = "Logovi";
            // 
            // lblText9
            // 
            this.lblText9.AutoSize = true;
            this.lblText9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblText9.Location = new System.Drawing.Point(654, 308);
            this.lblText9.Name = "lblText9";
            this.lblText9.Size = new System.Drawing.Size(60, 20);
            this.lblText9.TabIndex = 28;
            this.lblText9.Text = "Smene";
            // 
            // lblText8
            // 
            this.lblText8.AutoSize = true;
            this.lblText8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblText8.Location = new System.Drawing.Point(481, 316);
            this.lblText8.Name = "lblText8";
            this.lblText8.Size = new System.Drawing.Size(122, 20);
            this.lblText8.TabIndex = 27;
            this.lblText8.Text = "Pretplatne karte";
            // 
            // lblText7
            // 
            this.lblText7.AutoSize = true;
            this.lblText7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblText7.Location = new System.Drawing.Point(286, 316);
            this.lblText7.Name = "lblText7";
            this.lblText7.Size = new System.Drawing.Size(75, 20);
            this.lblText7.TabIndex = 26;
            this.lblText7.Text = "Operateri";
            // 
            // lblText5
            // 
            this.lblText5.AutoSize = true;
            this.lblText5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblText5.Location = new System.Drawing.Point(867, 145);
            this.lblText5.Name = "lblText5";
            this.lblText5.Size = new System.Drawing.Size(113, 20);
            this.lblText5.TabIndex = 25;
            this.lblText5.Text = "Lokacije i cene";
            // 
            // lblText6
            // 
            this.lblText6.AutoSize = true;
            this.lblText6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblText6.Location = new System.Drawing.Point(108, 316);
            this.lblText6.Name = "lblText6";
            this.lblText6.Size = new System.Drawing.Size(83, 20);
            this.lblText6.TabIndex = 24;
            this.lblText6.Text = "Nadzornici";
            // 
            // lblText4
            // 
            this.lblText4.AutoSize = true;
            this.lblText4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblText4.Location = new System.Drawing.Point(646, 145);
            this.lblText4.Name = "lblText4";
            this.lblText4.Size = new System.Drawing.Size(53, 20);
            this.lblText4.TabIndex = 23;
            this.lblText4.Text = "Pazari";
            // 
            // lblText3
            // 
            this.lblText3.AutoSize = true;
            this.lblText3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblText3.Location = new System.Drawing.Point(481, 145);
            this.lblText3.Name = "lblText3";
            this.lblText3.Size = new System.Drawing.Size(59, 20);
            this.lblText3.TabIndex = 22;
            this.lblText3.Text = "Računi";
            // 
            // lblText2
            // 
            this.lblText2.AutoSize = true;
            this.lblText2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblText2.Location = new System.Drawing.Point(286, 145);
            this.lblText2.Name = "lblText2";
            this.lblText2.Size = new System.Drawing.Size(45, 20);
            this.lblText2.TabIndex = 21;
            this.lblText2.Text = "Izlazi";
            // 
            // lblText1
            // 
            this.lblText1.AutoSize = true;
            this.lblText1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblText1.Location = new System.Drawing.Point(108, 145);
            this.lblText1.Name = "lblText1";
            this.lblText1.Size = new System.Drawing.Size(44, 20);
            this.lblText1.TabIndex = 20;
            this.lblText1.Text = "Ulazi";
            // 
            // lblRedni10
            // 
            this.lblRedni10.AutoSize = true;
            this.lblRedni10.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRedni10.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblRedni10.Location = new System.Drawing.Point(867, 244);
            this.lblRedni10.Name = "lblRedni10";
            this.lblRedni10.Size = new System.Drawing.Size(60, 37);
            this.lblRedni10.TabIndex = 19;
            this.lblRedni10.Text = "10.";
            // 
            // lblRedni9
            // 
            this.lblRedni9.AutoSize = true;
            this.lblRedni9.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRedni9.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblRedni9.Location = new System.Drawing.Point(655, 244);
            this.lblRedni9.Name = "lblRedni9";
            this.lblRedni9.Size = new System.Drawing.Size(44, 37);
            this.lblRedni9.TabIndex = 18;
            this.lblRedni9.Text = "9.";
            // 
            // lblRedni8
            // 
            this.lblRedni8.AutoSize = true;
            this.lblRedni8.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRedni8.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblRedni8.Location = new System.Drawing.Point(481, 244);
            this.lblRedni8.Name = "lblRedni8";
            this.lblRedni8.Size = new System.Drawing.Size(44, 37);
            this.lblRedni8.TabIndex = 17;
            this.lblRedni8.Text = "8.";
            // 
            // lblRedni7
            // 
            this.lblRedni7.AutoSize = true;
            this.lblRedni7.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRedni7.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblRedni7.Location = new System.Drawing.Point(286, 244);
            this.lblRedni7.Name = "lblRedni7";
            this.lblRedni7.Size = new System.Drawing.Size(44, 37);
            this.lblRedni7.TabIndex = 16;
            this.lblRedni7.Text = "7.";
            // 
            // lblRedni6
            // 
            this.lblRedni6.AutoSize = true;
            this.lblRedni6.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRedni6.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblRedni6.Location = new System.Drawing.Point(108, 244);
            this.lblRedni6.Name = "lblRedni6";
            this.lblRedni6.Size = new System.Drawing.Size(44, 37);
            this.lblRedni6.TabIndex = 15;
            this.lblRedni6.Text = "6.";
            // 
            // lblRedni5
            // 
            this.lblRedni5.AutoSize = true;
            this.lblRedni5.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRedni5.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblRedni5.Location = new System.Drawing.Point(873, 48);
            this.lblRedni5.Name = "lblRedni5";
            this.lblRedni5.Size = new System.Drawing.Size(44, 37);
            this.lblRedni5.TabIndex = 14;
            this.lblRedni5.Text = "5.";
            // 
            // lblRedni4
            // 
            this.lblRedni4.AutoSize = true;
            this.lblRedni4.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRedni4.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblRedni4.Location = new System.Drawing.Point(664, 48);
            this.lblRedni4.Name = "lblRedni4";
            this.lblRedni4.Size = new System.Drawing.Size(44, 37);
            this.lblRedni4.TabIndex = 13;
            this.lblRedni4.Text = "4.";
            // 
            // lblRedni3
            // 
            this.lblRedni3.AutoSize = true;
            this.lblRedni3.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRedni3.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblRedni3.Location = new System.Drawing.Point(472, 48);
            this.lblRedni3.Name = "lblRedni3";
            this.lblRedni3.Size = new System.Drawing.Size(44, 37);
            this.lblRedni3.TabIndex = 12;
            this.lblRedni3.Text = "3.";
            // 
            // lblRedni2
            // 
            this.lblRedni2.AutoSize = true;
            this.lblRedni2.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRedni2.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblRedni2.Location = new System.Drawing.Point(286, 48);
            this.lblRedni2.Name = "lblRedni2";
            this.lblRedni2.Size = new System.Drawing.Size(44, 37);
            this.lblRedni2.TabIndex = 11;
            this.lblRedni2.Text = "2.";
            // 
            // lblRedni1
            // 
            this.lblRedni1.AutoSize = true;
            this.lblRedni1.BackColor = System.Drawing.Color.RoyalBlue;
            this.lblRedni1.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRedni1.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblRedni1.Location = new System.Drawing.Point(108, 48);
            this.lblRedni1.Name = "lblRedni1";
            this.lblRedni1.Size = new System.Drawing.Size(42, 37);
            this.lblRedni1.TabIndex = 10;
            this.lblRedni1.Text = "1.";
            // 
            // panelZaIzborStanice
            // 
            this.panelZaIzborStanice.BackColor = System.Drawing.Color.DodgerBlue;
            this.panelZaIzborStanice.Controls.Add(this.cmbIzborStanice);
            this.panelZaIzborStanice.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelZaIzborStanice.Location = new System.Drawing.Point(3, 3);
            this.panelZaIzborStanice.Name = "panelZaIzborStanice";
            this.panelZaIzborStanice.Size = new System.Drawing.Size(1270, 104);
            this.panelZaIzborStanice.TabIndex = 1;
            // 
            // cmbIzborStanice
            // 
            this.cmbIzborStanice.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cmbIzborStanice.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbIzborStanice.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbIzborStanice.FormattingEnabled = true;
            this.cmbIzborStanice.Location = new System.Drawing.Point(474, 35);
            this.cmbIzborStanice.Name = "cmbIzborStanice";
            this.cmbIzborStanice.Size = new System.Drawing.Size(262, 32);
            this.cmbIzborStanice.TabIndex = 0;
            this.cmbIzborStanice.SelectionChangeCommitted += new System.EventHandler(this.cmbIzborStanice_SelectionChangeCommitted);
            // 
            // button6
            // 
            this.button6.BackgroundImage = global::WindowsFormsApp1.Properties.Resources.nadzornik;
            this.button6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button6.FlatAppearance.BorderSize = 0;
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.Location = new System.Drawing.Point(111, 284);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(75, 23);
            this.button6.TabIndex = 5;
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button5
            // 
            this.button5.BackgroundImage = global::WindowsFormsApp1.Properties.Resources.lokacija;
            this.button5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button5.FlatAppearance.BorderSize = 0;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Location = new System.Drawing.Point(870, 110);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 4;
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button10
            // 
            this.button10.BackgroundImage = global::WindowsFormsApp1.Properties.Resources.logovi;
            this.button10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button10.FlatAppearance.BorderSize = 0;
            this.button10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button10.Location = new System.Drawing.Point(870, 279);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(75, 23);
            this.button10.TabIndex = 9;
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // button4
            // 
            this.button4.BackgroundImage = global::WindowsFormsApp1.Properties.Resources.pazar;
            this.button4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Location = new System.Drawing.Point(649, 110);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 3;
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button7
            // 
            this.button7.BackgroundImage = global::WindowsFormsApp1.Properties.Resources.operater;
            this.button7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button7.FlatAppearance.BorderSize = 0;
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.Location = new System.Drawing.Point(280, 284);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(75, 23);
            this.button7.TabIndex = 6;
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button3
            // 
            this.button3.BackgroundImage = global::WindowsFormsApp1.Properties.Resources.racunCrni;
            this.button3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Location = new System.Drawing.Point(475, 110);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 2;
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button9
            // 
            this.button9.BackgroundImage = global::WindowsFormsApp1.Properties.Resources.smena;
            this.button9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button9.FlatAppearance.BorderSize = 0;
            this.button9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button9.Location = new System.Drawing.Point(649, 282);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(75, 23);
            this.button9.TabIndex = 8;
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button2
            // 
            this.button2.BackgroundImage = global::WindowsFormsApp1.Properties.Resources.izlaz;
            this.button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Location = new System.Drawing.Point(280, 110);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 1;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button8
            // 
            this.button8.BackgroundImage = global::WindowsFormsApp1.Properties.Resources.pretplatneKarte;
            this.button8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button8.FlatAppearance.BorderSize = 0;
            this.button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button8.Location = new System.Drawing.Point(475, 284);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(75, 23);
            this.button8.TabIndex = 7;
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button1
            // 
            this.button1.BackgroundImage = global::WindowsFormsApp1.Properties.Resources.ulaz;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(111, 110);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // frmZaNadzornika
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.RoyalBlue;
            this.ClientSize = new System.Drawing.Size(1276, 694);
            this.Controls.Add(this.tableLayoutPanelGlavneFormeNadzornika);
            this.Name = "frmZaNadzornika";
            this.Text = "Nadzor";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmZaNadzornika_FormClosed);
            this.Load += new System.EventHandler(this.frmZaNadzornika_Load);
            this.Resize += new System.EventHandler(this.frmZaNadzornika_Resize);
            this.tableLayoutPanelGlavneFormeNadzornika.ResumeLayout(false);
            this.panelZaDugmadKojaVodeKaFormama.ResumeLayout(false);
            this.panelZaPrikazDugmadi.ResumeLayout(false);
            this.panelZaPrikazDugmadi.PerformLayout();
            this.panelZaIzborStanice.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelGlavneFormeNadzornika;
        private System.Windows.Forms.Panel panelZaDugmadKojaVodeKaFormama;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panelZaIzborStanice;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Panel panelZaPrikazDugmadi;
        private System.Windows.Forms.Label lblRedni10;
        private System.Windows.Forms.Label lblRedni9;
        private System.Windows.Forms.Label lblRedni8;
        private System.Windows.Forms.Label lblRedni7;
        private System.Windows.Forms.Label lblRedni6;
        private System.Windows.Forms.Label lblRedni5;
        private System.Windows.Forms.Label lblRedni4;
        private System.Windows.Forms.Label lblRedni3;
        private System.Windows.Forms.Label lblRedni2;
        private System.Windows.Forms.Label lblRedni1;
        private System.Windows.Forms.Label lblText10;
        private System.Windows.Forms.Label lblText9;
        private System.Windows.Forms.Label lblText8;
        private System.Windows.Forms.Label lblText7;
        private System.Windows.Forms.Label lblText5;
        private System.Windows.Forms.Label lblText6;
        private System.Windows.Forms.Label lblText4;
        private System.Windows.Forms.Label lblText3;
        private System.Windows.Forms.Label lblText2;
        private System.Windows.Forms.Label lblText1;
        private System.Windows.Forms.ComboBox cmbIzborStanice;
    }
}