﻿using BusinessLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class frmZaNadzornika : Form
    {
        public frmZaNadzornika()
        {
            InitializeComponent();
            this.WindowState = FormWindowState.Maximized;
        }

        private void frmZaNadzornika_Load(object sender, EventArgs e)
        {
            UrediRaspored();
            BogacenjeKomboBoxaLokacije();
            InicijalizujIdLokacije();
            //MessageBox.Show(Program.idLokacijeIzKomboBoxaKodFormeZaNadzornika.ToString());
            OdredjivanjePravaNadzornika();
        }

        private void frmZaNadzornika_FormClosed(object sender, FormClosedEventArgs e)
        {
            NadzornikBusiness nadzorBusiness = new NadzornikBusiness();
            Nadzornik nadzornik = nadzorBusiness.GetNadzornikNaOsnovuKorisnickogImena(Program.korisnickoIme);

            NadzornikLog nadzornikLog = new NadzornikLog();
            nadzornikLog.NadzornikID = nadzornik.NadzornikID;
            nadzornikLog.VremePosle = DateTime.Now;

            NadzornikLogoviBusiness nlb = new NadzornikLogoviBusiness();
            nlb.UpdateNadzornikLogIzmenaVremenaIzlaza(nadzornikLog);

        }

        private void UrediRaspored()
        {
            
            int brojDugmadiURedu = 5;
            double visinaPanela = panelZaPrikazDugmadi.Height;
            double sirinaPanela = panelZaPrikazDugmadi.Width;
            double topPanela = panelZaPrikazDugmadi.Top - 51;
            double leftPanela = panelZaPrikazDugmadi.Left - 36;

            double prostorIzmedjuVrhaIRednogBroja = visinaPanela / 14;
            double prostorIzmedjulabelaIGornjegDugmeta = visinaPanela / 100;
            double prostorIzmedjuDugmetaIDonjegLabela = visinaPanela / 100;
            double prostorIzmedjuGornjegLabelaIDonjeglabela = visinaPanela / 4;
            double prostorIzmedjuLabelaIDonjegDugmeta = visinaPanela / 100;
            double prostorIzmedjuDonjegDugmetaIDonjegLabela = visinaPanela / 100;
            double visinaLabelaRednibBroj = lblRedni1.Height;
            double vidinaLabelaText = lblText1.Height;

            //MessageBox.Show("Visina panela " + panelZaPrikazDugmadi.Width + " Sabrani elementi sem dugmeta " + (prostorIzmedjuVrhaIRednogBroja + prostorIzmedjulabelaIGornjegDugmeta + prostorIzmedjuDugmetaIDonjegLabela + prostorIzmedjuGornjegLabelaIDonjeglabela + prostorIzmedjuLabelaIDonjegDugmeta + prostorIzmedjuDonjegDugmetaIDonjegLabela + visinaLabelaRednibBroj * 2 + vidinaLabelaText * 2) + " visina dugmadi " + button1.Height * 2);
            double visinaDugmeta = (visinaPanela - (prostorIzmedjuVrhaIRednogBroja + prostorIzmedjulabelaIGornjegDugmeta + prostorIzmedjuDugmetaIDonjegLabela + prostorIzmedjuGornjegLabelaIDonjeglabela + prostorIzmedjuLabelaIDonjegDugmeta + prostorIzmedjuDonjegDugmetaIDonjegLabela + visinaLabelaRednibBroj + vidinaLabelaText)) / 3;
            //double visinaDugmeta = 100;

            double prostorIzmedjuDugmadiHorizontalno = sirinaPanela / 5;
            double deoProstoraIzmedjuDugmadiHorizontalno = prostorIzmedjuDugmadiHorizontalno / (brojDugmadiURedu + 1);
            double sirinaDugmeta = (sirinaPanela - prostorIzmedjuDugmadiHorizontalno) / brojDugmadiURedu;

            button1.Width = (int)sirinaDugmeta;
            button2.Width = (int)sirinaDugmeta;
            button3.Width = (int)sirinaDugmeta;
            button4.Width = (int)sirinaDugmeta;
            button5.Width = (int)sirinaDugmeta;
            button6.Width = (int)sirinaDugmeta;
            button7.Width = (int)sirinaDugmeta;
            button8.Width = (int)sirinaDugmeta;
            button9.Width = (int)sirinaDugmeta;
            button10.Width = (int)sirinaDugmeta;

            button1.Height = (int)visinaDugmeta;
            button2.Height = (int)visinaDugmeta;
            button3.Height = (int)visinaDugmeta;
            button4.Height = (int)visinaDugmeta;
            button5.Height = (int)visinaDugmeta;
            button6.Height = (int)visinaDugmeta;
            button7.Height = (int)visinaDugmeta;
            button8.Height = (int)visinaDugmeta;
            button9.Height = (int)visinaDugmeta;
            button10.Height = (int)visinaDugmeta;

            lblRedni1.Top = (int)prostorIzmedjuVrhaIRednogBroja;
            lblRedni2.Top = (int)prostorIzmedjuVrhaIRednogBroja;
            lblRedni3.Top = (int)prostorIzmedjuVrhaIRednogBroja;
            lblRedni4.Top = (int)prostorIzmedjuVrhaIRednogBroja;
            lblRedni5.Top = (int)prostorIzmedjuVrhaIRednogBroja;

            button1.Top = (int)(lblRedni1.Bottom + prostorIzmedjulabelaIGornjegDugmeta);
            button2.Top = (int)(lblRedni2.Bottom + prostorIzmedjulabelaIGornjegDugmeta);
            button3.Top = (int)(lblRedni3.Bottom + prostorIzmedjulabelaIGornjegDugmeta);
            button4.Top = (int)(lblRedni4.Bottom + prostorIzmedjulabelaIGornjegDugmeta);
            button5.Top = (int)(lblRedni5.Bottom + prostorIzmedjulabelaIGornjegDugmeta);

            button1.Left = (int)(deoProstoraIzmedjuDugmadiHorizontalno + leftPanela);
            button2.Left = (int)(deoProstoraIzmedjuDugmadiHorizontalno + button1.Right);
            button3.Left = (int)(deoProstoraIzmedjuDugmadiHorizontalno + button2.Right);
            button4.Left = (int)(deoProstoraIzmedjuDugmadiHorizontalno + button3.Right);
            button5.Left = (int)(deoProstoraIzmedjuDugmadiHorizontalno + button4.Right);
            button6.Left = (int)(deoProstoraIzmedjuDugmadiHorizontalno + leftPanela);
            button7.Left = (int)(deoProstoraIzmedjuDugmadiHorizontalno + button6.Right);
            button8.Left = (int)(deoProstoraIzmedjuDugmadiHorizontalno + button7.Right);
            button9.Left = (int)(deoProstoraIzmedjuDugmadiHorizontalno + button8.Right);
            button10.Left = (int)(deoProstoraIzmedjuDugmadiHorizontalno + button9.Right);


            lblRedni1.Left = button1.Left + ((button1.Right - button1.Left) / 2) - lblRedni1.Width / 2;
            lblRedni2.Left = button2.Left + ((button2.Right - button2.Left) / 2) - lblRedni2.Width / 2;
            lblRedni3.Left = button3.Left + ((button3.Right - button3.Left) / 2) - lblRedni3.Width / 2;
            lblRedni4.Left = button4.Left + ((button4.Right - button4.Left) / 2) - lblRedni4.Width / 2;
            lblRedni5.Left = button5.Left + ((button5.Right - button5.Left) / 2) - lblRedni5.Width / 2;

            lblText1.Top = (int)(button1.Bottom + prostorIzmedjuDugmetaIDonjegLabela);
            lblText2.Top = (int)(button2.Bottom + prostorIzmedjuDugmetaIDonjegLabela);
            lblText3.Top = (int)(button3.Bottom + prostorIzmedjuDugmetaIDonjegLabela);
            lblText4.Top = (int)(button4.Bottom + prostorIzmedjuDugmetaIDonjegLabela);
            lblText5.Top = (int)(button5.Bottom + prostorIzmedjuDugmetaIDonjegLabela);

            lblText1.Left = (int)(button1.Left + ((button1.Right - button1.Left) / 2) - lblText1.Width / 2);
            lblText2.Left = (int)(button2.Left + ((button2.Right - button2.Left) / 2) - lblText2.Width / 2);
            lblText3.Left = (int)(button3.Left + ((button3.Right - button3.Left) / 2) - lblText3.Width / 2);
            lblText4.Left = (int)(button4.Left + ((button4.Right - button4.Left) / 2) - lblText4.Width / 2);
            lblText5.Left = (int)(button5.Left + ((button5.Right - button5.Left) / 2) - lblText5.Width / 2);

            lblRedni6.Top = (int)(lblText1.Bottom + prostorIzmedjuGornjegLabelaIDonjeglabela);
            lblRedni7.Top = (int)(lblText2.Bottom + prostorIzmedjuGornjegLabelaIDonjeglabela);
            lblRedni8.Top = (int)(lblText3.Bottom + prostorIzmedjuGornjegLabelaIDonjeglabela);
            lblRedni9.Top = (int)(lblText4.Bottom + prostorIzmedjuGornjegLabelaIDonjeglabela);
            lblRedni10.Top = (int)(lblText5.Bottom + prostorIzmedjuGornjegLabelaIDonjeglabela);

            button6.Top = (int)(lblRedni6.Bottom + prostorIzmedjuLabelaIDonjegDugmeta);
            button7.Top = (int)(lblRedni6.Bottom + prostorIzmedjuLabelaIDonjegDugmeta);
            button8.Top = (int)(lblRedni6.Bottom + prostorIzmedjuLabelaIDonjegDugmeta);
            button9.Top = (int)(lblRedni6.Bottom + prostorIzmedjuLabelaIDonjegDugmeta);
            button10.Top = (int)(lblRedni6.Bottom + prostorIzmedjuLabelaIDonjegDugmeta);

            lblRedni6.Left = (int)(button6.Left + ((button6.Right - button6.Left) / 2) - lblText6.Width / 2);
            lblRedni7.Left = (int)(button7.Left + ((button7.Right - button7.Left) / 2) - lblText6.Width / 2);
            lblRedni8.Left = (int)(button8.Left + ((button8.Right - button8.Left) / 2) - lblText6.Width / 2);
            lblRedni9.Left = (int)(button9.Left + ((button9.Right - button9.Left) / 2) - lblText6.Width / 2);
            lblRedni10.Left = (int)(button10.Left + ((button10.Right - button10.Left) / 2) - lblText6.Width / 2);

            lblText6.Top = (int)(button6.Bottom + prostorIzmedjuDugmetaIDonjegLabela);
            lblText7.Top = (int)(button7.Bottom + prostorIzmedjuDugmetaIDonjegLabela);
            lblText8.Top = (int)(button8.Bottom + prostorIzmedjuDugmetaIDonjegLabela);
            lblText9.Top = (int)(button9.Bottom + prostorIzmedjuDugmetaIDonjegLabela);
            lblText10.Top = (int)(button10.Bottom + prostorIzmedjuDugmetaIDonjegLabela);

            lblText6.Left = (int)(button6.Left + ((button6.Right - button6.Left) / 2) - lblText6.Width / 2);
            lblText7.Left = (int)(button7.Left + ((button7.Right - button7.Left) / 2) - lblText7.Width / 2);
            lblText8.Left = (int)(button8.Left + ((button8.Right - button8.Left) / 2) - lblText8.Width / 2);
            lblText9.Left = (int)(button9.Left + ((button9.Right - button9.Left) / 2) - lblText9.Width / 2);
            lblText10.Left = (int)(button10.Left + ((button10.Right - button10.Left) / 2) - lblText10.Width / 2);
        }

        private void frmZaNadzornika_Resize(object sender, EventArgs e)
        {
            UrediRaspored();
        }

        private void BogacenjeKomboBoxaLokacije()
        {
            LokacijaBusiness lb = new LokacijaBusiness();
            List<Lokacija> listaLokacija = lb.GetLokacije();

            cmbIzborStanice.DataSource = listaLokacija;
            cmbIzborStanice.DisplayMember = "Naziv";
            cmbIzborStanice.ValueMember = "ID";
        }

        private void InicijalizujIdLokacije()
        {
            //pozivam imena lokacije kako bih mogao da izbrojim lokacije, pa ako je broj nula onda se dole linija koda za popunjavanje combobox polja ne poziva
            LokacijaBusiness lb = new LokacijaBusiness();
            List<string> ListanaziviLokacija = lb.GetAllNaziviLokacijaZaKomboBox();
            int brojLokacija = ListanaziviLokacija.Count;
            if (brojLokacija > 0)
            {
                Program.idLokacijeIzKomboBoxaKodFormeZaNadzornika = int.Parse(cmbIzborStanice.SelectedValue.ToString());
            }
        }

        private void cmbIzborStanice_SelectionChangeCommitted(object sender, EventArgs e)
        {
            InicijalizujIdLokacije();
            //MessageBox.Show("" + Program.idLokacijeIzKomboBoxaKodFormeZaNadzornika);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            frmNadzorniciInsertPrikazAzuriranjeBrisanje fnipab = new frmNadzorniciInsertPrikazAzuriranjeBrisanje();
            fnipab.ShowDialog();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            frmOperater fo = new frmOperater();
            fo.ShowDialog();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            frmPretplatneKarteInsertUpdatePretragacs fpkiup = new frmPretplatneKarteInsertUpdatePretragacs();
            fpkiup.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmUlazi fu = new frmUlazi();
            fu.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmIzlazi fi = new frmIzlazi();
            fi.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            frmRacuni fr = new frmRacuni();
            fr.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            frmPazari fp = new frmPazari();
            fp.ShowDialog();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            frmLogovanja fl = new frmLogovanja();
            fl.ShowDialog();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            frmLokacijeCene flc = new frmLokacijeCene();
            flc.ShowDialog();
            BogacenjeKomboBoxaLokacije();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            frmSmene fs = new frmSmene();
            fs.ShowDialog();
        }

        private void OdredjivanjePravaNadzornika()
        {
            NadzornikBusiness nb = new NadzornikBusiness();
            Nadzornik nadzornik = nb.GetNadzornikNaOsnovuKorisnickogImena(Program.korisnickoIme);
            int privilegijaNadzornika = nadzornik.Privilegija;

            if (privilegijaNadzornika == 3)
            {
                button1.Enabled = true;
                button2.Enabled = true;
                button3.Enabled = true;
                button4.Enabled = true;
                button5.Enabled = true;
                button6.Enabled = true;
                button7.Enabled = true;
                button8.Enabled = true;
                button9.Enabled = true;
                button10.Enabled = true;
            }
            else if (privilegijaNadzornika == 2)
            {
                button1.Enabled = true;
                button2.Enabled = true;
                button3.Enabled = true;
                button4.Enabled = true;
                button5.Enabled = true;
                button6.Enabled = false;
                button7.Enabled = true;
                button8.Enabled = true;
                button9.Enabled = true;
                button10.Enabled = true;
            }
            else if (privilegijaNadzornika == 1)
            {
                button1.Enabled = true;
                button2.Enabled = true;
                button3.Enabled = true;
                button4.Enabled = true;
                button5.Enabled = false;
                button6.Enabled = false;
                button7.Enabled = false;
                button8.Enabled = false;
                button9.Enabled = false;
                button10.Enabled = true;
            }
        }
    }
}
