﻿using BusinessLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class partialFrmIzmenaNadzornika : Form
    {
        int idNadzornikaIzKonstruktora = 0;
        public partialFrmIzmenaNadzornika(int idNadzornikaIzKonstruktora)
        {
            InitializeComponent();
            this.idNadzornikaIzKonstruktora = idNadzornikaIzKonstruktora;
            this.StartPosition = FormStartPosition.CenterScreen;
        }

        private void partialFrmIzmenaNadzornika_Load(object sender, EventArgs e)
        {
            PopuniPoljaNaLoadu();
        }

        private void PopuniPoljaNaLoadu()
        {
            NadzornikBusiness nb = new NadzornikBusiness();
            Nadzornik nadzornik = nb.GetNadzornikaNaOsnovuIDa(this.idNadzornikaIzKonstruktora);

            txtIme.Text = nadzornik.Ime;
            txtKorsnicko.Text = nadzornik.Korisnicko;
            txtUlica.Text = nadzornik.Ulica;
            txtMesto.Text = nadzornik.Mesto;
            txtTelefon.Text = nadzornik.Telefon;
            txtSifra.Text = nadzornik.Sifra;
            int intPrivilegija = nadzornik.Privilegija;
            if (intPrivilegija == 1)
            {
                cmbPrivilegija.Text = "Nivo 1";
            }
            if (intPrivilegija == 2)
            {
                cmbPrivilegija.Text = "Nivo 2";
            }
            if (intPrivilegija == 3)
            {
                cmbPrivilegija.Text = "Nivo 3";
            }

        }

        private void btnIzmena_Click(object sender, EventArgs e)
        {
            IzmenaNadzornika();
        }
        private void IzmenaNadzornika()
        {
            Nadzornik nadzornik = new Nadzornik();
            nadzornik.NadzornikID = this.idNadzornikaIzKonstruktora;
            nadzornik.Ime = txtIme.Text;
            nadzornik.Korisnicko = txtKorsnicko.Text;
            nadzornik.Ulica = txtUlica.Text;
            nadzornik.Mesto = txtMesto.Text;
            nadzornik.Telefon = txtTelefon.Text;
            nadzornik.Sifra = txtSifra.Text;

            nadzornik.Privilegija = VratiPrivilegijuNaOsnovuStringa(cmbPrivilegija.Text);

            if (PopunjenaSvaPolja())
            {
                NadzornikBusiness nb = new NadzornikBusiness();
                int brojImenaKaoUneto = nb.PostojiOvakvoKorisnickoImeNaOsnovuImenaIIDa(nadzornik.Korisnicko.ToUpper(), nadzornik.NadzornikID);
                if (brojImenaKaoUneto > 0)
                {
                    MessageBox.Show("Već postoji ovakvo korisničko ime");
                }
                else
                {
                    int uspesanUpdate = nb.UpdateNadzornika(nadzornik);
                    if (uspesanUpdate == 0)
                    {
                        MessageBox.Show("Izmena nije uspela");
                    }
                    else
                    {
                        this.Close();
                    }
                }
            }
            else
            {
                MessageBox.Show("Nisu popunjena sva polja");
            }
        }
        private int VratiPrivilegijuNaOsnovuStringa(string str)
        {
            if (str == "Nivo 1")
            {
                return 1;
            }
            else if (str == "Nivo 2")
            {
                return 2;
            }
            else
            {
                return 3;
            }

        }

        private bool PopunjenaSvaPolja()
        {
            if (txtIme.Text == "" || string.IsNullOrEmpty(txtIme.Text) || txtKorsnicko.Text == "" || string.IsNullOrEmpty(txtKorsnicko.Text)
                || txtMesto.Text == "" || string.IsNullOrEmpty(txtMesto.Text) || txtSifra.Text == "" || string.IsNullOrEmpty(txtSifra.Text)
                || txtTelefon.Text == "" || string.IsNullOrEmpty(txtTelefon.Text) || txtUlica.Text == "" || string.IsNullOrEmpty(txtUlica.Text))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private void txtIme_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 13)
            {
                IzmenaNadzornika();
            }
        }

        private void txtKorsnicko_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 13)
            {
                IzmenaNadzornika();
            }
        }

        private void txtUlica_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 13)
            {
                IzmenaNadzornika();
            }
        }

        private void txtMesto_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 13)
            {
                IzmenaNadzornika();
            }
        }

        private void txtTelefon_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 13)
            {
                IzmenaNadzornika();
            }
        }

        private void txtSifra_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 13)
            {
                IzmenaNadzornika();
            }
        }
    }
}
